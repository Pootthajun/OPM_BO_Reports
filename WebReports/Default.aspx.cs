﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Text;
using System.IO;

public partial class _Default : System.Web.UI.Page 
{
    string printDate = "";
    string UserName = "";
    string UserOrg = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        string reportname = "SMM_UT0119";
        string ReportFormat = "PDF";

        if (Request.QueryString["ReportName"] != null) { 
         reportname =Request.QueryString["ReportName"];
        }
        if (Request["ReportFormat"] != null) {
            ReportFormat = Request["ReportFormat"];
        }

        printDate = OPM_BL.GetTHDateAbbr(DateTime.Now.ToString("yyyyMMdd", new System.Globalization.CultureInfo("en-US"))) + " " + DateTime.Now.ToString("HH:mm", new System.Globalization.CultureInfo("en-US"));
        string UserID = Request.QueryString["UserID"];
        DataTable uDT = OPM_BL.GetUserNameByID(UserID);
        if (uDT.Rows.Count > 0)
        {
            UserName = uDT.Rows[0]["fullname"].ToString();
            UserOrg = uDT.Rows[0]["org_abbr34"].ToString();
        }
        PrintReport(reportname, ReportFormat);
    }

    private void PrintReport(string reportname, string vReportFormat )
    {
        ReportDocument  rpt = new ReportDocument();
        string home_floder = System.Web.HttpContext.Current.Request.ApplicationPath + "/";
    
        try
        {
            if (reportname =="Test")
            {
                #region "Test"

                //string sql = " select pattern_code,pattern_name,user_id,last_date,grat_flag from CTLT_PATTERN where 1=1 ";
                //SqlParameter[] param = new SqlParameter[2];
                //sql += " and pattern_code=@pattern_code";
                //param[0] = OPM_BL.setParameter("@pattern_code", SqlDbType.VarChar, "ADM01");

                //DataTable dt = OPM_BL.GetDatatable(sql, param);
                //if (dt.Rows.Count > 0)
                //{
                //    string reportName = "CrystalReport";

                //    //rpt.Load(Server.MapPath(home_floder + "/Reports/" + reportName + ".rpt"));
                //    rpt.Load(Server.MapPath(home_floder + "/" + reportName + ".rpt"));
                //    rpt.DataDefinition.FormulaFields["TestFomu"].Text = "'xxxx'";
                //    rpt.SetDataSource(dt);

                //} 
                
               string sql = " select JOB_SEQ as Seq,JOB_DETAIL as Item,per_type,pos_id from SMM_JD_JOB_SUMMARY" + Environment.NewLine;
                DataTable DTS = new DataTable();
                SqlParameter[] param = new SqlParameter[0];
                DTS = OPM_BL.GetDatatable(sql, param);
                if (DTS.Rows.Count > 0)
                {
                    string reportName = "Test";
                    rpt.Load(Server.MapPath(home_floder + "/" + reportName + ".rpt"));
                    rpt.SetDataSource(DTS);
                }
                #endregion                
            }
            #region "__HRM"
            else if (reportname == "RPT_HRM_UT0601")
            {
                #region "RPT_HRM_UT0601_BackUp"

                ////string TypeName = "11";
                ////string DateFrom = "20000101";
                ////string DateTo = "20150630";
                ////string ORG = "65";
                ////string MovementCode = "1310";
                ////string BudgetYear = "2549";
                ////string UserName = "99999ชื่อสกุล99999";
                ////string UserOrg = "xxxxxหน่วยงานxxxxx";

                //SqlParameter[] param = new SqlParameter[6];
                //string TypeName = Request.QueryString["TypeName"];
                //string DateFrom = Request.QueryString["DateFrom"];
                //string DateTo = Request.QueryString["DateTo"];
                //string ORG = Request.QueryString["ORG"];
                //string MovementCode = Request.QueryString["MovementCode"];
                //string UserName = Request.QueryString["UserName"];
                //string UserOrg = Request.QueryString["UserOrg"];
                //string BudgetYear = Request.QueryString["BudgetYear"];
               

                //string sql = " select isnull(p.prefix_name,'') + isnull(p.name,'') + ' ' + isnull(p.surname,'') staff_name," + Environment.NewLine;
                //sql += " isnull(olc.line_name,'') old_pos_name, ogn.org_name old_org_name, isnull(hp.old_pos_id,'') old_pos_id, hp.old_salary," + Environment.NewLine;
                //sql += " nlc.line_name new_pos_name, ngn.org_name new_org_name, hp.pos_id new_pos_id,hp.salary new_salary," + Environment.NewLine;
                //sql += " convert(varchar(10),hp.effect_date,103)effect_date, str(hp.cmd_run_no)cmd_run_no, hp.cmd_budg, hp.cmd_date, hp.movement_code" + Environment.NewLine;
                //sql += " from PSST_HIS_POSITION hp" + Environment.NewLine;
                //sql += " inner join vw_CMN_PERSON p on p.id=hp.id" + Environment.NewLine;
                //sql += " left join PSST_LINE_CODE olc on olc.line_code=hp.old_line_code and olc.per_type=hp.per_type" + Environment.NewLine;
                //sql += " left join CTLT_ORGANIZE ogn on ogn.org_serial=hp.old_org_serial" + Environment.NewLine;
                //sql += " inner join PSST_LINE_CODE nlc on nlc.line_code=hp.line_code and nlc.per_type=hp.per_type" + Environment.NewLine;
                //sql += " inner join CTLT_ORGANIZE ngn on ngn.org_serial=hp.org_serial" + Environment.NewLine;
                //sql += " where hp.cmd_type=2";

                //if (TypeName != null)
                //{
                //    sql += " and hp.per_type=@per_type" + Environment.NewLine;
                //    param[0] = OPM_BL.setParameter("@per_type", SqlDbType.VarChar, TypeName);
                //}
                //if (DateFrom != null)
                //{
                //    sql += " and convert(varchar(8),hp.effect_date,112)>=@date_from" + Environment.NewLine;
                //    param[1] = OPM_BL.setParameter("@date_from", SqlDbType.VarChar, DateFrom);
                //}
                //if (DateTo != null)
                //{
                //    sql += " and convert(varchar(8),hp.effect_date,112)<=@date_to" + Environment.NewLine;
                //    param[2] = OPM_BL.setParameter("@date_to", SqlDbType.VarChar, DateTo);
                //}
                //if (ORG != null)
                //{
                //    sql += " and  hp.org_serial=@org_serial" + Environment.NewLine;
                //    param[3] = OPM_BL.setParameter("@org_serial", SqlDbType.VarChar, ORG);
                //}
                //if (MovementCode != null)
                //{
                //    sql += " and  hp.movement_code=@_MOVEMENT_CODE" + Environment.NewLine;
                //    param[4] = OPM_BL.setParameter("@_MOVEMENT_CODE", SqlDbType.VarChar, MovementCode);
                //}
                //if (BudgetYear != null)
                //{
                //    sql += " and  hp.cmd_budg=@_BUDGET_YEAR" + Environment.NewLine;
                //    param[5] = OPM_BL.setParameter("@_BUDGET_YEAR", SqlDbType.VarChar, BudgetYear);
                //}
                ////and convert(varchar(8),hp.effect_date,112) between @_DATE_FROM and @_DATE_TO

                //DataTable dt = OPM_BL.GetDatatable(sql, param);
                //if (dt.Rows.Count > 0)
                //{
                //    if (vReportFormat == "EXCEL")
                //    {
                //        lblErrorMessage.Text = "";
                //        string datefrom = "";//OPM_BL.GetTHDate(DateFrom);
                //        string dateto = "";//OPM_BL.GetTHDate(DateTo);
                //        ExcelReportENG.ExportReportHRM_RP01("","" , BudgetYear, UserName, UserOrg, dt, Response);
                //    }
                //    else if (vReportFormat == "PDF")
                //    {
                //        rpt.Load(Server.MapPath(home_floder + "/" + reportname + ".rpt"));
                //        rpt.DataDefinition.FormulaFields["DateFrom"].Text = "''";//"'" + OPM_BL.GetTHDate(DateFrom) + "'";
                //        rpt.DataDefinition.FormulaFields["DateTo"].Text = "''";//"'" + OPM_BL.GetTHDate(DateTo) + "'";
                //        rpt.DataDefinition.FormulaFields["BudgetYear"].Text = "'" + BudgetYear + "'";
                //        rpt.DataDefinition.FormulaFields["UserName"].Text = "'" + UserName + "'";
                //        rpt.DataDefinition.FormulaFields["UserOrg"].Text = "'" + UserOrg + "'";
                //        rpt.SetDataSource(dt);
                //        lblErrorMessage.Text = "";

                //        CreatePDFReport(rpt, reportname);
                //    }
                    
                //}
                //else
                //{
                //    lblErrorMessage.Text = "ไม่พบข้อมูล";
                //    CrystalReportViewer1.Visible = false;
                //}
                #endregion

                #region "RPT_HRM_UT0601"

                string strTitle = Request.QueryString["strTitle"];

                //string org_serial = "18635";
                //string per_type = "11";
                //string movement_code = "1320";
                //string pos_type = "";
                //string cur_lev = "4";
                //string pos_name = "นักวิชาการเงินและบัญชี";
                //string cmd_budg = "2533";
                //string UserName = "99999ชื่อสกุล99999";
                //string UserOrg = "xxxxxหน่วยงานxxxxx";

                //string sql = " select RTRIM(LTRIM(ISNULL(pr.PREFIX_NAME, '') + ISNULL(pe.NAME, '') + ' ' + ISNULL(pe.SURNAME, ''))) staff_name," + Environment.NewLine;
                // sql += " o.org_name,o.org_serial,isnull(a.admin_name,l.line_name) pos_name,lv.level_name_full,pt.per_type_name,movement_name," + Environment.NewLine;
                // //sql += " 'กลุ่มงาน ' + isnull(o.name_level3,'') +'/'+ 'ส่วน  ' + isnull(o.name_level4,'') +'/' + 'ฝ่าย ' + isnull(o.name_level5,'') name_level" + Environment.NewLine;
                // sql += " case when isnull(o.name_level4,'') ='' then o.name_level3 when isnull(o.name_level4,'') <>'' then  o.name_level3 + '/' + o.name_level4 ";
                // sql += " when isnull(o.name_level5,'') ='' then  o.name_level3 + '/' + o.name_level4 else o.name_level3 + '/' + o.name_level4 + '/' + o.name_level5";
                // sql += " end name_level";
                // sql += " from PSST_HIS_POSITION hp" + Environment.NewLine;
                // sql += " left join PSST_MOVEMENT_CODE m on hp.movement_code = m.movement_code" + Environment.NewLine;
                // sql += " left join PSST_POSITION ps on hp.pos_id = ps.pos_id and hp.per_type = ps.per_type" + Environment.NewLine;
                // sql += " left join CTLT_ORGANIZE o on hp.org_serial = o.org_serial" + Environment.NewLine;
                // sql += " left join PSST_LINE_CODE l on ps.line_code = l.line_code and ps.per_type=l.per_type" + Environment.NewLine;
                // sql += " left join PSST_ADMIN_CODE a on a.admin_code=ps.admin_code " + Environment.NewLine;
                // sql += " left join  PSST_PERSON AS pe ON ps.ID = pe.ID " + Environment.NewLine;
                // sql += " left join  CTLT_PREFIX_CODE AS pr ON pe.PREFIX_CODE = pr.PREFIX_CODE" + Environment.NewLine;
                // sql += " left join  PSST_PER_LEVEL AS lv ON ps.CUR_LEV = lv.CUR_LEV " + Environment.NewLine;
                // sql += " left join  SMM_PER_TYPE AS pt ON ps.PER_TYPE = pt.PER_TYPE " + Environment.NewLine;
                // sql += " where isnull(pe.NAME,'') <> '' ";//and ( isnull(o.name_level3,'') <> '' or isnull(o.name_level4,'') <> '' or isnull(o.name_level5,'') <>'' )" + Environment.NewLine;
                string sql = "";
                SqlParameter[] param = new SqlParameter[11];

                string org_serial = Request.QueryString["org_serial"];
                string per_type = Request.QueryString["per_type"];
                string movement_code = Request.QueryString["movement_code"];
                string pos_type = Request.QueryString["pos_type"];
                string cur_lev = Request.QueryString["cur_lev"];
                string pos_name = Request.QueryString["pos_name"];
                string cmd_budg = Request.QueryString["cmd_budg"];
                //string UserName = Request.QueryString["UserName"];
                //string UserOrg = Request.QueryString["UserOrg"];
                string BornDateFrom = Request.QueryString["BornDateFrom"];
                string BornDateTo = Request.QueryString["BornDateTo"];
                string EnterDateFrom = Request.QueryString["WORK_START"];
                string EnterDateTo = Request.QueryString["WORK_TO"];

                sql = "SELECT * FROM ( SELECT DISTINCT org.ORG_SERIAL,org.ORG_SERIAL_L2,org.ORG_SERIAL_L3,org.ORG_SERIAL_L4,org.ORG_SERIAL_L5,org.ORDER_SEQ,org.ORDER_SEQ_L3,org.ORG_NAME_L3,org.ORG_NAME_L4,org.ORG_NAME_L5,org.ORG_NAME_INDENT," + Environment.NewLine;
                sql += "  hp.POS_ID,  hp.ID, iif(per.PREFIX_CODE is NULL,'',pf.PREFIX_NAME)+per.NAME+'  '+per.SURNAME as FULL_NAME," + Environment.NewLine;
                sql += "	iif(adm.ADMIN_NAME is not NULL,adm.ADMIN_NAME,ln.LINE_NAME) as POS_NAME,isNULL(lv.LEVEL_NAME,'') as level_name_full,hp.PER_TYPE,pt.PER_TYPE_NAME,mv.MOVEMENT_NAME" + Environment.NewLine;
                sql += "FROM PSST_HIS_POSITION hp" + Environment.NewLine;
                sql += "  INNER JOIN " + Environment.NewLine;
                sql += "    (SELECT id, MAX(EFFECT_DATE) as EFFECT_DATE, MAX(hpin.LAST_DATE) as LAST_DATE" + Environment.NewLine;
                sql += "     FROM PSST_HIS_POSITION hpin " + Environment.NewLine;
                sql += "         inner join PSST_MOVEMENT_CODE mv on hpin.MOVEMENT_CODE=mv.MOVEMENT_CODE" + Environment.NewLine;
                sql += "     Where mv.movement_flag not in ('20','21','22','23','24') and mv.MOVEMENT_TYPE='1' and mv.MOVEMENT_CMD='A' " + Environment.NewLine;
                sql += "           and CMD_BUDG<=@_SELECT_YEAR" + Environment.NewLine;
                param[0] = OPM_BL.setParameter("@_SELECT_YEAR", SqlDbType.VarChar, cmd_budg);
                if (per_type != null)
                {
                    sql += "      and hpin.PER_TYPE=@_PER_TYPE   -- ถ้าระบุ PER_TYPE" + Environment.NewLine;
                    param[1] = OPM_BL.setParameter("@_PER_TYPE", SqlDbType.VarChar, per_type);
                }
                sql += "     GROUP BY id" + Environment.NewLine;
                sql += "    ) hpmax ON hp.id=hpmax.id AND hp.EFFECT_DATE=hpmax.EFFECT_DATE and hp.LAST_DATE=hpmax.LAST_DATE" + Environment.NewLine;
                sql += "  INNER JOIN vw_SMM_ORG org ON hp.ORG_SERIAL=org.ORG_SERIAL" + Environment.NewLine;
                sql += "  INNER JOIN PSST_PERSON per ON hp.ID=per.ID" + Environment.NewLine;
	            sql += "  LEFT JOIN PSST_POSITION pos ON hp.POS_ID=pos.POS_ID" + Environment.NewLine;
	            sql += "  LEFT JOIN PSST_ADMIN_CODE adm ON hp.ADMIN_CODE=adm.ADMIN_CODE" + Environment.NewLine;
	            sql += "  LEFT JOIN PSST_LINE_CODE ln ON hp.LINE_CODE=ln.LINE_CODE" + Environment.NewLine;
	            sql += "  LEFT JOIN PSST_PER_LEVEL lv ON hp.CUR_LEV=lv.CUR_LEV" + Environment.NewLine;
	            sql += "  LEFT JOIN SMM_PER_TYPE pt ON hp.PER_TYPE=pt.PER_TYPE" + Environment.NewLine;
	            sql += "  LEFT JOIN PSST_MOVEMENT_CODE mv ON hp.MOVEMENT_CODE=mv.MOVEMENT_CODE" + Environment.NewLine;
                sql += "  LEFT JOIN CTLT_PREFIX_CODE pf on per.PREFIX_CODE=pf.PREFIX_CODE" + Environment.NewLine;
                sql += "  Where 1=1 " + Environment.NewLine;
                if (org_serial != null)
                {
                    sql += "      AND (ORG_SERIAL_L2=@_ORG_SERIAL or ORG_SERIAL_L3=@_ORG_SERIAL or ORG_SERIAL_L4=@_ORG_SERIAL or ORG_SERIAL_L5=@_ORG_SERIAL)  -- กรณีระบุ @_ORG_SERIAL" + Environment.NewLine;
                    param[2] = OPM_BL.setParameter("@_ORG_SERIAL", SqlDbType.VarChar, org_serial);
                }
                if (movement_code != null)
                {
                    sql += "      AND hp.MOVEMENT_CODE like (SUBSTRING(@_MOVEMENT_CODE,1,2)+'%')    -- กรณีระบุ @_MOVEMENT_CODE" + Environment.NewLine;
                    param[3] = OPM_BL.setParameter("@_MOVEMENT_CODE", SqlDbType.VarChar, movement_code);
                }
                if (pos_type != null)
                {
                    sql += "	  AND lv.LEVEL_GROUP_NAME=@_POS_TYPE  -- กรณีระบุ @_POS_TYPE" + Environment.NewLine;
                    param[4] = OPM_BL.setParameter("@_POS_TYPE", SqlDbType.VarChar, pos_type);
                }
                if (cur_lev != null)
                {
                    sql += "	  AND lv.CUR_LEV=@_CUR_LEVEL  -- กรณีระบุ @_CUR_LEVEL" + Environment.NewLine;
                    param[5] = OPM_BL.setParameter("@_CUR_LEVEL", SqlDbType.VarChar, cur_lev);
                }
                if (pos_name != null)
                {   
	                sql += "	  AND (ln.LINE_NAME like '%'+@_POS_NAME+'%'  OR adm.ADMIN_NAME like '%'+@_POS_NAME+'%') -- กรณีระบุ @_POS_NAME" + Environment.NewLine;
                    param[6] = OPM_BL.setParameter("@_POS_NAME", SqlDbType.VarChar, pos_name);
                }
                
                if ((BornDateFrom != null) && (BornDateTo != null))
                {
                    sql += "	  AND convert(varchar(8),per.BORN_DATE,112) between @_AGE_START and @_AGE_END   -- กรณีระบุ @_AGE_START, AGE_END" + Environment.NewLine;
                    param[7] = OPM_BL.setParameter("@_AGE_START", SqlDbType.VarChar, BornDateFrom);
                    param[8] = OPM_BL.setParameter("@_AGE_END", SqlDbType.VarChar, BornDateTo);
                }
                if ((EnterDateFrom != null) && (EnterDateTo != null))
                {
                    sql += "	  AND convert(varchar(8),hp.EFFECT_DATE,112) between @_WORK_START and @_WORK_END   -- กรณีระบุ @_WORK_START, WORK_END" + Environment.NewLine;
                    param[9] = OPM_BL.setParameter("@_WORK_START", SqlDbType.VarChar, EnterDateFrom);
                    param[10] = OPM_BL.setParameter("@_WORK_END", SqlDbType.VarChar, EnterDateTo);
                }
                //sql += "  order by CASE WHEN org.ORDER_SEQ IS NULL THEN 1 ELSE 0 END, org.ORDER_SEQ,hp.POS_ID";
                sql += "  ) As TB order by CASE WHEN ORDER_SEQ IS NULL THEN 1 ELSE 0 END, ORDER_SEQ,POS_ID ";


                
                
                
                
                
             
                DataTable dt = OPM_BL.GetDatatable(sql, param);
                if (dt.Rows.Count > 0)
                {
                    if (vReportFormat == "EXCEL")
                    {
                        lblErrorMessage.Text = "";
                        ExcelReportENG.ExportReportRPT_HRM_UT0601(cmd_budg, UserName, UserOrg, dt, Response);
                    }
                    else if (vReportFormat == "PDF")
                    {
                        rpt.Load(Server.MapPath(home_floder + "/" + reportname + ".rpt"));
                        rpt.DataDefinition.FormulaFields["BudgetYear"].Text = "'" + cmd_budg + "'";
                        rpt.DataDefinition.FormulaFields["UserName"].Text = "'" + UserName + "'";
                        rpt.DataDefinition.FormulaFields["UserOrg"].Text = "'" + UserOrg + "'";
                        rpt.DataDefinition.FormulaFields["PrintDate"].Text = "'" + printDate + "'";
                        rpt.DataDefinition.FormulaFields["strTitle"].Text = "'" + strTitle + "'";


                        rpt.SetDataSource(dt);
                        rpt.SetParameterValue("para_Count",  dt.Rows.Count);

                        lblErrorMessage.Text = "";
                        CreatePDFReport(rpt, reportname);
                    }
                }
                else
                {
                    lblErrorMessage.Text = "ไม่พบข้อมูล";
                    CrystalReportViewer1.Visible = false;
                }
                #endregion
            }
            else if (reportname == "RPT_HRM_UT0602")
            {
                #region "RPT_HRM_UT0602"

                string  strTitle = Request.QueryString["strTitle"];

                //string TypeName = "11";
                //string DateFrom = "20121001";
                //string DateTo = "20130930";
                //string ORG = null;//"18635";
                //string BudgetYear = "2556";
                //string UserName = "99999ชื่อสกุล99999";
                //string UserOrg = "xxxxxหน่วยงานxxxxx";

                SqlParameter[] param = new SqlParameter[3];
                string per_type = Request.QueryString["per_type"];
                string per_type_name = "";
                per_type_name += Request.QueryString["per_type_name"];
                string org_serial = Request.QueryString["org_serial"];
                string cmd_budg = Request.QueryString["cmd_budg"];
                string sub_Title = string.Empty;
                sub_Title += Request.QueryString["subTitle"];

                //string UserName = Request.QueryString["UserName"];
                //string UserOrg = Request.QueryString["UserOrg"];
                string sql = "";
                if (string.IsNullOrEmpty(org_serial) || org_serial == "76")
                {
                    sql = " select vw.ORG_SERIAL,ORG_NAME,org_level," + Environment.NewLine;
                    sql += " (select count(hp.id)  from PSST_HIS_POSITION hp" + Environment.NewLine;
                    sql += " inner join vw_SMM_ORG so2 on so2.ORG_SERIAL=hp.ORG_SERIAL" + Environment.NewLine;
                    sql += " inner join PSST_PERSON per on hp.id=per.id" + Environment.NewLine;
                    sql += " inner join PSST_MOVEMENT_CODE mc on mc.movement_code=hp.movement_code" + Environment.NewLine;
                    sql += " where mc.movement_flag in ('10','11') and MOVEMENT_TYPE='1' and mc.MOVEMENT_CMD='A' and CMD_BUDG=@_BUDGET_YEAR" + Environment.NewLine;
                    if (!string.IsNullOrEmpty(per_type))
                    {
                        sql += " and per.PER_TYPE=@_PER_TYPE" + Environment.NewLine;
                    }
                    sql += " and so2.ORG_SERIAL_L2=vw.ORG_SERIAL_L2 and so2.ORG_SERIAL_L3=vw.ORG_SERIAL_L3)  new_emp," + Environment.NewLine;
                    sql += " (select count(hp.id)  from PSST_HIS_POSITION hp" + Environment.NewLine;
                    sql += " inner join vw_SMM_ORG so2 on so2.ORG_SERIAL=hp.ORG_SERIAL" + Environment.NewLine;
                    sql += " inner join PSST_PERSON per on hp.id=per.id" + Environment.NewLine;
                    sql += " inner join PSST_MOVEMENT_CODE mc on mc.movement_code=hp.movement_code" + Environment.NewLine;
                    sql += " where mc.movement_flag='12' and MOVEMENT_TYPE='1' and MOVEMENT_CMD='A' and CMD_BUDG=@_BUDGET_YEAR" + Environment.NewLine;
                    if (!string.IsNullOrEmpty(per_type))
                    {
                        sql += "  and per.PER_TYPE=@_PER_TYPE  " + Environment.NewLine;
                    }
                    sql += " and so2.ORG_SERIAL_L2=vw.ORG_SERIAL_L2 and so2.ORG_SERIAL_L3=vw.ORG_SERIAL_L3)  transfer_emp," + Environment.NewLine;
                    sql += " 0 as other_emp" + Environment.NewLine;
                    sql += " from vw_smm_org_level2  vw" + Environment.NewLine;
                    sql += " order by CASE WHEN vw.ORDER_SEQ IS NULL THEN 1 ELSE 0 END, vw.ORDER_SEQ";
                }
                else
                {
                    sql = " select vw.ORG_SERIAL,ORG_NAME,org_level," + Environment.NewLine;
                    sql += " (select count(hp.id)" + Environment.NewLine;
                    sql += " from PSST_HIS_POSITION hp" + Environment.NewLine;
                    sql += " inner join PSST_PERSON per on hp.id=per.id" + Environment.NewLine;
                    sql += " inner join PSST_MOVEMENT_CODE mc on mc.movement_code=hp.movement_code" + Environment.NewLine;
                    sql += " where mc.movement_flag in ('10','11') and MOVEMENT_TYPE='1' and mc.MOVEMENT_CMD='A' and CMD_BUDG=@_BUDGET_YEAR" + Environment.NewLine;
                    if (!string.IsNullOrEmpty(per_type))
                    {
                        sql += "  and per.PER_TYPE=@_PER_TYPE" + Environment.NewLine;
                    }
                    sql += "  and  hp.ORG_SERIAL=vw.ORG_SERIAL)  new_emp," + Environment.NewLine;
                    sql += " (select count(hp.id)" + Environment.NewLine;
                    sql += "   from PSST_HIS_POSITION hp" + Environment.NewLine;
                    sql += " inner join PSST_PERSON per on hp.id=per.id" + Environment.NewLine;
                    sql += " inner join PSST_MOVEMENT_CODE mc on mc.movement_code=hp.movement_code" + Environment.NewLine;
                    sql += " where mc.movement_flag='12' and MOVEMENT_TYPE='1' and MOVEMENT_CMD='A' and CMD_BUDG=@_BUDGET_YEAR" + Environment.NewLine;
                    if (!string.IsNullOrEmpty(per_type))
                    {
                        sql += "  and per.PER_TYPE=@_PER_TYPE  " + Environment.NewLine;
                    }
                    sql += "   and  hp.ORG_SERIAL=vw.ORG_SERIAL)  transfer_emp," + Environment.NewLine;
                    sql += "   0 as other_emp" + Environment.NewLine;
                    sql += " from vw_smm_org  vw" + Environment.NewLine;
                    sql += " where (ORG_SERIAL_L2=@_ORG_SERIAL or ORG_SERIAL_L3=@_ORG_SERIAL or ORG_SERIAL_L4=@_ORG_SERIAL or ORG_SERIAL_L5=@_ORG_SERIAL)" + Environment.NewLine;
                    sql += " order by ORG_SERIAL_L2,ORG_SERIAL_L3,ORG_SERIAL_L4,ORG_SERIAL_L5";
                }


                if (per_type != null)
                {
                    param[0] = OPM_BL.setParameter("@_PER_TYPE", SqlDbType.VarChar, per_type);
                }
                if (org_serial != null && org_serial != "76")
                {
                    param[1] = OPM_BL.setParameter("@_ORG_SERIAL", SqlDbType.VarChar, org_serial);
                }
                param[2] = OPM_BL.setParameter("@_BUDGET_YEAR", SqlDbType.VarChar, cmd_budg);

                DataTable dt = OPM_BL.GetDatatable(sql, param);
                if (dt.Rows.Count > 0)
                {
                    if (vReportFormat == "EXCEL")
                    {
                        lblErrorMessage.Text = "";
                        ExcelReportENG.ExportReportRPT_HRM_UT0602(cmd_budg, UserName, UserOrg, dt, Response);
                    }
                    else if (vReportFormat == "PDF") {
                        rpt.Load(Server.MapPath(home_floder + "/" + reportname + ".rpt"));
                        rpt.DataDefinition.FormulaFields["BudgetYear"].Text = "'" + cmd_budg + "'";
                        rpt.DataDefinition.FormulaFields["UserName"].Text = "'" + UserName + "'";
                        rpt.DataDefinition.FormulaFields["UserOrg"].Text = "'" + UserOrg + "'";
                        rpt.DataDefinition.FormulaFields["PrintDate"].Text = "'" + printDate + "'";
                        rpt.DataDefinition.FormulaFields["strTitle"].Text = "'" + strTitle + "'";
                        rpt.SetDataSource(dt);
                        lblErrorMessage.Text = "";
                        rpt.SetParameterValue("per_type_name", per_type_name);
                        rpt.SetParameterValue("sub_Title", sub_Title);
                        CreatePDFReport(rpt, reportname);
                    }
                }
                else
                {
                    lblErrorMessage.Text = "ไม่พบข้อมูล";
                    CrystalReportViewer1.Visible = false;
                }
                #endregion
            }

            else if (reportname == "RPT_HRM_UT0603")
            {
                #region "RPT_HRM_UT0603"

                SqlParameter[] param = new SqlParameter[3];
                string per_type = Request.QueryString["per_type"];
                string per_type_name = "";
                per_type_name += Request.QueryString["per_type_name"];
                string org_serial = Request.QueryString["org_serial"];
                string cmd_budg = Request.QueryString["cmd_budg"];
                string strTitle = "";
                strTitle += Request.QueryString["strTitle"];
                string sub_Title = string.Empty;
                sub_Title += Request.QueryString["subTitle"];
                string sql = "";
                //string UserName = Request.QueryString["UserName"];
                //string UserOrg = Request.QueryString["UserOrg"];

                //declare @_BUDGET_YEAR varchar(10)
                //set @_BUDGET_YEAR ='2557'

                //declare @_PER_TYPE varchar(10)
                //set @_PER_TYPE ='11'

                //declare @_ORG_SERIAL varchar(10)
                //set @_ORG_SERIAL ='2'

                //string sql = " select distinct po.org_serial,ogn.org_name,pt.per_type_name, " + Environment.NewLine;
                //sql += " (select count(pp.id) "+ Environment.NewLine;
                // sql += " from PSST_PERSON pp "+ Environment.NewLine;
                // sql += " inner join PSST_POSITION ppo on ppo.id=pp.id "+ Environment.NewLine;
                // sql += " inner join PSST_MOVEMENT_CODE mc on mc.movement_code=pp.movement_code "+ Environment.NewLine;
                // sql += " where ppo.org_serial=po.org_serial "+ Environment.NewLine;
                // sql += " and mc.movement_flag='21' "+ Environment.NewLine;
                // sql += " and pp.per_type=p.per_type "+ Environment.NewLine;
                // sql += " and convert(varchar(4),pp.retire_date,112)+543 = @_BUDGET_YEAR "+ Environment.NewLine;
                // sql += " ) cmd_retire, "+ Environment.NewLine;
                // sql += " (select count(pp.id) "+ Environment.NewLine;
                // sql += " from PSST_PERSON pp "+ Environment.NewLine;
                // sql += " inner join PSST_POSITION ppo on ppo.id=pp.id "+ Environment.NewLine;
                // sql += " inner join PSST_MOVEMENT_CODE mc on mc.movement_code=pp.movement_code "+ Environment.NewLine;
                // sql += " where ppo.org_serial=po.org_serial "+ Environment.NewLine;
                // sql += " and mc.movement_flag = '22' "+ Environment.NewLine;
                // sql += " and pp.per_type=p.per_type "+ Environment.NewLine;
                // sql += " and convert(varchar(4),pp.quit_date,112)+543 = @_BUDGET_YEAR "+ Environment.NewLine;
                // sql += " ) cmd_resign, "+ Environment.NewLine;
                // sql += " (select count(pp.id) "+ Environment.NewLine;
                // sql += " from PSST_PERSON pp "+ Environment.NewLine;
                // sql += " inner join PSST_POSITION ppo on ppo.id=pp.id "+ Environment.NewLine;
                // sql += " inner join PSST_MOVEMENT_CODE mc on mc.movement_code=pp.movement_code "+ Environment.NewLine;
                // sql += " where ppo.org_serial=po.org_serial "+ Environment.NewLine;
                // sql += " and mc.movement_flag='23' "+ Environment.NewLine;
                // sql += " and pp.per_type=p.per_type "+ Environment.NewLine;
                // sql += " and convert(varchar(4),pp.quit_date,112)+543 = @_BUDGET_YEAR "+ Environment.NewLine;
                // sql += " and pp.per_status='2' "+ Environment.NewLine;
                // sql += " ) cmd_transfer, "+ Environment.NewLine;
                // sql += "  (select count(pp.id) "+ Environment.NewLine;
                // sql += " from PSST_PERSON pp "+ Environment.NewLine;
                // sql += " inner join PSST_POSITION ppo on ppo.id=pp.id "+ Environment.NewLine;
                // sql += " inner join PSST_MOVEMENT_CODE mc on mc.movement_code=pp.movement_code "+ Environment.NewLine;
                // sql += " where ppo.org_serial=po.org_serial "+ Environment.NewLine;
                // sql += " and mc.movement_flag='24' "+ Environment.NewLine;
                // sql += " and pp.per_type=p.per_type "+ Environment.NewLine;
                // sql += " and convert(varchar(4),pp.quit_date,112)+543 = @_BUDGET_YEAR "+ Environment.NewLine;
                // sql += " and pp.per_status='2' "+ Environment.NewLine;
                // sql += " ) cmd_disciplinary, "+ Environment.NewLine;
                // sql += " (select count(pp.id) "+ Environment.NewLine;
                // sql += " from PSST_PERSON pp "+ Environment.NewLine;
                // sql += " inner join PSST_POSITION ppo on ppo.id=pp.id "+ Environment.NewLine;
                // sql += " inner join PSST_MOVEMENT_CODE mc on mc.movement_code=pp.movement_code "+ Environment.NewLine;
                // sql += "  where ppo.org_serial=po.org_serial "+ Environment.NewLine;
                // sql += " and mc.movement_flag='20' "+ Environment.NewLine;
                // sql += " and pp.per_type=p.per_type "+ Environment.NewLine;
                // sql += " and convert(varchar(4),pp.quit_date,112)+543 = @_BUDGET_YEAR "+ Environment.NewLine;
                // sql += " and pp.per_status='2' "+ Environment.NewLine;
                // sql += " ) cmd_other "+ Environment.NewLine;
                // sql += " from PSST_PERSON p "+ Environment.NewLine;
                // sql += " inner join PSST_POSITION po on po.id=p.id "+ Environment.NewLine;
                // sql += " inner join CTLT_ORGANIZE ogn on ogn.org_serial=po.org_serial "+ Environment.NewLine;
                // sql += " left join psst_per_type pt on p.per_type=pt.per_type  "+ Environment.NewLine;
                // sql += " where 1=1 " + Environment.NewLine;

               
                if (string.IsNullOrEmpty(org_serial) || org_serial == "76")
                {
                    sql = "select vw.ORG_SERIAL,ORG_NAME,org_level," + Environment.NewLine;
                    sql += " (select count(hp.id)  from PSST_HIS_POSITION hp" + Environment.NewLine;
                    sql += " inner join vw_SMM_ORG so2 on so2.ORG_SERIAL=hp.ORG_SERIAL" + Environment.NewLine;
                    sql += " inner join PSST_PERSON per on hp.id=per.id" + Environment.NewLine;
                    sql += " inner join PSST_MOVEMENT_CODE mc on mc.movement_code=hp.movement_code" + Environment.NewLine;
                    sql += " where mc.movement_flag='21' and MOVEMENT_TYPE='1' and mc.MOVEMENT_CMD='A' and CMD_BUDG=@_BUDGET_YEAR" + Environment.NewLine;
                    if (!string.IsNullOrEmpty(per_type))
                    {
                        sql += " and per.PER_TYPE=@_PER_TYPE" + Environment.NewLine;
                    }
                    sql += " and so2.ORG_SERIAL_L2=vw.ORG_SERIAL_L2 and so2.ORG_SERIAL_L3=vw.ORG_SERIAL_L3)  cmd_retire," + Environment.NewLine;
                    sql += " (select count(hp.id)  from PSST_HIS_POSITION hp" + Environment.NewLine;
                    sql += " inner join vw_SMM_ORG so2 on so2.ORG_SERIAL=hp.ORG_SERIAL" + Environment.NewLine;
                    sql += " inner join PSST_PERSON per on hp.id=per.id" + Environment.NewLine;
                    sql += " inner join PSST_MOVEMENT_CODE mc on mc.movement_code=hp.movement_code" + Environment.NewLine;
                    sql += " where mc.movement_flag='22' and MOVEMENT_TYPE='1' and MOVEMENT_CMD='A' and CMD_BUDG=@_BUDGET_YEAR" + Environment.NewLine;
                    if (!string.IsNullOrEmpty(per_type))
                    {
                        sql += " and per.PER_TYPE=@_PER_TYPE" + Environment.NewLine;
                    }
                    sql += " and so2.ORG_SERIAL_L2=vw.ORG_SERIAL_L2 and so2.ORG_SERIAL_L3=vw.ORG_SERIAL_L3)  cmd_resign," + Environment.NewLine;
                    sql += " (select count(hp.id)  from PSST_HIS_POSITION hp" + Environment.NewLine;
                    sql += " inner join vw_SMM_ORG so2 on so2.ORG_SERIAL=hp.ORG_SERIAL" + Environment.NewLine;
                    sql += " inner join PSST_PERSON per on hp.id=per.id" + Environment.NewLine;
                    sql += " inner join PSST_MOVEMENT_CODE mc on mc.movement_code=hp.movement_code" + Environment.NewLine;
                    sql += " where mc.movement_flag='23' and MOVEMENT_TYPE='1' and MOVEMENT_CMD='A' and CMD_BUDG=@_BUDGET_YEAR" + Environment.NewLine;
                    if (!string.IsNullOrEmpty(per_type))
                    {
                        sql += " and per.PER_TYPE=@_PER_TYPE " + Environment.NewLine;
                    }
                    sql += " and so2.ORG_SERIAL_L2=vw.ORG_SERIAL_L2 and so2.ORG_SERIAL_L3=vw.ORG_SERIAL_L3)  cmd_transfer," + Environment.NewLine;
                    sql += " (select count(hp.id)  from PSST_HIS_POSITION hp" + Environment.NewLine;
                    sql += " inner join vw_SMM_ORG so2 on so2.ORG_SERIAL=hp.ORG_SERIAL" + Environment.NewLine;
                    sql += " inner join PSST_PERSON per on hp.id=per.id" + Environment.NewLine;
                    sql += " inner join PSST_MOVEMENT_CODE mc on mc.movement_code=hp.movement_code" + Environment.NewLine;
                    sql += " where mc.movement_flag='24' and MOVEMENT_TYPE='1' and MOVEMENT_CMD='A' and CMD_BUDG=@_BUDGET_YEAR" + Environment.NewLine;
                    if (!string.IsNullOrEmpty(per_type))
                    {
                        sql += " and per.PER_TYPE=@_PER_TYPE    " + Environment.NewLine;
                    }
                    sql += " and so2.ORG_SERIAL_L2=vw.ORG_SERIAL_L2 and so2.ORG_SERIAL_L3=vw.ORG_SERIAL_L3)  cmd_disciplinary," + Environment.NewLine;
                    sql += " 0 as cmd_other,'' per_type_name" + Environment.NewLine;
                    sql += " from vw_smm_org_level2  vw" + Environment.NewLine;
                    sql += " order by CASE WHEN vw.ORDER_SEQ IS NULL THEN 1 ELSE 0 END, vw.ORDER_SEQ" + Environment.NewLine;
                }
                else
                {
                        sql = " select vw.ORG_SERIAL,ORG_NAME,org_level," + Environment.NewLine;
                        sql += " (select count(hp.id)" + Environment.NewLine;
                        sql += " from PSST_HIS_POSITION hp" + Environment.NewLine;
                        sql += " inner join PSST_PERSON per on hp.id=per.id" + Environment.NewLine;
                        sql += " inner join PSST_MOVEMENT_CODE mc on mc.movement_code=hp.movement_code" + Environment.NewLine;
                        sql += " where mc.movement_flag='21' and MOVEMENT_TYPE='1' and mc.MOVEMENT_CMD='A' and CMD_BUDG=@_BUDGET_YEAR" + Environment.NewLine;
                        if (!string.IsNullOrEmpty(per_type))
                        {
                            sql += " and per.PER_TYPE=@_PER_TYPE" + Environment.NewLine;
                        }
                        sql += " and  hp.ORG_SERIAL=vw.ORG_SERIAL)  cmd_retire," + Environment.NewLine;
                        sql += " (select count(hp.id)" + Environment.NewLine;
                        sql += " from PSST_HIS_POSITION hp" + Environment.NewLine;
                        sql += " inner join PSST_PERSON per on hp.id=per.id" + Environment.NewLine;
                        sql += " inner join PSST_MOVEMENT_CODE mc on mc.movement_code=hp.movement_code" + Environment.NewLine;
                        sql += " where mc.movement_flag='22' and MOVEMENT_TYPE='1' and MOVEMENT_CMD='A' and CMD_BUDG=@_BUDGET_YEAR" + Environment.NewLine;
                        if (!string.IsNullOrEmpty(per_type))
                        {
                            sql += " and per.PER_TYPE=@_PER_TYPE    " + Environment.NewLine;
                        }
                        sql += " and  hp.ORG_SERIAL=vw.ORG_SERIAL)  cmd_resign," + Environment.NewLine;
                        sql += " (select count(hp.id)" + Environment.NewLine;
                        sql += " from PSST_HIS_POSITION hp" + Environment.NewLine;
                        sql += " inner join PSST_PERSON per on hp.id=per.id" + Environment.NewLine;
                        sql += " inner join PSST_MOVEMENT_CODE mc on mc.movement_code=hp.movement_code" + Environment.NewLine;
                        sql += " where mc.movement_flag='23' and MOVEMENT_TYPE='1' and MOVEMENT_CMD='A' and CMD_BUDG=@_BUDGET_YEAR" + Environment.NewLine;
                        if (!string.IsNullOrEmpty(per_type))
                        {
                            sql += " and per.PER_TYPE=@_PER_TYPE" + Environment.NewLine;
                        }
                        sql += " and  hp.ORG_SERIAL=vw.ORG_SERIAL)  cmd_transfer," + Environment.NewLine;
                        sql += " (select count(hp.id)" + Environment.NewLine;
                        sql += " from PSST_HIS_POSITION hp" + Environment.NewLine;
                        sql += " inner join PSST_PERSON per on hp.id=per.id " + Environment.NewLine;
                        sql += " inner join PSST_MOVEMENT_CODE mc on mc.movement_code=hp.movement_code" + Environment.NewLine;
                        sql += " where mc.movement_flag='24' and MOVEMENT_TYPE='1' and MOVEMENT_CMD='A' and CMD_BUDG=@_BUDGET_YEAR" + Environment.NewLine;
                        if (!string.IsNullOrEmpty(per_type))
                        {
                            sql += " and per.PER_TYPE=@_PER_TYPE     " + Environment.NewLine;
                        }
                        sql += " and  hp.ORG_SERIAL=vw.ORG_SERIAL)  cmd_disciplinary," + Environment.NewLine;
                        sql += " (select count(hp.id)" + Environment.NewLine;
                        sql += " from PSST_HIS_POSITION hp" + Environment.NewLine;
                        sql += " inner join PSST_PERSON per on hp.id=per.id" + Environment.NewLine;
                        sql += " inner join PSST_MOVEMENT_CODE mc on mc.movement_code=hp.movement_code" + Environment.NewLine;
                        sql += " where mc.movement_flag='20' and MOVEMENT_TYPE='1' and MOVEMENT_CMD='A' and CMD_BUDG=@_BUDGET_YEAR" + Environment.NewLine;
                        if (!string.IsNullOrEmpty(per_type))
                        {
                            sql += " and per.PER_TYPE=@_PER_TYPE" + Environment.NewLine;
                        }
                        sql += " and  hp.ORG_SERIAL=vw.ORG_SERIAL)  cmd_other,'' per_type_name" + Environment.NewLine;
                        sql += " from vw_smm_org  vw" + Environment.NewLine;
                        sql += " where (ORG_SERIAL_L2=@_ORG_SERIAL or ORG_SERIAL_L3=@_ORG_SERIAL or ORG_SERIAL_L4=@_ORG_SERIAL or ORG_SERIAL_L5=@_ORG_SERIAL)" + Environment.NewLine;
                        sql += " order by ORG_SERIAL_L2,ORG_SERIAL_L3,ORG_SERIAL_L4,ORG_SERIAL_L5" + Environment.NewLine;
                }

                if (per_type != null)
                {
                    param[0] = OPM_BL.setParameter("@_PER_TYPE", SqlDbType.VarChar, per_type);
                }
                if (org_serial != null && org_serial != "76")
                {
                    param[1] = OPM_BL.setParameter("@_ORG_SERIAL", SqlDbType.VarChar, org_serial);
                }

                param[2] = OPM_BL.setParameter("@_BUDGET_YEAR", SqlDbType.VarChar, cmd_budg);

                DataTable dt = OPM_BL.GetDatatable(sql, param);
                if (dt.Rows.Count > 0)
                {
                    if (vReportFormat == "EXCEL")
                    {
                        lblErrorMessage.Text = "";
                        ExcelReportENG.ExportReportRPT_HRM_UT0603(cmd_budg, UserName, UserOrg, dt, Response);
                    }
                    else if (vReportFormat == "PDF")
                    {
                        rpt.Load(Server.MapPath(home_floder + "/" + reportname + ".rpt"));
                        rpt.DataDefinition.FormulaFields["BudgetYear"].Text = "'" + cmd_budg + "'";
                        rpt.DataDefinition.FormulaFields["UserName"].Text = "'" + UserName + "'";
                        rpt.DataDefinition.FormulaFields["UserOrg"].Text = "'" + UserOrg + "'";
                        rpt.DataDefinition.FormulaFields["PrintDate"].Text = "'" + printDate + "'";
                        rpt.DataDefinition.FormulaFields["strTitle"].Text = "'" + strTitle + "'";

                        rpt.SetDataSource(dt);
                        lblErrorMessage.Text = "";
                        rpt.SetParameterValue("per_type_name", per_type_name);
                        rpt.SetParameterValue("sub_Title", sub_Title);
                        CreatePDFReport(rpt, reportname);
                    }
                }
                else
                {
                    lblErrorMessage.Text = "ไม่พบข้อมูล";
                    CrystalReportViewer1.Visible = false;
                }
                #endregion
            }
            else if (reportname == "RPT_HRM_UT0604")
            {
                #region "RPT_HRM_UT0604"

                SqlParameter[] param = new SqlParameter[4];
                string per_type = Request.QueryString["per_type"];
                string org_serial = Request.QueryString["org_serial"];
                string cmd_budg = Request.QueryString["cmd_budg"];
                string strTitle = "";
                strTitle += Request.QueryString["strTitle"];
                string per_type_name = "";
                per_type_name += Request.QueryString["per_type_name"];
                string sub_Title = string.Empty;
                sub_Title += Request.QueryString["subTitle"];

                string TH_DateFrom = "ณ วันที่ " + Request.QueryString["TH_DateFrom"];
                string DateFrom = Request.QueryString["TH_DateFrom"];
                DateFrom = Convert.ToDateTime(DateFrom).ToString("yyyy/MM/dd", new System.Globalization.CultureInfo("en-US"));
                string TH_DateTo = "ณ วันที่ " + Request.QueryString["TH_DateTo"];
                string DateTo = Request.QueryString["TH_DateTo"];
                DateTo = Convert.ToDateTime(DateTo).ToString("yyyy/MM/dd", new System.Globalization.CultureInfo("en-US"));
                string Title_type = Request.QueryString["Title_type"];
                string sql = "";

                //string UserName = Request.QueryString["UserName"];
                //string UserOrg = Request.QueryString["UserOrg"];

                //declare @_LAST_YEAR varchar(10)
                //set @_LAST_YEAR ='2556'

                //declare @_SELECT_YEAR varchar(10)
                //set @_SELECT_YEAR ='2557'

                //declare @_PER_TYPE varchar(10)
                //set @_PER_TYPE ='11'

                //declare @_ORG_SERIAL varchar(10)
                //set @_ORG_SERIAL ='2'

                //string sql = " select last_year, select_year, org_serial," + Environment.NewLine;
                //sql += " 	org_name,per_type,per_type_name,last_year_in+last_year_out as budget_year_old_qty,last_year_in in_qty,last_year_out out_qty," + Environment.NewLine;
                //sql += " 	select_year_in+select_year_out budget_year_now_qty,select_year_in,select_year_out ,0 total_qty," + Environment.NewLine;
                //sql += " 	iif((select_year_in + select_year_out)=0,0,(select_year_in / (select_year_in + select_year_out))*100 )  in_percent," + Environment.NewLine;
                //sql += " 	iif((select_year_in + select_year_out)=0,0,(select_year_out/ (select_year_in + select_year_out))*100 )  out_percent" + Environment.NewLine;
                //sql += " 	from (" + Environment.NewLine;
                //sql += " 	select distinct @_LAST_YEAR last_year, @_SELECT_YEAR select_year, po.org_serial,ogn.org_name,p.per_type,pt.per_type_name, " + Environment.NewLine;
                //sql += " 		(select count(pp.id) " + Environment.NewLine;
                //sql += " 	     from PSST_PERSON pp " + Environment.NewLine;
                //sql += " 	     inner join PSST_POSITION ppo on ppo.id=pp.id " + Environment.NewLine;
                //sql += " 	     where ppo.org_serial=po.org_serial " + Environment.NewLine;
                //sql += " 	     and pp.per_type=p.per_type " + Environment.NewLine;
                //sql += " 	     and convert(varchar(4),pp.enter_date,112)+543 = @_LAST_YEAR " + Environment.NewLine;
                //sql += " 	     ) last_year_in, " + Environment.NewLine;
                //sql += " 	     (select count(pp.id) " + Environment.NewLine;
                //sql += " 	     from PSST_PERSON pp " + Environment.NewLine;
                //sql += " 	    inner join PSST_POSITION ppo on ppo.id=pp.id " + Environment.NewLine;
                //sql += " 	     where ppo.org_serial=po.org_serial " + Environment.NewLine;
                //sql += " 	     and pp.per_type=p.per_type " + Environment.NewLine;
                //sql += " 	     and convert(varchar(4),pp.quit_date,112)+543 = @_LAST_YEAR " + Environment.NewLine;
                //sql += " 	    ) last_year_out, " + Environment.NewLine;
                //sql += " 	     (select count(pp.id) " + Environment.NewLine;
                //sql += " 	     from PSST_PERSON pp " + Environment.NewLine;
                //sql += " 	     inner join PSST_POSITION ppo on ppo.id=pp.id " + Environment.NewLine;
                //sql += " 	     where ppo.org_serial=po.org_serial " + Environment.NewLine;
                //sql += " 	     and pp.per_type=p.per_type " + Environment.NewLine;
                //sql += " 	     and convert(varchar(4),pp.enter_date,112)+543 = @_SELECT_YEAR " + Environment.NewLine;
                //sql += " 	     ) select_year_in, " + Environment.NewLine;
                //sql += " 	     (select count(pp.id) " + Environment.NewLine;
                //sql += " 	     from PSST_PERSON pp " + Environment.NewLine;
                //sql += " 	    inner join PSST_POSITION ppo on ppo.id=pp.id " + Environment.NewLine;
                //sql += " 	     where ppo.org_serial=po.org_serial " + Environment.NewLine;
                //sql += " 	     and pp.per_type=p.per_type " + Environment.NewLine;
                //sql += " 	     and convert(varchar(4),pp.quit_date,112)+543 = @_SELECT_YEAR " + Environment.NewLine;
                //sql += " 	     ) select_year_out " + Environment.NewLine;
                //sql += " 	 from PSST_PERSON p " + Environment.NewLine;
                //sql += " 	inner join PSST_POSITION po on po.id=p.id " + Environment.NewLine;
                //sql += " 	inner join CTLT_ORGANIZE ogn on ogn.org_serial=po.org_serial " + Environment.NewLine;
                //sql += " 	left join psst_per_type pt on p.per_type=pt.per_type  " + Environment.NewLine;
                //sql += " 	where 1=1 " + Environment.NewLine;
                //if (per_type != null)
                //{
                //    sql += " and p.per_type=@_PER_TYPE" + Environment.NewLine;
                //    param[0] = OPM_BL.setParameter("@_PER_TYPE", SqlDbType.VarChar, per_type);
                //}
                //if (org_serial != null)
                //{
                //    sql += " and po.org_serial=@_ORG_SERIAL" + Environment.NewLine;
                //    param[1] = OPM_BL.setParameter("@_ORG_SERIAL", SqlDbType.VarChar, org_serial);
                //}
                //sql += " 	)T" + Environment.NewLine;
                sql += "select *,(LAST_YEAR+SELECT_YEAR_IN-SELECT_YEAR_OUT) as SELECT_YEAR," + Environment.NewLine;
                sql += "iif(LAST_YEAR=0,0.00,(cast(SELECT_YEAR_IN as decimal(9,2))/cast(LAST_YEAR as decimal(9,2))*100)) as IN_PERCENT ," + Environment.NewLine;
                sql += "iif(LAST_YEAR=0,0.00,(cast(SELECT_YEAR_OUT as decimal(9,2))/cast(LAST_YEAR as decimal(9,2))*100)) as OUT_PERCENT " + Environment.NewLine;
                sql += "from (" + Environment.NewLine;
                sql += "  select vw.ORG_SERIAL,vw.ORG_SERIAL_L2,vw.ORG_SERIAL_L3,vw.ORG_NAME_L3 as ORG_NAME,org_level," + Environment.NewLine;
                sql += "    (SELECT count(*) FROM PSST_HIS_POSITION hp" + Environment.NewLine;
                sql += "      inner JOIN (SELECT id, MAX(EFFECT_DATE) as EFFECT_DATE, MAX(hpin.LAST_DATE) as LAST_DATE" + Environment.NewLine;
                sql += "      FROM PSST_HIS_POSITION hpin " + Environment.NewLine;
                sql += "      inner join PSST_MOVEMENT_CODE mv on hpin.MOVEMENT_CODE=mv.MOVEMENT_CODE" + Environment.NewLine;
                sql += "      Where mv.movement_flag not in ('20','21','22','23','24') and mv.MOVEMENT_TYPE='1' and mv.MOVEMENT_CMD='A' " + Environment.NewLine;
                sql += "        and CMD_DATE<@_FROM_DATE" + Environment.NewLine;
                //-- ถ้าระบุ PER_TYPE
                if (!string.IsNullOrEmpty(per_type))
                {
                    sql += "        and hpin.PER_TYPE=@_PER_TYPE " + Environment.NewLine;
                }
                sql += "        and hpin.ORG_SERIAL=vw.ORG_SERIAL" + Environment.NewLine;
                sql += "      GROUP BY id) hpmax ON hp.id=hpmax.id AND hp.EFFECT_DATE=hpmax.EFFECT_DATE and hp.LAST_DATE=hpmax.LAST_DATE" + Environment.NewLine;
                sql += "	  ) as LAST_YEAR," + Environment.NewLine;
                sql += "    (SELECT count(*) FROM PSST_HIS_POSITION hp" + Environment.NewLine;
                sql += "      inner JOIN (SELECT id, MAX(EFFECT_DATE) as EFFECT_DATE" + Environment.NewLine;
                sql += "      FROM PSST_HIS_POSITION hpin " + Environment.NewLine;
                sql += "           inner join PSST_MOVEMENT_CODE mv on hpin.MOVEMENT_CODE=mv.MOVEMENT_CODE" + Environment.NewLine;
                sql += "	  WHERE CMD_DATE BETWEEN @_FROM_DATE AND @_TO_DATE" + Environment.NewLine;
                if (!string.IsNullOrEmpty(per_type))
                {
                    sql += "         and hpin.PER_TYPE=@_PER_TYPE   -- ถ้าระบุ PER_TYPE" + Environment.NewLine;
                }
                sql += "         and hpin.ORG_SERIAL=vw.ORG_SERIAL" + Environment.NewLine;
                sql += "         and mv.movement_flag in ('10','11','12') and mv.MOVEMENT_TYPE='1' and mv.MOVEMENT_CMD='A' " + Environment.NewLine;
                sql += "         GROUP BY id) hpmax ON hp.id=hpmax.id AND hp.EFFECT_DATE=hpmax.EFFECT_DATE) as SELECT_YEAR_IN," + Environment.NewLine;
                sql += "    (SELECT count(*) FROM PSST_HIS_POSITION hp" + Environment.NewLine;
                sql += "      inner JOIN (SELECT id, MAX(EFFECT_DATE) as EFFECT_DATE" + Environment.NewLine;
                sql += "      FROM PSST_HIS_POSITION hpin " + Environment.NewLine;
                sql += "           inner join PSST_MOVEMENT_CODE mv on hpin.MOVEMENT_CODE=mv.MOVEMENT_CODE" + Environment.NewLine;
                sql += "      WHERE CMD_DATE BETWEEN @_FROM_DATE AND @_TO_DATE" + Environment.NewLine;
                if (!string.IsNullOrEmpty(per_type))
                {
                    sql += "	    and hpin.PER_TYPE=@_PER_TYPE   -- ถ้าระบุ PER_TYPE" + Environment.NewLine;
                }
                sql += "	    and hpin.ORG_SERIAL=vw.ORG_SERIAL" + Environment.NewLine;
                sql += "        and mv.movement_flag in ('20','21','22','23','24') and mv.MOVEMENT_TYPE='1' and mv.MOVEMENT_CMD='A' " + Environment.NewLine;
                sql += "      GROUP BY id) hpmax ON hp.id=hpmax.id AND hp.EFFECT_DATE=hpmax.EFFECT_DATE) as SELECT_YEAR_OUT" + Environment.NewLine;
                sql += "    , vw.ORDER_SEQ" + Environment.NewLine;
                sql += "    from vw_smm_org  vw" + Environment.NewLine;
                //ถ้าระบุ ORG_SERIAL
                if (!string.IsNullOrEmpty(org_serial) && org_serial != "76")
                {
                    sql += "    where (ORG_SERIAL_L2=@_ORG_SERIAL or ORG_SERIAL_L3=@_ORG_SERIAL or ORG_SERIAL_L4=@_ORG_SERIAL or ORG_SERIAL_L5=@_ORG_SERIAL)" + Environment.NewLine;
                }
                sql += ") org order by CASE WHEN ORDER_SEQ IS NULL THEN 1 ELSE 0 END, ORDER_SEQ" + Environment.NewLine;


                //if (string.IsNullOrEmpty(org_serial) || org_serial == "76")
                //{
                //    sql = "Select org.ORG_SERIAL_L3 as ORG_SERIAL,org.ORG_SERIAL_L2,org.ORG_SERIAL_L3,org.ORG_NAME_L3,org.ORG_NAME_L3 as ORG_NAME,sum(org.LAST_YEAR) as LAST_YEAR,sum(org.SELECT_YEAR_IN) as SELECT_YEAR_IN,sum(org.SELECT_YEAR_OUT) as SELECT_YEAR_OUT,sum(org.LAST_YEAR+org.SELECT_YEAR_IN-org.SELECT_YEAR_OUT) as SELECT_YEAR" + Environment.NewLine;
                //    sql += ",iif(sum(org.LAST_YEAR)=0,0.00,(cast(sum(org.SELECT_YEAR_IN) as decimal(9,2))/cast(sum(org.LAST_YEAR) as decimal(9,2))*100)) as IN_PERCENT" + Environment.NewLine;
                //    sql += ",iif(sum(org.LAST_YEAR)=0,0.00,(cast(sum(org.SELECT_YEAR_OUT) as decimal(9,2))/cast(sum(org.LAST_YEAR) as decimal(9,2))*100)) as OUT_PERCENT" + Environment.NewLine;
                //    sql += ",org.ORDER_SEQ_L3, '3' as ORG_LEVEL" + Environment.NewLine;
                //    sql += "from (" + Environment.NewLine;
                //    sql += "  select vw.ORG_SERIAL,vw.ORG_SERIAL_L2,vw.ORG_SERIAL_L3,vw.ORG_NAME_L3,ORG_NAME," + Environment.NewLine;
                //    sql += "    (SELECT count(*) FROM PSST_HIS_POSITION hp" + Environment.NewLine;
                //    sql += "      inner JOIN (SELECT id, MAX(EFFECT_DATE) as EFFECT_DATE, MAX(hpin.LAST_DATE) as LAST_DATE" + Environment.NewLine;
                //    sql += "      FROM PSST_HIS_POSITION hpin" + Environment.NewLine;
                //    sql += "      inner join PSST_MOVEMENT_CODE mv on hpin.MOVEMENT_CODE=mv.MOVEMENT_CODE" + Environment.NewLine;
                //    sql += "      Where mv.movement_flag not in ('20','21','22','23','24') and mv.MOVEMENT_TYPE='1' and mv.MOVEMENT_CMD='A'" + Environment.NewLine;
                //    sql += "        and CMD_BUDG<=@_LAST_YEAR" + Environment.NewLine;
                //    if (per_type != null)
                //    {
                //        sql += "        and hpin.PER_TYPE=@_PER_TYPE" + Environment.NewLine;
                //    }
                //    sql += "        and hpin.ORG_SERIAL=vw.ORG_SERIAL" + Environment.NewLine;
                //    sql += "      GROUP BY id) hpmax ON hp.id=hpmax.id AND hp.EFFECT_DATE=hpmax.EFFECT_DATE and hp.LAST_DATE=hpmax.LAST_DATE" + Environment.NewLine;
                //    sql += "	  ) as LAST_YEAR," + Environment.NewLine;
                //    sql += "    (SELECT count(*) FROM PSST_HIS_POSITION hp" + Environment.NewLine;
                //    sql += "      inner JOIN (SELECT id, MAX(EFFECT_DATE) as EFFECT_DATE" + Environment.NewLine;
                //    sql += "      FROM PSST_HIS_POSITION hpin " + Environment.NewLine;
                //    sql += "           inner join PSST_MOVEMENT_CODE mv on hpin.MOVEMENT_CODE=mv.MOVEMENT_CODE" + Environment.NewLine;
                //    sql += "	  WHERE CMD_BUDG=@_SELECT_YEAR" + Environment.NewLine;
                //    if (per_type != null)
                //    {
                //        sql += "         and hpin.PER_TYPE=@_PER_TYPE " + Environment.NewLine;
                //    }
                //    sql += "         and hpin.ORG_SERIAL=vw.ORG_SERIAL" + Environment.NewLine;
                //    sql += "         and mv.movement_flag in ('10','11','12') and mv.MOVEMENT_TYPE='1' and mv.MOVEMENT_CMD='A'" + Environment.NewLine;
                //    sql += "         GROUP BY id) hpmax ON hp.id=hpmax.id AND hp.EFFECT_DATE=hpmax.EFFECT_DATE) as SELECT_YEAR_IN," + Environment.NewLine;
                //    sql += "    (SELECT count(*) FROM PSST_HIS_POSITION hp" + Environment.NewLine;
                //    sql += "      inner JOIN (SELECT id, MAX(EFFECT_DATE) as EFFECT_DATE" + Environment.NewLine;
                //    sql += "      FROM PSST_HIS_POSITION hpin" + Environment.NewLine;
                //    sql += "           inner join PSST_MOVEMENT_CODE mv on hpin.MOVEMENT_CODE=mv.MOVEMENT_CODE" + Environment.NewLine;
                //    sql += "      WHERE CMD_BUDG=@_SELECT_YEAR" + Environment.NewLine;
                //    if (per_type != null)
                //    {
                //        sql += "	    and hpin.PER_TYPE=@_PER_TYPE" + Environment.NewLine;
                //    }
                //    sql += "	    and hpin.ORG_SERIAL=vw.ORG_SERIAL" + Environment.NewLine;
                //    sql += "        and mv.movement_flag in ('20','21','22','23','24') and mv.MOVEMENT_TYPE='1' and mv.MOVEMENT_CMD='A' " + Environment.NewLine;
                //    sql += "      GROUP BY id) hpmax ON hp.id=hpmax.id AND hp.EFFECT_DATE=hpmax.EFFECT_DATE) as SELECT_YEAR_OUT" + Environment.NewLine;
                //    sql += "    , vw.ORDER_SEQ, vw.ORDER_SEQ_L3" + Environment.NewLine;
                //    sql += "    from vw_smm_org  vw" + Environment.NewLine;
                //    sql += "	Where ORG_SERIAL_L3 IS NOT NULL" + Environment.NewLine;
                //    sql += "  ) org" + Environment.NewLine;
                //    sql += "  group by org.ORG_SERIAL_L2,org.ORG_SERIAL_L3,org.ORG_NAME_L3,org.ORDER_SEQ_L3" + Environment.NewLine;
                //    sql += "order by CASE WHEN org.ORDER_SEQ_L3 IS NULL THEN 1 ELSE 0 END, org.ORDER_SEQ_L3" + Environment.NewLine;
                //}
                //else
                //{
                //   sql = "select *,(LAST_YEAR+SELECT_YEAR_IN-SELECT_YEAR_OUT) as SELECT_YEAR," + Environment.NewLine;
                //   sql += "iif(LAST_YEAR=0,0.00,(cast(SELECT_YEAR_IN as decimal(9,2))/cast(LAST_YEAR as decimal(9,2))*100)) as IN_PERCENT ," + Environment.NewLine;
                //   sql += "iif(LAST_YEAR=0,0.00,(cast(SELECT_YEAR_OUT as decimal(9,2))/cast(LAST_YEAR as decimal(9,2))*100)) as OUT_PERCENT " + Environment.NewLine;
                //   sql += "from (" + Environment.NewLine;
                //   sql += "  select vw.ORG_SERIAL,vw.ORG_SERIAL_L2,vw.ORG_SERIAL_L3,vw.ORG_NAME_L3,ORG_NAME,org_level," + Environment.NewLine;
                //   sql += "    (SELECT count(*) FROM PSST_HIS_POSITION hp" + Environment.NewLine;
                //   sql += "      inner JOIN (SELECT id, MAX(EFFECT_DATE) as EFFECT_DATE, MAX(hpin.LAST_DATE) as LAST_DATE" + Environment.NewLine;
                //   sql += "      FROM PSST_HIS_POSITION hpin " + Environment.NewLine;
                //   sql += "      inner join PSST_MOVEMENT_CODE mv on hpin.MOVEMENT_CODE=mv.MOVEMENT_CODE" + Environment.NewLine;
                //   sql += "      Where mv.movement_flag not in ('20','21','22','23','24') and mv.MOVEMENT_TYPE='1' and mv.MOVEMENT_CMD='A' " + Environment.NewLine;
                //   sql += "        and CMD_BUDG<=@_LAST_YEAR" + Environment.NewLine;
                //   if (per_type != null)
                //   {
                //       sql += "        and hpin.PER_TYPE=@_PER_TYPE" + Environment.NewLine;
                //   }
                //   sql += "        and hpin.ORG_SERIAL=vw.ORG_SERIAL" + Environment.NewLine;
                //   sql += "      GROUP BY id) hpmax ON hp.id=hpmax.id AND hp.EFFECT_DATE=hpmax.EFFECT_DATE and hp.LAST_DATE=hpmax.LAST_DATE" + Environment.NewLine;
                //   sql += "	  ) as LAST_YEAR," + Environment.NewLine;
                //   sql += "    (SELECT count(*) FROM PSST_HIS_POSITION hp" + Environment.NewLine;
                //   sql += "      inner JOIN (SELECT id, MAX(EFFECT_DATE) as EFFECT_DATE" + Environment.NewLine;
                //   sql += "      FROM PSST_HIS_POSITION hpin " + Environment.NewLine;
                //   sql += "           inner join PSST_MOVEMENT_CODE mv on hpin.MOVEMENT_CODE=mv.MOVEMENT_CODE" + Environment.NewLine;
                //   sql += "	  WHERE CMD_BUDG=@_SELECT_YEAR" + Environment.NewLine;
                //   if (per_type != null)
                //   {
                //       sql += "         and hpin.PER_TYPE=@_PER_TYPE" + Environment.NewLine;
                //   }
                //   sql += "         and hpin.ORG_SERIAL=vw.ORG_SERIAL" + Environment.NewLine;
                //   sql += "         and mv.movement_flag in ('10','11','12') and mv.MOVEMENT_TYPE='1' and mv.MOVEMENT_CMD='A' " + Environment.NewLine;
                //   sql += "         GROUP BY id) hpmax ON hp.id=hpmax.id AND hp.EFFECT_DATE=hpmax.EFFECT_DATE) as SELECT_YEAR_IN," + Environment.NewLine;
                //   sql += "    (SELECT count(*) FROM PSST_HIS_POSITION hp" + Environment.NewLine;
                //   sql += "      inner JOIN (SELECT id, MAX(EFFECT_DATE) as EFFECT_DATE" + Environment.NewLine;
                //   sql += "      FROM PSST_HIS_POSITION hpin " + Environment.NewLine;
                //   sql += "           inner join PSST_MOVEMENT_CODE mv on hpin.MOVEMENT_CODE=mv.MOVEMENT_CODE" + Environment.NewLine;
                //   sql += "      WHERE CMD_BUDG=@_SELECT_YEAR" + Environment.NewLine;
                //   if (per_type != null)
                //   {
                //       sql += "	    and hpin.PER_TYPE=@_PER_TYPE" + Environment.NewLine;
                //   }
                //   sql += "	    and hpin.ORG_SERIAL=vw.ORG_SERIAL" + Environment.NewLine;
                //   sql += "        and mv.movement_flag in ('20','21','22','23','24') and mv.MOVEMENT_TYPE='1' and mv.MOVEMENT_CMD='A' " + Environment.NewLine;
                //   sql += "      GROUP BY id) hpmax ON hp.id=hpmax.id AND hp.EFFECT_DATE=hpmax.EFFECT_DATE) as SELECT_YEAR_OUT" + Environment.NewLine;
                //   sql += "    , vw.ORDER_SEQ" + Environment.NewLine;
                //   sql += "    from vw_smm_org  vw" + Environment.NewLine;
                //   sql += "    where (ORG_SERIAL_L2=@_ORG_SERIAL or ORG_SERIAL_L3=@_ORG_SERIAL or ORG_SERIAL_L4=@_ORG_SERIAL or ORG_SERIAL_L5=@_ORG_SERIAL)" + Environment.NewLine;
                //   sql += ") org order by CASE WHEN ORDER_SEQ IS NULL THEN 1 ELSE 0 END, ORDER_SEQ" + Environment.NewLine;
                //}

                if (!string.IsNullOrEmpty(per_type))
                {
                    param[0] = OPM_BL.setParameter("@_PER_TYPE", SqlDbType.VarChar, per_type);
                }
                if (!string.IsNullOrEmpty(org_serial) && org_serial != "76")
                {
                    param[1] = OPM_BL.setParameter("@_ORG_SERIAL", SqlDbType.VarChar, org_serial);
                }
                param[2] = OPM_BL.setParameter("@_FROM_DATE", SqlDbType.VarChar, DateFrom);
                param[3] = OPM_BL.setParameter("@_TO_DATE", SqlDbType.VarChar, DateTo);







                DataTable dt = OPM_BL.GetDatatable(sql, param);
                if (dt.Rows.Count > 0)
                {
                    if (vReportFormat == "EXCEL")
                    {
                        lblErrorMessage.Text = "";
                        ExcelReportENG.ExportReportRPT_HRM_UT0604(cmd_budg, UserName, UserOrg, dt, Response);
                    }
                    else if (vReportFormat == "PDF")
                    {
                        rpt.Load(Server.MapPath(home_floder + "/" + reportname + ".rpt"));
                        rpt.DataDefinition.FormulaFields["BudjetYearOld"].Text = "'" + (Convert.ToInt16(cmd_budg) - 1).ToString() + "'";
                        rpt.DataDefinition.FormulaFields["BudgetYear"].Text = "'" + cmd_budg + "'";
                        rpt.DataDefinition.FormulaFields["UserName"].Text = "'" + UserName + "'";
                        rpt.DataDefinition.FormulaFields["UserOrg"].Text = "'" + UserOrg + "'";
                        rpt.DataDefinition.FormulaFields["PrintDate"].Text = "'" + printDate + "'";
                        rpt.DataDefinition.FormulaFields["strTitle"].Text = "'" + strTitle + "'";
                        rpt.SetDataSource(dt);
                        lblErrorMessage.Text = "";
                        rpt.SetParameterValue("per_type_name", per_type_name);
                        rpt.SetParameterValue("sub_Title", sub_Title);
                        rpt.SetParameterValue("TH_DateFrom", TH_DateFrom);
                        rpt.SetParameterValue("TH_DateTo", TH_DateTo);
                        rpt.SetParameterValue("Title_type", Title_type);
                        CreatePDFReport(rpt, reportname);
                    }
                }
                else
                {
                    lblErrorMessage.Text = "ไม่พบข้อมูล";
                    CrystalReportViewer1.Visible = false;
                }
                #endregion
            }
            else if (reportname == "HRM_RP05")
            {
                #region "HRM_RP05"
                SqlParameter[] param = new SqlParameter[5];
                //string TypeName = "11";
                //string TimeRule = "90";
                //string DateFrom = "20150301";
                //string DateTo = "20150630";
                //string ORG = "18639";

                string TypeName = Request.QueryString["TypeName"];
                string TimeRule = Request.QueryString["TimeRule"];
                string DateFrom = Request.QueryString["DateFrom"];
                string DateTo = Request.QueryString["DateTo"];
                string ORG = Request.QueryString["ORG"];

                string sql = " select * from (select 'PSS4R10'  report_code, 'บัญชีการลงเวลาการปฏิบัติราชการ' report_name, p.per_type, p.per_type_name," + Environment.NewLine;
                    sql += " tp.org_serial, org.name_level3, isnull(org.name_level4,org.name_level3) name_level4 ,org.org_abbr," + Environment.NewLine;
                    sql += " '' comment_detail, '' leave_name, '' leave_status, '' leave_status_name," + Environment.NewLine;
                    sql += " (select dbo.Thaidate(tp.downtime_date)) downtime_date,tp.downtime_date downtime_date_date , tp.psst_time_rule_time_code time_code, tr.time_name," + Environment.NewLine;
                    sql += " isnull(p.prefix_name,'') + isnull(p.name,'') + ' ' + isnull(p.surname,'') staff_name," + Environment.NewLine;
                    sql += " tp.time_come,tp.time_back,tp.remarks" + Environment.NewLine;
                    sql += " from HRM_TIME_PROCESS tp" + Environment.NewLine;
                    sql += " inner join CTLT_ORGANIZE org on tp.org_serial=org.org_serial" + Environment.NewLine;
                    sql += " inner join vw_CMN_PERSON p on p.user_code=tp.user_code" + Environment.NewLine;
                    sql += " inner join PSST_TIME_RULE tr on tr.time_code=tp.psst_time_rule_time_code" + Environment.NewLine;
                    if (TypeName != null)
                    {
                        sql += " where p.per_type=@_PER_TYPE" + Environment.NewLine;
                        param[0] = OPM_BL.setParameter("@_PER_TYPE", SqlDbType.VarChar, TypeName);
                    }
                    if (TimeRule != null)
                    {
                        sql += " and tp.psst_time_rule_time_code=@_TIME_CODE" + Environment.NewLine;
                        param[1] = OPM_BL.setParameter("@_TIME_CODE", SqlDbType.VarChar, TimeRule);
                    }
                    if ((DateFrom != null) && (DateTo != null))
                    {
                        sql += " and convert(varchar(8),tp.downtime_date,112) between @_DATEFROM and @_DATETO" + Environment.NewLine;
                        param[2] = OPM_BL.setParameter("@_DATEFROM", SqlDbType.VarChar, DateFrom);
                        param[3] = OPM_BL.setParameter("@_DATETO", SqlDbType.VarChar, DateTo);
                    }
                    
                    if (ORG != null)
                    {
                        sql += " and tp.org_serial=@_ORG_SERIAL" + Environment.NewLine;
                        param[4] = OPM_BL.setParameter("@_ORG_SERIAL", SqlDbType.VarChar, ORG);
                    }

                    sql += " union all" + Environment.NewLine;
                    sql += " select 'PSS4R11' report_code,'บัญชีการลงเวลาการปฏิบัติราชการ' report_name, p.per_type, p.per_type_name," + Environment.NewLine;
                    sql += " td.org_serial, org.name_level3, isnull(org.name_level4,org.name_level3) name_level4 ,org.org_abbr," + Environment.NewLine;
                    sql += " 'รายการที่ยังไม่ได้ลงเวลาตามหลักเกณฑ์ประเภท' comment_detail, ml.leave_name," + Environment.NewLine;
                    sql += " isnull(td.leave_status,'') leave_status,case td.leave_status when '1' then 'ส่งใบลา' when '2' then 'ยังไม่ส่งใบลา' when '3' then 'ยกเว้นการส่งใบลา' end leave_status_name," + Environment.NewLine;
                    sql += " (select dbo.Thaidate(td.downtime_date)) downtime_date,td.downtime_date downtime_date_date ,td.time_code, tr.time_name," + Environment.NewLine;
                    sql += " isnull(p.prefix_name,'') + isnull(p.name,'') + ' ' + isnull(p.surname,'') staff_name," + Environment.NewLine;
                    sql += " td.time_come,td.time_back,td.remarks" + Environment.NewLine;
                    sql += " from HRM_TIME_DATALATE td" + Environment.NewLine;
                    sql += " inner join CTLT_ORGANIZE org on td.org_serial=org.org_serial" + Environment.NewLine;
                    sql += " inner join vw_CMN_PERSON p on p.id=td.person_id" + Environment.NewLine;
                    sql += " inner join PSST_TIME_RULE tr on tr.time_code=td.time_code" + Environment.NewLine;
                    sql += " inner join PDM_MS_LEAVE ml on ml.leave_id=td.leave_id" + Environment.NewLine;
                    if (TypeName != null)
                    { 
                      sql += " where p.per_type=@_PER_TYPE" + Environment.NewLine;
                    }
                    if (TimeRule != null)
                    { 
                      sql += " and td.time_code=@_TIME_CODE" + Environment.NewLine;
                    }
                    if ((DateFrom != null) && (DateTo != null))
                    { 
                      sql += " and convert(varchar(8),td.downtime_date,112) between @_DATEFROM and @_DATETO" + Environment.NewLine;
                    }
                    if (ORG != null)
                    { 
                      sql += " and td.org_serial=@_ORG_SERIAL";
                    }
                    sql += ")T order by report_code,downtime_date_date";
                

                DataTable dt = OPM_BL.GetDatatable(sql, param);
                if (dt.Rows.Count > 0)
                {
                    rpt.Load(Server.MapPath(home_floder + "/" + reportname + ".rpt"));
                    rpt.SetDataSource(dt);
                    lblErrorMessage.Text = "";
                    CrystalReportViewer1.Visible = true;
                }
                else
                {
                    lblErrorMessage.Text = "ไม่พบข้อมูล";
                    CrystalReportViewer1.Visible = false;
                }
                #endregion
            }
            else if (reportname == "RPT_HRM_UT0507_Approve")
            {
                #region "RPT_HRM_UT0507_Approve"

                string LeaveNoteID = Request.QueryString["LeaveNoteID"];
                string LeaveRemarkID = Request.QueryString["LeaveRemarkID"];
                string DateType = Request.QueryString["DateType"];
                string LeaveStatus = Request.QueryString["LeaveStatus"];
                string SearchStaff = Request.QueryString["SearchStaff"];
                string strperson_id = Request.QueryString["person_id"];
                string strDateFrom = Request.QueryString["DateFrom"];
                string strDateTo = Request.QueryString["DateTo"];
                long person_id;
                DateTime DateFrom;
                DateTime DateTo;

                //if (!string.IsNullOrEmpty(strDateFrom) && !string.IsNullOrEmpty(strDateTo))
                //{
                    DateFrom = Convert.ToDateTime(strDateFrom);
                    DateTo = Convert.ToDateTime(strDateTo);
                //}
                //if (!string.IsNullOrEmpty(strperson_id))
                //{
                    person_id = Convert.ToInt64(strperson_id);
                //}

                string _LeaveStatus = "2";
                string sql = "select lh.id, isnull(lh.leaveno_type,'') + '/' + isnull(lh.leaveno_seq,'') + '/' + isnull(lh.leaveno_year,'') doc_no, " + Environment.NewLine;
                sql += " isnull(p.prefix_name,'') + isnull(p.name,'') + ' ' + isnull(p.surname,'') staff_name, l.leave_name, " + Environment.NewLine;
                sql += " lh.created_date, lh.start_date,lh.end_date,lh.leave_day, " + Environment.NewLine;
                sql += " l.leave_type_id,lh.leave_status, isnull(l.approve,0) approve_by, " + Environment.NewLine;

                sql += " case lh.leave_status when '1' then 'อยู่ระหว่างตรวจสอบ' " + Environment.NewLine;
                sql += " 		when '2' then 'อยู่ระหว่างพิจารณา' " + Environment.NewLine;
                sql += " 		when '3' then 'อนุญาต' " + Environment.NewLine;
                sql += " 		when '4' then 'ไม่อนุญาต' " + Environment.NewLine;
                sql += " 		when '5' then 'ยกเลิกใบลา' " + Environment.NewLine;
                sql += " 		else  'ยังไม่ส่งใบลา'  end status_name " + Environment.NewLine;

                sql += ", 'N' leave_group " + Environment.NewLine;
                sql += " from HRM_LEAVE_HIS lh " + Environment.NewLine;
                sql += " inner join vw_CMN_PERSON p on p.id=lh.person_id " + Environment.NewLine;
                sql += " inner join PDM_MS_LEAVE l on l.leave_id=lh.leave_id " + Environment.NewLine;
                if (_LeaveStatus == "2")
                { //กรณีเป็นการรออนุญาต
                    sql += " inner join HRM_LEAVE_HIS_APPROVER ha on ha.leave_his_id=lh.id " + Environment.NewLine;
                }

                ////  เอาออกเนื่องจากการตรวจสอบ ใบลามราอนุมัติโดย กจท ต้องให้ผู้ตรวจสอบเลือกผู้อนุมัติให้ ภายใต้หน่วยงาน กจท level 3
                //sql += " where l.approve=0 " + Environment.NewLine;  //ในหน้าจอรออนุญาติแสดงเฉพาะใบลาที่อนุญาตโดยผู้บังคับบัญชา



                sql += " where l.leave_id not in ('" + OPM_BL.GetSysconfig("LeaveGroupID") + "')" + Environment.NewLine;  //ไม่ต้องแสดงรายการที่ Auto Insert จากการอนุญาตลาไปราชการเป็นหมู่คณะ
                if (_LeaveStatus == "2")
                { //กรณีเป็นการรออนุญาต
                    sql += " and ha.person_id_approve=@_PERSON_ID " + Environment.NewLine;
                }

                SqlParameter[] param = new SqlParameter[7];
                //param[0] = OPM_BL.setParameter("@_LEAVE_TYPE_ID", SqlDbType.VarChar, LeaveTypeID);

                if (LeaveNoteID != "0" && LeaveRemarkID == "0")
                {
                    sql += " and lh.leave_id=@_LEAVE_ID" + Environment.NewLine;
                    param[0] = OPM_BL.setParameter("@_LEAVE_ID", SqlDbType.VarChar, LeaveNoteID);
                }
                else if (LeaveNoteID == "0" && LeaveRemarkID != "0")
                {
                    sql += " and lh.leave_id=@_LEAVE_ID" + Environment.NewLine;
                    param[0] = OPM_BL.setParameter("@_LEAVE_ID", SqlDbType.VarChar, LeaveRemarkID);
                }
                else if (LeaveNoteID != "0" && LeaveRemarkID != "0")
                {
                    sql += " and lh.leave_id in (@_LEAVE_NOTE_ID,@_LEAVE_REMARK_ID)";
                    param[0] = OPM_BL.setParameter("@_LEAVE_NOTE_ID", SqlDbType.VarChar, LeaveNoteID);
                    param[1] = OPM_BL.setParameter("@_LEAVE_REMARK_ID", SqlDbType.VarChar, LeaveRemarkID);
                }

                if (DateType == "0")
                { //ทั้งหมด
                    if (DateFrom.Year != 1)
                    {
                        sql += " and (isnull(lh.updated_date,lh.created_date) <= @_DATE_FROM ";
                        sql += " or lh.start_date <= @_DATE_FROM ";
                        sql += " or lh.comm_date <= @_DATE_FROM )";
                        param[2] = OPM_BL.setParameter("@_DATE_FROM", SqlDbType.Date, DateFrom);
                    }
                    if (DateTo.Year != 1)
                    {
                        sql += " and (isnull(lh.updated_date,lh.created_date) >= @_DATE_TO ";
                        sql += " or lh.start_date >= @_DATE_TO ";
                        sql += " or lh.comm_date >= @_DATE_TO )";
                        param[3] = OPM_BL.setParameter("@_DATE_TO", SqlDbType.Date, DateTo);
                    }
                }
                else if (DateType == "1")
                {  //วันที่บันทึกวันลา
                    if (DateFrom.Year != 1)
                    {
                        sql += " and isnull(lh.updated_date,lh.created_date) <= @_DATE_FROM ";
                        param[2] = OPM_BL.setParameter("@_DATE_FROM", SqlDbType.Date, DateFrom);
                    }
                    if (DateTo.Year != 1)
                    {
                        sql += " and isnull(lh.updated_date,lh.created_date) >= @_DATE_TO ";
                        param[3] = OPM_BL.setParameter("@_DATE_TO", SqlDbType.Date, DateTo);
                    }
                }
                else if (DateType == "2")
                {//วันที่ลา
                    if (DateFrom.Year != 1)
                    {
                        sql += " and lh.start_date <= @_DATE_FROM ";
                        param[2] = OPM_BL.setParameter("@_DATE_FROM", SqlDbType.Date, DateFrom);
                    }
                    if (DateTo.Year != 1)
                    {
                        sql += " and lh.start_date >= @_DATE_TO ";
                        param[3] = OPM_BL.setParameter("@_DATE_TO", SqlDbType.Date, DateTo);
                    }
                }
                else if (DateType == "3")
                { //วันที่อนุญาติ
                    if (DateFrom.Year != 1)
                    {
                        sql += " and lh.comm_date <= @_DATE_FROM ";
                        param[2] = OPM_BL.setParameter("@_DATE_FROM", SqlDbType.Date, DateFrom);
                    }
                    if (DateTo.Year != 1)
                    {
                        sql += " and lh.comm_date >= @_DATE_TO ";
                        param[3] = OPM_BL.setParameter("@_DATE_TO", SqlDbType.Date, DateTo);
                    }
                }

                if (LeaveStatus != "0")
                {
                    sql += " and lh.leave_status=@_LEAVE_STATUS";
                    param[4] = OPM_BL.setParameter("@_LEAVE_STATUS", SqlDbType.VarChar, LeaveStatus);
                }
                else
                {
                    sql += " and lh.leave_status in ('2','3','4')";  //รออนุญาต, อนุญาต, ไม่อนุญาต
                }

                if (SearchStaff != "")
                {
                    sql += " and (isnull(p.prefix_name,'') + isnull(p.name,'') + ' ' + isnull(p.surname,'') like '%' + @_SEARCH_STAFF + '%' " + Environment.NewLine;
                    sql += " or p.pos_id like '%' + @_SEARCH_STAFF +'%')";
                    param[5] = OPM_BL.setParameter("@_SEARCH_STAFF", SqlDbType.VarChar, SearchStaff);
                }
                
                param[6] = OPM_BL.setParameter("@_PERSON_ID", SqlDbType.BigInt, person_id);

                DataTable dt = OPM_BL.GetDatatable(sql, param);

                

                dt.DefaultView.RowFilter = "leave_type_id=1";
                DataTable dtType1 = dt.DefaultView.ToTable();
                dt.DefaultView.RowFilter = "leave_type_id=2";
                DataTable dtType2 = dt.DefaultView.ToTable();
                dt.DefaultView.RowFilter = "id in (368946,368953)";
                DataTable dtType0 = dt.DefaultView.ToTable();
                if (dt.Rows.Count > 0)
                {
                    
                    if (vReportFormat == "PDF")
                    {
                        rpt.Load(Server.MapPath(home_floder + "/" + reportname + ".rpt"));

                        rpt.DataDefinition.FormulaFields["UserName"].Text = "'" + UserName + "'";
                        rpt.DataDefinition.FormulaFields["UserOrg"].Text = "'" + UserOrg + "'";
                        rpt.DataDefinition.FormulaFields["PrintDate"].Text = "'" + printDate + "'";
                        rpt.SetDataSource(dtType0);
                        rpt.Subreports["LEAVE_NOTE"].SetDataSource(dtType1);
                        rpt.Subreports["LEAVE_REMARK"].SetDataSource(dtType2);

                        lblErrorMessage.Text = "";

                      

                        CreatePDFReport(rpt, reportname);
                    }
                }
                else
                {
                    lblErrorMessage.Text = "ไม่พบข้อมูล";
                    CrystalReportViewer1.Visible = false;
                }
                #endregion
            }
            else if (reportname == "RPT_HRM_UT0605")
            {
                #region "RPT_HRM_UT0605"

                //string TypeName =  "11";
                //string TimeCode = "90";// "90";
                //string DateFrom = "20100118";
                //string DateTo = "20100119";
                //string ORG =  "65";
                //string UserName = "99999ชื่อสกุล99999";
                //string UserOrg = "xxxxxหน่วยงานxxxxx";
                //string PersonID = null;// "407";

                SqlParameter[] param = new SqlParameter[6];
                string per_type = Request.QueryString["per_type"];
                string time_code = Request.QueryString["time_code"];
                string downtime_date_from = Request.QueryString["downtime_date_from"];
                string downtime_date_to = Request.QueryString["downtime_date_to"];
                string org_serial = Request.QueryString["org_serial"];
                //string UserName = Request.QueryString["UserName"];
                //string UserOrg = Request.QueryString["UserOrg"];
                string person_id = Request.QueryString["person_id"];
                string IsHoliday = Request.QueryString["IsHoliday"];
                string ckTask = Request.QueryString["ckTask"];
                string UserOrg = Request.QueryString["UserOrg"];
                //string sql = "  select isnull(p.prefix_name,'') + isnull(p.name,'') + ' ' + isnull(p.surname,'') staff_name, " + Environment.NewLine;
                //sql += " o.name_level3,isnull(o.name_level4,o.name_level3) name_level4, isnull(o.name_level5,isnull(o.name_level4,o.name_level3)) name_level5, " + Environment.NewLine;
                //sql += " tr.time_code, tr.time_name, SUBSTRING(t.time_come,1,5) +' น.' time_come, SUBSTRING(t.time_back,1,5) +' น.' time_back, (select dbo.Thaidate(t.downtime_date)) downtime_date , " + Environment.NewLine;
                //sql += " p.per_type,p.per_type_name, t.remarks, " + Environment.NewLine;
                //sql += " (select  [dbo].[GetFormatTimeFromSec](t.time_come,t.time_back)) timeformat " + Environment.NewLine;
                //sql += " from HRM_TIME_PROCESS t " + Environment.NewLine;
                //sql += " inner join vw_CMN_PERSON p on t.person_id=p.id " + Environment.NewLine;
                //sql += " inner join PSST_TIME_RULE tr on tr.time_code=t.psst_time_rule_time_code " + Environment.NewLine;
                //sql += " inner join CTLT_ORGANIZE o on o.org_serial=t.org_serial  " + Environment.NewLine;
                //sql += " where t.org_serial<>0" + Environment.NewLine;

                //if (per_type != null)
                //{
                //    sql += " and p.per_type=@per_type" + Environment.NewLine;
                //    param[0] = OPM_BL.setParameter("@per_type", SqlDbType.VarChar, per_type);
                //}
                //if (time_code != null)
                //{
                //    sql += " and tr.time_code=@time_code" + Environment.NewLine;
                //    param[1] = OPM_BL.setParameter("@time_code", SqlDbType.VarChar, time_code);
                //}
                //if (downtime_date_from != null)
                //{
                //    sql += " and convert(varchar(8),t.downtime_date,112)>=@date_from" + Environment.NewLine;
                //    param[2] = OPM_BL.setParameter("@date_from", SqlDbType.VarChar, downtime_date_from);
                //}
                //if (downtime_date_to != null)
                //{
                //    sql += " and convert(varchar(8),t.downtime_date,112)<=@date_to" + Environment.NewLine;
                //    param[3] = OPM_BL.setParameter("@date_to", SqlDbType.VarChar, downtime_date_to);
                //}
                //if (org_serial != null)
                //{
                //    sql += " and t.org_serial=@org_serial" + Environment.NewLine;
                //    param[4] = OPM_BL.setParameter("@org_serial", SqlDbType.VarChar, org_serial);
                //}
                //if (person_id != null) {
                //    sql += " and t.person_id=@person_id " + Environment.NewLine;
                //    param[5] = OPM_BL.setParameter("@person_id", SqlDbType.BigInt, Convert.ToInt64(person_id));
                //}
                //if ((IsHoliday != null)&&(IsHoliday != "F")) {
                //    sql += " and (select  [dbo].[CheckIsHoliday](day(downtime_date),month(downtime_date))) > 0" + Environment.NewLine;
                //}

                //sql += "  order by t.downtime_date,p.per_type, o.name_level3,o.name_level4,o.name_level5, " + Environment.NewLine;
                //sql += " isnull(p.prefix_name,'') + isnull(p.name,'') + ' ' + isnull(p.surname,'')";
                string sql = "Select t.person_id,isnull(p.prefix_name,'') + isnull(p.name,'') + ' ' + isnull(p.surname,'') staff_name, " + Environment.NewLine;
                sql += " o.ORG_NAME,o.ORG_NAME_L3 as name_level3,o.ORG_NAME_L4 as name_level4,o.ORG_NAME_L5 as name_level5, o.ORG_NAME_INDENT,t.org_serial,o.ORG_SERIAL_L2,o.ORG_SERIAL_L3,o.ORG_SERIAL_L4,o.ORG_SERIAL_L5," + Environment.NewLine;
                sql += " tr.time_code, tr.time_name, SUBSTRING(t.time_come,1,5) +' น.' time_come, SUBSTRING(t.time_back,1,5) +' น.' time_back, (select dbo.Thaidate(t.downtime_date)) downtime_date ," + Environment.NewLine;
                sql += " p.per_type,p.per_type_name, t.remarks," + Environment.NewLine;
                sql += " (select  [dbo].[GetFormatTimeFromSec](t.time_come,t.time_back)) timeformat" + Environment.NewLine;
                sql += " from " + Environment.NewLine;
                sql += " (-- คนลงเวลาปกติ" + Environment.NewLine;
                sql += " select " + Environment.NewLine;
                sql += " [process_date],[person_id],[org_serial],[psst_time_rule_time_code] as time_code,[downtime_date],[time_come],[time_back],[remarks]" + Environment.NewLine;
                sql += " from HRM_TIME_PROCESS where convert(varchar(8),downtime_date,112)>=@DATE_FROM and convert(varchar(8),downtime_date,112)<=@DATE_TO" + Environment.NewLine;
                sql += " UNION ALL" + Environment.NewLine;
                sql += " -- คนลงเวลาไม่ปกติ (สาย-ลา-กลับก่อน)" + Environment.NewLine;
                sql += "select" + Environment.NewLine;
                sql += "    dl.[process_date],dl.[person_id],dl.[org_serial],dl.[time_code],dl.[downtime_date],dl.[time_come],dl.[time_back],dl.[remarks]" + Environment.NewLine;
                sql += " from HRM_TIME_DATALATE dl " + Environment.NewLine;
                sql += " left join HRM_TIME_PROCESS tp ON dl.person_id=tp.person_id and dl.downtime_date=tp.downtime_date" + Environment.NewLine;
                sql += " where convert(varchar(8),dl.downtime_date,112)>=@DATE_FROM and convert(varchar(8),dl.downtime_date,112)<=@DATE_TO " + Environment.NewLine;
                sql += " and tp.person_id is null" + Environment.NewLine;
                sql += " ) t -- ข้อมูลการลงเวลาทั้งหมด" + Environment.NewLine;
                sql += " inner join vw_CMN_PERSON p on t.person_id=p.id " + Environment.NewLine;
                sql += " inner join PSST_TIME_RULE tr on tr.time_code=t.time_code  " + Environment.NewLine;
                sql += " inner join vw_SMM_ORG o on o.org_serial=t.org_serial   " + Environment.NewLine;
                sql += " where t.org_serial<>0 " + Environment.NewLine;

                if (!string.IsNullOrEmpty(per_type))
                {
                    sql += " and p.per_type=@per_type   -- ถ้าระบุ @PER_TYPE " + Environment.NewLine;
                    param[0] = OPM_BL.setParameter("@per_type", SqlDbType.VarChar, per_type);
                }

                if (!string.IsNullOrEmpty(time_code))
                {
                    sql += " and tr.time_code=@time_code " + Environment.NewLine;
                    param[1] = OPM_BL.setParameter("@time_code", SqlDbType.VarChar, time_code);
                }

                sql += " and convert(varchar(8),t.downtime_date,112)>=@date_from " + Environment.NewLine;
                sql += " and convert(varchar(8),t.downtime_date,112)<=@date_to  " + Environment.NewLine;
                param[2] = OPM_BL.setParameter("@date_from", SqlDbType.VarChar, downtime_date_from);
                param[3] = OPM_BL.setParameter("@date_to", SqlDbType.VarChar, downtime_date_to);

                if (!string.IsNullOrEmpty(org_serial) && org_serial != "76")
                {
                    sql += "and (o.ORG_SERIAL_L2=@ORG_SERIAL or o.ORG_SERIAL_L3=@ORG_SERIAL or o.ORG_SERIAL_L4=@ORG_SERIAL or o.ORG_SERIAL_L5=@ORG_SERIAL)  --ถ้าระบุ @ORG_SERIAL" + Environment.NewLine;
                    param[4] = OPM_BL.setParameter("@ORG_SERIAL", SqlDbType.VarChar, org_serial);
                }
                if (!string.IsNullOrEmpty(person_id))
                {
                    sql += " and t.person_id=@PERSON_ID " + Environment.NewLine;
                    param[5] = OPM_BL.setParameter("@person_id", SqlDbType.BigInt, Convert.ToInt64(person_id));
                }
                sql += " order by t.downtime_date,p.per_type,o.ORG_SERIAL_L2,o.ORG_SERIAL_L3,o.ORG_SERIAL_L4,o.ORG_SERIAL_L5, isnull(p.prefix_name,'') + isnull(p.name,'') + ' ' + isnull(p.surname,'') ";

                DataTable dt = OPM_BL.GetDatatable(sql, param);
                if (dt.Rows.Count > 0)
                {

                    if (vReportFormat == "PDF")
                    {
                        if (person_id == null)
                        {
                            reportname += "_ORG";
                        }
                        else
                        {
                            reportname += "_PERSON";
                            //กรณีเลือกชื่อคนให้แสดงอีกรายงานนึง
                        }
                        rpt.Load(Server.MapPath(home_floder + "/" + reportname + ".rpt"));
                        rpt.DataDefinition.FormulaFields["UserName"].Text = "'" + UserName + "'";
                        rpt.DataDefinition.FormulaFields["UserOrg"].Text = "'" + UserOrg + "'";
                        rpt.DataDefinition.FormulaFields["PrintDate"].Text = "'" + printDate + "'";
                        rpt.DataDefinition.FormulaFields["ckTask"].Text = "'" + ckTask + "'";
                        rpt.SetDataSource(dt);
                        lblErrorMessage.Text = "";
                        CreatePDFReport(rpt, reportname);
                        //CrystalReportViewer1.Visible = true;
                    }
                    else if (vReportFormat == "EXCEL")
                    {
                        if (person_id == null)
                        {
                            ExcelReportENG.ExportReportRPT_HRM_UT0605_ORG(dt, UserName, Response);
                        }

                        else
                        {
                            ExcelReportENG.ExportReportRPT_HRM_UT0605_PERSON(dt, UserName, Response);
                        }
                    }

                }
                else
                {
                    lblErrorMessage.Text = "ไม่พบข้อมูล";
                    CrystalReportViewer1.Visible = false;
                }

                #endregion
            }
            else if (reportname == "RPT_HRM_UT0606")
            {
                #region "RPT_HRM_UT0606_Backup"

                ////string TypeName = "11";
                ////string ORG = "18634";
                ////string Month = "200511";
                ////string UserName = "99999ชื่อสกุล99999";
                ////string UserOrg = "xxxxxหน่วยงานxxxxx";

                //SqlParameter[] param = new SqlParameter[3];
                //string TypeName = Request.QueryString["TypeName"];
                //string ORG = Request.QueryString["ORG"];
                //string Month = Request.QueryString["Month"];
                //string UserName = Request.QueryString["UserName"];
                //string UserOrg = Request.QueryString["UserOrg"];

                //string sql = " select distinct o.org_serial, o.name_level3, o.name_level4, " + Environment.NewLine;
                //    sql += " p.id person_id, isnull(p.prefix_name,'') + isnull(p.name,'') + ' ' + isnull(p.surname,'') staff_name, p.pos_id," + Environment.NewLine;
                //    sql += " dbo.fn_RP07_GetLeaveDate(@_MONTH,p.id,1) sick_date," + Environment.NewLine;
                //    sql += " dbo.fn_RP07_GetLeaveDate(@_MONTH,p.id,3) private_date," + Environment.NewLine;
                //    sql += " dbo.fn_RP07_GetLeaveDate(@_MONTH,p.id,91) late_date," + Environment.NewLine;
                //    sql += " dbo.fn_RP07_GetLeaveDate(@_MONTH,p.id,92) back_before_date," + Environment.NewLine;
                //    sql += " dbo.fn_RP07_GetLeaveDate(@_MONTH,p.id,4) vacation_date," + Environment.NewLine;
                //    sql += " dbo.fn_RP07_GetLeaveDateOther(@_MONTH,p.id) other_date," + Environment.NewLine;
                //    sql += " dbo.fn_RP07_GetLeaveSum(@_MONTH,p.id,1) sick_qty," + Environment.NewLine;
                //    sql += " dbo.fn_RP07_GetLeaveSum(@_MONTH,p.id,3) private_qty," + Environment.NewLine;
                //    sql += " dbo.fn_RP07_GetLeaveSum(@_MONTH,p.id,91) late_qty," + Environment.NewLine;
                //    sql += " dbo.fn_RP07_GetLeaveSum(@_MONTH,p.id,92) back_before_qty," + Environment.NewLine;
                //    sql += " dbo.fn_RP07_GetLeaveSum(@_MONTH,p.id,4) vacation_qty," + Environment.NewLine;
                //    sql += " dbo.fn_RP07_GetLeaveSumOther(@_MONTH,p.id) other_qty" + Environment.NewLine;
                //    sql += " from HRM_LEAVE_HIS lh" + Environment.NewLine;
                //    sql += " inner join vw_CMN_PERSON p on lh.person_id=p.ID" + Environment.NewLine;
                //    sql += " inner join CTLT_ORGANIZE o on o.org_serial=p.org_serial where 1=1 " + Environment.NewLine;

                //    param[0] = OPM_BL.setParameter("@_MONTH", SqlDbType.VarChar, Month);
                //    if (TypeName != null)
                //    {
                //        sql += " and p.PER_TYPE = @_PER_TYPE" + Environment.NewLine;
                //        param[1] = OPM_BL.setParameter("@_PER_TYPE", SqlDbType.VarChar, TypeName);
                //    }
                //    if (ORG != null)
                //    {
                //        sql += " and o.org_serial=@_ORG_SERIAL" + Environment.NewLine;
                //        param[2] = OPM_BL.setParameter("@_ORG_SERIAL", SqlDbType.VarChar, ORG);
                //    }
                //    sql += " and convert(varchar(6),lh.start_date,112) = @_MONTH " + Environment.NewLine;
                //    sql += " order by o.name_level3,o.name_level4,staff_name";
                //DataTable dt = OPM_BL.GetDatatable(sql, param);
                //if (dt.Rows.Count > 0)
                //{
                //    string strMonth =  OPM_BL.GetMonthTH(Month);
                //    rpt.Load(Server.MapPath(home_floder + "/" + reportname + ".rpt"));
                //    rpt.DataDefinition.FormulaFields["MonthName"].Text = "'" + strMonth + "'";
                //    rpt.DataDefinition.FormulaFields["UserName"].Text = "'" + UserName + "'";
                //    rpt.DataDefinition.FormulaFields["UserOrg"].Text = "'" + UserOrg + "'";
                //    rpt.SetDataSource(dt);
                //    lblErrorMessage.Text = "";
                //    CrystalReportViewer1.Visible = true;
                //}
                //else
                //{
                //    lblErrorMessage.Text = "ไม่พบข้อมูล";
                //    CrystalReportViewer1.Visible = false;
                //}
                #endregion

                #region "RPT_HRM_UT0606"
                SqlParameter[] param = new SqlParameter[7];
                string per_type = Request.QueryString["per_type"];
                string time_code = Request.QueryString["time_code"];
                string downtime_date_from = Request.QueryString["downtime_date_from"];
                string downtime_date_to = Request.QueryString["downtime_date_to"];
                string org_serial = Request.QueryString["org_serial"];
                //string UserName = Request.QueryString["UserName"];
                //string UserOrg = Request.QueryString["UserOrg"];
                string person_id = Request.QueryString["person_id"];
                string remarks = Request.QueryString["remarks"];
                string strTitle = Request.QueryString["strTitle"];

                //string sql = "Select t.person_id,isnull(p.prefix_name,'') + isnull(p.name,'') + ' ' + isnull(p.surname,'') staff_name, " + Environment.NewLine;
                //sql += " o.ORG_NAME,o.ORG_NAME_L3 as name_level3,o.ORG_NAME_L4 as name_level4,o.ORG_NAME_L5 as name_level5, o.ORG_NAME_INDENT,t.org_serial,o.ORG_SERIAL_L2,o.ORG_SERIAL_L3,o.ORG_SERIAL_L4,o.ORG_SERIAL_L5," + Environment.NewLine;
                //sql += " tr.time_code, tr.time_name, SUBSTRING(t.time_come,1,5) +' น.' time_come, SUBSTRING(t.time_back,1,5) +' น.' time_back, (select dbo.Thaidate(t.downtime_date)) downtime_date ," + Environment.NewLine;
                //sql += " p.per_type,p.per_type_name, t.remarks," + Environment.NewLine;
                //sql += " (select  [dbo].[GetFormatTimeFromSec](t.time_come,t.time_back)) timeformat" + Environment.NewLine;
                //sql += " from " + Environment.NewLine;
                //sql += " (-- คนลงเวลาปกติ" + Environment.NewLine;
                //sql += " select " + Environment.NewLine;
                //sql += " [process_date],[person_id],[org_serial],[psst_time_rule_time_code] as time_code,[downtime_date],[time_come],[time_back],[remarks]" + Environment.NewLine;
                //sql += " from HRM_TIME_PROCESS where convert(varchar(8),downtime_date,112)>=@DATE_FROM and convert(varchar(8),downtime_date,112)<=@DATE_TO" + Environment.NewLine;
                //sql += " UNION ALL" + Environment.NewLine;
                //sql += " -- คนลงเวลาไม่ปกติ (สาย-ลา-กลับก่อน)" + Environment.NewLine;
                //sql += "select" + Environment.NewLine;
                //sql += "    dl.[process_date],dl.[person_id],dl.[org_serial],dl.[time_code],dl.[downtime_date],dl.[time_come],dl.[time_back],dl.[remarks]" + Environment.NewLine;
                //sql += " from HRM_TIME_DATALATE dl " + Environment.NewLine;
                //sql += " left join HRM_TIME_PROCESS tp ON dl.person_id=tp.person_id and dl.downtime_date=tp.downtime_date" + Environment.NewLine;
                //sql += " where convert(varchar(8),dl.downtime_date,112)>=@DATE_FROM and convert(varchar(8),dl.downtime_date,112)<=@DATE_TO " + Environment.NewLine;
                //sql += " and tp.person_id is null" + Environment.NewLine;
                //sql += " ) t -- ข้อมูลการลงเวลาทั้งหมด" + Environment.NewLine;
                //sql += " inner join vw_CMN_PERSON p on t.person_id=p.id " + Environment.NewLine;
                //sql += " inner join PSST_TIME_RULE tr on tr.time_code=t.time_code  " + Environment.NewLine;
                //sql += " inner join vw_SMM_ORG o on o.org_serial=t.org_serial   " + Environment.NewLine;
                //sql += " where t.org_serial<>0 " + Environment.NewLine;
                //if (!string.IsNullOrEmpty(per_type))
                //{
                //    sql += " and p.per_type=@per_type   -- ถ้าระบุ @PER_TYPE " + Environment.NewLine;
                //    param[0] = OPM_BL.setParameter("@per_type", SqlDbType.VarChar, per_type);
                //}

                //if (!string.IsNullOrEmpty(time_code))
                //{
                //    sql += " and tr.time_code=@time_code " + Environment.NewLine;
                //    param[1] = OPM_BL.setParameter("@time_code", SqlDbType.VarChar, time_code);
                //}

                //sql += " and convert(varchar(8),t.downtime_date,112)>=@date_from " + Environment.NewLine;
                //sql += " and convert(varchar(8),t.downtime_date,112)<=@date_to  " + Environment.NewLine;
                //param[2] = OPM_BL.setParameter("@date_from", SqlDbType.VarChar, downtime_date_from);
                //param[3] = OPM_BL.setParameter("@date_to", SqlDbType.VarChar, downtime_date_to);

                //if (!string.IsNullOrEmpty(org_serial))
                //{
                //    sql += "and (o.ORG_SERIAL_L2=@ORG_SERIAL or o.ORG_SERIAL_L3=@ORG_SERIAL or o.ORG_SERIAL_L4=@ORG_SERIAL or o.ORG_SERIAL_L5=@ORG_SERIAL)  --ถ้าระบุ @ORG_SERIAL" + Environment.NewLine;
                //    param[4] = OPM_BL.setParameter("@ORG_SERIAL", SqlDbType.VarChar, org_serial);
                //}
                //if (!string.IsNullOrEmpty(person_id))
                //{
                //    sql += " and t.person_id=@PERSON_ID " + Environment.NewLine;
                //    param[5] = OPM_BL.setParameter("@person_id", SqlDbType.BigInt, Convert.ToInt64(person_id));
                //}
                //if (!string.IsNullOrEmpty(remarks))
                //{
                //    sql += " and t.remarks=@REMARK  " + Environment.NewLine;
                //    param[5] = OPM_BL.setParameter("@REMARK", SqlDbType.VarChar, remarks);
                //}
                //sql += " order by t.downtime_date,p.per_type,o.ORG_SERIAL_L2,o.ORG_SERIAL_L3,o.ORG_SERIAL_L4,o.ORG_SERIAL_L5, isnull(p.prefix_name,'') + isnull(p.name,'') + ' ' + isnull(p.surname,'') ";
                string sql = "";

                sql += "Select t.person_id,isnull(p.prefix_name,'') + isnull(p.name,'') + ' ' + isnull(p.surname,'') staff_name, " + Environment.NewLine;
                sql += "    o.ORG_NAME,o.ORG_NAME_L3 as name_level3,o.ORG_NAME_L4 as name_level4,o.ORG_NAME_L5 as name_level5, o.ORG_NAME_INDENT,t.org_serial,o.ORG_SERIAL_L2,o.ORG_SERIAL_L3,o.ORG_SERIAL_L4,o.ORG_SERIAL_L5," + Environment.NewLine;
                sql += "    tr.time_code, tr.time_name, SUBSTRING(t.time_come,1,5) +' น.' time_come, SUBSTRING(t.time_back,1,5) +' น.' time_back, (select dbo.Thaidate(t.downtime_date)) downtime_date , " + Environment.NewLine;
                sql += "    p.per_type,p.per_type_name, t.remarks, " + Environment.NewLine;
                sql += "    (select  [dbo].[GetFormatTimeFromSec](t.time_come,t.time_back)) timeformat " + Environment.NewLine;
                sql += "from " + Environment.NewLine;
                sql += "   (-- คนลงเวลาปกติ" + Environment.NewLine;
                sql += "    select" + Environment.NewLine;
                sql += "        [process_date],[person_id],[org_serial],[psst_time_rule_time_code] as time_code,[downtime_date],[time_come],[time_back],[remarks]" + Environment.NewLine;
                sql += "    from HRM_TIME_PROCESS where convert(varchar(8),downtime_date,112)>=@DATE_FROM and convert(varchar(8),downtime_date,112)<=@DATE_TO" + Environment.NewLine;
                sql += "      UNION ALL" + Environment.NewLine;
                sql += "    -- คนลงเวลาไม่ปกติ (สาย-ลา-กลับก่อน)" + Environment.NewLine;
                sql += "    select" + Environment.NewLine;
                sql += "        dl.[process_date],dl.[person_id],dl.[org_serial],dl.[time_code],dl.[downtime_date],dl.[time_come],dl.[time_back],dl.[remarks]" + Environment.NewLine;
                sql += "    from HRM_TIME_DATALATE dl " + Environment.NewLine;
                sql += "       left join HRM_TIME_PROCESS tp ON dl.person_id=tp.person_id and dl.downtime_date=tp.downtime_date" + Environment.NewLine;
                sql += "    where convert(varchar(8),dl.downtime_date,112)>=@DATE_FROM and convert(varchar(8),dl.downtime_date,112)<=@DATE_TO" + Environment.NewLine;
                sql += "       and tp.person_id is null" + Environment.NewLine;
                sql += "    ) t -- ข้อมูลการลงเวลาทั้งหมด" + Environment.NewLine;
                sql += "inner join vw_CMN_PERSON p on t.person_id=p.id " + Environment.NewLine;
                sql += "inner join PSST_TIME_RULE tr on tr.time_code=t.time_code " + Environment.NewLine;
                sql += "inner join vw_SMM_ORG o on o.org_serial=t.org_serial  " + Environment.NewLine;
                sql += "where t.org_serial<>0" + Environment.NewLine;
                if (!string.IsNullOrEmpty(per_type))
                {
                    sql += "    and p.per_type=@per_type   -- ถ้าระบุ @PER_TYPE" + Environment.NewLine;
                    param[0] = OPM_BL.setParameter("@per_type", SqlDbType.VarChar, per_type);
                }
                if (!string.IsNullOrEmpty(time_code))
                {
                    sql += "    and tr.time_code=@time_code" + Environment.NewLine;
                    param[1] = OPM_BL.setParameter("@time_code", SqlDbType.VarChar, time_code);
                }
                sql += "    and convert(varchar(8),t.downtime_date,112)>=@date_from" + Environment.NewLine;
                sql += "    and convert(varchar(8),t.downtime_date,112)<=@date_to" + Environment.NewLine;
                param[2] = OPM_BL.setParameter("@date_from", SqlDbType.VarChar, downtime_date_from);
                param[3] = OPM_BL.setParameter("@date_to", SqlDbType.VarChar, downtime_date_to);
                if (!string.IsNullOrEmpty(org_serial) && org_serial != "76")
                {
                    sql += "	and (o.ORG_SERIAL_L2=@ORG_SERIAL or o.ORG_SERIAL_L3=@ORG_SERIAL or o.ORG_SERIAL_L4=@ORG_SERIAL or o.ORG_SERIAL_L5=@ORG_SERIAL)  --ถ้าระบุ @ORG_SERIAL + Environment.NewLine;" + Environment.NewLine;
                    param[4] = OPM_BL.setParameter("@ORG_SERIAL", SqlDbType.VarChar, org_serial);
                }
                if (!string.IsNullOrEmpty(person_id))
                {
                    sql += "    and t.person_id=@PERSON_ID    -- ถ้าระบุ @PERSON_ID" + Environment.NewLine;
                    param[5] = OPM_BL.setParameter("@PERSON_ID", SqlDbType.VarChar, person_id);
                }
                if (!string.IsNullOrEmpty(remarks))
                {
                    sql += "    and t.remarks=@REMARK    -- ถ้าระบุ @REMARK" + Environment.NewLine;
                    param[6] = OPM_BL.setParameter("@REMARK", SqlDbType.VarChar, remarks);
                }
                sql += " order by t.downtime_date,p.per_type,o.ORG_SERIAL_L2,o.ORG_SERIAL_L3,o.ORG_SERIAL_L4,o.ORG_SERIAL_L5, isnull(p.prefix_name,'') + isnull(p.name,'') + ' ' + isnull(p.surname,'')" + Environment.NewLine;



                DataTable dt = OPM_BL.GetDatatable(sql, param);
                if (dt.Rows.Count > 0)
                {

                    if (vReportFormat == "PDF")
                    {
                        if (person_id == null)
                        {
                            reportname += "_ORG";
                            rpt.Load(Server.MapPath(home_floder + "/" + reportname + ".rpt"));
                            rpt.DataDefinition.FormulaFields["UserName"].Text = "'" + UserName + "'";
                            rpt.DataDefinition.FormulaFields["UserOrg"].Text = "'" + UserOrg + "'";
                            rpt.DataDefinition.FormulaFields["PrintDate"].Text = "'" + printDate + "'";
                            rpt.DataDefinition.FormulaFields["strTitle"].Text = "'" + strTitle + "'";
                        }
                        else
                        {
                            reportname += "_PERSON";
                            //กรณีเลือกชื่อคนให้แสดงอีกรายงานนึง
                            rpt.Load(Server.MapPath(home_floder + "/" + reportname + ".rpt"));
                            rpt.DataDefinition.FormulaFields["UserName"].Text = "'" + UserName + "'";
                            rpt.DataDefinition.FormulaFields["UserOrg"].Text = "'" + UserOrg + "'";
                            rpt.DataDefinition.FormulaFields["DateFrom"].Text = "'" + OPM_BL.GetTHDate(downtime_date_from) + "'";
                            rpt.DataDefinition.FormulaFields["DateTo"].Text = "'" + OPM_BL.GetTHDate(downtime_date_to) + "'";
                            rpt.DataDefinition.FormulaFields["PrintDate"].Text = "'" + printDate + "'";
                            rpt.DataDefinition.FormulaFields["strTitle"].Text = "'" + strTitle + "'";
                        }

                        rpt.SetDataSource(dt);
                        lblErrorMessage.Text = "";
                        CreatePDFReport(rpt, reportname);
                        //CrystalReportViewer1.Visible = true;
                    }
                    else if (vReportFormat == "EXCEL")
                    {
                        if (person_id == null)
                        {
                            ExcelReportENG.ExportReportRPT_HRM_UT0606_ORG(dt, UserName, Response);
                        }

                        else
                        {

                            ExcelReportENG.ExportReportRPT_HRM_UT0606_PERSON(OPM_BL.GetTHDate(downtime_date_from), OPM_BL.GetTHDate(downtime_date_to), dt, UserName, Response);
                        }



                    }

                }
                else
                {
                    lblErrorMessage.Text = "ไม่พบข้อมูล";
                    CrystalReportViewer1.Visible = false;
                }
                #endregion
            }
            else if (reportname == "RPT_HRM_UT0607")
            {
                #region "RPT_HRM_UT0607"
                //string TypeName = "11";
                //string ORG = "18634";
                //string Month = "200511";
                //string UserName = "99999ชื่อสกุล99999";
                //string UserOrg = "xxxxxหน่วยงานxxxxx";

                SqlParameter[] param = new SqlParameter[4];
                string per_type = Request.QueryString["per_type"];
                string org_serial = Request.QueryString["org_serial"];
                string month = Request.QueryString["month"];
                //string UserName = Request.QueryString["UserName"];
                //string UserOrg = Request.QueryString["UserOrg"];
                string person_id = Request.QueryString["person_id"];
                string strTitle = Request.QueryString["strTitle"];
                string Header_Org = Request.QueryString["Header_Org"];
                string Sub_Org = Request.QueryString["Sub_Org"];

                string sql = " select distinct o.org_serial, o.name_level3, isnull(o.name_level4,'') name_level4, " + Environment.NewLine;
                sql += " p.id person_id, isnull(p.prefix_name,'') + isnull(p.name,'') + ' ' + isnull(p.surname,'') staff_name, p.pos_id," + Environment.NewLine;
                sql += " dbo.fn_RP07_GetLeaveDate(@_MONTH,p.id,1) sick_date," + Environment.NewLine;
                sql += " dbo.fn_RP07_GetLeaveDate(@_MONTH,p.id,3) private_date," + Environment.NewLine;
                sql += " dbo.fn_RP07_GetLeaveDate(@_MONTH,p.id,91) late_date," + Environment.NewLine;
                sql += " dbo.fn_RP07_GetLeaveDate(@_MONTH,p.id,92) back_before_date," + Environment.NewLine;
                sql += " dbo.fn_RP07_GetLeaveDate(@_MONTH,p.id,4) vacation_date," + Environment.NewLine;
                sql += " dbo.fn_RP07_GetLeaveDateOther(@_MONTH,p.id) other_date," + Environment.NewLine;
                sql += " dbo.fn_RP07_GetLeaveSum(@_MONTH,p.id,1) sick_qty," + Environment.NewLine;
                sql += " dbo.fn_RP07_GetLeaveSum(@_MONTH,p.id,3) private_qty," + Environment.NewLine;
                sql += " dbo.fn_RP07_GetLeaveSum(@_MONTH,p.id,91) late_qty," + Environment.NewLine;
                sql += " dbo.fn_RP07_GetLeaveSum(@_MONTH,p.id,92) back_before_qty," + Environment.NewLine;
                sql += " dbo.fn_RP07_GetLeaveSum(@_MONTH,p.id,4) vacation_qty," + Environment.NewLine;
                sql += " dbo.fn_RP07_GetLeaveSumOther(@_MONTH,p.id) other_qty" + Environment.NewLine;
                sql += " from HRM_LEAVE_HIS lh" + Environment.NewLine;
                sql += " inner join vw_CMN_PERSON p on lh.person_id=p.ID" + Environment.NewLine;
                sql += " inner join CTLT_ORGANIZE o on o.org_serial=p.org_serial where 1=1 " + Environment.NewLine;

                param[0] = OPM_BL.setParameter("@_MONTH", SqlDbType.VarChar, month);
                if (per_type != null)
                {
                    sql += " and p.PER_TYPE = @_PER_TYPE" + Environment.NewLine;
                    param[1] = OPM_BL.setParameter("@_PER_TYPE", SqlDbType.VarChar, per_type);
                }
                if (org_serial != null)
                {
                    sql += " and o.org_serial=@_ORG_SERIAL" + Environment.NewLine;
                    param[2] = OPM_BL.setParameter("@_ORG_SERIAL", SqlDbType.VarChar, org_serial);
                }
                if ((person_id != null) && (person_id != "0"))
                {
                    sql += " and p.id=@person_id " + Environment.NewLine;
                    param[3] = OPM_BL.setParameter("@person_id", SqlDbType.BigInt, Convert.ToInt64(person_id));
                }
                sql += " and convert(varchar(6),lh.start_date,112) = @_MONTH " + Environment.NewLine;
                sql += " order by o.name_level3,name_level4,staff_name";
                DataTable dt = OPM_BL.GetDatatable(sql, param);
                if (dt.Rows.Count > 0)
                {
                    string strMonth = OPM_BL.GetMonthTH(month);
                    if (vReportFormat == "EXCEL")
                    {
                        lblErrorMessage.Text = "";
                        ExcelReportENG.ExportReportRPT_HRM_UT0607(strMonth, UserName, UserOrg, dt, Response);
                    }
                    else if (vReportFormat == "PDF")
                    {
                        rpt.Load(Server.MapPath(home_floder + "/" + reportname + ".rpt"));
                        rpt.DataDefinition.FormulaFields["MonthName"].Text = "'" + strMonth + "'";
                        rpt.DataDefinition.FormulaFields["UserName"].Text = "'" + UserName + "'";
                        rpt.DataDefinition.FormulaFields["UserOrg"].Text = "'" + UserOrg + "'";
                        rpt.DataDefinition.FormulaFields["PrintDate"].Text = "'" + printDate + "'";
                        rpt.DataDefinition.FormulaFields["strTitle"].Text = "'" + strTitle + "'";
                        rpt.SetDataSource(dt);
                        lblErrorMessage.Text = "";

                        rpt.SetParameterValue("Header_Org", Header_Org);
                        rpt.SetParameterValue("Sub_Org", Sub_Org);

                        CreatePDFReport(rpt, reportname);
                    }
                }
                else
                {
                    lblErrorMessage.Text = "ไม่พบข้อมูล";
                    CrystalReportViewer1.Visible = false;
                }
                #endregion
            }
            else if (reportname == "RPT_HRM_UT0608")
            {
                #region "RPT_HRM_UT0608"

                //string per_type = "11";
                //string DateFrom = "20051107";
                //string DateTo = "20060228";
                //string org_serial = "18651";
                //string cmd_budg = "2549";
                //string UserName = "99999ชื่อสกุล99999";
                //string UserOrg = "xxxxxหน่วยงานxxxxx";

                SqlParameter[] param = new SqlParameter[5];
                string per_type = Request.QueryString["per_type"];
                string DateFrom = Request.QueryString["DateFrom"];
                string DateTo = Request.QueryString["DateTo"];
                string org_serial = Request.QueryString["org_serial"];
                //string UserName = Request.QueryString["UserName"];
                //string UserOrg = Request.QueryString["UserOrg"];
                string cmd_budg = Request.QueryString["cmd_budg"];
                string person_id = Request.QueryString["person_id"];
                string strTitle = Request.QueryString["strTitle"];

                string sql = " select distinct o.org_serial, o.name_level3, isnull(o.name_level4,o.name_level3) name_level4," + Environment.NewLine;
                sql += " p.id person_id, isnull(p.prefix_name,'') + isnull(p.name,'') + ' ' + isnull(p.surname,'') staff_name," + Environment.NewLine;
                sql += " p.per_type, p.per_type_name," + Environment.NewLine;
                sql += " dbo.fn_RP08_GetLeaveDate(@_DATE_FROM, @_DATE_TO,p.id,3) private_date," + Environment.NewLine;
                sql += " dbo.fn_RP08_GetLeaveDate(@_DATE_FROM, @_DATE_TO,p.id,1) sick_date," + Environment.NewLine;
                sql += " dbo.fn_RP08_GetLeaveQty(@_DATE_FROM, @_DATE_TO,p.id,3)+dbo.fn_RP08_GetLeaveQty(@_DATE_FROM, @_DATE_TO,p.id,1) sum_private_sick," + Environment.NewLine;
                sql += " dbo.fn_RP08_GetLeaveQty(@_DATE_FROM, @_DATE_TO,p.id,91) late_qty," + Environment.NewLine;
                sql += " dbo.fn_RP08_GetLeaveQty(@_DATE_FROM, @_DATE_TO,p.id,92) back_before_qty" + Environment.NewLine;
                sql += " from HRM_LEAVE_HIS lh" + Environment.NewLine;
                sql += " inner join vw_CMN_PERSON p on lh.person_id=p.ID" + Environment.NewLine;
                sql += " inner join CTLT_ORGANIZE o on o.org_serial=p.org_serial where 1 = 1 " + Environment.NewLine;
                param[0] = OPM_BL.setParameter("@_DATE_FROM", SqlDbType.VarChar, DateFrom);
                param[1] = OPM_BL.setParameter("@_DATE_TO", SqlDbType.VarChar, DateTo);
                if (per_type != null)
                {
                    sql += "  and p.PER_TYPE = @_PER_TYPE" + Environment.NewLine;
                    param[2] = OPM_BL.setParameter("@_PER_TYPE", SqlDbType.VarChar, per_type);
                }
                if (org_serial != null)
                {
                    sql += " and o.org_serial=@_ORG_SERIAL" + Environment.NewLine;
                    param[3] = OPM_BL.setParameter("@_ORG_SERIAL", SqlDbType.VarChar, org_serial);
                }
                if ((person_id != null) && (person_id != "0"))
                {
                    sql += " and p.id=@person_id " + Environment.NewLine;
                    param[4] = OPM_BL.setParameter("@person_id", SqlDbType.BigInt, Convert.ToInt64(person_id));
                }

                sql += " and convert(varchar(8),lh.start_date,112) between @_DATE_FROM and @_DATE_TO" + Environment.NewLine;
                sql += " order by name_level3";

                DataTable dt = OPM_BL.GetDatatable(sql, param);
                if (dt.Rows.Count > 0)
                {
                    string strDateFrom = OPM_BL.GetTHDate(DateFrom);
                    string strDateTo = OPM_BL.GetTHDate(DateTo);
                    if (vReportFormat == "EXCEL")
                    {
                        lblErrorMessage.Text = "";
                        ExcelReportENG.ExportReportRPT_HRM_UT0608(cmd_budg, strDateFrom, strDateTo, UserName, UserOrg, dt, Response);
                    }
                    else if (vReportFormat == "PDF")
                    {
                        rpt.Load(Server.MapPath(home_floder + "/" + reportname + ".rpt"));
                        rpt.DataDefinition.FormulaFields["DateFrom"].Text = "'" + strDateFrom + "'";
                        rpt.DataDefinition.FormulaFields["DateTo"].Text = "'" + strDateTo + "'";
                        rpt.DataDefinition.FormulaFields["BudgetYear"].Text = "'" + cmd_budg + "'";
                        rpt.DataDefinition.FormulaFields["UserName"].Text = "'" + UserName + "'";
                        rpt.DataDefinition.FormulaFields["UserOrg"].Text = "'" + UserOrg + "'";
                        rpt.DataDefinition.FormulaFields["PrintDate"].Text = "'" + printDate + "'";
                        rpt.DataDefinition.FormulaFields["strTitle"].Text = "'" + strTitle + "'";
                        rpt.SetDataSource(dt);
                        lblErrorMessage.Text = "";

                        CreatePDFReport(rpt, reportname);
                    }
                }
                else
                {
                    lblErrorMessage.Text = "ไม่พบข้อมูล";
                    CrystalReportViewer1.Visible = false;
                }
                #endregion
            }
            else if (reportname == "RPT_HRM_UT0609")
            {
                #region "RPT_HRM_UT0609"

                //string DateFrom = "20131001";
                //string DateTo = "20140331";
                //string org_serial = "17867";
                //string UserName = "99999ชื่อสกุล99999";
                //string UserOrg = "xxxxxหน่วยงานxxxxx";
                //string cmd_budg = "2556";
                //string person_id = "6044";
                //ลูกจ้างประจำ นายสมชาย สุขป้อม	กลุ่มงานพัสดุและบริหารทรัพย์สิน		29 มี.ค. 2549	1	ปวดข้อเท้า	28 มี.ค. 2549	28 มี.ค. 2549	

                SqlParameter[] param = new SqlParameter[4];
                string DateFrom = Request.QueryString["DateFrom"];
                string DateTo = Request.QueryString["DateTo"];
                string org_serial = Request.QueryString["org_serial"];
                //string UserName = Request.QueryString["UserName"];
                //string UserOrg = Request.QueryString["UserOrg"];
                string cmd_budg = Request.QueryString["cmd_budg"];
                string person_id = Request.QueryString["person_id"];
                string MonthPeriod = Request.QueryString["MonthPeriod"];
                string strTitle = Request.QueryString["strTitle"];

                string sql = "select isnull(p.prefix_name,'') + isnull(p.name,'') + ' ' + isnull(p.surname,'') person_name, " + Environment.NewLine;
                sql += " p.org_name, p.pos_id, " + Environment.NewLine;
                sql += "  (select [dbo].[ThaiDateAbbrMonth](h.created_date))created_date, str(l.leave_id) leave_id,  isnull(h.leave_reason,isnull(h.remarks,l.leave_name)) leave_reason, " + Environment.NewLine;
                sql += " (select [dbo].[ThaiDateAbbrMonth](h.start_date)) start_date, (select [dbo].[ThaiDateAbbrMonth](h.end_date)) end_date, h.leave_day, h.leave_day leave_sum " + Environment.NewLine;
                sql += " from HRM_LEAVE_HIS h " + Environment.NewLine;
                sql += " inner join vw_CMN_PERSON p on p.id=h.person_id " + Environment.NewLine;
                sql += " inner join PDM_MS_LEAVE l on l.leave_id=h.leave_id " + Environment.NewLine;
                sql += " where l.leave_id in (1,3,91,92,4) " + Environment.NewLine;
                sql += " and h.leave_status='3'" + Environment.NewLine;
                sql += " and convert(varchar(8),h.[start_date],112) between @_DATE_FROM and @_DATE_TO " + Environment.NewLine;

                param[0] = OPM_BL.setParameter("@_DATE_FROM", SqlDbType.VarChar, DateFrom);
                param[1] = OPM_BL.setParameter("@_DATE_TO", SqlDbType.VarChar, DateTo);

                if (org_serial != null)
                {
                    sql += " and p.org_serial=@_ORG_SERIAL";
                    param[2] = OPM_BL.setParameter("@_ORG_SERIAL", SqlDbType.VarChar, org_serial);
                }

                if ((person_id != null) && (person_id != "0"))
                {
                    sql += " and h.person_id=@person_id " + Environment.NewLine;
                    param[3] = OPM_BL.setParameter("@person_id", SqlDbType.BigInt, Convert.ToInt64(person_id));
                }

                DataTable dt = OPM_BL.GetDatatable(sql, param);
                if (dt.Rows.Count > 0)
                {
                    string strDateFrom = OPM_BL.GetTHDate(DateFrom);
                    string strDateTo = OPM_BL.GetTHDate(DateTo);
                    if (vReportFormat == "EXCEL")
                    {
                        lblErrorMessage.Text = "";
                        ExcelReportENG.ExportReportRPT_HRM_UT0609(cmd_budg, MonthPeriod, UserName, UserOrg, dt, Response);
                    }
                    else if (vReportFormat == "PDF")
                    {
                        rpt.Load(Server.MapPath(home_floder + "/" + reportname + ".rpt"));
                        rpt.DataDefinition.FormulaFields["DateFrom"].Text = "'" + strDateFrom + "'";
                        rpt.DataDefinition.FormulaFields["DateTo"].Text = "'" + strDateTo + "'";
                        rpt.DataDefinition.FormulaFields["BudgetYear"].Text = "'" + cmd_budg + "'";
                        rpt.DataDefinition.FormulaFields["MonthPeriod"].Text = "'" + MonthPeriod + "'";
                        rpt.DataDefinition.FormulaFields["UserName"].Text = "'" + UserName + "'";
                        rpt.DataDefinition.FormulaFields["UserOrg"].Text = "'" + UserOrg + "'";
                        rpt.DataDefinition.FormulaFields["PrintDate"].Text = "'" + printDate + "'";
                        rpt.DataDefinition.FormulaFields["strTitle"].Text = "'" + strTitle + "'";
                        rpt.SetDataSource(dt);
                        lblErrorMessage.Text = "";

                        CreatePDFReport(rpt, reportname);
                    }
                }
                else
                {
                    lblErrorMessage.Text = "ไม่พบข้อมูล";
                    CrystalReportViewer1.Visible = false;
                }

                #endregion
            }
            else if (reportname == "RPT_HRM_UT0610")
            {
                #region "RPT_HRM_UT0610"
                //string TypeName = "11";
                //string DateFrom = "20051107";
                //string DateTo = "20060228";
                //string ORG = "18651";
                //string BudgetYear = "2549";
                //string UserName = "99999ชื่อสกุล99999";
                //string UserOrg = "xxxxxหน่วยงานxxxxx";

                SqlParameter[] param = new SqlParameter[5];
                string per_type = Request.QueryString["per_type"];
                string DateFrom = Request.QueryString["DateFrom"];
                string DateTo = Request.QueryString["DateTo"];
                string org_serial = Request.QueryString["org_serial"];
                //string UserName = Request.QueryString["UserName"];
                //string UserOrg = Request.QueryString["UserOrg"];
                string cmd_budg = Request.QueryString["cmd_budg"];
                string person_id = Request.QueryString["person_id"];
                string strTitle = Request.QueryString["strTitle"];

                string sql = " select distinct o.org_serial, o.name_level3, isnull(o.name_level4,o.name_level3) name_level4," + Environment.NewLine;
                sql += " p.id person_id, isnull(p.prefix_name,'') + isnull(p.name,'') + ' ' + isnull(p.surname,'') staff_name," + Environment.NewLine;
                sql += " p.per_type, p.per_type_name,p.pos_id," + Environment.NewLine;
                sql += " dbo.fn_RP08_GetLeaveDate(@_DATE_FROM, @_DATE_TO,p.id,1) sick_date," + Environment.NewLine;
                sql += " dbo.fn_RP08_GetLeaveDate(@_DATE_FROM, @_DATE_TO,p.id,3) private_date," + Environment.NewLine;
                sql += " dbo.fn_RP08_GetLeaveDate(@_DATE_FROM, @_DATE_TO,p.id,91) late_date" + Environment.NewLine;
                sql += " from HRM_LEAVE_HIS lh" + Environment.NewLine;
                sql += " inner join vw_CMN_PERSON p on lh.person_id=p.ID" + Environment.NewLine;
                sql += " inner join CTLT_ORGANIZE o on o.org_serial=p.org_serial where 1 = 1 " + Environment.NewLine;

                param[0] = OPM_BL.setParameter("@_DATE_FROM", SqlDbType.VarChar, DateFrom);
                param[1] = OPM_BL.setParameter("@_DATE_TO", SqlDbType.VarChar, DateTo);
                if (per_type != null)
                {
                    sql += "  and p.PER_TYPE = @_PER_TYPE" + Environment.NewLine;
                    param[2] = OPM_BL.setParameter("@_PER_TYPE", SqlDbType.VarChar, per_type);
                }
                if (org_serial != null)
                {
                    sql += " and o.org_serial=@_ORG_SERIAL" + Environment.NewLine;
                    param[3] = OPM_BL.setParameter("@_ORG_SERIAL", SqlDbType.VarChar, org_serial);
                }
                if ((person_id != null) && (person_id != "0"))
                {
                    sql += " and p.id=@person_id " + Environment.NewLine;
                    param[4] = OPM_BL.setParameter("@person_id", SqlDbType.BigInt, Convert.ToInt64(person_id));
                }
                sql += " and convert(varchar(8),lh.start_date,112) between @_DATE_FROM and @_DATE_TO" + Environment.NewLine;
                sql += " order by name_level3";

                DataTable dt = OPM_BL.GetDatatable(sql, param);
                if (dt.Rows.Count > 0)
                {
                    string strDateFrom = OPM_BL.GetTHDate(DateFrom);
                    string strDateTo = OPM_BL.GetTHDate(DateTo);
                    if (vReportFormat == "EXCEL")
                    {
                        lblErrorMessage.Text = "";
                        ExcelReportENG.ExportReportRPT_HRM_UT0610(cmd_budg, strDateFrom, strDateTo, UserName, UserOrg, dt, Response);
                    }
                    else if (vReportFormat == "PDF")
                    {
                        rpt.Load(Server.MapPath(home_floder + "/" + reportname + ".rpt"));
                        rpt.DataDefinition.FormulaFields["DateFrom"].Text = "'" + strDateFrom + "'";
                        rpt.DataDefinition.FormulaFields["DateTo"].Text = "'" + strDateTo + "'";
                        rpt.DataDefinition.FormulaFields["BudgetYear"].Text = "'" + cmd_budg + "'";
                        rpt.DataDefinition.FormulaFields["UserName"].Text = "'" + UserName + "'";
                        rpt.DataDefinition.FormulaFields["UserOrg"].Text = "'" + UserOrg + "'";
                        rpt.DataDefinition.FormulaFields["PrintDate"].Text = "'" + printDate + "'";
                        rpt.DataDefinition.FormulaFields["strTitle"].Text = "'" + strTitle + "'";
                        rpt.SetDataSource(dt);
                        lblErrorMessage.Text = "";

                        CreatePDFReport(rpt, reportname);
                    }

                }
                else
                {
                    lblErrorMessage.Text = "ไม่พบข้อมูล";
                    CrystalReportViewer1.Visible = false;
                }
                #endregion
            }
            else if (reportname == "RPT_HRM_UT0611")
            {
                #region "RPT_HRM_UT0611"
                //string TypeName = "11"; //ข้าราชการพลเรือน
                //string ORG = "17867"; //กลุ่มพัฒนาบุคคลและเสริมสร้างวินัย
                //string person_id = 6044; //นางสาวชมพูนุท สุขศรีมั่งมี
                //string BudgetYear = "2555";
                //string UserName = "99999ชื่อสกุล99999";
                //string UserOrg = "xxxxxหน่วยงานxxxxx";

                SqlParameter[] param = new SqlParameter[4];
                string TypeName = Request.QueryString["per_type"];
                string ORG = Request.QueryString["org_serial"];
                string person_id = Request.QueryString["person_id"];
                //string UserName = Request.QueryString["UserName"];
                //string UserOrg = Request.QueryString["UserOrg"];
                string BudgetYear = Request.QueryString["cmd_budg"];
                string strTitle = Request.QueryString["strTitle"];

                string sql = " select o.org_serial, o.name_level3, isnull(o.name_level4,o.name_level3) name_level4, p.per_type_name, " + Environment.NewLine;
                sql += " p.id person_id, isnull(p.prefix_name,'') + isnull(p.name,'') + ' ' + isnull(p.surname,'') staff_name, @_BUDGET_YEAR budget_year, " + Environment.NewLine;
                sql += " sv.sum_rest, sv.this_rest, sv.sum_rest+sv.this_rest tot_rest, sv.leave_rest, (sv.sum_rest+sv.this_rest)-sv.leave_rest remain_rest " + Environment.NewLine;
                sql += " from  vw_CMN_PERSON p  " + Environment.NewLine;
                sql += " left join HRM_SUM_VACATION sv on p.id=sv.person_id  " + Environment.NewLine;
                sql += " inner join CTLT_ORGANIZE o on o.org_serial=p.org_serial " + Environment.NewLine;
                sql += " where 1=1  " + Environment.NewLine;

                param[0] = OPM_BL.setParameter("@_BUDGET_YEAR", SqlDbType.VarChar, BudgetYear);
                if (TypeName != null)
                {
                    sql += " and p.PER_TYPE = @_PER_TYPE " + Environment.NewLine;
                    param[1] = OPM_BL.setParameter("@_PER_TYPE", SqlDbType.VarChar, TypeName);
                }
                if (ORG != null && ORG != "76")
                {
                    sql += " and o.org_serial= @_ORG_SERIAL " + Environment.NewLine;
                    param[2] = OPM_BL.setParameter("@_ORG_SERIAL", SqlDbType.VarChar, ORG);
                }

                if ((person_id != null) && (person_id != "0"))
                {
                    sql += " and p.id=@_PERSON_ID " + Environment.NewLine;
                    param[3] = OPM_BL.setParameter("@_PERSON_ID", SqlDbType.BigInt, Convert.ToInt64(person_id));
                }

                sql += " and sv.budget_year=@_BUDGET_YEAR " + Environment.NewLine;
                sql += " order by name_level3,name_level4,staff_name";

                DataTable dt = OPM_BL.GetDatatable(sql, param);
                if (dt.Rows.Count > 0)
                {
                    if (vReportFormat == "EXCEL")
                    {
                        lblErrorMessage.Text = "";
                        ExcelReportENG.ExportReportRPT_HRM_UT0611(BudgetYear, UserName, UserOrg, dt, Response);
                    }
                    else if (vReportFormat == "PDF")
                    {
                        rpt.Load(Server.MapPath(home_floder + "/" + reportname + ".rpt"));
                        rpt.DataDefinition.FormulaFields["BudgetYear"].Text = "'" + BudgetYear + "'";
                        rpt.DataDefinition.FormulaFields["UserName"].Text = "'" + UserName + "'";
                        rpt.DataDefinition.FormulaFields["UserOrg"].Text = "'" + UserOrg + "'";
                        rpt.DataDefinition.FormulaFields["PrintDate"].Text = "'" + printDate + "'";
                        rpt.DataDefinition.FormulaFields["strTitle"].Text = "'" + strTitle + "'";
                        rpt.SetDataSource(dt);
                        lblErrorMessage.Text = "";

                        CreatePDFReport(rpt, reportname);
                    }

                }
                else
                {
                    lblErrorMessage.Text = "ไม่พบข้อมูล";
                    CrystalReportViewer1.Visible = false;
                }
                #endregion
            }
            else if (reportname == "RPT_HRM_UT0612")
            {
                #region "RPT_HRM_UT0612"


                string TypeName = Request.QueryString["TypeName"];
                string ORG = Request.QueryString["ORG"];
                string DateFrom = Request.QueryString["DateFrom"];
                string DateTo = Request.QueryString["DateTo"];
                string MsLeaveType = Request.QueryString["leave_type_id"];
                //string UserName = Request.QueryString["UserName"];
                //string UserOrg = Request.QueryString["UserOrg"];
                string BudgetYear = Request.QueryString["BudgetYear"];
                string FormatType = Request.QueryString["FormatType"];

                //TypeName = "11";
                //DateFrom = "20121001";//"20140101";
                //DateTo = "20140930"; //"20140501";
                //MsLeaveType = "2";
                //UserName = "99999ชื่อสกุล99999";
                //UserOrg = "xxxxxหน่วยงานxxxxx";
                //ORG = "3";//"36";
                //BudgetYear = "2556";  //กรณีเลือกเดือนเดียว ไม่ต้องระบุปีงบประมาณ
                //FormatType = "BAR_GRAPH"; //TABLE, BAR_GRAPH, LINE_GRAPH

                DataTable dt = ReportsEngine.WebReportsENG.GetQueryReportHRM_UT0612(Request);

                reportname = reportname + "_" + FormatType;

                if (dt.Rows.Count > 0)
                {
                    if (vReportFormat == "EXCEL")
                    {
                        lblErrorMessage.Text = "";
                        ExcelReportENG.ExportReportRPT_HRM_UT0612(BudgetYear, ORG, OPM_BL.GetTHDate(DateFrom), OPM_BL.GetTHDate(DateTo), UserName, UserOrg, dt, Response, FormatType);

                    }
                    else
                    {

                        string strBudgetYear = "";
                        if (BudgetYear == "")
                        {
                            strBudgetYear = "ตั้งแต่วันที่ " + OPM_BL.GetTHDate(DateFrom) + " ถึงวันที่ " + OPM_BL.GetTHDate(DateTo);
                        }
                        else
                        {
                            strBudgetYear = "ปีงบประมาณ " + BudgetYear;
                        }

                        if (FormatType == "TABLE")
                        {
                            rpt.Load(Server.MapPath(home_floder + "/" + reportname + ".rpt"));
                            rpt.DataDefinition.FormulaFields["cmd_budg"].Text = "'" + strBudgetYear + "'";
                            rpt.DataDefinition.FormulaFields["UserName"].Text = "'" + UserName + "'";
                            rpt.DataDefinition.FormulaFields["UserOrg"].Text = "'" + UserOrg + "'";
                            rpt.DataDefinition.FormulaFields["PrintDate"].Text = "'" + printDate + "'";
                            rpt.SetDataSource(dt);
                            lblErrorMessage.Text = "";
                            CreatePDFReport(rpt, reportname);
                        }
                        else if (FormatType == "BAR_GRAPH")
                        {
                            lblErrorMessage.Text = "Report not found.";
                            CrystalReportViewer1.Visible = false;
                        }
                        else if (FormatType == "LINE_GRAPH")
                        {
                            lblErrorMessage.Text = "Report not found.";
                            CrystalReportViewer1.Visible = false;
                        }

                    }
                }
                else
                {
                    lblErrorMessage.Text = "ไม่พบข้อมูล";
                    CrystalReportViewer1.Visible = false;
                }
                #endregion
            }
            else if (reportname == "RPT_HRM_UT0613")
            {
                #region "RPT_HRM_UT0613"
                string TypeName = Request.QueryString["TypeName"];
                string ORG = Request.QueryString["ORG"];
                string DateFrom = Request.QueryString["DateFrom"];
                string DateTo = Request.QueryString["DateTo"];
                string MsLeaveType = Request.QueryString["leave_type_id"];
                //string UserName = Request.QueryString["UserName"];
                //string UserOrg = Request.QueryString["UserOrg"];
                string BudgetYear = Request.QueryString["BudgetYear"];
                string FormatType = Request.QueryString["FormatType"];

                DataTable dt = ReportsEngine.WebReportsENG.GetQueryReportHRM_UT0613(Request);
                reportname = reportname + "_" + FormatType;

                if (dt.Rows.Count > 0)
                {
                    if (vReportFormat == "EXCEL")
                    {
                        lblErrorMessage.Text = "";
                        ExcelReportENG.ExportReportRPT_HRM_UT0613(BudgetYear, ORG, OPM_BL.GetTHDate(DateFrom), OPM_BL.GetTHDate(DateTo), UserName, UserOrg, dt, Response, FormatType);

                    }
                    else
                    {

                        string strBudgetYear = "";
                        if (BudgetYear == "")
                        {
                            strBudgetYear = "ตั้งแต่วันที่ " + OPM_BL.GetTHDate(DateFrom) + " ถึงวันที่ " + OPM_BL.GetTHDate(DateTo);
                        }
                        else
                        {
                            strBudgetYear = "ปีงบประมาณ " + BudgetYear;
                        }

                        if (FormatType == "TABLE")
                        {
                            rpt.Load(Server.MapPath(home_floder + "/" + reportname + ".rpt"));
                            rpt.DataDefinition.FormulaFields["cmd_budg"].Text = "'" + strBudgetYear + "'";
                            rpt.DataDefinition.FormulaFields["UserName"].Text = "'" + UserName + "'";
                            rpt.DataDefinition.FormulaFields["UserOrg"].Text = "'" + UserOrg + "'";
                            rpt.DataDefinition.FormulaFields["PrintDate"].Text = "'" + printDate + "'";
                            rpt.SetDataSource(dt);
                            lblErrorMessage.Text = "";
                            CreatePDFReport(rpt, reportname);
                        }
                        else if (FormatType == "BAR_GRAPH")
                        {
                            lblErrorMessage.Text = "Report not found.";
                            CrystalReportViewer1.Visible = false;
                        }
                        else if (FormatType == "LINE_GRAPH")
                        {
                            lblErrorMessage.Text = "Report not found.";
                            CrystalReportViewer1.Visible = false;
                        }

                    }
                }
                else
                {
                    lblErrorMessage.Text = "ไม่พบข้อมูล";
                    CrystalReportViewer1.Visible = false;
                }
                #endregion
            }
            else if (reportname == "RPT_HRM_UT0614")
            {
                #region "RPT_HRM_UT0614"

                string TypeName = Request.QueryString["TypeName"];
                string ORG = Request.QueryString["org_serial"];
                string differentiate = Request.QueryString["differentiate"];
                string differentiate_name = Request.QueryString["differentiate_name"];
                string PerTypeName = Request.QueryString["PerTypeName"];
                //string UserName = Request.QueryString["UserName"];
                //string UserOrg = Request.QueryString["UserOrg"];
                string FormatType = Request.QueryString["FormatType"];

                #region "คิวรี่เก่า"
                //TypeName = "11";
                //ORG = "44";
                //differentiate = "SEX";   //จำแนกตาม

                ////คิวรี่ข้อมูลตามที่จำแนก
                //string sql = "";
                //switch (differentiate){
                //    case "SEX":  //เพศ
                //        sql = "select p.org_name, case p.sex when '1' then 'ชาย' when '2' then 'หญิง' end differentiate , count(p.id) person_qty " + Environment.NewLine;
                //        sql += " from vw_cmn_person p " + Environment.NewLine;
                //        sql += " where per_status='1' " + Environment.NewLine;
                //        sql += " and p.org_name is not null " + Environment.NewLine;
                //        sql += " and p.per_type = @_PER_TYPE " + Environment.NewLine;
                //        sql += " and p.org_serial=@_ORG_SERIAL " + Environment.NewLine;
                //        sql += " group by p.org_name, case p.sex when '1' then 'ชาย' when '2' then 'หญิง' end " + Environment.NewLine;
                //        sql += " order by p.org_name";
                //        break;

                //    case "DEGREE": //ระดับการศึกษา
                //        sql = " select po.org_serial,o.org_name, edl.educ_lev_name differentiate , count(p.id) person_qty " + Environment.NewLine;
                //        sql += " from psst_person p " + Environment.NewLine;
                //        sql += " inner join psst_position po on p.id=po.id " + Environment.NewLine;
                //        sql += " inner join ctlt_organize o on po.org_serial=o.org_serial " + Environment.NewLine;
                //        sql += " inner join psst_educ_code edu on edu.educ_code=p.educ_code " + Environment.NewLine;
                //        sql += " inner join psst_educ_lev  edl on edl.educ_lev=edu.educ_lev " + Environment.NewLine;
                //        sql += " where per_status='1' " + Environment.NewLine;
                //        sql += " and p.per_type = @_PER_TYPE " + Environment.NewLine;
                //        sql += " and o.org_serial=@_ORG_SERIAL " + Environment.NewLine;
                //        sql += " group by  po.org_serial,o.org_name, edl.educ_lev_name " + Environment.NewLine;
                //        sql += " order by o.org_name";
                //        break;

                //    case "LINE":  //ตำแหน่งในสายงาน
                //        sql = "select po.org_serial,o.org_name, lc.line_name differentiate , count(p.id) person_qty " + Environment.NewLine;
                //        sql += " from psst_person p " + Environment.NewLine;
                //        sql += " inner join psst_position po on p.id=po.id " + Environment.NewLine;
                //        sql += " inner join ctlt_organize o on po.org_serial=o.org_serial " + Environment.NewLine;
                //        sql += " inner join psst_line_code lc on lc.line_code=po.line_code and po.per_type=lc.per_type " + Environment.NewLine;
                //        sql += " where per_status='1' " + Environment.NewLine;
                //        sql += " and p.per_type = @_PER_TYPE " + Environment.NewLine;
                //        sql += " and o.org_serial=@_ORG_SERIAL " + Environment.NewLine;
                //        sql += " group by  po.org_serial,o.org_name, lc.line_name " + Environment.NewLine;
                //        sql += " order by o.org_name";
                //        break;

                //    case "ORG":   //หน่วยงาน
                //        sql = "select po.org_serial,o.org_name, o.org_name differentiate , count(p.id) person_qty " + Environment.NewLine;
                //        sql += " from psst_person p " + Environment.NewLine;
                //        sql += " inner join psst_position po on p.id=po.id " + Environment.NewLine;
                //        sql += " inner join ctlt_organize o on po.org_serial=o.org_serial " + Environment.NewLine;
                //        sql += " where per_status='1' " + Environment.NewLine;
                //        sql += " and p.per_type = @_PER_TYPE " + Environment.NewLine;
                //        sql += " and o.org_serial=@_ORG_SERIAL " + Environment.NewLine;
                //        sql += " group by  po.org_serial,o.org_name " + Environment.NewLine;
                //        sql += " order by o.org_name";

                //        break;
                //    case "LEVEL":   //ระดับตำแหน่ง
                //        sql = "select po.org_serial,o.org_name, l.level_name differentiate , count(p.id) person_qty " + Environment.NewLine;
                //        sql += " from psst_person p " + Environment.NewLine;
                //        sql += " inner join psst_position po on p.id=po.id " + Environment.NewLine;
                //        sql += " inner join ctlt_organize o on po.org_serial=o.org_serial " + Environment.NewLine;
                //        sql += " inner join psst_per_level l on l.cur_lev=po.cur_lev and l.per_type=po.per_type " + Environment.NewLine;
                //        sql += " where per_status='1' " + Environment.NewLine;
                //        sql += " and p.per_type = @_PER_TYPE " + Environment.NewLine;
                //        sql += " and o.org_serial=@_ORG_SERIAL " + Environment.NewLine;
                //        sql += " group by  po.org_serial,o.org_name,l.level_name " + Environment.NewLine;
                //        sql += " order by o.org_name";

                //        break;
                //}


                ////กำหนดค่าให้ SqlParameter
                //SqlParameter[] param = new SqlParameter[2];
                //param[0] = OPM_BL.setParameter("@_PER_TYPE", SqlDbType.VarChar, TypeName);
                //param[1] = OPM_BL.setParameter("@_ORG_SERIAL", SqlDbType.BigInt, Convert.ToInt64(ORG));
                #endregion


                reportname = reportname + "_" + FormatType;
                //DataTable dt = OPM_BL.GetDatatable(sql, param);
                DataTable dt = ReportsEngine.WebReportsENG.GetQueryReportHRM_UT0614(Request);

                if (dt.Rows.Count > 0)
                {
                    string enDate = DateTime.Now.ToString("yyyyMMdd", new System.Globalization.CultureInfo("en-US"));
                    string thDate = OPM_BL.GetTHDate(enDate);
                    if (vReportFormat == "EXCEL")
                    {
                        lblErrorMessage.Text = "";
                        ExcelReportENG.ExportReportRPT_HRM_UT0614(PerTypeName, ORG, differentiate_name, thDate, UserName, UserOrg, dt, Response, FormatType);
                    }
                    else
                    {

                        if (FormatType == "TABLE")
                        {
                            rpt.Load(Server.MapPath(home_floder + "/" + reportname + ".rpt"));
                            rpt.DataDefinition.FormulaFields["thDate"].Text = "'" + thDate + "'";
                            rpt.DataDefinition.FormulaFields["differentiate_name"].Text = "'" + differentiate_name + "'";
                            rpt.DataDefinition.FormulaFields["PerTypeName"].Text = "'" + PerTypeName + "'";
                            rpt.DataDefinition.FormulaFields["UserName"].Text = "'" + UserName + "'";
                            rpt.DataDefinition.FormulaFields["UserOrg"].Text = "'" + UserOrg + "'";
                            rpt.DataDefinition.FormulaFields["PrintDate"].Text = "'" + printDate + "'";
                            rpt.SetDataSource(dt);
                            lblErrorMessage.Text = "";
                            CreatePDFReport(rpt, reportname);
                        }
                        else if (FormatType == "BAR_GRAPH")
                        {
                            lblErrorMessage.Text = "Report not found.";
                            CrystalReportViewer1.Visible = false;
                        }
                        else if (FormatType == "LINE_GRAPH")
                        {
                            lblErrorMessage.Text = "Report not found.";
                            CrystalReportViewer1.Visible = false;
                        }

                    }
                }
                else
                {
                    lblErrorMessage.Text = "ไม่พบข้อมูล";
                    CrystalReportViewer1.Visible = false;
                }
                dt.Dispose();

                #endregion
            }
            else if (reportname == "RPT_HRM_UT0615")
            {
                #region "RPT_HRM_UT0615"

                string per_type = Request.QueryString["per_type"];
                string per_type_name = "";
                per_type_name += Request.QueryString["per_type_name"];
                string org_serial = Request.QueryString["org_serial"];
                string cmd_budg = Request.QueryString["cmd_budg"];
                //string UserName = Request.QueryString["UserName"];
                //string UserOrg = Request.QueryString["UserOrg"];
                string DateFrom = Request.QueryString["DateFrom"];
                string DateTo = Request.QueryString["DateTo"];
                string strTitle = " ";
                strTitle += Request.QueryString["strTitle"];
                string sub_Title = " ";
                sub_Title += Request.QueryString["subTitle"];

                //DateFrom = "20160411";
                //DateTo = "20160411";
                //per_type = "11";
                //cmd_budg = "2558";

                //string sql = " select distinct po.org_serial,ogn.org_name,p.per_type,pt.per_type_name, " + Environment.NewLine;
                ////ยอดยกมา
                //sql += " 	(select count(pp.id) " + Environment.NewLine;
                //sql += "     from PSST_PERSON pp " + Environment.NewLine;
                //sql += "     inner join PSST_POSITION ppo on ppo.id=pp.id " + Environment.NewLine;
                //sql += "     inner join PSST_MOVEMENT_CODE mc on mc.movement_code=pp.movement_code " + Environment.NewLine;
                //sql += "     where ppo.org_serial=po.org_serial " + Environment.NewLine;
                //sql += "     and pp.per_type=p.per_type " + Environment.NewLine;
                //sql += "     and convert(varchar(8),pp.enter_date,112) < @_DATE_FROM " + Environment.NewLine;
                //sql += " 	and pp.per_status='1' " + Environment.NewLine;
                //sql += "     ) balance_emp, " + Environment.NewLine;
                ////บรรจุ
                //sql += "     (select count(pp.id) " + Environment.NewLine;
                //sql += "     from PSST_PERSON pp " + Environment.NewLine;
                //sql += "     inner join PSST_POSITION ppo on ppo.id=pp.id " + Environment.NewLine;
                //sql += "     inner join PSST_MOVEMENT_CODE mc on mc.movement_code=pp.movement_code " + Environment.NewLine;
                //sql += "     where ppo.org_serial=po.org_serial " + Environment.NewLine;
                //sql += "     and mc.movement_flag='11' " + Environment.NewLine;
                //sql += "     and pp.per_type=p.per_type " + Environment.NewLine;
                //sql += "     and convert(varchar(8),pp.enter_date,112) between @_DATE_FROM and @_DATE_TO " + Environment.NewLine;
                //sql += "     ) new_emp, " + Environment.NewLine;
                ////ลาออก
                //sql += "     (select count(pp.id) " + Environment.NewLine;
                //sql += "     from PSST_PERSON pp " + Environment.NewLine;
                //sql += "     inner join PSST_POSITION ppo on ppo.id=pp.id " + Environment.NewLine;
                //sql += "     inner join PSST_MOVEMENT_CODE mc on mc.movement_code=pp.movement_code " + Environment.NewLine;
                //sql += "     where ppo.org_serial=po.org_serial " + Environment.NewLine;
                //sql += "     and mc.movement_flag = '22' " + Environment.NewLine;
                //sql += "     and pp.per_type=p.per_type " + Environment.NewLine;
                //sql += "     and convert(varchar(8),pp.quit_date,112) between @_DATE_FROM and @_DATE_TO " + Environment.NewLine;
                //sql += "     ) resign_emp " + Environment.NewLine;
                //sql += " from PSST_PERSON p " + Environment.NewLine;
                //sql += " inner join PSST_POSITION po on po.id=p.id " + Environment.NewLine;
                //sql += " inner join CTLT_ORGANIZE ogn on ogn.org_serial=po.org_serial " + Environment.NewLine;
                //sql += " left join psst_per_type pt on p.per_type=pt.per_type  " + Environment.NewLine;
                //sql += " where 1=1 " + Environment.NewLine;
                ////sql += " and p.per_type=@_PER_TYPE " + Environment.NewLine;
                ////sql += " and po.org_serial=@_ORG_SERIAL " + Environment.NewLine;
                string sql = "";
                if (string.IsNullOrEmpty(org_serial) || org_serial == "76")
                {
                    sql = "Select org.ORG_SERIAL_L3 as ORG_SERIAL,org.ORG_SERIAL_L2,org.ORG_SERIAL_L3,org.ORG_NAME_L3,org.ORG_NAME_L3 as ORG_NAME,sum(org.BALANCE_EMP) as BALANCE_EMP,sum(org.NEW_EMP) as NEW_EMP,sum(org.RESIGN_EMP) as RESIGN_EMP,sum(org.BALANCE_EMP+org.NEW_EMP-org.RESIGN_EMP) as TOTAL" + Environment.NewLine;
                    sql += ",org.ORDER_SEQ_L3" + Environment.NewLine;
                    sql += "from (" + Environment.NewLine;
                    sql += "  select vw.ORG_SERIAL,vw.ORG_SERIAL_L2,vw.ORG_SERIAL_L3,vw.ORG_NAME_L3,vw.org_name_indent as ORG_NAME,org_level," + Environment.NewLine;
                    sql += "    (SELECT count(*) FROM PSST_HIS_POSITION hp" + Environment.NewLine;
                    sql += "      inner JOIN (SELECT id, MAX(EFFECT_DATE) as EFFECT_DATE, MAX(hpin.LAST_DATE) as LAST_DATE" + Environment.NewLine;
                    sql += "      FROM PSST_HIS_POSITION hpin " + Environment.NewLine;
                    sql += "      inner join PSST_MOVEMENT_CODE mv on hpin.MOVEMENT_CODE=mv.MOVEMENT_CODE" + Environment.NewLine;
                    sql += "      Where mv.movement_flag not in ('20','21','22','23','24') and mv.MOVEMENT_TYPE='1' and mv.MOVEMENT_CMD='A' " + Environment.NewLine;
                    sql += "        and convert(varchar(8),hpin.EFFECT_DATE,112)<@_START_DATE" + Environment.NewLine;
                    if (!string.IsNullOrEmpty(per_type))
                    {
                        sql += "        and hpin.PER_TYPE=@_PER_TYPE   -- ถ้าระบุ PER_TYPE" + Environment.NewLine;
                    }
                    sql += "        and hpin.ORG_SERIAL=vw.ORG_SERIAL" + Environment.NewLine;
                    sql += "      GROUP BY id) hpmax ON hp.id=hpmax.id AND hp.EFFECT_DATE=hpmax.EFFECT_DATE and hp.LAST_DATE=hpmax.LAST_DATE" + Environment.NewLine;
                    sql += "	  ) as BALANCE_EMP," + Environment.NewLine;
                    sql += "    (SELECT count(*) FROM PSST_HIS_POSITION hp" + Environment.NewLine;
                    sql += "      inner JOIN (SELECT id, MAX(EFFECT_DATE) as EFFECT_DATE" + Environment.NewLine;
                    sql += "      FROM PSST_HIS_POSITION hpin " + Environment.NewLine;
                    sql += "           inner join PSST_MOVEMENT_CODE mv on hpin.MOVEMENT_CODE=mv.MOVEMENT_CODE" + Environment.NewLine;
                    sql += "	  WHERE convert(varchar(8),hpin.EFFECT_DATE,112) between @_START_DATE and @_END_DATE" + Environment.NewLine;
                    if (!string.IsNullOrEmpty(per_type))
                    {
                        sql += "         and hpin.PER_TYPE=@_PER_TYPE   -- ถ้าระบุ PER_TYPE" + Environment.NewLine;
                    }
                    sql += "         and hpin.ORG_SERIAL=vw.ORG_SERIAL" + Environment.NewLine;
                    sql += "         and mv.movement_flag in ('10','11','12') and mv.MOVEMENT_TYPE='1' and mv.MOVEMENT_CMD='A' " + Environment.NewLine;
                    sql += "         GROUP BY id) hpmax ON hp.id=hpmax.id AND hp.EFFECT_DATE=hpmax.EFFECT_DATE) as NEW_EMP," + Environment.NewLine;
                    sql += "    (SELECT count(*) FROM PSST_HIS_POSITION hp" + Environment.NewLine;
                    sql += "      inner JOIN (SELECT id, MAX(EFFECT_DATE) as EFFECT_DATE" + Environment.NewLine;
                    sql += "      FROM PSST_HIS_POSITION hpin " + Environment.NewLine;
                    sql += "           inner join PSST_MOVEMENT_CODE mv on hpin.MOVEMENT_CODE=mv.MOVEMENT_CODE" + Environment.NewLine;
                    sql += "      WHERE convert(varchar(8),hpin.EFFECT_DATE,112)between @_START_DATE and @_END_DATE" + Environment.NewLine;
                    if (per_type != null)
                    {
                        sql += "	    and hpin.PER_TYPE=@_PER_TYPE   -- ถ้าระบุ PER_TYPE" + Environment.NewLine;
                    }
                    sql += "	    and hpin.ORG_SERIAL=vw.ORG_SERIAL" + Environment.NewLine;
                    sql += "        and mv.movement_flag in ('20','21','22','23','24') and mv.MOVEMENT_TYPE='1' and mv.MOVEMENT_CMD='A' " + Environment.NewLine;
                    sql += "      GROUP BY id) hpmax ON hp.id=hpmax.id AND hp.EFFECT_DATE=hpmax.EFFECT_DATE) as RESIGN_EMP" + Environment.NewLine;
                    sql += "    , vw.ORDER_SEQ, vw.ORDER_SEQ_L3" + Environment.NewLine;
                    sql += "    from vw_smm_org  vw" + Environment.NewLine;
                    sql += "	Where ORG_SERIAL_L3 IS NOT NULL" + Environment.NewLine;
                    sql += "  ) org" + Environment.NewLine;
                    sql += "  group by org.ORG_SERIAL_L2,org.ORG_SERIAL_L3,org.ORG_NAME_L3,org.ORDER_SEQ_L3" + Environment.NewLine;
                    sql += "order by CASE WHEN org.ORDER_SEQ_L3 IS NULL THEN 1 ELSE 0 END, org.ORDER_SEQ_L3" + Environment.NewLine;

                }
                else
                {

                    sql = "select *,(BALANCE_EMP+NEW_EMP-RESIGN_EMP) as TOTAL" + Environment.NewLine;
                    sql += "from (" + Environment.NewLine;
                    sql += "  select vw.ORG_SERIAL,vw.ORG_SERIAL_L2,vw.ORG_SERIAL_L3,vw.ORG_NAME_L3,vw.org_name_indent as ORG_NAME,org_level," + Environment.NewLine;
                    sql += "    (SELECT count(*) FROM PSST_HIS_POSITION hp" + Environment.NewLine;
                    sql += "      inner JOIN (SELECT id, MAX(EFFECT_DATE) as EFFECT_DATE, MAX(hpin.LAST_DATE) as LAST_DATE" + Environment.NewLine;
                    sql += "      FROM PSST_HIS_POSITION hpin " + Environment.NewLine;
                    sql += "      inner join PSST_MOVEMENT_CODE mv on hpin.MOVEMENT_CODE=mv.MOVEMENT_CODE" + Environment.NewLine;
                    sql += "      Where mv.movement_flag not in ('20','21','22','23','24') and mv.MOVEMENT_TYPE='1' and mv.MOVEMENT_CMD='A' " + Environment.NewLine;
                    sql += "        and convert(varchar(8),hpin.EFFECT_DATE,112)<@_START_DATE" + Environment.NewLine;
                    if (per_type != null)
                    {
                        sql += "        and hpin.PER_TYPE=@_PER_TYPE   -- ถ้าระบุ PER_TYPE" + Environment.NewLine;
                    }
                    sql += "        and hpin.ORG_SERIAL=vw.ORG_SERIAL" + Environment.NewLine;
                    sql += "      GROUP BY id) hpmax ON hp.id=hpmax.id AND hp.EFFECT_DATE=hpmax.EFFECT_DATE and hp.LAST_DATE=hpmax.LAST_DATE" + Environment.NewLine;
                    sql += "	  ) as BALANCE_EMP," + Environment.NewLine;
                    sql += "    (SELECT count(*) FROM PSST_HIS_POSITION hp" + Environment.NewLine;
                    sql += "      inner JOIN (SELECT id, MAX(EFFECT_DATE) as EFFECT_DATE" + Environment.NewLine;
                    sql += "      FROM PSST_HIS_POSITION hpin " + Environment.NewLine;
                    sql += "           inner join PSST_MOVEMENT_CODE mv on hpin.MOVEMENT_CODE=mv.MOVEMENT_CODE" + Environment.NewLine;
                    sql += "	  WHERE convert(varchar(8),hpin.EFFECT_DATE,112) between @_START_DATE and @_END_DATE" + Environment.NewLine;
                    if (per_type != null)
                    {
                        sql += "         and hpin.PER_TYPE=@_PER_TYPE   -- ถ้าระบุ PER_TYPE" + Environment.NewLine;
                    }
                    sql += "         and hpin.ORG_SERIAL=vw.ORG_SERIAL" + Environment.NewLine;
                    sql += "         and mv.movement_flag in ('10','11','12') and mv.MOVEMENT_TYPE='1' and mv.MOVEMENT_CMD='A' " + Environment.NewLine;
                    sql += "         GROUP BY id) hpmax ON hp.id=hpmax.id AND hp.EFFECT_DATE=hpmax.EFFECT_DATE) as NEW_EMP," + Environment.NewLine;
                    sql += "    (SELECT count(*) FROM PSST_HIS_POSITION hp" + Environment.NewLine;
                    sql += "      inner JOIN (SELECT id, MAX(EFFECT_DATE) as EFFECT_DATE" + Environment.NewLine;
                    sql += "      FROM PSST_HIS_POSITION hpin " + Environment.NewLine;
                    sql += "           inner join PSST_MOVEMENT_CODE mv on hpin.MOVEMENT_CODE=mv.MOVEMENT_CODE" + Environment.NewLine;
                    sql += "      WHERE convert(varchar(8),hpin.EFFECT_DATE,112) between @_START_DATE and @_END_DATE" + Environment.NewLine;
                    if (per_type != null)
                    {
                        sql += "	    and hpin.PER_TYPE=@_PER_TYPE   -- ถ้าระบุ PER_TYPE" + Environment.NewLine;
                    }
                    sql += "	    and hpin.ORG_SERIAL=vw.ORG_SERIAL" + Environment.NewLine;
                    sql += "        and mv.movement_flag in ('20','21','22','23','24') and mv.MOVEMENT_TYPE='1' and mv.MOVEMENT_CMD='A' " + Environment.NewLine;
                    sql += "      GROUP BY id) hpmax ON hp.id=hpmax.id AND hp.EFFECT_DATE=hpmax.EFFECT_DATE) as RESIGN_EMP" + Environment.NewLine;
                    sql += "    , vw.ORDER_SEQ" + Environment.NewLine;
                    sql += "    from vw_smm_org  vw" + Environment.NewLine;
                    sql += "    where (ORG_SERIAL_L2=@_ORG_SERIAL or ORG_SERIAL_L3=@_ORG_SERIAL or ORG_SERIAL_L4=@_ORG_SERIAL or ORG_SERIAL_L5=@_ORG_SERIAL)" + Environment.NewLine;
                    sql += ") org order by CASE WHEN ORDER_SEQ IS NULL THEN 1 ELSE 0 END, ORDER_SEQ" + Environment.NewLine;

                }


                SqlParameter[] param = new SqlParameter[4];
                param[0] = OPM_BL.setParameter("@_START_DATE", SqlDbType.VarChar, DateFrom);
                param[1] = OPM_BL.setParameter("@_END_DATE", SqlDbType.VarChar, DateTo);

                if (per_type != null)
                {
                    param[2] = OPM_BL.setParameter("@_PER_TYPE", SqlDbType.VarChar, per_type);
                }
                if (org_serial != null)
                {
                    param[3] = OPM_BL.setParameter("@_ORG_SERIAL", SqlDbType.VarChar, org_serial);
                }




                DataTable dt = OPM_BL.GetDatatable(sql, param);
                if (dt.Rows.Count > 0)
                {
                    string strDateFrom = OPM_BL.GetTHDate(DateFrom);
                    string strDateTo = OPM_BL.GetTHDate(DateTo);
                    if (vReportFormat == "EXCEL")
                    {
                        lblErrorMessage.Text = "";
                        ExcelReportENG.ExportReportRPT_HRM_UT0615(cmd_budg, strDateFrom, strDateTo, UserName, UserOrg, dt, Response);
                    }
                    else if (vReportFormat == "PDF")
                    {
                        rpt.Load(Server.MapPath(home_floder + "/" + reportname + ".rpt"));
                        rpt.DataDefinition.FormulaFields["DateFrom"].Text = "'" + strDateFrom + "'";
                        rpt.DataDefinition.FormulaFields["DateTo"].Text = "'" + strDateTo + "'";
                        rpt.DataDefinition.FormulaFields["UserName"].Text = "'" + UserName + "'";
                        rpt.DataDefinition.FormulaFields["UserOrg"].Text = "'" + UserOrg + "'";
                        rpt.DataDefinition.FormulaFields["PrintDate"].Text = "'" + printDate + "'";
                        rpt.DataDefinition.FormulaFields["strTitle"].Text = "'" + strTitle + "'";
                        rpt.SetDataSource(dt);
                        lblErrorMessage.Text = "";

                        rpt.SetParameterValue("per_type_name", per_type_name);

                        rpt.SetParameterValue("sub_Title", sub_Title);

                        CreatePDFReport(rpt, reportname);


                    }
                }
                else
                {
                    lblErrorMessage.Text = "ไม่พบข้อมูล";
                    CrystalReportViewer1.Visible = false;
                }
                #endregion
            }
            else if (reportname == "RPT_HRM_UT0616")
            {
                #region "RPT_HRM_UT0616"
                SqlParameter[] param = new SqlParameter[3];
                string per_type = Request.QueryString["per_type"];
                string PerTypeName = Request.QueryString["per_type_name"];
                string org_serial = Request.QueryString["org_serial"];
                string ReportType = Request.QueryString["ReportType"];
                string PosStatus = Request.QueryString["PosStatus"];
                string strTitle = Request.QueryString["strTitle"];
                string ScreenID = Request.QueryString["ScreenID"];
                string org_Head = "";
                org_Head += Request.QueryString["org_Head"];
                string sub_Title = "";
                sub_Title += Request.QueryString["subTitle"];

                //string UserName = Request.QueryString["UserName"];
                //string UserOrg = Request.QueryString["UserOrg"];

                //TypeName = "11";  //11= ข้าราชการ
                //PerTypeName = "ข้าราชการพลเรือน";
                //ORG = "44";
                //PosStatus = "เฉพาะมีคนครอง";   //ค่าเลือกทั้งหมด ให้ส่งค่า null ถ้าเลือกเฉพาะมีคนครอง ให้ส่งค่า "เฉพาะมีคนครอง"
                //ReportType = "POSITION";  //มี POSITION, MANPOWER11,MANPOWER14

                string sql = "";
                switch (ReportType)
                {
                    case "POSITION":  //รายงานตำแหน่งจำแนกตามหน่วยงานใน สปน.
                        #region "รายงานตำแหน่งจำแนกตามหน่วยงานใน สปน."

                        //sql = "select po.org_serial,po.org_name, po.pos_id,po.pos_name, po.per_type_name, " + Environment.NewLine;
                        //sql += " po.pos_status_name, po.per_fullname " + Environment.NewLine;
                        //sql += " from vw_cmn_position po " + Environment.NewLine;
                        //sql += " where 1=1" + Environment.NewLine;

                        sql += "select * from (" + Environment.NewLine;
                        sql += "	select org.ORG_SERIAL_L2,org.ORG_SERIAL_L3  as org_serial,org.ORG_SERIAL_L4,org.ORG_SERIAL_L5,org.ORG_NAME_L3 as org_name,org.ORG_NAME_INDENT," + Environment.NewLine;
                        sql += "	convert(bigint,po.pos_id) pos_id ,ISNULL(ADM.ADMIN_NAME,LINE.LINE_NAME) as pos_name, iif(po.CUR_LEV<='20',lv.LEVEL_NAME,'') as LEVEL_NAME, po.CUR_LEV,pt.per_type, PT.per_type_name," + Environment.NewLine;
                        sql += "	isnull(prefix.prefix_name,'') + isnull(p.name,'') + ' ' + isnull(p.surname,'') per_fullname,po.pos_id as strposid,org.ORDER_SEQ" + Environment.NewLine;
                        sql += "	from PSST_POSITION po" + Environment.NewLine;
                        sql += "	LEFT JOIN PSST_ADMIN_CODE ADM ON po.ADMIN_CODE=ADM.ADMIN_CODE" + Environment.NewLine;
                        sql += "	LEFT JOIN PSST_LINE_CODE LINE ON po.LINE_CODE=LINE.LINE_CODE AND po.PER_TYPE=LINE.PER_TYPE" + Environment.NewLine;
                        sql += "	INNER JOIN SMM_PER_TYPE PT on PT.per_type=po.per_type" + Environment.NewLine;
                        sql += "	LEFT JOIN PSST_PERSON p on p.id=po.id" + Environment.NewLine;
                        sql += "	LEFT JOIN CTLT_PREFIX_CODE PREFIX ON p.PREFIX_CODE=PREFIX.PREFIX_CODE" + Environment.NewLine;
                        sql += "	LEFT JOIN PSST_PER_LEVEL lv ON po.CUR_LEV=lv.CUR_LEV" + Environment.NewLine;
                        sql += "	INNER JOIN vw_SMM_ORG org on po.ORG_SERIAL=org.ORG_SERIAL" + Environment.NewLine;
                        sql += "	where 1=1" + Environment.NewLine;

                        if (org_serial != null)
                        {
                            sql += "	AND (org.ORG_SERIAL_L2=@_ORG_SERIAL or org.ORG_SERIAL_L3=@_ORG_SERIAL or org.ORG_SERIAL_L4=@_ORG_SERIAL or org.ORG_SERIAL_L5=@_ORG_SERIAL) -- ถ้าระบุ @_ORG_SERIAL" + Environment.NewLine;
                            param[0] = OPM_BL.setParameter("@_ORG_SERIAL", SqlDbType.BigInt, Convert.ToInt64(org_serial));
                        }
                        if (per_type != null)
                        {
                            sql += "	AND po.PER_TYPE=@_PER_TYPE  -- ถ้าระบุ @_PER_TYPE" + Environment.NewLine;
                            param[1] = OPM_BL.setParameter("@_PER_TYPE", SqlDbType.VarChar, per_type);
                        }
                        if (PosStatus != null)
                        {
                            if (PosStatus == "มีคนครอง")
                            {
                                sql += "	AND po.id is not NULL -- ถ้าระบุ มีคนครอง" + Environment.NewLine;
                            }
                            else
                            {
                                sql += "	AND po.id is NULL -- ถ้าระบุ ไม่มีคนครอง" + Environment.NewLine;
                            }
                        }

                        sql += ") T where pos_name is not null " + Environment.NewLine;
                        sql += "order by CASE WHEN ORDER_SEQ IS NULL THEN 1 ELSE 0 END, ORDER_SEQ,ORG_NAME_INDENT,per_type, convert(bigint,pos_id)";


                        //กำหนดค่าให้ SqlParameter
                        //if (per_type != null)
                        //{
                        //    sql += " and po.per_type = @_PER_TYPE " + Environment.NewLine;
                        //    param[0] = OPM_BL.setParameter("@_PER_TYPE", SqlDbType.VarChar, per_type);
                        //}
                        //if (org_serial != null && org_serial != "76")
                        //{
                        //    sql += " and po.org_serial=@_ORG_SERIAL " + Environment.NewLine;
                        //    param[1] = OPM_BL.setParameter("@_ORG_SERIAL", SqlDbType.BigInt, Convert.ToInt64(org_serial));
                        //}
                        //if (PosStatus != null)
                        //{
                        //    sql += " and po.pos_status_name = @_POS_STATUS " + Environment.NewLine;
                        //    param[2] = OPM_BL.setParameter("@_POS_STATUS", SqlDbType.VarChar, PosStatus);
                        //}
                        //sql += " order by po.org_name,po.pos_id,po.pos_name";
                        #endregion
                        break;
                    case "MANPOWER11":  //รายงานแสดงอัตรากำลังของ สปน. (ข้าราชการ)
                        #region "รายงานแสดงอัตรากำลังของ สปน. (ข้าราชการ)"

                        if (string.IsNullOrEmpty(org_serial) || org_serial == "76")
                        {

                        }
                        else
                        {
                            sql += " select *,(CUR_LEV_12 +CUR_LEV_13+CUR_LEV_14+CUR_LEV_15+CUR_LEV_16+CUR_LEV_17+CUR_LEV_18+CUR_LEV_19+CUR_LEV_20+CUR_LEV_21+CUR_LEV_22+CUR_LEV_23+CUR_LEV_24 ) as TOTAL from (" + Environment.NewLine;
                            sql += " select vworg.org_serial,vworg.ORG_SERIAL_L2,vworg.ORG_SERIAL_L3,vworg.ORG_SERIAL_L4,vworg.ORG_SERIAL_L5,vworg.ORG_NAME_L3,vworg.ORG_NAME_INDENT,vworg.org_name,vworg.ORDER_SEQ,vworg.ORDER_SEQ_L3," + Environment.NewLine;
                            sql += "    sum(CASE pos.CUR_LEV WHEN '12' THEN 1 ELSE 0 END) as CUR_LEV_12," + Environment.NewLine;
                            sql += "    sum(CASE pos.CUR_LEV WHEN '13' THEN 1 ELSE 0 END) as CUR_LEV_13," + Environment.NewLine;
                            sql += "    sum(CASE pos.CUR_LEV WHEN '14' THEN 1 ELSE 0 END) as CUR_LEV_14," + Environment.NewLine;
                            sql += "    sum(CASE pos.CUR_LEV WHEN '15' THEN 1 ELSE 0 END) as CUR_LEV_15," + Environment.NewLine;
                            sql += "    sum(CASE pos.CUR_LEV WHEN '16' THEN 1 ELSE 0 END) as CUR_LEV_16," + Environment.NewLine;
                            sql += "    sum(CASE pos.CUR_LEV WHEN '17' THEN 1 ELSE 0 END) as CUR_LEV_17," + Environment.NewLine;
                            sql += "    sum(CASE pos.CUR_LEV WHEN '18' THEN 1 ELSE 0 END) as CUR_LEV_18," + Environment.NewLine;
                            sql += "    sum(CASE pos.CUR_LEV WHEN '19' THEN 1 ELSE 0 END) as CUR_LEV_19," + Environment.NewLine;
                            sql += "    sum(CASE pos.CUR_LEV WHEN '20' THEN 1 ELSE 0 END) as CUR_LEV_20," + Environment.NewLine;
                            sql += "    sum(CASE pos.CUR_LEV WHEN '21' THEN 1 ELSE 0 END) as CUR_LEV_21," + Environment.NewLine;
                            sql += "    sum(CASE pos.CUR_LEV WHEN '22' THEN 1 ELSE 0 END) as CUR_LEV_22," + Environment.NewLine;
                            sql += "    sum(CASE pos.CUR_LEV WHEN '23' THEN 1 ELSE 0 END) as CUR_LEV_23," + Environment.NewLine;
                            sql += "    sum(CASE pos.CUR_LEV WHEN '24' THEN 1 ELSE 0 END) as CUR_LEV_24," + Environment.NewLine;
                            sql += "    sum(CASE pos.CUR_LEV WHEN '25' THEN 1 ELSE 0 END) as CUR_LEV_25," + Environment.NewLine;
                            sql += "    sum(CASE pos.CUR_LEV WHEN '26' THEN 1 ELSE 0 END) as CUR_LEV_26," + Environment.NewLine;
                            sql += "    sum(CASE pos.CUR_LEV WHEN '27' THEN 1 ELSE 0 END) as CUR_LEV_27," + Environment.NewLine;
                            sql += "    sum(CASE pos.CUR_LEV WHEN '28' THEN 1 ELSE 0 END) as CUR_LEV_28," + Environment.NewLine;
                            sql += "    sum(CASE pos.CUR_LEV WHEN '29' THEN 1 ELSE 0 END) as CUR_LEV_29," + Environment.NewLine;
                            sql += "    sum(CASE pos.CUR_LEV WHEN '30' THEN 1 ELSE 0 END) as CUR_LEV_30," + Environment.NewLine;
                            sql += "    sum(CASE pos.CUR_LEV WHEN '31' THEN 1 ELSE 0 END) as CUR_LEV_31," + Environment.NewLine;
                            sql += "    sum(CASE pos.CUR_LEV WHEN '32' THEN 1 ELSE 0 END) as CUR_LEV_32" + Environment.NewLine;
                            sql += " from vw_SMM_ORG vworg LEFT JOIN" + Environment.NewLine;
                            sql += "    (" + Environment.NewLine;
                            sql += "       select org.ORG_SERIAL,org.ORG_SERIAL_L2,org.ORG_SERIAL_L3,org.ORG_SERIAL_L4,org.ORG_SERIAL_L5," + Environment.NewLine;
                            sql += "         po.POS_ID,isNULL(po.CUR_LEV,LNG.LENGTH_MAX) as CUR_LEV,po.ID, po.PER_TYPE" + Environment.NewLine;
                            sql += "       from PSST_POSITION po" + Environment.NewLine;
                            sql += "         LEFT JOIN PSST_LENGTH_CODE LNG ON po.LENGTH_CODE=LNG.LENGTH_CODE" + Environment.NewLine;
                            sql += "         INNER JOIN vw_SMM_ORG org on po.ORG_SERIAL=org.ORG_SERIAL" + Environment.NewLine;
                            sql += "       where 1=1 AND not (po.ADMIN_CODE is NULL AND po.LINE_CODE is NULL)" + Environment.NewLine;
                            if (per_type != null)
                            {
                                sql += "      AND po.PER_TYPE=@_PER_TYPE  -- ถ้าระบุ @_PER_TYPE" + Environment.NewLine;
                                param[0] = OPM_BL.setParameter("@_PER_TYPE", SqlDbType.VarChar, per_type);
                            }
                            sql += "    ) pos ON vworg.ORG_SERIAL=pos.ORG_SERIAL" + Environment.NewLine;
                            sql += "    LEFT JOIN PSST_PER_LEVEL lv ON pos.CUR_LEV=lv.CUR_LEV" + Environment.NewLine;
                            sql += "    where 1=1" + Environment.NewLine;
                            if (org_serial != null)
                            {
                                sql += "      AND (vworg.ORG_SERIAL_L2=@_ORG_SERIAL or vworg.ORG_SERIAL_L3=@_ORG_SERIAL or vworg.ORG_SERIAL_L4=@_ORG_SERIAL or vworg.ORG_SERIAL_L5=@_ORG_SERIAL) -- ถ้าระบุ @_ORG_SERIAL" + Environment.NewLine;
                                param[1] = OPM_BL.setParameter("@_ORG_SERIAL", SqlDbType.BigInt, Convert.ToInt64(org_serial));
                            }
                            if (PosStatus != null)
                            {
                                if (PosStatus == "มีคนครอง")
                                {
                                    sql += "	 AND id is not NULL -- ถ้าระบุ มีคนครอง" + Environment.NewLine;
                                }
                                else
                                {
                                    sql += "	 AND id is NULL -- ถ้าระบุ ไม่มีคนครอง" + Environment.NewLine;
                                }
                            }
                            sql += "    group by vworg.ORG_SERIAL,vworg.ORG_SERIAL_L2,vworg.ORG_SERIAL_L3,vworg.ORG_SERIAL_L4,vworg.ORG_SERIAL_L5,vworg.ORG_NAME_L3,vworg.ORG_NAME_INDENT,vworg.ORG_NAME,vworg.ORDER_SEQ,vworg.ORDER_SEQ_L3" + Environment.NewLine;
                            sql += "	) TB ";
                        }
                        //sql += "select * from (" + Environment.NewLine;
                        //sql += "	select org.ORG_SERIAL_L2,org.ORG_SERIAL_L3  as org_serial,org.ORG_SERIAL_L4,org.ORG_SERIAL_L5,org.ORG_NAME_L3 as org_name,org.ORG_NAME_INDENT," + Environment.NewLine;
                        //sql += "	convert(bigint,po.pos_id) pos_id ,ISNULL(ADM.ADMIN_NAME,LINE.LINE_NAME) as pos_name, iif(po.CUR_LEV<='20',lv.LEVEL_NAME,'') as LEVEL_NAME, po.CUR_LEV,pt.per_type, PT.per_type_name," + Environment.NewLine;
                        //sql += "	isnull(prefix.prefix_name,'') + isnull(p.name,'') + ' ' + isnull(p.surname,'') per_fullname,po.pos_id as strposid,org.ORDER_SEQ" + Environment.NewLine;
                        //sql += "	from PSST_POSITION po" + Environment.NewLine;
                        //sql += "	LEFT JOIN PSST_ADMIN_CODE ADM ON po.ADMIN_CODE=ADM.ADMIN_CODE" + Environment.NewLine;
                        //sql += "	LEFT JOIN PSST_LINE_CODE LINE ON po.LINE_CODE=LINE.LINE_CODE AND po.PER_TYPE=LINE.PER_TYPE" + Environment.NewLine;
                        //sql += "	INNER JOIN SMM_PER_TYPE PT on PT.per_type=po.per_type" + Environment.NewLine;
                        //sql += "	LEFT JOIN PSST_PERSON p on p.id=po.id" + Environment.NewLine;
                        //sql += "	LEFT JOIN CTLT_PREFIX_CODE PREFIX ON p.PREFIX_CODE=PREFIX.PREFIX_CODE" + Environment.NewLine;
                        //sql += "	LEFT JOIN PSST_PER_LEVEL lv ON po.CUR_LEV=lv.CUR_LEV" + Environment.NewLine;
                        //sql += "	INNER JOIN vw_SMM_ORG org on po.ORG_SERIAL=org.ORG_SERIAL" + Environment.NewLine;
                        //sql += "	where 1=1" + Environment.NewLine;
                        ////sql = "select distinct po.org_serial, isnull(isnull(po.org_abbr,po.org_name),'') org_name, ";//+ ' '+ isnull(isnull(o.abbr_level4,o.name_level4),'') org_name, " + Environment.NewLine;
                        ////sql += " 	(select count(pp.id) " + Environment.NewLine;
                        ////sql += " 	from vw_cmn_position pp " + Environment.NewLine;
                        ////sql += " 	where pp.org_serial=o.org_serial " + Environment.NewLine;
                        ////if (PosStatus != null)
                        ////    sql += "    and pp.pos_status_name=@_POS_STATUS" + Environment.NewLine;

                        ////sql += " 	and pp.level_group_name='ประเภททั่วไป' and pp.level_type_name='ระดับปฏิบัติงาน') gen_work, " + Environment.NewLine;
                        ////sql += " 	(select count(pp.id) " + Environment.NewLine;
                        ////sql += " 	from vw_cmn_position pp " + Environment.NewLine;
                        ////sql += " 	where pp.org_serial=o.org_serial " + Environment.NewLine;
                        ////if (PosStatus != null)
                        ////    sql += "    and pp.pos_status_name=@_POS_STATUS" + Environment.NewLine;

                        ////sql += " 	and pp.level_group_name='ประเภททั่วไป' and pp.level_type_name='ระดับชำนาญงาน') gen_expert, " + Environment.NewLine;
                        ////sql += " 	(select count(pp.id) " + Environment.NewLine;
                        ////sql += " 	from vw_cmn_position pp " + Environment.NewLine;
                        ////sql += " 	where pp.org_serial=o.org_serial " + Environment.NewLine;
                        ////if (PosStatus != null)
                        ////    sql += "    and pp.pos_status_name=@_POS_STATUS" + Environment.NewLine;

                        ////sql += " 	and pp.level_group_name='ประเภททั่วไป' and pp.level_type_name='ระดับอาวุโส') gen_senior, " + Environment.NewLine;
                        ////sql += " 	(select count(pp.id) " + Environment.NewLine;
                        ////sql += " 	from vw_cmn_position pp " + Environment.NewLine;
                        ////sql += " 	where pp.org_serial=o.org_serial " + Environment.NewLine;

                        //////ระดับทักษะพิเศษ
                        ////if (PosStatus != null)
                        ////    sql += "    and pp.pos_status_name=@_POS_STATUS" + Environment.NewLine;

                        ////sql += " 	and pp.level_group_name='ประเภททั่วไป' and pp.level_type_name='ระดับทักษะพิเศษ') gen_spacial, " + Environment.NewLine;
                        ////sql += " 	(select count(pp.id) " + Environment.NewLine;
                        ////sql += " 	from vw_cmn_position pp " + Environment.NewLine;
                        ////sql += " 	where pp.org_serial=o.org_serial " + Environment.NewLine;

                        ////if (PosStatus != null)
                        ////    sql += "    and pp.pos_status_name=@_POS_STATUS" + Environment.NewLine;

                        ////sql += " 	and pp.level_group_name='ประเภทวิชาการ' and pp.level_type_name='ระดับปฏิบัติการ') academic_work, " + Environment.NewLine;
                        ////sql += " 	(select count(pp.id) " + Environment.NewLine;
                        ////sql += " 	from vw_cmn_position pp " + Environment.NewLine;
                        ////sql += " 	where pp.org_serial=o.org_serial " + Environment.NewLine;
                        ////if (PosStatus != null)
                        ////    sql += "    and pp.pos_status_name=@_POS_STATUS" + Environment.NewLine;

                        ////sql += " 	and pp.level_group_name='ประเภทวิชาการ' and pp.level_type_name='ระดับชำนาญการ') academic_expert, " + Environment.NewLine;
                        ////sql += " 	(select count(pp.id) " + Environment.NewLine;
                        ////sql += " 	from vw_cmn_position pp " + Environment.NewLine;
                        ////sql += " 	where pp.org_serial=o.org_serial " + Environment.NewLine;
                        ////if (PosStatus != null)
                        ////    sql += "    and pp.pos_status_name=@_POS_STATUS" + Environment.NewLine;

                        ////sql += " 	and pp.level_group_name='ประเภทวิชาการ' and pp.level_type_name='ระดับชำนาญการพิเศษ') academic_specialist, " + Environment.NewLine;
                        ////sql += " 	(select count(pp.id) " + Environment.NewLine;
                        ////sql += " 	from vw_cmn_position pp " + Environment.NewLine;
                        ////sql += " 	where pp.org_serial=o.org_serial " + Environment.NewLine;
                        ////if (PosStatus != null)
                        ////    sql += "    and pp.pos_status_name=@_POS_STATUS" + Environment.NewLine;

                        ////sql += " 	and pp.level_group_name='ประเภทวิชาการ' and pp.level_type_name='ระดับเชี่ยวชาญ') academic_professional, " + Environment.NewLine;
                        ////sql += " 	(select count(pp.id) " + Environment.NewLine;
                        ////sql += " 	from vw_cmn_position pp " + Environment.NewLine;
                        ////sql += " 	where pp.org_serial=o.org_serial " + Environment.NewLine;

                        //////ระดับทรงคุณวุฒิ
                        ////if (PosStatus != null)
                        ////    sql += "    and pp.pos_status_name=@_POS_STATUS" + Environment.NewLine;

                        ////sql += " 	and pp.level_group_name='ประเภทวิชาการ' and pp.level_type_name='ระดับทรงคุณวุฒิ') academic_theexpert, " + Environment.NewLine;
                        ////sql += " 	(select count(pp.id) " + Environment.NewLine;
                        ////sql += " 	from vw_cmn_position pp " + Environment.NewLine;
                        ////sql += " 	where pp.org_serial=o.org_serial " + Environment.NewLine;

                        ////if (PosStatus != null)
                        ////    sql += "    and pp.pos_status_name=@_POS_STATUS" + Environment.NewLine;

                        ////sql += " 	and pp.level_group_name='ประเภทอำนวยการ' and pp.level_type_name='ระดับต้น') director_begin, " + Environment.NewLine;
                        ////sql += " 	(select count(pp.id) " + Environment.NewLine;
                        ////sql += " 	from vw_cmn_position pp " + Environment.NewLine;
                        ////sql += " 	where pp.org_serial=o.org_serial " + Environment.NewLine;
                        ////if (PosStatus != null)
                        ////    sql += "    and pp.pos_status_name=@_POS_STATUS" + Environment.NewLine;

                        ////sql += " 	and pp.level_group_name='ประเภทอำนวยการ' and pp.level_type_name='ระดับสูง') director_high, " + Environment.NewLine;
                        ////sql += " 	(select count(pp.id) " + Environment.NewLine;
                        ////sql += " 	from vw_cmn_position pp " + Environment.NewLine;
                        ////sql += " 	where pp.org_serial=o.org_serial " + Environment.NewLine;
                        ////if (PosStatus != null)
                        ////    sql += "    and pp.pos_status_name=@_POS_STATUS" + Environment.NewLine;

                        ////sql += " 	and pp.level_group_name='ประเภทบริหาร' and pp.level_type_name='ระดับต้น') manage_begin, " + Environment.NewLine;
                        ////sql += " 	(select count(pp.id) " + Environment.NewLine;
                        ////sql += " 	from vw_cmn_position pp " + Environment.NewLine;
                        ////sql += " 	where pp.org_serial=o.org_serial " + Environment.NewLine;
                        ////if (PosStatus != null)
                        ////    sql += "    and pp.pos_status_name=@_POS_STATUS" + Environment.NewLine;

                        ////sql += " 	and pp.level_group_name='ประเภทบริหาร' and pp.level_type_name='ระดับสูง') manage_high " + Environment.NewLine;
                        ////sql += " from vw_cmn_position po " + Environment.NewLine;
                        ////sql += " inner join ctlt_organize o on po.org_serial=o.org_serial " + Environment.NewLine;
                        ////sql += " where po.org_serial is not null " + Environment.NewLine;
                        //////sql += " and po.per_type = @_PER_TYPE " + Environment.NewLine;
                        //////sql += " and po.org_serial=@_ORG_SERIAL " + Environment.NewLine;

                        //////กำหนดค่าให้ SqlParameter
                        ////if (per_type != null)
                        ////{
                        ////    sql += " and po.per_type = @_PER_TYPE " + Environment.NewLine;
                        ////    param[0] = OPM_BL.setParameter("@_PER_TYPE", SqlDbType.VarChar, per_type);
                        ////}
                        ////if (org_serial != null && org_serial != "76")
                        ////{
                        ////    sql += " and po.org_serial=@_ORG_SERIAL " + Environment.NewLine;
                        ////    param[1] = OPM_BL.setParameter("@_ORG_SERIAL", SqlDbType.BigInt, Convert.ToInt64(org_serial));
                        ////}
                        ////if (PosStatus != null)
                        ////{
                        ////    param[2] = OPM_BL.setParameter("@_POS_STATUS", SqlDbType.VarChar, PosStatus);
                        ////}
                        ////sql += " or po.org_serial in (select org_serial from ctlt_organize where upper_org_serial=@_ORG_SERIAL)";
                        ////sql += " order by  isnull(isnull(po.org_abbr,po.org_name),'') ";
                        #endregion
                        break;
                    case "MANPOWER14":  //รายงานแสดงอัตรากำลังของ สปน. (พนักงานราชการ)
                        #region "รายงานแสดงอัตรากำลังของ สปน. (พนักงานราชการ)"
                        sql = "select distinct po.org_serial, isnull(o.name_level3,'') + ' '+ isnull(o.name_level4,'') org_name, " + Environment.NewLine;
                        sql += " 	(select count(pp.pos_id) " + Environment.NewLine;
                        sql += " 	from psst_position pp " + Environment.NewLine;
                        sql += " 	inner join psst_per_level pl on pp.cur_lev=pl.cur_lev and pp.per_type=pl.per_type " + Environment.NewLine;
                        sql += " 	inner join psst_pos_status ps on ps.pos_status=pp.pos_status " + Environment.NewLine;
                        sql += " 	where pp.org_serial=o.org_serial " + Environment.NewLine;
                        if (PosStatus != null)
                            sql += " 		and ps.pos_status_name=@_POS_STATUS " + Environment.NewLine;

                        sql += " 	and pl.level_name='กลุ่มงานบริการ') group_service, " + Environment.NewLine;
                        sql += " 	(select count(pp.pos_id) " + Environment.NewLine;
                        sql += " 	from psst_position pp " + Environment.NewLine;
                        sql += " 	inner join psst_per_level pl on pp.cur_lev=pl.cur_lev and pp.per_type=pl.per_type " + Environment.NewLine;
                        sql += " 	inner join psst_pos_status ps on ps.pos_status=pp.pos_status " + Environment.NewLine;
                        sql += " 	where pp.org_serial=o.org_serial " + Environment.NewLine;
                        if (PosStatus != null)
                            sql += " 		and ps.pos_status_name=@_POS_STATUS " + Environment.NewLine;

                        sql += " 	and pl.level_name='กลุ่มงานเทคนิค') group_technique, " + Environment.NewLine;
                        sql += " 	(select count(pp.pos_id) " + Environment.NewLine;
                        sql += " 	from psst_position pp " + Environment.NewLine;
                        sql += " 	inner join psst_per_level pl on pp.cur_lev=pl.cur_lev and pp.per_type=pl.per_type " + Environment.NewLine;
                        sql += " 	inner join psst_pos_status ps on ps.pos_status=pp.pos_status " + Environment.NewLine;
                        sql += " 	where pp.org_serial=o.org_serial " + Environment.NewLine;
                        if (PosStatus != null)
                            sql += " 		and ps.pos_status_name=@_POS_STATUS " + Environment.NewLine;
                        sql += " 	and pl.level_name='กลุ่มงานบริหารทั่วไป') group_manage, " + Environment.NewLine;
                        sql += " 	(select count(pp.pos_id) " + Environment.NewLine;
                        sql += " 	from psst_position pp " + Environment.NewLine;
                        sql += " 	inner join psst_per_level pl on pp.cur_lev=pl.cur_lev and pp.per_type=pl.per_type " + Environment.NewLine;
                        sql += " 	inner join psst_pos_status ps on ps.pos_status=pp.pos_status " + Environment.NewLine;
                        sql += " 	where pp.org_serial=o.org_serial " + Environment.NewLine;
                        if (PosStatus != null)
                            sql += " 		and ps.pos_status_name=@_POS_STATUS " + Environment.NewLine;
                        sql += " 	and pl.level_name='กลุ่มงานวิชาชีพเฉพาะ') group_professional, " + Environment.NewLine;
                        sql += " 	(select count(pp.pos_id) " + Environment.NewLine;
                        sql += " 	from psst_position pp " + Environment.NewLine;
                        sql += " 	inner join psst_per_level pl on pp.cur_lev=pl.cur_lev and pp.per_type=pl.per_type " + Environment.NewLine;
                        sql += " 	inner join psst_pos_status ps on ps.pos_status=pp.pos_status " + Environment.NewLine;
                        sql += " 	where pp.org_serial=o.org_serial " + Environment.NewLine;
                        if (PosStatus != null)
                            sql += " 		and ps.pos_status_name=@_POS_STATUS " + Environment.NewLine;
                        sql += " 	and pl.level_name='กลุ่มงานเชี่ยวชาญเฉพาะ') group_skill, " + Environment.NewLine;
                        sql += " 	(select count(pp.pos_id) " + Environment.NewLine;
                        sql += " 	from psst_position pp " + Environment.NewLine;
                        sql += " 	inner join psst_per_level pl on pp.cur_lev=pl.cur_lev and pp.per_type=pl.per_type " + Environment.NewLine;
                        sql += " 	inner join psst_pos_status ps on ps.pos_status=pp.pos_status " + Environment.NewLine;
                        sql += " 	where pp.org_serial=o.org_serial " + Environment.NewLine;
                        if (PosStatus != null)
                            sql += " 		and ps.pos_status_name=@_POS_STATUS " + Environment.NewLine;
                        sql += " 	and pl.cur_lev in ('30','31','32')) group_spacial " + Environment.NewLine;
                        sql += " from psst_position po " + Environment.NewLine;
                        sql += " inner join ctlt_organize o on po.org_serial=o.org_serial " + Environment.NewLine;
                        sql += " where po.org_serial is not null " + Environment.NewLine;
                        //sql += " and po.per_type = @_PER_TYPE " + Environment.NewLine;
                        //sql += " and po.org_serial=@_ORG_SERIAL " + Environment.NewLine;

                        //กำหนดค่าให้ SqlParameter
                        if (per_type != null)
                        {
                            sql += " and po.per_type = @_PER_TYPE " + Environment.NewLine;
                            param[0] = OPM_BL.setParameter("@_PER_TYPE", SqlDbType.VarChar, per_type);
                        }
                        if (org_serial != null && org_serial != "76")
                        {
                            sql += " and po.org_serial=@_ORG_SERIAL " + Environment.NewLine;
                            param[1] = OPM_BL.setParameter("@_ORG_SERIAL", SqlDbType.BigInt, Convert.ToInt64(org_serial));
                        }
                        if (PosStatus != null)
                        {
                            param[2] = OPM_BL.setParameter("@_POS_STATUS", SqlDbType.VarChar, PosStatus);
                        }

                        sql += " order by isnull(o.name_level3,'') + ' '+ isnull(o.name_level4,'')";


                        #endregion
                        break;
                }

                reportname = reportname + "_" + ReportType;
                string ReportHeader = "รายงานตารางแสดงอัตรากำลังของ สปน. ตามกรอบอัตรากำลัง";
                string ReportHeaderPosition = "ตามกรอบอัตรากำลัง";
                if ((PosStatus != null) && PosStatus != "")
                {
                    ReportHeader = "รายงานตารางแสดงอัตรากำลังของ สปน. ตามอัตรากำลังที่มีคนครอง";
                    ReportHeaderPosition = "ตามกรอบอัตรากำลังที่มีคนครอง";
                }

                DataTable dt = OPM_BL.GetDatatable(sql, param);
                if (dt.Rows.Count > 0)
                {
                    if (vReportFormat == "EXCEL")
                    {
                        lblErrorMessage.Text = "";
                        switch (ReportType)
                        {
                            case "POSITION":
                                ExcelReportENG.ExportReportRPT_HRM_UT0616_POSITION(UserName, UserOrg, dt, Response);
                                break;
                            case "MANPOWER11":
                                ExcelReportENG.ExportReportRPT_HRM_UT0616_MANPOWER11(ReportHeader, PerTypeName, UserName, UserOrg, dt, Response);
                                break;
                            case "MANPOWER14":
                                ExcelReportENG.ExportReportRPT_HRM_UT0616_MANPOWER14(ReportHeader, PerTypeName, UserName, UserOrg, dt, Response);
                                break;
                        }
                    }
                    else if (vReportFormat == "PDF")
                    {
                        rpt.Load(Server.MapPath(home_floder + "/" + reportname + ".rpt"));
                        rpt.DataDefinition.FormulaFields["UserName"].Text = "'" + UserName + "'";
                        rpt.DataDefinition.FormulaFields["UserOrg"].Text = "'" + UserOrg + "'";
                        rpt.DataDefinition.FormulaFields["PrintDate"].Text = "'" + printDate + "'";
                        switch (ReportType)
                        {
                            case "POSITION":
                                rpt.DataDefinition.FormulaFields["ReportHeaderPosition"].Text = "'" + ReportHeaderPosition + "'";
                                rpt.DataDefinition.FormulaFields["strTitle"].Text = "'" + strTitle + "'";
                                break;
                            case "MANPOWER11":
                                rpt.DataDefinition.FormulaFields["ReportHeader"].Text = "'" + ReportHeader + "'";
                                rpt.DataDefinition.FormulaFields["PerTypeName"].Text = "'" + PerTypeName + "'";
                                break;
                            case "MANPOWER14":
                                rpt.DataDefinition.FormulaFields["ReportHeader"].Text = "'" + ReportHeader + "'";
                                rpt.DataDefinition.FormulaFields["PerTypeName"].Text = "'" + PerTypeName + "'";
                                break;
                        }

                        rpt.SetDataSource(dt);
                        rpt.SetParameterValue("sub_title", sub_Title);
                        rpt.SetParameterValue("org_Head", org_Head);
                        rpt.SetParameterValue("ScreenID", ScreenID);
                        lblErrorMessage.Text = "";

                        CreatePDFReport(rpt, reportname);
                    }

                }
                else
                {
                    lblErrorMessage.Text = "ไม่พบข้อมูล";
                    CrystalReportViewer1.Visible = false;
                }
                dt.Dispose();

                #endregion
            }
            else if (reportname == "RPT_HRM_UT0617")
            {
                #region "RPT_HRM_UT0617"
                //string TypeName = "11";
                //string ORG = "43";//ส่วนประสานงานติดตามผลและประเมินผล
                //string person_id = 28027;//นายเกียรติกมล จังโส
                //string DateFrom = "";//01 เม.ย.2549- 30 ก.ย. 2549
                //string DateTo = "";
                //string BudgetYear = "2549";
                //string UserName = "99999ชื่อสกุล99999";


                SqlParameter[] param = new SqlParameter[6];
                string TypeName = Request.QueryString["per_type"];
                string LeaveID = Request.QueryString["leave_id"];
                string ORG = Request.QueryString["org_serial"];
                string person_id = Request.QueryString["person_id"];
                string DateFrom = Request.QueryString["DateFrom"];
                string DateTo = Request.QueryString["DateTo"];
                //string UserName = Request.QueryString["UserName"];
                //string UserOrg = Request.QueryString["UserOrg"];
                string remark = Request.QueryString["Remark"];
                string strTitle = Request.QueryString["strTitle"];

                string sql = "select p.id person_id, isnull(p.prefix_name,'') + isnull(p.name,'') + ' ' + isnull(p.surname,'') staff_name, " + Environment.NewLine;
                sql += " p.per_type, o.org_serial, o.org_name, " + Environment.NewLine;
                sql += " l.leave_name, (select [dbo].[ThaiDateAbbrMonth](h.start_date)) start_date,(select [dbo].[ThaiDateAbbrMonth](h.end_date))end_date" + Environment.NewLine;
                sql += " ,h.leave_seq, h.leave_day, isnull(h.leave_reason,h.remarks) leave_reason " + Environment.NewLine;
                sql += " from  vw_CMN_PERSON p  " + Environment.NewLine;
                sql += " inner join CTLT_ORGANIZE o on o.org_serial=p.org_serial " + Environment.NewLine;
                sql += " inner join HRM_LEAVE_HIS h on p.id=h.person_id  " + Environment.NewLine;
                sql += " inner join PDM_MS_LEAVE l on l.leave_id=h.leave_id " + Environment.NewLine;
                sql += " where 1=1  " + Environment.NewLine;
                if (!string.IsNullOrEmpty(TypeName))
                {
                    sql += " and p.PER_TYPE = @_PER_TYPE " + Environment.NewLine;
                    param[0] = OPM_BL.setParameter("@_PER_TYPE", SqlDbType.VarChar, TypeName);
                }
                if (!string.IsNullOrEmpty(ORG))
                {
                    sql += " and o.org_serial= @_ORG_SERIAL " + Environment.NewLine;
                    param[1] = OPM_BL.setParameter("@_ORG_SERIAL", SqlDbType.BigInt, Convert.ToInt64(ORG));
                }
                if (!string.IsNullOrEmpty(person_id))
                {
                    sql += " and p.id=@_PERSON_ID " + Environment.NewLine;
                    param[2] = OPM_BL.setParameter("@_PERSON_ID", SqlDbType.BigInt, Convert.ToInt64(person_id));
                }

                sql += " and convert(varchar(8),h.[start_date],112) between @_DATE_FROM and @_DATE_TO " + Environment.NewLine;
                param[3] = OPM_BL.setParameter("@_DATE_FROM", SqlDbType.VarChar, DateFrom);
                param[4] = OPM_BL.setParameter("@_DATE_TO", SqlDbType.VarChar, DateTo);
                if ((LeaveID != null) && (LeaveID != "0"))
                {
                    sql += " and h.leave_id = @_LEAVE_ID " + Environment.NewLine;
                    param[5] = OPM_BL.setParameter("@_LEAVE_ID", SqlDbType.VarChar, LeaveID);
                }

                //if ((remark != null) && (remark != "0"))
                //{
                //    sql += " isnull(h.leave_reason,h.remarks) = @_REMARK " + Environment.NewLine;
                //    param[5] = OPM_BL.setParameter("@_REMARK", SqlDbType.VarChar, remark);
                //}

                sql += " order by name_level3,name_level4,staff_name";
                DataTable dt = OPM_BL.GetDatatable(sql, param);
                if (dt.Rows.Count > 0)
                {
                    string strDateFrom = OPM_BL.GetTHDate(DateFrom);
                    string strDateTo = OPM_BL.GetTHDate(DateTo);
                    if (vReportFormat == "EXCEL")
                    {
                        lblErrorMessage.Text = "";
                        ExcelReportENG.ExportReportRPT_HRM_UT0617(strDateFrom, strDateTo, UserName, UserOrg, dt, Response);
                    }
                    else if (vReportFormat == "PDF")
                    {
                        rpt.Load(Server.MapPath(home_floder + "/" + reportname + ".rpt"));
                        rpt.DataDefinition.FormulaFields["DateFrom"].Text = "'" + strDateFrom + "'";
                        rpt.DataDefinition.FormulaFields["DateTo"].Text = "'" + strDateTo + "'";
                        rpt.DataDefinition.FormulaFields["UserName"].Text = "'" + UserName + "'";
                        rpt.DataDefinition.FormulaFields["UserOrg"].Text = "'" + UserOrg + "'";
                        rpt.DataDefinition.FormulaFields["PrintDate"].Text = "'" + printDate + "'";
                        rpt.DataDefinition.FormulaFields["strTitle"].Text = "'" + strTitle + "'";
                        rpt.SetDataSource(dt);
                        lblErrorMessage.Text = "";

                        CreatePDFReport(rpt, reportname);
                    }

                }
                else
                {
                    lblErrorMessage.Text = "ไม่พบข้อมูล";
                    CrystalReportViewer1.Visible = false;
                }
                #endregion
            }
            #endregion //__OPM

            #region "__SMM"
            else if (reportname == "SMM_RP01")
            {
                #region "SMM_RP01 OLD"
                //string TypeName = "11";
                //string ORG = "66";
                //string StaffName = "ฐิติกานต์ นิรามัย";
                //string POSName = "";
                //string UserName = "99999ชื่อสกุล99999";
                //string UserOrg = "xxxxxหน่วยงานxxxxx";

                //SqlParameter[] param = new SqlParameter[4];
                //string TypeName = Request.QueryString["TypeName"];
                //string ORG = Request.QueryString["ORG"];
                //string StaffName = Request.QueryString["StaffName"];
                //string POSName = Request.QueryString["POSName"];
                //string UserName = Request.QueryString["UserName"];
                //string UserOrg = Request.QueryString["UserOrg"];

                //param[0] = OPM_BL.setParameter("@_PER_TYPE", SqlDbType.VarChar, TypeName);
                //param[1] = OPM_BL.setParameter("@_ORG_SERIAL", SqlDbType.VarChar, ORG);
                //string sql = " select p.per_type," + Environment.NewLine;
                //sql += " isnull(p.prefix_name,'')+isnull(p.name,'') + ' ' + isnull(p.surname,'') staff_name," + Environment.NewLine;
                //sql += " org.org_serial,org.org_name,org.name_level4," + Environment.NewLine;
                //sql += " lc.line_name line_position_name, ac.admin_name admin_position_name," + Environment.NewLine;
                //sql += " le.level_name, po.pos_id, jd.job_detail" + Environment.NewLine;
                //sql += " from PSST_POSITION po" + Environment.NewLine;
                //sql += " inner join CTLT_ORGANIZE org on po.org_serial=org.org_serial" + Environment.NewLine;
                //sql += " left join PSST_LINE_CODE lc on lc.line_code=po.line_code" + Environment.NewLine;
                //sql += " left join PSST_ADMIN_CODE ac on ac.admin_code=po.admin_code" + Environment.NewLine;
                //sql += " left join vw_CMN_PERSON p on p.id=po.id" + Environment.NewLine;
                //sql += " left join PSST_PER_LEVEL le on le.cur_lev=po.cur_lev" + Environment.NewLine;
                //sql += " left join SMM_JD_JOB_SUMMARY jd on po.pos_id=jd.pos_id" + Environment.NewLine;
                //sql += " where po.per_type=@_PER_TYPE" + Environment.NewLine;
                //if (POSName != null)
                //{
                //    sql += " and (lc.line_name like '%'+@_POS_NAME + '%' or ac.admin_name like '%' + @_POS_NAME + '%')" + Environment.NewLine;
                //    param[2] = OPM_BL.setParameter("@_POS_NAME", SqlDbType.VarChar, POSName);
                //}
                //if (StaffName != null)
                //{
                //    sql += " and (isnull(p.prefix_name,'')+isnull(p.name,'') + ' ' + isnull(p.surname,'') like '%' + @_STAFF_NAME + '%')" + Environment.NewLine;
                //    param[3] = OPM_BL.setParameter("@_STAFF_NAME", SqlDbType.VarChar, StaffName);
                //}

                //sql += " and po.org_serial=@_ORG_SERIAL" + Environment.NewLine;
                //sql += " order by org.name_level4, org.org_serial" + Environment.NewLine;

                //DataTable dt = OPM_BL.GetDatatable(sql, param);
                //if (dt.Rows.Count > 0)
                //{
                //    rpt.Load(Server.MapPath(home_floder + "/" + reportname + ".rpt"));
                //    rpt.DataDefinition.FormulaFields["UserName"].Text = "'" + UserName + "'";
                //    rpt.DataDefinition.FormulaFields["UserOrg"].Text = "'" + UserOrg + "'";
                //    //string printDate = DateTime.Now.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("th-TH"));
                //    rpt.DataDefinition.FormulaFields["PrintDate"].Text = "'" + printDate + "'";

                //    rpt.SetDataSource(dt);
                //    lblErrorMessage.Text = "";
                //    CrystalReportViewer1.Visible = true;
                //}
                //else
                //{
                //    lblErrorMessage.Text = "ไม่พบข้อมูล";
                //    CrystalReportViewer1.Visible = false;
                //}
                #endregion

                #region "SMM_RP01"

                string ORG_SERIAL = Request.QueryString["ORG_SERIAL"];
                string ORG_NAME = Request.QueryString["ORG_NAME"];
                //string UserName = Request.QueryString["UserName"];
                //string UserOrg = Request.QueryString["UserOrg"];
                string PageCode = Request.QueryString["PageCode"];
                string UserID = Request.QueryString["UserID"];
                string Str_Title = "";
                Str_Title += (PageCode == "(SMM_UT0501)") ? Request.QueryString["strTitle"] : "หน่วยงาน " + ORG_NAME;
                string Sub_Title = "";
                Sub_Title += Request.QueryString["subTitle"];
                DataTable uDT = OPM_BL.GetUserNameByID(UserID);

                //ORG_SERIAL = "3";
                //ORG_NAME = "xxxxxxxxxxxx";
                //UserName = "99999ชื่อสกุล99999";
                //UserOrg = "xxxxxหน่วยงานxxxxx";

                //string ORG = Request.QueryString["ORG"];
                //string sql = "declare @_org_serial varchar(10)" + Environment.NewLine;
                //sql +="set @_org_serial ='5'" + Environment.NewLine;
                //sql += " select * from (select c.org_serial,org_name ,upper_org_serial,'3' [level],job_seq,job_detail" + Environment.NewLine;
                //sql += " from ctlt_organize c inner join smm_organize_job j on c.org_serial=j.org_serial " + Environment.NewLine;
                //sql += " where c.org_serial = @_org_serial " + Environment.NewLine;
                //sql += " union " + Environment.NewLine;
                //sql += " select c.org_serial,org_name ,upper_org_serial,'4' [level],job_seq,job_detail " + Environment.NewLine;
                //sql += " from ctlt_organize c inner join smm_organize_job j on c.org_serial=j.org_serial " + Environment.NewLine;
                //sql += " where c.upper_org_serial = @_org_serial " + Environment.NewLine;
                //sql += " union " + Environment.NewLine;
                //sql += " select c.org_serial,org_name ,upper_org_serial,'5' [level],job_seq,job_detail " + Environment.NewLine;
                //sql += " from ctlt_organize c inner join smm_organize_job j on c.org_serial=j.org_serial  " + Environment.NewLine;
                //sql += " where upper_org_serial in (select distinct org_serial from ctlt_organize where upper_org_serial=@_org_serial)" + Environment.NewLine;
                //sql += " ) T order by level,org_name";

                string sql = " select c.org_serial,org_name ,upper_org_serial,'3' [level],job_seq,job_detail" + Environment.NewLine;
                sql += " from ctlt_organize c inner join smm_organize_job j on c.org_serial=j.org_serial " + Environment.NewLine;
                sql += " where c.org_serial = @_org_serial " + Environment.NewLine;
                sql += " order by org_name";
                SqlParameter[] param3 = new SqlParameter[1];
                param3[0] = OPM_BL.setParameter("@_org_serial", SqlDbType.VarChar, ORG_SERIAL);
                DataTable dt_level3 = OPM_BL.GetDatatable(sql, param3);

                sql = " select c.org_serial,org_name ,upper_org_serial,'4' [level],job_seq,job_detail " + Environment.NewLine;
                sql += " from ctlt_organize c inner join smm_organize_job j on c.org_serial=j.org_serial " + Environment.NewLine;
                sql += " where c.upper_org_serial = @_org_serial " + Environment.NewLine;
                sql += " order by org_name";
                SqlParameter[] param4 = new SqlParameter[1];
                param4[0] = OPM_BL.setParameter("@_org_serial", SqlDbType.VarChar, ORG_SERIAL);
                DataTable dt_level4 = OPM_BL.GetDatatable(sql, param4);

                sql = " select c.org_serial,org_name ,upper_org_serial,'5' [level],job_seq,job_detail " + Environment.NewLine;
                sql += " from ctlt_organize c inner join smm_organize_job j on c.org_serial=j.org_serial  " + Environment.NewLine;
                sql += " where upper_org_serial in (select distinct org_serial from ctlt_organize where upper_org_serial=@_org_serial)" + Environment.NewLine;
                sql += " order by org_name";
                SqlParameter[] param5 = new SqlParameter[1];
                param5[0] = OPM_BL.setParameter("@_org_serial", SqlDbType.VarChar, ORG_SERIAL);
                DataTable dt_level5 = OPM_BL.GetDatatable(sql, param5);


                if (dt_level3.Rows.Count > 0)
                {
                    rpt.Load(Server.MapPath(home_floder + "/" + reportname + ".rpt"));
                    rpt.DataDefinition.FormulaFields["UserName"].Text = "'" + UserName + "'";
                    rpt.DataDefinition.FormulaFields["UserOrg"].Text = "'" + UserOrg + "'";
                    rpt.DataDefinition.FormulaFields["org_name"].Text = "'" + ORG_NAME + "'";
                    rpt.DataDefinition.FormulaFields["PrintDate"].Text = "'" + printDate + "'";

                    rpt.Subreports["SMM_RP01_LEVEL3"].SetDataSource(dt_level3);
                    rpt.Subreports["SMM_RP01_LEVEL4"].SetDataSource(dt_level4);
                    rpt.Subreports["SMM_RP01_LEVEL5"].SetDataSource(dt_level5);
                    rpt.SetDataSource(dt_level3);

                    if (vReportFormat == "EXCEL")
                    {
                        lblErrorMessage.Text = "";
                        ExcelReportENG.ExportReportSMM_RP01(ORG_SERIAL, ORG_NAME, UserName, UserOrg, dt_level3, dt_level4, dt_level5, Response);
                    }
                    else if (vReportFormat == "WORD")
                    {
                        rpt.SetParameterValue("Str_Title", Str_Title);
                        rpt.SetParameterValue("sub_Title", Sub_Title);
                        rpt.SetParameterValue("PageCode", PageCode);
                        lblErrorMessage.Text = "";
                        CreateWordReport(rpt, reportname);
                        //CreateExcelReport(rpt, reportname);
                    }
                    else if (vReportFormat == "PDF")
                    {
                        rpt.SetParameterValue("Str_Title", Str_Title);
                        rpt.SetParameterValue("sub_Title", Sub_Title);
                        rpt.SetParameterValue("PageCode", PageCode);
                        lblErrorMessage.Text = "";
                        CreatePDFReport(rpt, reportname);
                    }

                }
                else
                {
                    lblErrorMessage.Text = "ไม่พบข้อมูล";
                    CrystalReportViewer1.Visible = false;
                }

                #endregion
            }
            else if (reportname == "SMM_RP02")
            {
                #region "SMM_RP02"
                string TypeName = Request.QueryString["TypeName"];
                string ORG_SERIAL = Request.QueryString["ORG_SERIAL"];
                string ORG_NAME = Request.QueryString["ORG_NAME"];
                string Head_title = "";
                Head_title += Request.QueryString["Head_title"];
                //string UserOrg = Request.QueryString["UserOrg"];
                string ChartType = Request.QueryString["CHART_TYPE"];
                string UserID = Request.QueryString["UserID"];
                string strTitle = Request.QueryString["strTitle"];
                string subTitle = "";
                subTitle += Request.QueryString["subTitle"];

                //TypeName = "11";
                //ORG_SERAIL = "5";
                //ORG_NAME = "xxxxxxxx";
                //UserName = "99999ชื่อสกุล99999";
                //UserOrg = "xxxxxหน่วยงานxxxxx";
                //ChartType = "2";

                SqlParameter[] param = new SqlParameter[2];
                param[0] = OPM_BL.setParameter("@_PER_TYPE", SqlDbType.VarChar, TypeName);
                param[1] = OPM_BL.setParameter("@_ORG_SERIAL", SqlDbType.VarChar, ORG_SERIAL);
                //string sql = " select po.per_type," + Environment.NewLine;
                //sql += " org.org_serial,org.org_name," + Environment.NewLine;
                //sql += " lc.line_name line_position_name, count(po.pos_id) qty" + Environment.NewLine;
                //sql += " from PSST_POSITION po" + Environment.NewLine;
                //sql += " inner join CTLT_ORGANIZE org on po.org_serial=org.org_serial" + Environment.NewLine;
                //sql += " left join PSST_LINE_CODE lc on lc.line_code=po.line_code" + Environment.NewLine;
                //sql += " where po.per_type=@_PER_TYPE" + Environment.NewLine;
                //sql += " and po.org_serial=@_ORG_SERIAL" + Environment.NewLine;
                //sql += " group by po.per_type, org.org_serial,org.org_name,lc.line_name" + Environment.NewLine;
                //sql += " order by org.org_name" + Environment.NewLine;
                string sql = " select POS_NAME,SEQ,count(*) as POS_COUNT from";
                sql += " (";
                sql += " select iif(admin_name is null,LINE_NAME ,ADMIN_NAME) as POS_NAME,iif(ADMIN_NAME is null,2,1) as SEQ";
                sql += " from vw_CMN_PERSON ";
                sql += " where PER_STATUS=1 ";
                if (!string.IsNullOrEmpty(TypeName))
                {
                    sql += " and PER_TYPE=@_PER_TYPE ";
                }
                if (!string.IsNullOrEmpty(ORG_SERIAL))
                {
                    sql += " and ORG_SERIAL_LEVEL3=@_ORG_SERIAL";
                }
                sql += " ) as POS_COUNT";
                sql += " group by POS_NAME ,SEQ";
                sql += " order by SEQ";

                DataTable dt = OPM_BL.GetDatatable(sql, param);
                if (dt.Rows.Count > 0)
                {
                    rpt.Load(Server.MapPath(home_floder + "/" + reportname + "_" + ChartType + ".rpt"));
                    rpt.DataDefinition.FormulaFields["UserName"].Text = "'" + UserName + "'";
                    rpt.DataDefinition.FormulaFields["UserOrg"].Text = "'" + UserOrg + "'";
                    rpt.DataDefinition.FormulaFields["ORG_NAME"].Text = "'" + ORG_NAME + "'";
                    rpt.DataDefinition.FormulaFields["PrintDate"].Text = "'" + printDate + "'";
                    rpt.DataDefinition.FormulaFields["strTitle"].Text = "'" + strTitle + "'";
                    rpt.SetDataSource(dt);
                    lblErrorMessage.Text = "";
                    if (vReportFormat == "EXCEL")
                    {
                        rpt.SetParameterValue("Head_title", Head_title);
                        rpt.SetParameterValue("sub_title", subTitle);
                        lblErrorMessage.Text = "";
                        CreateExcelReport(rpt, reportname);

                    }
                    else if (vReportFormat == "PDF")
                    {
                        rpt.SetParameterValue("Head_title", Head_title);
                        rpt.SetParameterValue("sub_title", subTitle);
                        CreatePDFReport(rpt, reportname);
                    }


                }
                else
                {
                    lblErrorMessage.Text = "ไม่พบข้อมูล";
                    CrystalReportViewer1.Visible = false;
                }
                #endregion
            }
            else if (reportname == "SMM_RP03")
            {
                #region "SMM_RP03"
                //string ORG = "66";
                //string ORG_Name = "xxxx";
                //string ReportType = "1";
                //string UserName = "99999ชื่อสกุล99999";
                //string UserOrg = "xxxxxหน่วยงานxxxxx";
                //string TotalRecord = "0";

                SqlParameter[] param = new SqlParameter[1];
                string ORG = Request.QueryString["ORG"];
                string ORG_Name = Request.QueryString["ORG_Name"];
                string ReportType = Request.QueryString["ReportType"];
                //string UserName = Request.QueryString["UserName"];
                //string UserOrg = Request.QueryString["UserOrg"];
                string subTitle = "";
                subTitle += Request.QueryString["subTitle"];

                string[] arrORG = ORG_Name.Split(';');
                ORG_Name = arrORG[0];

                param[0] = OPM_BL.setParameter("@_ORG_SERIAL", SqlDbType.VarChar, ORG);
                string sql = "";
                if (ReportType == "1")
                {
                    sql += " select * from (";
                    sql += " select convert(bigint,po.pos_id) pos_id ,ISNULL(ADM.ADMIN_NAME,LINE.LINE_NAME) POS_NAME , pt.per_type, PT.per_type_name," + Environment.NewLine;
                    sql += " isnull(prefix.prefix_name,'') + isnull(p.name,'') + ' ' + isnull(p.surname,'') staff_name,po.pos_id as strposid" + Environment.NewLine;
                    sql += " from PSST_POSITION po" + Environment.NewLine;
                    sql += " LEFT JOIN PSST_ADMIN_CODE ADM ON po.ADMIN_CODE=ADM.ADMIN_CODE" + Environment.NewLine;
                    sql += " LEFT JOIN PSST_LINE_CODE LINE ON po.LINE_CODE=LINE.LINE_CODE" + Environment.NewLine;
                    sql += " AND po.PER_TYPE=LINE.PER_TYPE" + Environment.NewLine;
                    sql += " INNER JOIN SMM_PER_TYPE PT on PT.per_type=po.per_type" + Environment.NewLine;
                    sql += " LEFT JOIN PSST_PERSON p on p.id=po.id" + Environment.NewLine;
                    sql += " LEFT JOIN CTLT_PREFIX_CODE PREFIX ON p.PREFIX_CODE=PREFIX.PREFIX_CODE" + Environment.NewLine;
                    sql += " where po.org_serial=@_ORG_SERIAL" + Environment.NewLine;
                    sql += " ) T where pos_name is not null order by per_type, convert(bigint,pos_id)";
                    //sql += " order by pt.per_type, convert(bigint,po.pos_id) ";
                }
                else
                {

                }


                DataTable dt = OPM_BL.GetDatatable(sql, param);
                if (dt.Rows.Count > 0)
                {
                    rpt.Load(Server.MapPath(home_floder + "/" + reportname + ".rpt"));
                    rpt.DataDefinition.FormulaFields["UserName"].Text = "'" + UserName + "'";
                    rpt.DataDefinition.FormulaFields["UserOrg"].Text = "'" + UserOrg + "'";
                    rpt.DataDefinition.FormulaFields["org_name"].Text = "'" + ORG_Name + "'";
                    rpt.DataDefinition.FormulaFields["ToTalRecord"].Text = "'" + dt.Rows.Count.ToString() + "'";
                    rpt.DataDefinition.FormulaFields["PrintDate"].Text = "'" + printDate + "'";

                    rpt.SetDataSource(dt);

                    lblErrorMessage.Text = "";
                    CrystalReportViewer1.Visible = true;
                }
                else
                {
                    lblErrorMessage.Text = "ไม่พบข้อมูล";
                    CrystalReportViewer1.Visible = false;
                }
                #endregion
            }
            else if (reportname == "SMM_RP04")
            {
                #region "SMM_RP04"
                #region "BackUp"
                //string TypeName = "11";
                //string ORG = "2";
                //string POSName = "ปลัดสำนักนายกรัฐมนตรี";
                ////string POSName = "";
                //string Per_FullName = "หม่อมหลวงปนัดดา ดิศกุล";

                //SqlParameter[] param = new SqlParameter[4];
                ////string TypeName = Request.QueryString["TypeName"];
                ////string ORG = Request.QueryString["ORG"];
                ////string POSName = Request.QueryString["POSName"];
                ////string Per_FullName = Request.QueryString["Per_FullName"];

                //string sql = "-----ส่วนที่ 1 ข้อมูลทั่วไป" + Environment.NewLine;
                //sql += " select 'ส่วนที่ 1 ข้อมูลทั่วไป' job_detail, p.pos_id, p.POS_NAME,p.line_name, p.pos_type_name, p.name_level3, p.org_name, p.org_position, p.org_level," + Environment.NewLine;
                //sql += " 0 under_qty, 0 official_qty, p.per_fullname," + Environment.NewLine;
                //sql += " 'ส่วนที่ 2 หน้าที่คสามรับผิดชอบโดยสรุป ( Job Summary )' job_summary ,js.job_seq, js.job_detail job_detail2,p.per_type," + Environment.NewLine;
                //sql += " 'ส่วนที่ ' + ltrim(rtrim(str(jd.group_id))) + ' ' + jd.group_name job_specification,  jd.sub_group_name, jd.jd_seq, jd.jd_detail," + Environment.NewLine;
                //sql += " 'ส่วนที่ 5 ทักษะ ความรู้ ที่จำเป็นในงาน' job_skill, sk.type_name skill_type_name,  sk.item_name skill_item_name" + Environment.NewLine;
                //sql += " from vw_CMN_POSITION p" + Environment.NewLine;
                //sql += " left join (" + Environment.NewLine;
                //sql += " -----------ส่วนที่ 2 หน้าที่คสามรับผิดชอบโดยสรุป ( Job Summary )-----------------" + Environment.NewLine;
                //sql += "        SELECT JOB_ID,POS_ID,PER_TYPE,ORG_SERIAL,CUR_LEV,JOB_SEQ,JOB_DETAIL,ACTIVE_STATUS" + Environment.NewLine;
                //sql += "        FROM SMM_JD_JOB_SUMMARY" + Environment.NewLine;
                //sql += "        where ACTIVE_STATUS=1" + Environment.NewLine;
                //sql += " ) js on js.POS_ID=p.POS_ID and p.per_type=js.per_type" + Environment.NewLine;
                //sql += " left join (" + Environment.NewLine;
                //sql += " --------@GROUP_ID=3---ส่วนที่ 3 หน้าที่ความรับผิดที่ทำอยู่ในปัจจุบัน (รวมถึงสิ่งที่ต้องทำในอนาคตด้วย)-----------------" + Environment.NewLine;
                //sql += " --------@GROUP_ID=4---ส่วนที่ 4 คุณสมบัติที่จำเป็นในงาน (Job Specifications)-----------------" + Environment.NewLine;
                //sql += "        SELECT   POS_JD.JD_ID,POS_JD.POS_ID,POS_JD.PER_TYPE,POS_JD.ORG_SERIAL" + Environment.NewLine;
                //sql += "                      ,JD_GROUP.GROUP_ID,JD_GROUP.GROUP_NAME" + Environment.NewLine;
                //sql += "                      ,SUB_GROUP.SUB_GROUP_ID,SUB_GROUP.SUB_GROUP_NAME" + Environment.NewLine;
                //sql += "                      ,POS_JD.JD_SEQ,POS_JD.JD_DETAIL,POS_JD.ACTIVE_STATUS" + Environment.NewLine;
                //sql += "        FROM SMM_POS_JD34 POS_JD" + Environment.NewLine;
                //sql += "        INNER JOIN SMM_JD_SUB_GROUP34 SUB_GROUP ON SUB_GROUP.SUB_GROUP_ID = POS_JD.SUB_GROUP_ID AND SUB_GROUP.ACTIVE_STATUS=1" + Environment.NewLine;
                //sql += "        INNER JOIN SMM_JD_GROUP34 JD_GROUP ON JD_GROUP.GROUP_ID = SUB_GROUP.GROUP_ID" + Environment.NewLine;
                //sql += " ) jd on p.pos_id=jd.pos_id and p.per_type=jd.per_type" + Environment.NewLine;

                //sql += " left join (" + Environment.NewLine;
                //sql += " -----------ส่วนที่ 5 ทักษะ คสามรู้ ที่จำเป็นในงาน-----------------" + Environment.NewLine;
                //sql += "        SELECT  SMM_JD_EVAL_SCORE.EVAL_ID,SMM_JD_EVAL_SCORE.ADMIN_CODE,SMM_JD_EVAL_SCORE.LINE_CODE  ,SMM_JD_EVAL_SCORE.PER_TYPE" + Environment.NewLine;
                //sql += "                      ,SMM_JD_EVAL_TYPE.TYPE_ID ,SMM_JD_EVAL_TYPE.TYPE_NAME" + Environment.NewLine;
                //sql += "                      ,SMM_JD_EVAL_ITEM.ITEM_ID,SMM_JD_EVAL_ITEM.ITEM_NAME,ISNULL(SMM_JD_EVAL_SCORE.PASS_SCORE,0) PASS_SCORE" + Environment.NewLine;
                //sql += "        FROM SMM_JD_EVAL_SCORE" + Environment.NewLine;
                //sql += "        LEFT JOIN SMM_JD_EVAL_ITEM ON SMM_JD_EVAL_ITEM.ITEM_ID= SMM_JD_EVAL_SCORE.ITEM_ID AND SMM_JD_EVAL_ITEM.ACTIVE_STATUS=1" + Environment.NewLine;
                //sql += "        LEFT JOIN SMM_JD_EVAL_TYPE ON SMM_JD_EVAL_TYPE.TYPE_ID = SMM_JD_EVAL_ITEM.TYPE_ID" + Environment.NewLine;
                //sql += "        where SMM_JD_EVAL_SCORE.ACTIVE_STATUS = 1" + Environment.NewLine;
                //sql += " ) sk on sk.admin_code=p.admin_code and sk.line_code=p.line_code and sk.per_type=p.per_type" + Environment.NewLine;
                //sql += " where p.per_type=@_PER_TYPE" + Environment.NewLine;
                //sql += " and p.pos_name like '%'+ @_POS_NAME +'%'" + Environment.NewLine;

                //sql += " and p.org_serial=@_ORG_SERIAL" + Environment.NewLine;

                //param[0] = OPM_BL.setParameter("@_PER_TYPE", SqlDbType.VarChar, TypeName);
                //param[1] = OPM_BL.setParameter("@_POS_NAME", SqlDbType.VarChar, POSName);
                //param[2] = OPM_BL.setParameter("@_ORG_SERIAL", SqlDbType.VarChar, ORG);
                //if (Per_FullName != null)
                //{
                //    sql += " and p.per_fullname like '%'+ @_PER_FULLNAME +'%'" + Environment.NewLine;
                //    param[3] = OPM_BL.setParameter("@_PER_FULLNAME", SqlDbType.VarChar, Per_FullName);
                //}

                //StringBuilder section1 = new StringBuilder();
                //StringBuilder section2 = new StringBuilder();
                //StringBuilder section3 = new StringBuilder();
                //StringBuilder section4 = new StringBuilder();
                //StringBuilder section5 = new StringBuilder();

                //StringBuilder DetailReport = new StringBuilder();
                //DataTable dt = OPM_BL.GetDatatable(sql, param);
                //if (dt.Rows.Count > 0)
                //{
                //    //Start GenReportDetail
                //    string blank = "    ";
                //    string dd = "&nbsp;&nbsp;&nbsp;";

                //    DataTable dtposid = new DataTable();
                //    dtposid = dt.DefaultView.ToTable(true, "pos_id").Copy();
                //    for (int i = 0; i <= dtposid.Rows.Count - 1; i++)
                //    {
                //        dt.DefaultView.RowFilter = "pos_id='" + dtposid.Rows[i]["pos_id"] + "'";
                //        DataTable dtsection1 = new DataTable();
                //        dtsection1 = dt.DefaultView.ToTable(true, "job_detail", "pos_id", "pos_name", "line_name", "pos_type_name", "name_level3", "org_name", "org_position", "org_level", "under_qty", "official_qty", "per_fullname").Copy();
                //        if (dtsection1.Rows.Count > 0)
                //        {
                //            //ส่วนที่ 1 ข้อมูลทั่วไป(Job Tiltle)
                //            string job_detail = dtsection1.Rows[0]["job_detail"].ToString();
                //            string pos_id = dtsection1.Rows[0]["pos_id"].ToString();
                //            string pos_name = dtsection1.Rows[0]["pos_name"].ToString();
                //            string line_name = dtsection1.Rows[0]["line_name"].ToString();
                //            string pos_type_name = dtsection1.Rows[0]["pos_type_name"].ToString();
                //            string name_level3 = dtsection1.Rows[0]["name_level3"].ToString();
                //            string org_name = dtsection1.Rows[0]["org_name"].ToString();
                //            string org_position = dtsection1.Rows[0]["org_position"].ToString();
                //            string org_level = dtsection1.Rows[0]["org_level"].ToString();
                //            string under_qty = dtsection1.Rows[0]["under_qty"].ToString();
                //            string official_qty = dtsection1.Rows[0]["official_qty"].ToString();
                //            string per_fullname = dtsection1.Rows[0]["per_fullname"].ToString();

                //            //DetailReport.Append("<b><p align=''center''>ชื่อส่วนราชการ สำนักงานปลัดสำนักนายกรัฐมนตรี</p></b>");
                //            //DetailReport.Append("<b><p align=''center''>แบบบรรยายลักษณะงาน(Job Description)</p></b>");
                //            //DetailReport.Append("<p align=''Left''>" + blank + "</p>");
                //            //DetailReport.Append("<p align=''right''>ตำแหน่งเลขที่  " + pos_id + "</p>");
                //            //DetailReport.Append("<p align=''Left''>" + blank + "</p>");
                //            //DetailReport.Append("<p align=''Left''>" + blank + "</p>");

                //            //DetailReport.Append("<b><p align=''Left''>ส่วนที่ 1 ข้อมูลทั่วไป(Job Tiltle)</p></b>");
                //            //DetailReport.Append("<p align=''Left''>ชื่อตำแหน่งในการบริหารงาน : " + pos_name + "</p>");
                //            //DetailReport.Append("<p align=''Left''>ชื่อสายงาน : " + line_name + "</p>");
                //            //DetailReport.Append("<p align=''Left''>ประเภท/ระดับ : " + pos_type_name + "</p>");
                //            //DetailReport.Append("<p align=''Left''>ชื่อหน่วยงาน(สำนัก/กอง) : " + name_level3 + "</p>");
                //            //DetailReport.Append("<p align=''Left''>ชื่อส่วนงาน/กลุ่มงาน/ฝ่าย/งาน : " + org_name + "</p>");
                //            //DetailReport.Append("<p align=''Left''>ชื่อตำแหน่งผู้บังคับบัญชาโดยตรง : " + org_position + " ประเภท/ระดับ : " + org_level + "</p>");
                //            //DetailReport.Append("<p align=''Left''>จำนวนผู้ใต้บังคับบัญชา จำนวน : " + under_qty + " คน       ข้าราชการ : " + official_qty + " คน</p>");

                //            section1.Append("<b><p align=''center''>ชื่อส่วนราชการ สำนักงานปลัดสำนักนายกรัฐมนตรี</p></b>");
                //            section1.Append("<b><p align=''center''>แบบบรรยายลักษณะงาน(Job Description)</p></b>");
                //            section1.Append("<p align=''Left''>" + blank + "</p>");
                //            section1.Append("<p align=''right''>ตำแหน่งเลขที่  " + pos_id + "</p>");
                //            section1.Append("<p align=''Left''>" + blank + "</p>");
                //            section1.Append("<p align=''Left''>" + blank + "</p>");

                //            section1.Append("<b><p align=''Left''>ส่วนที่ 1 ข้อมูลทั่วไป(Job Tiltle)</p></b>");
                //            section1.Append("<p align=''Left''>ชื่อตำแหน่งในการบริหารงาน : " + pos_name + "</p>");
                //            section1.Append("<p align=''Left''>ชื่อสายงาน : " + line_name + "</p>");
                //            section1.Append("<p align=''Left''>ประเภท/ระดับ : " + pos_type_name + "</p>");
                //            section1.Append("<p align=''Left''>ชื่อหน่วยงาน(สำนัก/กอง) : " + name_level3 + "</p>");
                //            section1.Append("<p align=''Left''>ชื่อส่วนงาน/กลุ่มงาน/ฝ่าย/งาน : " + org_name + "</p>");
                //            section1.Append("<p align=''Left''>ชื่อตำแหน่งผู้บังคับบัญชาโดยตรง : " + org_position + " ประเภท/ระดับ : " + org_level + "</p>");
                //            section1.Append("<p align=''Left''>จำนวนผู้ใต้บังคับบัญชา จำนวน : " + under_qty + " คน       ข้าราชการ : " + official_qty + " คน</p>");

                //        }


                //        DetailReport.Append("<p align=''Left''>" + blank + "</p>");
                //        DetailReport.Append("<p align=''Left''>" + blank + "</p>");
                //        //ส่วนที่ 2 หน้าที่ความรับผิดชอบโดยสรุป (Job Summary)
                //        DataTable dtsection2 = new DataTable();
                //        dtsection2 = dt.DefaultView.ToTable(true, "job_summary", "job_seq", "job_detail2", "per_type").Copy();
                //        //DetailReport.Append("<b><p align=''Left''>ส่วนที่ 2 หน้าที่ความรับผิดชอบโดยสรุป (Job Summary)</p></b>");
                //        section2.Append("<b><p align=''Left''>ส่วนที่ 2 หน้าที่ความรับผิดชอบโดยสรุป (Job Summary)</p></b>");
                //        if (dtsection2.Rows.Count > 0)
                //        {
                //            string job_detail = "";
                //            for (int count = 0; count <= dtsection2.Rows.Count - 1; count++)
                //            {
                //                job_detail += dd + (count + 1) + " " + dtsection2.Rows[count]["job_detail2"].ToString() + "</br>";
                //            }
                //            //DetailReport.Append("<p align=''Left''>" + job_detail + "</p>");
                //            section2.Append("<p align=''Left''>" + job_detail + "</p>");
                //        }


                //        DetailReport.Append("<p align=''Left''>" + blank + "</p>");
                //        DetailReport.Append("<p align=''Left''>" + blank + "</p>");
                //        //ส่วนที่ 3 หน้าที่ความรับผิดชอบที่ทำอยู่ในปัจจุบัน (รวมถึงสิ่งที่ต้องทำในอนาคต)
                //        DataTable dtsection3 = new DataTable();
                //        dtsection3 = dt.DefaultView.ToTable(true, "job_specification", "sub_group_name", "jd_seq", "jd_detail").Copy();
                //        dtsection3.DefaultView.RowFilter = "job_specification ='ส่วนที่ 3 หน้าที่ความรับผิดชอบที่ทำอยู่ในปัจจุบัน (รวมถึงสิ่งที่ต้องทำในอนาคต)'";
                //        //DetailReport.Append("<b><p align=''Left''>ส่วนที่ 3 หน้าที่ความรับผิดชอบที่ทำอยู่ในปัจจุบัน (รวมถึงสิ่งที่ต้องทำในอนาคต)</p></b>");
                //        section3.Append("<b><p align=''Left''>ส่วนที่ 3 หน้าที่ความรับผิดชอบที่ทำอยู่ในปัจจุบัน (รวมถึงสิ่งที่ต้องทำในอนาคต)</p></b>");
                //        if (dtsection3.Rows.Count > 0)
                //        {
                //            DataTable dtsubgroup = new DataTable();
                //            dtsubgroup = dtsection3.DefaultView.ToTable(true, "sub_group_name").Copy();
                //            for (int count = 0; count <= dtsubgroup.Rows.Count - 1; count++)
                //            {
                //                string sub_group_name = dtsubgroup.Rows[count]["sub_group_name"].ToString();
                //                dtsection3.DefaultView.RowFilter = "sub_group_name ='" + sub_group_name + "'";
                //                //DetailReport.Append("<b><p align=''Left''>"+ (count + 1 ) + ") " + sub_group_name + "</p></b>");
                //                section3.Append("<b><p align=''Left''>" + (count + 1) + ") " + sub_group_name + "</p></b>");

                //                DataTable section3dt = new DataTable();
                //                section3dt = dtsection3.DefaultView.ToTable(true, "jd_detail").Copy();
                //                for (int count_dt = 0; count_dt <= section3dt.Rows.Count - 1; count_dt++)
                //                {
                //                    string jd_detail = section3dt.Rows[count_dt]["jd_detail"].ToString();
                //                    //DetailReport.Append("<p align=''Left''>" + dd + (count + 1) + "." + (count_dt + 1) + "  " + jd_detail + "</dd></p>");
                //                    section3.Append("<p align=''Left''>" + dd + (count + 1) + "." + (count_dt + 1) + "  " + jd_detail + "</dd></p>");
                //                }
                //                dtsection3.DefaultView.RowFilter = "";

                //            }
                //        }

                //        DetailReport.Append("<p align=''Left''>" + blank + "</p>");
                //        DetailReport.Append("<p align=''Left''>" + blank + "</p>");
                //        //ส่วนที่ 4 คุณสมบัติที่จำเป็นในงาน (Job Specifications)
                //        DataTable dtsection4 = new DataTable();
                //        dtsection4 = dt.DefaultView.ToTable(true, "job_specification", "sub_group_name", "jd_seq", "jd_detail").Copy();
                //        dtsection4.DefaultView.RowFilter = "job_specification = 'ส่วนที่ 4 คุณสมบัติที่จำเป็นในงาน (Job Specifications)'";
                //        //DetailReport.Append("<b><p align=''Left''>ส่วนที่ 4 คุณสมบัติที่จำเป็นในงาน (Job Specifications)</p></b>");
                //        section4.Append("<b><p align=''Left''>ส่วนที่ 4 คุณสมบัติที่จำเป็นในงาน (Job Specifications)</p></b>");
                //        if (dtsection4.Rows.Count > 0)
                //        {
                //            DataTable dtsubgroup = new DataTable();
                //            dtsubgroup = dtsection4.DefaultView.ToTable(true, "sub_group_name").Copy();
                //            for (int count = 0; count <= dtsubgroup.Rows.Count - 1; count++)
                //            {
                //                string sub_group_name = dtsubgroup.Rows[count]["sub_group_name"].ToString();
                //                dtsection4.DefaultView.RowFilter = "sub_group_name ='" + sub_group_name + "'";
                //                //DetailReport.Append("<b><p align=''Left''>" + (count + 1) + ") " + sub_group_name + "</p></b>");
                //                section4.Append("<b><p align=''Left''>" + (count + 1) + ") " + sub_group_name + "</p></b>");

                //                DataTable section4dt = new DataTable();
                //                section4dt = dtsection4.DefaultView.ToTable(true, "jd_detail").Copy();
                //                for (int count_dt = 0; count_dt <= section4dt.Rows.Count - 1; count_dt++)
                //                {
                //                    string jd_detail = section4dt.Rows[count_dt]["jd_detail"].ToString();
                //                    //DetailReport.Append("<p align=''Left''>" + dd + (count + 1) + "." + (count_dt + 1) + "  "+ jd_detail + "</dd></p>");
                //                    section4.Append("<p align=''Left''>" + dd + (count + 1) + "." + (count_dt + 1) + "  " + jd_detail + "</dd></p>");
                //                }
                //                dtsection4.DefaultView.RowFilter = "";

                //            }
                //        }

                //        DetailReport.Append("<p align=''Left''>" + blank + "</p>");
                //        DetailReport.Append("<p align=''Left''>" + blank + "</p>");
                //        //ส่วนที่ 5 ทักษะ ความรู้ ที่จำเป็นในงาน
                //        DataTable dtsection5 = new DataTable();
                //        dtsection5 = dt.DefaultView.ToTable(true, "job_skill", "skill_type_name", "skill_item_name").Copy();
                //        //DetailReport.Append("<b><p align=''Left''>ส่วนที่ 5 ทักษะ ความรู้ ที่จำเป็นในงาน</p></b>");
                //        section5.Append("<b><p align=''Left''>ส่วนที่ 5 ทักษะ ความรู้ ที่จำเป็นในงาน</p></b>");
                //        if (dtsection5.Rows.Count > 0)
                //        {
                //            DataTable dtsubgroup = new DataTable();
                //            dtsubgroup = dtsection5.DefaultView.ToTable(true, "skill_type_name").Copy();
                //            for (int count = 0; count <= dtsubgroup.Rows.Count - 1; count++)
                //            {
                //                string skill_type_name = dtsubgroup.Rows[count]["skill_type_name"].ToString();
                //                dtsection5.DefaultView.RowFilter = "skill_type_name ='" + skill_type_name + "'";
                //                //DetailReport.Append("<b><p align=''Left''>" + (count + 1) + ") " + skill_type_name + "</p></b>");
                //                section5.Append("<b><p align=''Left''>" + (count + 1) + ") " + skill_type_name + "</p></b>");

                //                DataTable section5dt = new DataTable();
                //                section5dt = dtsection5.DefaultView.ToTable(true, "skill_item_name").Copy();
                //                for (int count_dt = 0; count_dt <= section5dt.Rows.Count - 1; count_dt++)
                //                {
                //                    string skill_item_name = section5dt.Rows[count_dt]["skill_item_name"].ToString();
                //                    //DetailReport.Append("<p align=''Left''>" + dd + (count + 1) + "." + (count_dt + 1) + "  " + skill_item_name + "</dd></p>");
                //                    section5.Append("<p align=''Left''>" + dd + (count + 1) + "." + (count_dt + 1) + "  " + skill_item_name + "</dd></p>");
                //                }
                //                dtsection5.DefaultView.RowFilter = "";

                //            }
                //        }


                //    }

                //    //End  GenReport Detail

                //    string strsql = "select " + section1 + " section1," + section2 + " section2," + section3 + " section3, " + section4 + " section4, " + section5 + " section5";
                //    DataTable dt_tmp = OPM_BL.GetDatatable(strsql, param);
                //    if (dt.Rows.Count > 0)
                //    {

                //    }

                //    rpt.Load(Server.MapPath(home_floder + "/" + reportname + ".rpt"));
                //    rpt.DataDefinition.FormulaFields["DetailReport"].Text = "'" + DetailReport.ToString() + "'";
                //    //rpt.SetDataSource(dt);
                //    lblErrorMessage.Text = "";
                //    CrystalReportViewer1.Visible = true;
                //}
                //else
                //{
                //    lblErrorMessage.Text = "ไม่พบข้อมูล";
                //    CrystalReportViewer1.Visible = false;
                //}
                #endregion

                SqlParameter[] param = new SqlParameter[4];
                string TypeName = Request.QueryString["TypeName"];
                string ORG = Request.QueryString["ORG"];
                string POSName = Request.QueryString["POSName"];
                string Per_FullName = Request.QueryString["Per_FullName"];
                string POSID = Request.QueryString["POSID"];
                string strTitle = "";
                strTitle += Request.QueryString["strTitle"];
                string subTitle = "";
                subTitle += Request.QueryString["subTitle"];
                string Page_Code = Request.QueryString["PageCode"];
                //TypeName = "11";
                //ORG = "2";
                //POSName = "ปลัดสำนักนายกรัฐมนตรี";
                //POSName = "";
                //Per_FullName = "นพปฎล";

                string sql = "";
                sql += " select POS.POS_ID,POS.PER_TYPE,AMC.ADMIN_NAME,LINE.LINE_NAME,LGC.LENGTH_NAME,NAME_LEVEL3 ORG_NAME,ISNULL(NAME_LEVEL4,NAME_LEVEL5) AS GROUP_NAME,vwCMDL.PARENT_COM_CODE,vwCMDL.COM_CODE" + Environment.NewLine;
                sql += " ,ISNULL(ISNULL(AMC2.ADMIN_NAME,LINE2.LINE_NAME),'-') PARENT_POSITION_NAME,ISNULL(LGC2.LENGTH_NAME,'-') AS PARENT_LENGTH_NAME," + Environment.NewLine;
                sql += " (select count(*) from vw_ODS_COMMAND_LINE where PARENT_COM_CODE=vwCMDL.COM_CODE) under_qty ," + Environment.NewLine;
                sql += " (select count(*) from vw_ODS_COMMAND_LINE where PARENT_COM_CODE=vwCMDL.COM_CODE and PER_TYPE in (10,11,14)) official_qty,POS.CUR_LEV" + Environment.NewLine;
                sql += " ,ISNULL(POS.ADMIN_CODE,'') ADMIN_CODE,ISNULL(POS.LINE_CODE,'') LINE_CODE" + Environment.NewLine;
                sql += " FROM   PSST_POSITION AS POS " + Environment.NewLine;
                sql += " LEFT OUTER JOIN PSST_ADMIN_CODE AMC on POS.ADMIN_CODE = AMC.ADMIN_CODE" + Environment.NewLine;
                sql += " LEFT OUTER JOIN PSST_LINE_CODE AS LINE ON POS.LINE_CODE = LINE.LINE_CODE AND POS.PER_TYPE = LINE.PER_TYPE " + Environment.NewLine;
                sql += " LEFT OUTER JOIN PSST_LENGTH_CODE AS LGC ON POS.LENGTH_CODE = LGC.LENGTH_CODE " + Environment.NewLine;
                sql += " INNER JOIN CTLT_ORGANIZE AS ORG ON POS.ORG_SERIAL = ORG.ORG_SERIAL" + Environment.NewLine;
                sql += " LEFT OUTER JOIN vw_ODS_COMMAND_LINE vwCMDL ON POS.POS_ID = vwCMDL.POS_ID and POS.PER_TYPE = vwCMDL.PER_TYPE" + Environment.NewLine;
                sql += " LEFT OUTER JOIN vw_ODS_COMMAND_LINE vwCMDL2 ON vwCMDL.PARENT_COM_CODE = vwCMDL2.COM_CODE" + Environment.NewLine;
                sql += " LEFT OUTER JOIN PSST_POSITION POS2 ON vwCMDL2.POS_ID = POS2.POS_ID and POS2.PER_TYPE = vwCMDL2.PER_TYPE" + Environment.NewLine;
                sql += " LEFT OUTER JOIN PSST_ADMIN_CODE AMC2 on POS2.ADMIN_CODE = AMC2.ADMIN_CODE " + Environment.NewLine;
                sql += " LEFT OUTER JOIN PSST_LINE_CODE AS LINE2 ON POS2.LINE_CODE = LINE2.LINE_CODE AND POS2.PER_TYPE = LINE2.PER_TYPE" + Environment.NewLine;
                sql += " LEFT OUTER JOIN PSST_LENGTH_CODE AS LGC2 ON POS2.LENGTH_CODE = LGC2.LENGTH_CODE" + Environment.NewLine;
                sql += " LEFT OUTER JOIN PSST_PERSON AS PER ON POS.ID = PER.ID" + Environment.NewLine;
                sql += " where 1=1" + Environment.NewLine;


                if (TypeName != null)
                {
                    sql += " and pos.PER_TYPE =@_PER_TYPE" + Environment.NewLine;
                    param[0] = OPM_BL.setParameter("@_PER_TYPE", SqlDbType.VarChar, TypeName);
                }
                if (POSName != null)
                {
                    sql += " and (ISNULL(AMC.ADMIN_NAME,'') like '%' + @_POSNAME + '%'  or ISNULL(LINE.LINE_NAME,'') like '%' + @_POSNAME + '%' or POS.POS_ID like '%' + @_POSNAME + '%') " + Environment.NewLine;
                    param[1] = OPM_BL.setParameter("@_POSNAME", SqlDbType.VarChar, POSName);
                }
                if (ORG != null && ORG != "76")
                {
                    sql += " and pos.org_serial = @_ORG_SERIAL" + Environment.NewLine;
                    param[2] = OPM_BL.setParameter("@_ORG_SERIAL", SqlDbType.VarChar, ORG);
                }
                if (Per_FullName != null)
                {
                    sql += " and (ISNULL(PER.NAME, '')like '%' + @_PERSON_NAME + '%' or ISNULL(PER.SURNAME, '') like '%' + @_PERSON_NAME + '%')" + Environment.NewLine;
                    param[3] = OPM_BL.setParameter("@_PERSON_NAME", SqlDbType.VarChar, Per_FullName);
                }

                if (POSID != null)
                {
                    sql += " and POS.POS_ID = @_POSID" + Environment.NewLine;
                    param[2] = OPM_BL.setParameter("@_POSID", SqlDbType.VarChar, POSID);
                }

                DataTable dt = OPM_BL.GetDatatable(sql, param);
                if (dt.Rows.Count > 0)
                {

                    //section2
                    sql = " select job_seq,job_detail,per_type,pos_id from SMM_JD_JOB_SUMMARY" + Environment.NewLine;
                    DataTable DTS2 = new DataTable();
                    DTS2 = OPM_BL.GetDatatable(sql, null);

                    //section3
                    sql = " select SMM_POS_JD34.SUB_GROUP_ID,SMM_JD_SUB_GROUP34.SUB_GROUP_NAME,JD_SEQ,JD_DETAIL,POS_ID,PER_TYPE from SMM_POS_JD34 " + Environment.NewLine;
                    sql += " inner join SMM_JD_SUB_GROUP34 on SMM_POS_JD34.SUB_GROUP_ID = SMM_JD_SUB_GROUP34.SUB_GROUP_ID  Where Group_ID='3'" + Environment.NewLine;
                    DataTable DTS3 = new DataTable();
                    DTS3 = OPM_BL.GetDatatable(sql, null);

                    ////section4
                    sql = " select SMM_POS_JD34.SUB_GROUP_ID,SMM_JD_SUB_GROUP34.SUB_GROUP_NAME,JD_SEQ,JD_DETAIL,POS_ID,PER_TYPE from SMM_POS_JD34 " + Environment.NewLine;
                    sql += " inner join SMM_JD_SUB_GROUP34 on SMM_POS_JD34.SUB_GROUP_ID = SMM_JD_SUB_GROUP34.SUB_GROUP_ID  Where Group_ID='4'" + Environment.NewLine;
                    DataTable DTS4 = new DataTable();
                    DTS4 = OPM_BL.GetDatatable(sql, null);


                    //section5
                    sql = "select distinct T.ITEM_ID,T.ITEM_NAME,T.PASS_SCORE,S.TYPE_ID,S.TYPE_NAME,S.ADMIN_CODE,S.LINE_CODE,S.PER_TYPE,S.CUR_LEV,isnull(ItemQty,0) ItemQty " + Environment.NewLine;
                    sql += " from (select distinct type_id,type_name ,ISNULL(ADMIN_CODE,'') ADMIN_CODE,ISNULL(LINE_CODE,'') LINE_CODE,ISNULL(PER_TYPE,'')PER_TYPE,ISNULL(CUR_LEV ,'0') CUR_LEV " + Environment.NewLine;
                    sql += " from SMM_JD_EVAL_SCORE right join SMM_JD_EVAL_TYPE on 1=1) S " + Environment.NewLine;
                    sql += " left join (" + Environment.NewLine;
                    sql += " select SCORE.ITEM_ID,Item.ITEM_NAME,PASS_SCORE,TYPE.TYPE_NAME,ITEM.TYPE_ID,ISNULL(ADMIN_CODE,'') ADMIN_CODE,ISNULL(LINE_CODE,'') LINE_CODE,ISNULL(PER_TYPE,'')PER_TYPE,ISNULL(CUR_LEV,0)CUR_LEV" + Environment.NewLine;
                    sql += " ,(Select Count(I.Item_ID) From SMM_JD_EVAL_ITEM I where I.TYPE_ID = ITEM.TYPE_ID)ItemQty " + Environment.NewLine;
                    sql += " from SMM_JD_EVAL_TYPE TYPE " + Environment.NewLine;
                    sql += " Left Join SMM_JD_EVAL_ITEM ITEM  ON ITEM.TYPE_ID = TYPE.TYPE_ID" + Environment.NewLine;
                    sql += " left join SMM_JD_EVAL_SCORE SCORE ON SCORE.ITEM_ID=ITEM.ITEM_ID" + Environment.NewLine;
                    sql += " )T on S.TYPE_ID = T.TYPE_ID and S.ADMIN_CODE=T.ADMIN_CODE and S.LINE_CODE=T.LINE_CODE and S.PER_TYPE=T.PER_TYPE and S.CUR_LEV=T.CUR_LEV" + Environment.NewLine;
                    sql += " order by ADMIN_CODE,LINE_CODE,PER_TYPE,CUR_LEV,Type_id,item_name" + Environment.NewLine;


                    //sql = " select SCORE.ITEM_ID,Item.ITEM_NAME,PASS_SCORE,TYPE.TYPE_NAME,ITEM.TYPE_ID,ISNULL(ADMIN_CODE,'') ADMIN_CODE,LINE_CODE,PER_TYPE,CUR_LEV from SMM_JD_EVAL_SCORE SCORE" + Environment.NewLine;
                    //sql += " INNER JOIN SMM_JD_EVAL_ITEM ITEM ON SCORE.ITEM_ID=ITEM.ITEM_ID" + Environment.NewLine;
                    //sql += " INNER JOIN SMM_JD_EVAL_TYPE TYPE ON ITEM.TYPE_ID = TYPE.TYPE_ID" + Environment.NewLine;
                    DataTable DTS5 = new DataTable();
                    DTS5 = OPM_BL.GetDatatable(sql, null);

                    if (vReportFormat == "EXCEL")
                    {
                        lblErrorMessage.Text = "";
                        ExcelReportENG.ExportReportSMM_RP04(dt, DTS2, DTS3, DTS4, DTS5, Response);
                    }
                    else if (vReportFormat == "PDF")
                    {
                        rpt.Load(Server.MapPath(home_floder + "/" + reportname + ".rpt"));
                        rpt.DataDefinition.FormulaFields["UserName"].Text = "'" + UserName + "'";
                        rpt.DataDefinition.FormulaFields["UserOrg"].Text = "'" + UserOrg + "'";
                        //rpt.DataDefinition.FormulaFields["org_name"].Text = "'" + ORG_NAME + "'";
                        rpt.DataDefinition.FormulaFields["PrintDate"].Text = "'" + printDate + "'";


                        if (DTS2.Rows.Count > 0)
                        {
                            rpt.Subreports["SMM_RP04_Section2"].SetDataSource(DTS2);
                        }
                        rpt.Subreports["SMM_RP04_Section3"].SetDataSource(DTS3);
                        rpt.Subreports["SMM_RP04_Section4"].SetDataSource(DTS4);
                        rpt.Subreports["SMM_RP04_Section5"].SetDataSource(DTS5);




                        rpt.SetDataSource(dt);
                        lblErrorMessage.Text = "";
                        rpt.SetParameterValue("strTitle", strTitle);
                        rpt.SetParameterValue("subTitle", subTitle);
                        rpt.SetParameterValue("Page_Code", Page_Code);
                        CreatePDFReport(rpt, reportname);

                        
                    }



                }
                else
                {
                    lblErrorMessage.Text = "ไม่พบข้อมูล";
                    CrystalReportViewer1.Visible = false;
                }
                #endregion
            }
            else if (reportname == "SMM_RP07")
            {
                #region "SMM_RP07"
                SqlParameter[] param = new SqlParameter[10];
                string TypeName = Request.QueryString["TypeName"];//**
                string POSName = Request.QueryString["POSName"];
                string LevelName = Request.QueryString["LevelName"];
                string ReqName = Request.QueryString["ReqName"];
                string ProjectName = Request.QueryString["ProjectName"];
                string ProjectDes = Request.QueryString["ProjectDes"];
                string ReqDateFrom = Request.QueryString["ReqDateFrom"];//**
                string ReqDateTo = Request.QueryString["ReqDateTo"];//**
                string AppDateFrom = Request.QueryString["AppDateFrom"];
                string AppDateTo = Request.QueryString["AppDateTo"];
                string strTitle1 = Request.QueryString["strTitle1"];
                string strTitle2 = Request.QueryString["strTitle2"];

                //string UserName = Request.QueryString["UserName"];
                //string UserOrg = Request.QueryString["UserOrg"];

                //TypeName = "11";
                //POSName = "ผู้ตรวจราชการสำนักนายกรัฐมนตรี";
                //LevelName = "ประเภทบริหาร ระดับสูง";
                //ReqName = "นางจิรชัย มูลทองโร่ย";
                //ProjectName = Request.QueryString["ProjectName"];
                //ProjectDes = Request.QueryString["ProjectDes"];
                //ReqDateFrom = "20160201";
                //ReqDateTo = "20160220";
                //UserName = "99999ชื่อสกุล99999";
                //UserOrg = "xxxxxหน่วยงานxxxxx";

                param[0] = OPM_BL.setParameter("@_PER_TYPE", SqlDbType.VarChar, TypeName);

                string sql = " select isnull(pre.prefix_name,'') + isnull(pr.req_by_name,'') + ' ' + isnull(pr.req_by_surname,'') req_by_name," + Environment.NewLine;
                sql += " opo.line_name old_pos_name,opo.level_name_full, npo.line_name req_pos_name , npo.org_name,(select dbo.ThaiDateAbbrMonth(pr.req_date)) req_date,pr.approve_status," + Environment.NewLine;
                sql += " case pr.approve_status when '1' then 'ผ่าน'" + Environment.NewLine;
                sql += " else 'ไม่ผ่าน'" + Environment.NewLine;
                sql += " end approve_status_name,(select dbo.ThaiDateAbbrMonth(pr.approve_date)) approve_date,pt.PER_TYPE_NAME" + Environment.NewLine;
                sql += " from SMM_PER_PROMOTE_REQ pr" + Environment.NewLine;
                sql += " left join CTLT_PREFIX_CODE pre on pre.prefix_code=pr.req_by_prefix_code" + Environment.NewLine;
                sql += " inner join vw_CMN_POSITION opo on opo.pos_id=pr.from_pos_id and opo.per_type=pr.from_per_type" + Environment.NewLine;
                sql += " inner join vw_CMN_POSITION npo on npo.pos_id=pr.to_pos_id and npo.per_type=pr.to_per_type" + Environment.NewLine;
                sql += " left join smm_project prj on prj.req_id=pr.req_id" + Environment.NewLine;
                sql += " left join SMM_PER_TYPE pt on pr.from_per_type = pt.per_type" + Environment.NewLine;
                sql += " where 1=1" + Environment.NewLine;
                sql += " and pr.from_per_type=@_PER_TYPE" + Environment.NewLine;
                if (POSName != null)
                {
                    sql += " and (opo.line_name like '%'+ @_POS_NAME +'%' or npo.line_name like '%'+ @_POS_NAME +'%' or opo.pos_id like '%'+ @_POS_NAME +'%' or npo.pos_id like '%'+ @_POS_NAME +'%')" + Environment.NewLine;
                    param[1] = OPM_BL.setParameter("@_POS_NAME", SqlDbType.VarChar, POSName);
                }
                if (LevelName != null)
                {
                    sql += " and opo.level_name_full like '%'+ @_LEVEL_NAME +'%' " + Environment.NewLine;
                    param[2] = OPM_BL.setParameter("@_LEVEL_NAME", SqlDbType.VarChar, LevelName);
                }
                if (ReqName != null)
                {
                    sql += " and isnull(pre.prefix_name,'') + isnull(pr.req_by_name,'') + ' ' + isnull(pr.req_by_surname,'') like '%'+ @_REQ_NAME + '%'" + Environment.NewLine;
                    param[3] = OPM_BL.setParameter("@_REQ_NAME", SqlDbType.VarChar, ReqName);
                }
                if (ProjectName != null)
                {
                    sql += " and prj.proj_name like '%'+ @_PROJECT_NAME +'%'" + Environment.NewLine;
                    param[4] = OPM_BL.setParameter("@_PROJECT_NAME", SqlDbType.VarChar, ProjectName);
                }
                if (ProjectDes != null)
                {
                    sql += " and prj.proj_description like '%' + @_PROJECT_DESC + '%'" + Environment.NewLine;
                    param[5] = OPM_BL.setParameter("@_PROJECT_DESC", SqlDbType.VarChar, ProjectDes);
                }

                //param[6] = OPM_BL.setParameter("@_DATE_FROM", SqlDbType.VarChar, DateFrom);
                //param[7] = OPM_BL.setParameter("@_DATE_TO", SqlDbType.VarChar, DateTo);
                //sql += " and convert(varchar(8),pr.req_date,112) between @_DATE_FROM and @_DATE_TO" + Environment.NewLine;
                if (ReqDateFrom != null && ReqDateTo != null)
                {
                    param[6] = OPM_BL.setParameter("@_REQ_DATE_FROM", SqlDbType.VarChar, ReqDateFrom);
                    param[7] = OPM_BL.setParameter("@_REQ_DATE_TO", SqlDbType.VarChar, ReqDateTo);
                    sql += " and convert(varchar(8),pr.req_date,112) between @_REQ_DATE_FROM and @_REQ_DATE_TO" + Environment.NewLine;
                }

                if (AppDateFrom != null && AppDateTo != null)
                {
                    param[8] = OPM_BL.setParameter("@_APP_DATE_FROM", SqlDbType.VarChar, AppDateFrom);
                    param[9] = OPM_BL.setParameter("@_APP_DATE_TO", SqlDbType.VarChar, AppDateTo);
                    sql += " and convert(varchar(8),pr.approve_date,112) between @_APP_DATE_FROM and @_APP_DATE_TO" + Environment.NewLine;
                }

                DataTable dt = OPM_BL.GetDatatable(sql, param);
                if (dt.Rows.Count > 0)
                {

                    //DataRow dr;
                    //dr = dt.NewRow();
                    //dr["req_by_name"] = "นางจิรชัย มูลทองโร่ย2";
                    //dr["old_pos_name"] = "ผู้ตรวจราชการสำนักนายกรัฐมนตรี2";
                    //dr["level_name_full"] = "ประเภทบริหาร ระดับสูง";
                    //dr["req_pos_name"] = "ผู้ตรวจราชการสำนักนายกรัฐมนตรี2";
                    //dr["org_name"] = "ส่วนกลาง2";
                    //dr["req_date"] = "2015-08-10";
                    //dr["approve_status_name"] = "ผ่าน2";
                    //dr["approve_date"] = "2015-08-12";
                    //dr["PER_TYPE_NAME"] = "ข้าราชการพลเรือนสามัญ";
                    //dt.Rows.Add(dr);

                    if (vReportFormat == "EXCEL")
                    {
                        lblErrorMessage.Text = "";
                        ExcelReportENG.ExportReportSMM_RP07(UserName, UserOrg, dt, Response);
                    }
                    else if (vReportFormat == "PDF")
                    {
                        rpt.Load(Server.MapPath(home_floder + "/" + reportname + ".rpt"));
                        rpt.DataDefinition.FormulaFields["UserName"].Text = "'" + UserName + "'";
                        rpt.DataDefinition.FormulaFields["UserOrg"].Text = "'" + UserOrg + "'";
                        rpt.DataDefinition.FormulaFields["PrintDate"].Text = "'" + printDate + "'";
                        rpt.DataDefinition.FormulaFields["strTitle1"].Text = "'" + strTitle1 + "'";
                        rpt.DataDefinition.FormulaFields["strTitle2"].Text = "'" + strTitle2 + "'";
                        rpt.SetDataSource(dt);
                        lblErrorMessage.Text = "";

                        CreatePDFReport(rpt, reportname);
                    }

                }
                else
                {
                    lblErrorMessage.Text = "ไม่พบข้อมูล";
                    CrystalReportViewer1.Visible = false;
                }
                #endregion
            }
            else if (reportname == "SMM_RP08")
            {
                #region "SMM_RP08"

                SqlParameter[] param = new SqlParameter[7];
                string POSName = Request.QueryString["POSName"];
                string LevelName = Request.QueryString["LevelName"];
                string ReqName = Request.QueryString["ReqName"];
                string ReqDateFrom = Request.QueryString["ReqDateFrom"];//**
                string ReqDateTo = Request.QueryString["ReqDateTo"];//**
                string AppDateFrom = Request.QueryString["AppDateFrom"];
                string AppDateTo = Request.QueryString["ReqDateTo"];
                //string UserName = Request.QueryString["UserName"];
                //string UserOrg = Request.QueryString["UserOrg"];

                //POSName = "ผู้ตรวจราชการสำนักนายกรัฐมนตรี";
                //LevelName = "ต้น";
                //ReqName = "นางจิรชัย มูลทองโร่ย";
                //ReqDateFrom = "20150501";
                //ReqDateTo = "20160530";
                //UserName = "99999ชื่อสกุล99999";
                //UserOrg = "xxxxxหน่วยงานxxxxx";

                string sql = " select isnull(pre.prefix_name,'') + isnull(pr.req_by_name,'') + ' ' + isnull(pr.req_by_surname,'') req_by_name," + Environment.NewLine;
                sql += " opo.line_name old_pos_name, opo.org_name old_org_name, opo.name_level3 old_org_name3," + Environment.NewLine;
                sql += " npo.line_name new_pos_name, npo.level_name_full, npo.org_name new_org_name, npo.name_level3 new_org_name3," + Environment.NewLine;
                sql += " opo.pos_id old_pos_id,npo.pos_id new_pos_id, (select dbo.ThaiDateAbbrMonth(pr.req_date)) req_date," + Environment.NewLine;
                sql += " (select dbo.ThaiDateAbbrMonth(pr.approve_date)) approve_date" + Environment.NewLine;
                sql += " from SMM_PER_PROMOTE_REQ pr" + Environment.NewLine;
                sql += " left join CTLT_PREFIX_CODE pre on pre.prefix_code=pr.req_by_prefix_code" + Environment.NewLine;
                sql += " inner join vw_CMN_POSITION opo on opo.pos_id=pr.from_pos_id and opo.per_type=pr.from_per_type" + Environment.NewLine;
                sql += " inner join vw_CMN_POSITION npo on npo.pos_id=pr.to_pos_id and npo.per_type=pr.to_per_type" + Environment.NewLine;
                sql += " left join PSST_PER_LEVEL ole on ole.cur_lev=pr.from_lev and opo.per_type=ole.per_type" + Environment.NewLine;
                sql += " where approve_date is not null " + Environment.NewLine;

                if (POSName != null)
                {
                    sql += " and (opo.line_name  like '%'+@_POS_NAME+'%' or opo.pos_id like '%'+ @_POS_NAME +'%')" + Environment.NewLine;
                    param[0] = OPM_BL.setParameter("@_POS_NAME", SqlDbType.VarChar, POSName);

                }

                if (ReqName != null)
                {
                    sql += " and isnull(pre.prefix_name,'') + isnull(pr.req_by_name,'') + ' ' + isnull(pr.req_by_surname,'') like '%'+ @_REQ_NAME + '%'" + Environment.NewLine;
                    param[1] = OPM_BL.setParameter("@_REQ_NAME", SqlDbType.VarChar, ReqName);
                }
                if (LevelName != null)
                {
                    sql += " and ole.level_name like '%'+@_LEVEL_NAME+'%'" + Environment.NewLine;
                    param[2] = OPM_BL.setParameter("@_LEVEL_NAME", SqlDbType.VarChar, LevelName);
                }

                //sql += " and convert(varchar(8),pr.req_date,112) between @_DATE_FROM and @_DATE_TO" + Environment.NewLine;
                if (ReqDateFrom != null && ReqDateTo != null)
                {
                    param[3] = OPM_BL.setParameter("@_REQ_DATE_FROM", SqlDbType.VarChar, ReqDateFrom);
                    param[4] = OPM_BL.setParameter("@_REQ_DATE_TO", SqlDbType.VarChar, ReqDateTo);
                    sql += " and convert(varchar(8),pr.req_date,112) between @_REQ_DATE_FROM and @_REQ_DATE_TO" + Environment.NewLine;
                }

                if (AppDateFrom != null && AppDateTo != null)
                {
                    param[5] = OPM_BL.setParameter("@_APP_DATE_FROM", SqlDbType.VarChar, AppDateFrom);
                    param[6] = OPM_BL.setParameter("@_APP_DATE_TO", SqlDbType.VarChar, AppDateTo);
                    sql += " and convert(varchar(8),pr.approve_date,112) between @_APP_DATE_FROM and @_APP_DATE_TO" + Environment.NewLine;
                }

                DataTable dt = OPM_BL.GetDatatable(sql, param);
                if (dt.Rows.Count > 0)
                {
                    //DataRow dr;
                    //dr = dt.NewRow();
                    //dr["req_by_name"] = "นางจิรชัย มูลทองโร่ย2";
                    //dr["old_pos_name"] = "ผู้ตรวจราชการสำนักนายกรัฐมนตรี2";
                    //dr["old_org_name"] = "ผู้ตรวจราชการสำนักนายกรัฐมนตรี2";
                    //dr["old_org_name3"] = "ผู้ตรวจราชการสำนักนายกรัฐมนตรี2";
                    //dr["new_pos_name"] = "ผู้ตรวจราชการสำนักนายกรัฐมนตรี2";
                    //dr["level_name_full"] = "ประเภทบริหาร ระดับสูง";
                    //dr["new_org_name"] = "ผู้ตรวจราชการสำนักนายกรัฐมนตรี2";
                    //dr["new_org_name3"] = "ผู้ตรวจราชการสำนักนายกรัฐมนตรี2";
                    //dr["approve_date"] = "2015-08-12";
                    //dt.Rows.Add(dr);

                    string DateFrom = OPM_BL.GetTHDate(ReqDateFrom);
                    string DateTo = OPM_BL.GetTHDate(ReqDateTo);
                    if (vReportFormat == "EXCEL")
                    {
                        lblErrorMessage.Text = "";
                        ExcelReportENG.ExportReportSMM_RP08(DateFrom, DateTo, UserName, UserOrg, dt, Response);
                    }
                    else if (vReportFormat == "PDF")
                    {
                        //string printDate = DateTime.Now.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("th-TH"));
                        rpt.Load(Server.MapPath(home_floder + "/" + reportname + ".rpt"));
                        rpt.DataDefinition.FormulaFields["UserName"].Text = "'" + UserName + "'";
                        rpt.DataDefinition.FormulaFields["UserOrg"].Text = "'" + UserOrg + "'";
                        rpt.DataDefinition.FormulaFields["DateFrom"].Text = "'" + DateFrom + "'";
                        rpt.DataDefinition.FormulaFields["DateTo"].Text = "'" + DateTo + "'";
                        rpt.DataDefinition.FormulaFields["PrintDate"].Text = "'" + printDate + "'";
                        rpt.SetDataSource(dt);
                        lblErrorMessage.Text = "";

                        CreatePDFReport(rpt, reportname);
                    }
                }
                else
                {
                    lblErrorMessage.Text = "ไม่พบข้อมูล";
                    CrystalReportViewer1.Visible = false;
                }
                #endregion
            }

            else if (reportname == "SMM_RP09")
            {
                #region "SMM_RP09"
                SqlParameter[] param = new SqlParameter[8];
                string POSName = Request.QueryString["POSName"];
                string LevelName = Request.QueryString["LevelName"];
                string ReqName = Request.QueryString["ReqName"];
                string ReqDateFrom = Request.QueryString["ReqDateFrom"];//**
                string ReqDateTo = Request.QueryString["ReqDateTo"];//**
                string AppDateFrom = Request.QueryString["AppDateFrom"];
                string AppDateTo = Request.QueryString["AppDateTo"];
                //string UserName = Request.QueryString["UserName"];
                string Page_Code = Request.QueryString["PageCode"];
                string ProjectName = Request.QueryString["ProjectName"];


                string sql = "";

                sql += "select pj.proj_name, isnull(pre.prefix_name,'') + isnull(pr.req_by_name,'') + ' ' + isnull(pr.req_by_surname,'') req_by_name," + Environment.NewLine;
                sql += "  opo.line_name old_pos_name, opo.org_name old_org_name, opo.name_level3 old_org_name3," + Environment.NewLine;
                sql += "  npo.line_name new_pos_name, nle.level_name_full, npo.org_name new_org_name, npo.name_level3 new_org_name3," + Environment.NewLine;
                sql += "  npo.pos_id,(select dbo.ThaiDateAbbrMonth(pr.approve_date)) approve_date,(select dbo.ThaiDateAbbrMonth(pr.req_date)) req_date" + Environment.NewLine;
                sql += "from SMM_PROJECT pj" + Environment.NewLine;
                sql += "  inner join SMM_PER_PROMOTE_REQ pr on pr.req_id=pj.req_id" + Environment.NewLine;
                sql += "  left join CTLT_PREFIX_CODE pre on pre.prefix_code=pr.req_by_prefix_code" + Environment.NewLine;
                sql += "  inner join vw_CMN_POSITION opo on opo.pos_id=pr.from_pos_id and opo.per_type=pr.from_per_type" + Environment.NewLine;
                sql += "  inner join vw_CMN_POSITION npo on npo.pos_id=pr.to_pos_id and npo.per_type=pr.to_per_type" + Environment.NewLine;
                sql += "  left join PSST_PER_LEVEL nle on nle.cur_lev=pr.TO_LEV and npo.per_type=nle.per_type" + Environment.NewLine;
                sql += "where 1=1 AND APPROVE_STATUS=1" + Environment.NewLine;


                if (POSName != null)
                {
                    sql += " and (opo.line_name  like '%'+@_POS_NAME+'%' or opo.pos_id like '%'+ @_POS_NAME +'%')" + Environment.NewLine;
                    param[0] = OPM_BL.setParameter("@_POS_NAME", SqlDbType.VarChar, POSName);

                }
                if (ReqName != null)
                {
                    sql += " and isnull(pre.prefix_name,'') + isnull(pr.req_by_name,'') + ' ' + isnull(pr.req_by_surname,'') like '%'+ @_REQ_NAME + '%'" + Environment.NewLine;
                    param[1] = OPM_BL.setParameter("@_REQ_NAME", SqlDbType.VarChar, ReqName);
                }
                if (ProjectName != null)
                {
                    sql += " and pj.proj_name like '%'+@_PROJ_NAME+'%'" + Environment.NewLine;
                    param[2] = OPM_BL.setParameter("@_PROJECT_NAME", SqlDbType.VarChar, ProjectName);
                }
                if (ReqDateFrom != null && ReqDateTo != null)
                {
                    param[3] = OPM_BL.setParameter("@_REQ_DATE_FROM", SqlDbType.VarChar, ReqDateFrom);
                    param[4] = OPM_BL.setParameter("@_REQ_DATE_TO", SqlDbType.VarChar, ReqDateTo);
                    sql += " and convert(varchar(8),pr.req_date,112) between @_REQ_DATE_FROM and @_REQ_DATE_TO" + Environment.NewLine;
                }

                if (LevelName != null)
                {
                    sql += " and ole.level_name like '%'+@_LEVEL_NAME+'%'" + Environment.NewLine;
                    param[5] = OPM_BL.setParameter("@_LEVEL_NAME", SqlDbType.VarChar, LevelName);
                }

                if (AppDateFrom != null && AppDateTo != null)
                {
                    param[6] = OPM_BL.setParameter("@_APP_DATE_FROM", SqlDbType.VarChar, AppDateFrom);
                    param[7] = OPM_BL.setParameter("@_APP_DATE_TO", SqlDbType.VarChar, AppDateTo);
                    sql += " and convert(varchar(8),pr.approve_date,112) between @_APP_DATE_FROM and @_APP_DATE_TO" + Environment.NewLine;
                }




                DataTable dt = OPM_BL.GetDatatable(sql, param);
                DataRow dr;
                #region "tmpdt"
                //dr = dt.NewRow();
                //dr["proj_name"] = "บันทึกข้อตกลงประเมินผลการดำเนินงานขององค์กรสื่อสารมวลชนแห่งประเทศไทยประจำปีงบประมาณ 2545";
                //dr["req_by_name"] = "นางสาวเพ็ญจา แต้มบรรจง";
                //dr["old_pos_name"] = "เจ้าหน้าที่วิเคราะห์นโยบายและแผน 5 ส่วนกิจกรรมเฉลิมพระเกียรติ";
                //dr["old_org_name"] = "สำนักแผนงานและกิจการพิเศษสำนักงานปลัดสำนักนายกรัฐมนตรี";
                //dr["old_org_name3"] = "สำนักนายกรัฐมนตรี";
                //dr["new_pos_name"] = "เจ้าหน้าที่วิเคราะห์นโยบายและแผน ส่วนกิจการเฉลิมพระเกียรติ";
                //dr["level_name_full"] = "สำนักแผนงานและกิจการพิเศษสำนักงานปลัดสำนักนายกรัฐมนตรี";
                //dr["new_org_name"] = "สำนักแผนงานและกิจการพิเศษสำนักงานปลัดสำนักนายกรัฐมนตรี";
                //dr["new_org_name3"] = "สำนักนายกรัฐมนตรี";
                //dr["pos_id"] = "2";
                //dr["approve_date"] = "2015-08-12";
                //dt.Rows.Add(dr);

                //dr = dt.NewRow();
                //dr["proj_name"] = "งบประมาณลงทุนประจำปีงบประมาณ พ.ศ. 2545 ขององค์กรสื่อสารมวลชนแห่งประเทศไทย";
                //dr["req_by_name"] = "นางสาวเพ็ญจา แต้มบรรจง";
                //dr["old_pos_name"] = "เจ้าหน้าที่วิเคราะห์นโยบายและแผน 5 ส่วนกิจกรรมเฉลิมพระเกียรติ";
                //dr["old_org_name"] = "สำนักแผนงานและกิจการพิเศษสำนักงานปลัดสำนักนายกรัฐมนตรี";
                //dr["old_org_name3"] = "สำนักนายกรัฐมนตรี";
                //dr["new_pos_name"] = "เจ้าหน้าที่วิเคราะห์นโยบายและแผน ส่วนกิจการเฉลิมพระเกียรติ";
                //dr["level_name_full"] = "สำนักแผนงานและกิจการพิเศษสำนักงานปลัดสำนักนายกรัฐมนตรี";
                //dr["new_org_name"] = "สำนักแผนงานและกิจการพิเศษสำนักงานปลัดสำนักนายกรัฐมนตรี";
                //dr["new_org_name3"] = "สำนักนายกรัฐมนตรี";
                //dr["pos_id"] = "2";
                //dr["approve_date"] = "2015-08-12";
                //dt.Rows.Add(dr);

                //dr = dt.NewRow();
                //dr["proj_name"] = "แผนการปฏิบัติงาน งบทำงานและแผนการด้านการเงิน ประจำปีงบประมาณ 2545 ขององค์กรสือสารมวลชนแห่งประเทศไทย";
                //dr["req_by_name"] = "นางสาวเพ็ญจา แต้มบรรจง";
                //dr["old_pos_name"] = "เจ้าหน้าที่วิเคราะห์นโยบายและแผน 5 ส่วนกิจกรรมเฉลิมพระเกียรติ";
                //dr["old_org_name"] = "สำนักแผนงานและกิจการพิเศษสำนักงานปลัดสำนักนายกรัฐมนตรี";
                //dr["old_org_name3"] = "สำนักนายกรัฐมนตรี";
                //dr["new_pos_name"] = "เจ้าหน้าที่วิเคราะห์นโยบายและแผน ส่วนกิจการเฉลิมพระเกียรติ";
                //dr["level_name_full"] = "สำนักแผนงานและกิจการพิเศษสำนักงานปลัดสำนักนายกรัฐมนตรี";
                //dr["new_org_name"] = "สำนักแผนงานและกิจการพิเศษสำนักงานปลัดสำนักนายกรัฐมนตรี";
                //dr["new_org_name3"] = "สำนักนายกรัฐมนตรี";
                //dr["pos_id"] = "2";
                //dr["approve_date"] = "2015-08-12";
                //dt.Rows.Add(dr);

                //dr = dt.NewRow();
                //dr["proj_name"] = "การบริหารจัดการงบประมาณของโครงการเพิ่มขีดสมรรถนะในการกำกับและติดตามการปฏิบัติราชการ ในภูมิภาคของรองนายกรัฐมนตรี พศ 2547";
                //dr["req_by_name"] = "นางสาวสมปอง ยิ่งสุขกลม";
                //dr["old_pos_name"] = "เจ้าหน้าที่วิเคราะห์นโยบายและแผน6ว";
                //dr["old_org_name"] = "ผู้ช่วยตรวจราชการสำนักนายกรัฐมนตรี สำนักตรวจราชการ";
                //dr["old_org_name3"] = "สำนักงานปลัดสำนักนายกรัฐมนตรี สำนักนายกรัฐมนตรี";
                //dr["new_pos_name"] = "เจ้าหน้าที่วิเคราะห์นโยบายและแผน 7 ว";
                //dr["level_name_full"] = "";
                //dr["new_org_name"] = "ผู้ช่วยผู้ตรวจสอบราชการสำนักนายกรัฐมนตรี สำนักตรวจราชการ";
                //dr["new_org_name3"] = "สำนักงานปลัดสำนักนายกรัฐมนตรี สำนักนายกรัฐมนตรี";
                //dr["pos_id"] = "2";
                //dr["approve_date"] = "2015-08-12";
                //dt.Rows.Add(dr);

                //dr = dt.NewRow();
                //dr["proj_name"] = "การแก้ปัญหาการฉ้อฉลหลอกลวงนักท่องเที่ยว";
                //dr["req_by_name"] = "นางสาวสมปอง ยิ่งสุขกลม";
                //dr["old_pos_name"] = "เจ้าหน้าที่วิเคราะห์นโยบายและแผน6ว";
                //dr["old_org_name"] = "ผู้ช่วยตรวจราชการสำนักนายกรัฐมนตรี สำนักตรวจราชการ";
                //dr["old_org_name3"] = "สำนักงานปลัดสำนักนายกรัฐมนตรี สำนักนายกรัฐมนตรี";
                //dr["new_pos_name"] = "เจ้าหน้าที่วิเคราะห์นโยบายและแผน 7 ว";
                //dr["level_name_full"] = "";
                //dr["new_org_name"] = "ผู้ช่วยผู้ตรวจสอบราชการสำนักนายกรัฐมนตรี สำนักตรวจราชการ";
                //dr["new_org_name3"] = "สำนักงานปลัดสำนักนายกรัฐมนตรี สำนักนายกรัฐมนตรี";
                //dr["pos_id"] = "2";
                //dr["approve_date"] = "2015-08-12";
                //dt.Rows.Add(dr);

                //dr = dt.NewRow();
                //dr["proj_name"] = "ร้องเรียนให้เปิดทางเดินเข้าออกซอยพระสิงห์เสนีย์ที่ถูกปิดกั้น";
                //dr["req_by_name"] = "นางสาวสมปอง ยิ่งสุขกลม";
                //dr["old_pos_name"] = "เจ้าหน้าที่วิเคราะห์นโยบายและแผน6ว";
                //dr["old_org_name"] = "ผู้ช่วยตรวจราชการสำนักนายกรัฐมนตรี สำนักตรวจราชการ";
                //dr["old_org_name3"] = "สำนักงานปลัดสำนักนายกรัฐมนตรี สำนักนายกรัฐมนตรี";
                //dr["new_pos_name"] = "เจ้าหน้าที่วิเคราะห์นโยบายและแผน 7 ว";
                //dr["level_name_full"] = "";
                //dr["new_org_name"] = "ผู้ช่วยผู้ตรวจสอบราชการสำนักนายกรัฐมนตรี สำนักตรวจราชการ";
                //dr["new_org_name3"] = "สำนักงานปลัดสำนักนายกรัฐมนตรี สำนักนายกรัฐมนตรี";
                //dr["pos_id"] = "2";
                //dr["approve_date"] = "2015-08-12";
                //dt.Rows.Add(dr);
                #endregion



                if (dt.Rows.Count > 0)
                {
                    int cnt_r = 0;
                    DataTable tmpDT = new DataTable();
                    tmpDT.Columns.Add("proj_name");
                    tmpDT.Columns.Add("req_by_name");
                    tmpDT.Columns.Add("old_pos_name");
                    tmpDT.Columns.Add("old_org_name");
                    tmpDT.Columns.Add("old_org_name3");
                    tmpDT.Columns.Add("new_pos_name");
                    tmpDT.Columns.Add("level_name_full");
                    tmpDT.Columns.Add("new_org_name");
                    tmpDT.Columns.Add("new_org_name3");
                    tmpDT.Columns.Add("pos_id");
                    tmpDT.Columns.Add("approve_date");
                    tmpDT.Columns.Add("req_date");
                    DataRow tmpDR;

                    DataTable dtreqname = new DataTable();
                    dtreqname = dt.DefaultView.ToTable(true, "req_by_name", "old_pos_name", "old_org_name", "old_org_name3", "new_pos_name", "new_org_name", "new_org_name3").Copy();
                    for (int cnt = 0; cnt <= dtreqname.Rows.Count - 1; cnt++)
                    {
                        string req_by_name = dtreqname.Rows[cnt]["req_by_name"].ToString();
                        string old_pos_name = dtreqname.Rows[cnt]["old_pos_name"].ToString();
                        string old_org_name = dtreqname.Rows[cnt]["old_org_name"].ToString();
                        string old_org_name3 = dtreqname.Rows[cnt]["old_org_name3"].ToString();
                        string new_pos_name = dtreqname.Rows[cnt]["new_pos_name"].ToString();
                        string new_org_name = dtreqname.Rows[cnt]["new_org_name"].ToString();
                        string new_org_name3 = dtreqname.Rows[cnt]["new_org_name3"].ToString();

                        dt.DefaultView.RowFilter = "req_by_name ='" + req_by_name + "' and old_pos_name='" + old_pos_name + "' and old_org_name ='" + old_org_name + "' and old_org_name3='" + old_org_name3 + "' and new_pos_name='" + new_pos_name + "' and new_org_name='" + new_org_name + "' and new_org_name3='" + new_org_name3 + "'";
                        DataTable tempDT_Detail = new DataTable();
                        tempDT_Detail = dt.DefaultView.ToTable();

                        string proj_name = "";
                        if (tempDT_Detail.Rows.Count > 0)
                        {
                            tmpDR = tmpDT.NewRow();
                            var sourceRow = tempDT_Detail.Rows[0];
                            tmpDR.ItemArray = sourceRow.ItemArray.Clone() as object[];

                            for (int cnt_dt = 0; cnt_dt <= tempDT_Detail.Rows.Count - 1; cnt_dt++)
                            {
                                cnt_r += 1;
                                proj_name += cnt_r + ". &nbsp;&nbsp;&nbsp;" + tempDT_Detail.Rows[cnt_dt]["proj_name"].ToString() + "</br>";
                                proj_name += "  " + "</br>";
                            }

                            tmpDR["proj_name"] = proj_name;
                            tmpDT.Rows.Add(tmpDR);
                        }


                    }



                    string DateFrom = (!string.IsNullOrEmpty(ReqDateFrom)) ? OPM_BL.GetTHDate(ReqDateFrom) : "-";
                    string DateTo = (!string.IsNullOrEmpty(ReqDateTo)) ? OPM_BL.GetTHDate(ReqDateTo) : "-";
                    if (vReportFormat == "EXCEL")
                    {
                        lblErrorMessage.Text = "";
                        ExcelReportENG.ExportReportSMM_RP09(DateFrom, DateTo, UserName, UserOrg, tmpDT, Response);
                    }
                    else if (vReportFormat == "PDF")
                    {
                        //string printDate = DateTime.Now.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("th-TH"));
                        rpt.Load(Server.MapPath(home_floder + "/" + reportname + ".rpt"));
                        rpt.DataDefinition.FormulaFields["UserName"].Text = "'" + UserName + "'";
                        rpt.DataDefinition.FormulaFields["UserOrg"].Text = "'" + UserOrg + "'";
                        rpt.DataDefinition.FormulaFields["DateFrom"].Text = "'" + DateFrom + "'";
                        rpt.DataDefinition.FormulaFields["DateTo"].Text = "'" + DateTo + "'";
                        rpt.DataDefinition.FormulaFields["PrintDate"].Text = "'" + printDate + "'";
                        rpt.SetDataSource(tmpDT);
                        lblErrorMessage.Text = "";
                        rpt.SetParameterValue("Page_Code", Page_Code);
                        CreatePDFReport(rpt, reportname);
                    }

                }
                else
                {
                    lblErrorMessage.Text = "ไม่พบข้อมูล";
                    CrystalReportViewer1.Visible = false;
                }
                #endregion
            }

            else if (reportname == "SMM_RP10")
            {
                #region "SMM_RP10"
                string start_req = Request.QueryString["start_req"];//สำหรับวันที่เริ่ม-สิ้นสุด วันส่งผลงาน
                string end_req = Request.QueryString["end_req"];
                string start_approve = Request.QueryString["start_approve"];//สำหรับวันที่เริ่ม-สิ้นสุด วันผ่านการประเมิน
                string end_approve = Request.QueryString["end_approve"];
                string start_year = Request.QueryString["start_year"];//สำหรับวันที่เริ่ม-สิ้นสุด ปีงบประมาณ
                string end_year = Request.QueryString["end_year"];
                //string UserName = Request.QueryString["UserName"];
                //string UserOrg = Request.QueryString["UserOrg"];
                string ChartType = Request.QueryString["CHART_TYPE"];
                string UserType = Request.QueryString["USER_TYPE"];
                string ReportType = Request.QueryString["REPORT_TYPE"];
                string PeriodType = Request.QueryString["PERIOD_TYPE"];


                //start_req = "20150101";
                //end_req = "20161230";
                //start_approve = "20160510";
                //end_approve = "20160510";
                //start_year = "2558";
                //end_year = "2559";
                //ChartType = "2";//1 แท่ง, 2 เส้น, 3 วงกลม
                //UserType = "1";//"" ทั้งหมด, 1 ผ่าน, 0 ไม่ผ่าน
                //ReportType = "2";//1 POS, 2 LEVEL
                //UserName = "99999ชื่อสกุล99999";
                //UserOrg = "xxxxxหน่วยงานxxxxx";
                //PeriodType = "1";//1 DATE, 2 YEAR

                string sql = "";
                SqlParameter[] param = new SqlParameter[9];

                #region "_POS"
                if (ReportType == "1")
                {


                    //1 DATE
                    if (PeriodType == "1")
                    {
                        //sql = " select '' BUDGET_YEAR  ," + Environment.NewLine;
                        //sql += " PER.POS_NAME NAME,count(*) as AMOUNT" + Environment.NewLine;
                        //sql += " from SMM_PER_PROMOTE_REQ PRO inner join " + Environment.NewLine;
                        //sql += " (Select ID,NAME,SURNAME" + Environment.NewLine;
                        //sql += " ,iif(ADMIN_NAME is NULL,LINE_NAME,ADMIN_NAME) as POS_NAME" + Environment.NewLine;
                        //sql += " from vw_CMN_PERSON " + Environment.NewLine;
                        //sql += " ) PER on PRO.REQ_BY_ID=PER.ID" + Environment.NewLine;
                        //sql += " Where PRO.APPROVE_STATUS=1 " + Environment.NewLine;

                        sql = "select '' BUDGET_YEAR," + Environment.NewLine;
                        sql += "    PER.POS_NAME NAME,count(*) as AMOUNT" + Environment.NewLine;
                        sql += "from SMM_PER_PROMOTE_REQ PRO inner join " + Environment.NewLine;
                        sql += "    (Select POS_ID,ID--,NAME,SURNAME" + Environment.NewLine;
                        sql += "        ,iif(ADMIN_NAME is NULL,LINE_NAME,ADMIN_NAME) as POS_NAME" + Environment.NewLine;
                        sql += "     from vw_CMN_POSITION" + Environment.NewLine;
                        sql += "     ) PER on PRO.TO_POS_ID=PER.POS_ID" + Environment.NewLine;
                        sql += "Where PRO.APPROVE_STATUS=1 and PER.POS_NAME  is not null" + Environment.NewLine;


                        if (UserType != "")
                        {
                            sql += " AND PRO.PASSED=@_user_type" + Environment.NewLine;  // กรณีระบุที่ผ่านการประเมิน (1=ผ่านการประเมิน ,0=ไม่ผ่านการประเมิน)
                            param[0] = OPM_BL.setParameter("@_user_type", SqlDbType.VarChar, UserType);
                        }
                        if (start_req != null && end_req != null)
                        {
                            sql += " AND convert(varchar(10),PRO.REQ_DATE,112) >=@_start_req AND convert(varchar(10),PRO.REQ_DATE,112) <=@_end_req " + Environment.NewLine;     // กรณีระบุวันที่ส่งผลงาน
                            param[1] = OPM_BL.setParameter("@_start_req", SqlDbType.VarChar, start_req);
                            param[2] = OPM_BL.setParameter("@_end_req", SqlDbType.VarChar, end_req);
                        }
                        if (start_approve != null && end_approve != null)
                        {
                            sql += " AND convert(varchar(10),PRO.APPROVE_DATE,112) >=@_start_approve AND convert(varchar(10),PRO.APPROVE_DATE,112) <=@_end_approve " + Environment.NewLine;  // กรณีระบุวันที่ผ่านการอนุมัติ
                            param[3] = OPM_BL.setParameter("@_start_approve", SqlDbType.VarChar, start_approve);
                            param[4] = OPM_BL.setParameter("@_end_approve", SqlDbType.VarChar, end_approve);
                        }
                        //sql += " Group By PER.POS_NAME" + Environment.NewLine;
                        sql += "Group By PER.POS_NAME" + Environment.NewLine;
                    }

                    // 2 YEAR
                    if (PeriodType == "2")
                    {
                        //sql = " select str(iif(month(REQ_DATE)>9,year(REQ_DATE)+544,year(REQ_DATE)+543)) as BUDGET_YEAR  ," + Environment.NewLine;
                        //sql += " PER.POS_NAME NAME,count(*) as AMOUNT" + Environment.NewLine;
                        //sql += " from SMM_PER_PROMOTE_REQ PRO inner join " + Environment.NewLine;
                        //sql += " (Select ID,NAME,SURNAME" + Environment.NewLine;
                        //sql += " ,iif(ADMIN_NAME is NULL,LINE_NAME,ADMIN_NAME) as POS_NAME" + Environment.NewLine;
                        //sql += " from vw_CMN_PERSON " + Environment.NewLine;
                        //sql += " ) PER on PRO.REQ_BY_ID=PER.ID" + Environment.NewLine;
                        //sql += " Where PRO.APPROVE_STATUS=1 " + Environment.NewLine;
                        sql = " select str(iif(month(REQ_DATE)>9,year(REQ_DATE)+544,year(REQ_DATE)+543)) as BUDGET_YEAR  , " + Environment.NewLine;
                        sql += "      PER.POS_NAME NAME,count(*) as AMOUNT " + Environment.NewLine;
                        sql += "  from  " + Environment.NewLine;
                        sql += "     SMM_PER_PROMOTE_REQ PRO inner join  " + Environment.NewLine;
                        sql += "     (Select POS_ID,ID " + Environment.NewLine;
                        sql += "        ,iif(ADMIN_NAME is NULL,LINE_NAME,ADMIN_NAME) as POS_NAME " + Environment.NewLine;
                        sql += "      from vw_CMN_POSITION  " + Environment.NewLine;
                        sql += "      ) PER on PRO.TO_POS_ID=PER.POS_ID " + Environment.NewLine;
                        sql += "  Where PRO.APPROVE_STATUS=1  and PER.POS_NAME  is not null " + Environment.NewLine;

                        if (UserType != "")
                        {
                            sql += " AND PRO.PASSED=@_user_type" + Environment.NewLine;  // กรณีระบุที่ผ่านการประเมิน (1=ผ่านการประเมิน ,0=ไม่ผ่านการประเมิน)
                            param[0] = OPM_BL.setParameter("@_user_type", SqlDbType.VarChar, UserType);
                        }
                        if (start_year != null && end_year != null)
                        {
                            //sql += " AND iif(month(REQ_DATE)>9,year(REQ_DATE)+544,year(REQ_DATE)+543)>=@_start_year" + Environment.NewLine;//  -- กรณีระบุปีงบประมาณ
                            //sql += " AND iif(month(REQ_DATE)>9,year(REQ_DATE)+544,year(REQ_DATE)+543)<=@_end_year " + Environment.NewLine;
                            sql += "  AND iif(month(REQ_DATE)>9,year(REQ_DATE)+544,year(REQ_DATE)+543)>=@_start_year -- กรณีระบุปีงบประมาณ " + Environment.NewLine;
                            sql += "  AND iif(month(REQ_DATE)>9,year(REQ_DATE)+544,year(REQ_DATE)+543)<=@_end_year  " + Environment.NewLine;

                            param[5] = OPM_BL.setParameter("@_start_year", SqlDbType.VarChar, start_year);
                            param[6] = OPM_BL.setParameter("@_end_year", SqlDbType.VarChar, end_year);
                            //sql += " Group By iif(month(REQ_DATE)>9,year(REQ_DATE)+544,year(REQ_DATE)+543),PER.POS_NAME" + Environment.NewLine;
                        }

                        sql += "  Group By iif(month(REQ_DATE)>9,year(REQ_DATE)+544,year(REQ_DATE)+543),PER.POS_NAME " + Environment.NewLine;
                    }

                }
                #endregion


                #region "_LEVEL"
                if (ReportType == "2")
                {

                    //1 DATE
                    if (PeriodType == "1")
                    {

                        sql = "select '' BUDGET_YEAR ," + Environment.NewLine;
                        sql += "     PER.LEVEL_NAME_FULL as NAME,count(*) as AMOUNT" + Environment.NewLine;
                        sql += "from SMM_PER_PROMOTE_REQ PRO LEFT join " + Environment.NewLine;
                        sql += "      PSST_PER_LEVEL PER on PRO.TO_LEV=PER.CUR_LEV" + Environment.NewLine;
                        sql += "Where PRO.APPROVE_STATUS=1  " + Environment.NewLine;


                        if (UserType != "")
                        {
                            sql += " AND PRO.PASSED=@_user_type" + Environment.NewLine;  // กรณีระบุที่ผ่านการประเมิน (1=ผ่านการประเมิน ,0=ไม่ผ่านการประเมิน)
                            param[0] = OPM_BL.setParameter("@_user_type", SqlDbType.VarChar, UserType);
                        }

                        if (start_req != null && end_req != null)
                        {
                            sql += " AND convert(varchar(10),PRO.REQ_DATE,112) >=@_start_req AND convert(varchar(10),PRO.REQ_DATE,112) <=@_end_req " + Environment.NewLine;     // กรณีระบุวันที่ส่งผลงาน
                            param[1] = OPM_BL.setParameter("@_start_req", SqlDbType.VarChar, start_req);
                            param[2] = OPM_BL.setParameter("@_end_req", SqlDbType.VarChar, end_req);
                        }
                        if (start_approve != null && end_approve != null)
                        {
                            sql += " AND convert(varchar(10),PRO.APPROVE_DATE,112) >=@_start_approve AND convert(varchar(10),PRO.APPROVE_DATE,112) <=@_end_approve " + Environment.NewLine;  // กรณีระบุวันที่ผ่านการอนุมัติ
                            param[3] = OPM_BL.setParameter("@_start_approve", SqlDbType.VarChar, start_approve);
                            param[4] = OPM_BL.setParameter("@_end_approve", SqlDbType.VarChar, end_approve);
                        }
                        sql += "Group By PER.LEVEL_NAME_FULL" + Environment.NewLine;
                    }
                    // 2 YEAR
                    if (PeriodType == "2")
                    {
                        //sql = " select str(iif(month(REQ_DATE)>9,year(REQ_DATE)+544,year(REQ_DATE)+543)) as BUDGET_YEAR ," + Environment.NewLine;
                        //sql += " PER.LEVEL_NAME_FULL NAME,count(*) as AMOUNT" + Environment.NewLine;
                        //sql += " from SMM_PER_PROMOTE_REQ PRO inner join " + Environment.NewLine;
                        //sql += " (Select ID,NAME,SURNAME" + Environment.NewLine;
                        //sql += " ,isnull(LEVEL_NAME_FULL,'') LEVEL_NAME_FULL " + Environment.NewLine;
                        //sql += " from vw_CMN_PERSON " + Environment.NewLine;
                        //sql += " ) PER on PRO.REQ_BY_ID=PER.ID" + Environment.NewLine;
                        //sql += " Where PRO.APPROVE_STATUS=1 " + Environment.NewLine;

                        sql = "select str(iif(month(REQ_DATE)>9,year(REQ_DATE)+544,year(REQ_DATE)+543)) as BUDGET_YEAR , " + Environment.NewLine;
                        sql += "     PER.LEVEL_NAME_FULL NAME,count(*) as AMOUNT " + Environment.NewLine;
                        sql += "from SMM_PER_PROMOTE_REQ PRO LEFT join  " + Environment.NewLine;
                        sql += "      PSST_PER_LEVEL PER on PRO.TO_LEV=PER.CUR_LEV " + Environment.NewLine;
                        sql += "Where PRO.APPROVE_STATUS=1  " + Environment.NewLine;


                        if (UserType != "")
                        {
                            sql += " AND PRO.PASSED=@_user_type" + Environment.NewLine;  // กรณีระบุที่ผ่านการประเมิน (1=ผ่านการประเมิน ,0=ไม่ผ่านการประเมิน)
                            param[0] = OPM_BL.setParameter("@_user_type", SqlDbType.VarChar, UserType);
                        }

                        if (start_year != null && end_year != null)
                        {
                            sql += " AND iif(month(REQ_DATE)>9,year(REQ_DATE)+544,year(REQ_DATE)+543)>=@_start_year" + Environment.NewLine;//  -- กรณีระบุปีงบประมาณ
                            sql += " AND iif(month(REQ_DATE)>9,year(REQ_DATE)+544,year(REQ_DATE)+543)<=@_end_year " + Environment.NewLine;
                            param[5] = OPM_BL.setParameter("@_start_year", SqlDbType.VarChar, start_year);
                            param[6] = OPM_BL.setParameter("@_end_year", SqlDbType.VarChar, end_year);
                            //sql += " Group By iif(month(REQ_DATE)>9,year(REQ_DATE)+544,year(REQ_DATE)+543),PER.LEVEL_NAME_FULL" + Environment.NewLine;
                        }
                        sql += "Group By iif(month(REQ_DATE)>9,year(REQ_DATE)+544,year(REQ_DATE)+543),PER.LEVEL_NAME_FULL " + Environment.NewLine;

                    }

                }
                #endregion

                DataTable dt = OPM_BL.GetDatatable(sql, param);
                if (dt.Rows.Count > 0)
                {

                    #region "_ReportName"
                    //1 DATE 
                    if (PeriodType == "1")
                    {
                        //แท่ง
                        if (ChartType == "1") { reportname = "SMM_RP10_DATE1"; }
                        //เส้น
                        if (ChartType == "2") { reportname = "SMM_RP10_DATE2"; }
                        //วงกลม
                        if (ChartType == "3") { reportname = "SMM_RP10_DATE3"; }
                    }
                    //2 YEAR
                    if (PeriodType == "2")
                    {
                        //แท่ง
                        if (ChartType == "1") { reportname = "SMM_RP10_YEAR1"; }
                        //เส้น
                        if (ChartType == "2") { reportname = "SMM_RP10_YEAR2"; }
                        //วงกลม
                        if (ChartType == "3") { reportname = "SMM_RP10_YEAR3"; }
                    }
                    #endregion

                    rpt.Load(Server.MapPath(home_floder + "/" + reportname + ".rpt"));
                    rpt.DataDefinition.FormulaFields["UserName"].Text = "'" + UserName + "'";
                    rpt.DataDefinition.FormulaFields["UserOrg"].Text = "'" + UserOrg + "'";
                    if (ReportType == "1") { ReportType = "ตำแหน่ง"; }
                    if (ReportType == "2") { ReportType = "ระดับ"; }
                    rpt.DataDefinition.FormulaFields["ReportType"].Text = "'" + ReportType + "'";
                    rpt.DataDefinition.FormulaFields["PrintDate"].Text = "'" + printDate + "'";

                    string strTitle = string.Empty;
                    if (start_req != null && end_req != null)
                    {
                        string DateFrom = OPM_BL.GetTHDate(start_req);
                        string DateTo = OPM_BL.GetTHDate(end_req);
                        strTitle += "วันที่ส่งผลงาน " + DateFrom + " ถึง " + DateTo;

                    }
                    if (start_approve != null && end_approve != null)
                    {
                        string DateFrom = OPM_BL.GetTHDate(start_approve);
                        string DateTo = OPM_BL.GetTHDate(end_approve);
                        strTitle += " วันที่ผ่านการประเมิน " + DateFrom + " ถึง " + DateTo;
                    }

                    if (start_year != null && end_year != null)
                    {
                        strTitle = "";
                        strTitle += "ช่วงปีงบประมาณ " + start_year + " ถึง " + end_year;
                    }

                    //1 DATE 
                    if (PeriodType == "1")
                    {
                        string DateFrom = "";
                        string DateTo = "";
                        if (start_approve != null && end_approve != null)
                        {
                            DateFrom = OPM_BL.GetTHDate(start_approve);
                            DateTo = OPM_BL.GetTHDate(end_approve);
                        }
                        if (UserType == "1") { UserType = "ผ่าน"; }
                        if (UserType == "0") { UserType = "ไม่ผ่าน"; }
                        if (UserType == "") { UserType = "ทั้งหมด"; }
                        rpt.DataDefinition.FormulaFields["UserType"].Text = "'" + UserType + "'";
                        rpt.DataDefinition.FormulaFields["DateFrom"].Text = "'" + DateFrom + "'";
                        rpt.DataDefinition.FormulaFields["DateTo"].Text = "'" + DateTo + "'";
                    }
                    //2 YEAR
                    if (PeriodType == "2")
                    {
                        if (UserType == "1") { UserType = "ผู้ผ่าน"; }
                        if (UserType == "0") { UserType = "ผู้ไม่ผ่าน"; }
                        if (UserType == "") { UserType = "ทั้งหมด"; }
                        rpt.DataDefinition.FormulaFields["UserType"].Text = "'" + UserType + "'";
                        rpt.DataDefinition.FormulaFields["FromYear"].Text = "'" + start_year + "'";
                        rpt.DataDefinition.FormulaFields["ToYear"].Text = "'" + end_year + "'";
                    }

                    rpt.SetDataSource(dt);
                    lblErrorMessage.Text = "";
                    if (vReportFormat == "EXCEL")
                    {
                        lblErrorMessage.Text = "";
                        CreateExcelReport(rpt, reportname);

                    }
                    else if (vReportFormat == "PDF")
                    {
                        rpt.SetParameterValue("strTitle", strTitle);
                        CreatePDFReport(rpt, reportname);
                    }


                }
                else
                {
                    lblErrorMessage.Text = "ไม่พบข้อมูล";
                    CrystalReportViewer1.Visible = false;
                }
                #endregion
            }

            else if (reportname == "SMM_RP11")
            {
                #region "SMM_UT0511"

                //string UserName = Request.QueryString["UserName"];
                //string UserOrg = Request.QueryString["UserOrg"];

                DataTable dt = ReportsEngine.WebReportsENG.GetQueryReportSMM_UT0511(Request);
                if (dt.Rows.Count > 0)
                {
                    if (vReportFormat == "EXCEL")
                    {
                        lblErrorMessage.Text = "";
                        ExcelReportENG.ExportReportSMM_RP11(UserName, UserOrg, dt, Response);
                    }
                    else if (vReportFormat == "PDF")
                    {
                        //rpt.Load(Server.MapPath(home_floder + "/" + reportname + ".rpt"));
                        //rpt.DataDefinition.FormulaFields["UserName"].Text = "'" + UserName + "'";
                        //rpt.DataDefinition.FormulaFields["UserOrg"].Text = "'" + UserOrg + "'";
                        //rpt.DataDefinition.FormulaFields["PrintDate"].Text = "'" + printDate + "'";
                        //rpt.SetDataSource(dt);
                        //lblErrorMessage.Text = "";

                        //CreatePDFReport(rpt, reportname);
                    }
                }
                else
                {
                    lblErrorMessage.Text = "ไม่พบข้อมูล";
                    CrystalReportViewer1.Visible = false;
                }
                dt.Dispose();
                #endregion

            }
            else if (reportname == "SMM_RP12")
            {
                #region "SMM_RP12"

                string person_name = Request.QueryString["Per_FullName"];
                string pos_name = Request.QueryString["POSName"];
                string ORG = Request.QueryString["ORG"];
                string AppDateFrom = Request.QueryString["DateFrom"];//**
                string AppDateTo = Request.QueryString["DateTo"];//**
                string strTitle = Request.QueryString["strTitle"];//**

                //string UserName = Request.QueryString["UserName"];
                //string UserOrg = Request.QueryString["UserOrg"];

                //AppDateFrom = "20160318";
                //AppDateTo = "20160318";
                //org =ส่วนแผนและประเมินผล

                SqlParameter[] p = new SqlParameter[5];
                string sql = "select r.req_by_name, r.req_by_surname, po.pos_name, o.org_name,pj.RECOMMENT, (select dbo.ThaiDateAbbrMonth(r.approve_date)) approve_date " + Environment.NewLine;
                sql += " from SMM_PER_PROMOTE_REQ r " + Environment.NewLine;
                sql += " inner join vw_CMN_POSITION po on po.pos_id=r.to_pos_id and po.per_type=r.to_per_type " + Environment.NewLine;
                sql += " inner join CTLT_ORGANIZE o on o.org_serial=r.to_org_serial " + Environment.NewLine;
                sql += " inner join SMM_PROJECT pj on pj.req_id=r.REQ_ID " + Environment.NewLine;
                sql += " where 1=1 " + Environment.NewLine;
                if (person_name != null)
                {
                    sql += " and (r.req_by_name like '%' + @_PERSON_NAME + '%' or r.req_by_surname like '%' + @_PERSON_NAME + '%') " + Environment.NewLine;
                    p[0] = OPM_BL.setParameter("@_PERSON_NAME", SqlDbType.VarChar, person_name);
                }
                if (pos_name != null)
                {
                    sql += " and po.pos_name like '%' + @_POS_NAME + '%' " + Environment.NewLine;
                    p[1] = OPM_BL.setParameter("@_POS_NAME", SqlDbType.VarChar, pos_name);
                }
                if (ORG != null && ORG != "76")
                {
                    sql += " and o.org_serial = @_ORG_SERIAL " + Environment.NewLine;
                    p[2] = OPM_BL.setParameter("@_ORG_SERIAL", SqlDbType.BigInt, Convert.ToInt64(ORG));
                }

                sql += " and convert(varchar(10),r.approve_date,112) between @_APPROVE_DATE_FROM and @_APPROVE_DATE_TO " + Environment.NewLine;
                sql += " order by r.req_by_name, r.req_by_surname, po.pos_name, o.org_name";
                p[3] = OPM_BL.setParameter("@_APPROVE_DATE_FROM", SqlDbType.VarChar, AppDateFrom);
                p[4] = OPM_BL.setParameter("@_APPROVE_DATE_TO", SqlDbType.VarChar, AppDateTo);

                DataTable dt = OPM_BL.GetDatatable(sql, p);
                if (dt.Rows.Count > 0)
                {
                    string DateFrom = OPM_BL.GetTHDate(AppDateFrom);
                    string DateTo = OPM_BL.GetTHDate(AppDateTo);
                    if (vReportFormat == "EXCEL")
                    {
                        lblErrorMessage.Text = "";
                        ExcelReportENG.ExportReportSMM_RP12(DateFrom, DateTo, UserName, UserOrg, dt, Response);
                    }
                    else if (vReportFormat == "PDF")
                    {
                        rpt.Load(Server.MapPath(home_floder + "/" + reportname + ".rpt"));
                        rpt.DataDefinition.FormulaFields["UserName"].Text = "'" + UserName + "'";
                        rpt.DataDefinition.FormulaFields["UserOrg"].Text = "'" + UserOrg + "'";
                        rpt.DataDefinition.FormulaFields["DateFrom"].Text = "'" + DateFrom + "'";
                        rpt.DataDefinition.FormulaFields["DateTo"].Text = "'" + DateTo + "'";
                        rpt.DataDefinition.FormulaFields["PrintDate"].Text = "'" + printDate + "'";
                        rpt.DataDefinition.FormulaFields["strTitle"].Text = "'" + strTitle + "'";
                        rpt.SetDataSource(dt);
                        lblErrorMessage.Text = "";

                        CreatePDFReport(rpt, reportname);
                    }
                }
                else
                {
                    lblErrorMessage.Text = "ไม่พบข้อมูล";
                    CrystalReportViewer1.Visible = false;
                }
                dt.Dispose();
                #endregion
            }
            else if (reportname == "SMM_RP13")
            {
                #region "SMM_UT0513"

                string ApproveStatus = Request.QueryString["approve_status"];  //null=อยู่ระหว่างพิจารณา,  1=ผ่าน  ,  0=ไม่ผ่าน
                string ReqDateFrom = Request.QueryString["req_date_from"];//**
                string ReqDateTo = Request.QueryString["req_date_to"];//**
                string AppDateFrom = Request.QueryString["approve_date_from"];
                string AppDateTo = Request.QueryString["approve_date_to"];
                string ReportType = Request.QueryString["report_type"]; //1=ตำแหน่ง  ,  2=ทั้งหมด
                string PosID = Request.QueryString["to_pos_id"];
                string PosName = Request.QueryString["to_pos_name"];
                string CurLev = Request.QueryString["to_lev"];
                //string UserName = Request.QueryString["UserName"];
                //string UserOrg = Request.QueryString["UserOrg"];

                //ReqDateFrom = "20150801";
                //ReqDateTo = "20160530";
                //AppDateFrom ="20160318"
                //AppDateTo ="20160318"

                SqlParameter[] p = new SqlParameter[7];
                string sql = "select r.req_by_name, r.req_by_surname, po.pos_name, lev.level_name, po.pos_id,o.org_name  " + Environment.NewLine;
                sql += " from SMM_PER_PROMOTE_REQ r " + Environment.NewLine;
                sql += " inner join vw_CMN_POSITION po on po.pos_id=r.to_pos_id and po.per_type=r.to_per_type " + Environment.NewLine;
                sql += " inner join CTLT_ORGANIZE o on o.org_serial=r.to_org_serial " + Environment.NewLine;
                sql += " inner join vw_CMN_PER_LEVEL lev on lev.cur_lev=r.to_lev and r.to_per_type=lev.per_type " + Environment.NewLine;
                sql += " where 1=1 " + Environment.NewLine;
                if (ApproveStatus != null && ApproveStatus != "null")
                {
                    sql += " and r.approve_status=@_APPROVE_STATUS " + Environment.NewLine;
                    p[0] = OPM_BL.setParameter("@_APPROVE_STATUS", SqlDbType.VarChar, ApproveStatus);
                }
                if (ApproveStatus == "null")
                {
                    sql += " and r.approve_status is null " + Environment.NewLine;
                }
                if (ReqDateFrom != null)
                {
                    sql += " and convert(varchar(10),r.req_date,112) >= @_REQ_DATEFROM " + Environment.NewLine;
                    p[1] = OPM_BL.setParameter("@_REQ_DATEFROM", SqlDbType.VarChar, ReqDateFrom);
                }
                if (ReqDateTo != null)
                {
                    sql += " and convert(varchar(10),r.req_date,112) <= @_REQ_DATETO " + Environment.NewLine;
                    p[2] = OPM_BL.setParameter("@_REQ_DATETO", SqlDbType.VarChar, ReqDateTo);
                }
                if (AppDateFrom != null)
                {
                    sql += " and convert(varchar(10),r.approve_date,112) >= @_APPROVE_DATEFROM  " + Environment.NewLine;
                    p[3] = OPM_BL.setParameter("@_APPROVE_DATEFROM", SqlDbType.VarChar, AppDateFrom);
                }
                if (AppDateTo != null)
                {
                    sql += " and convert(varchar(10),r.approve_date,112) <= @_APPROVE_DATETO " + Environment.NewLine;
                    p[4] = OPM_BL.setParameter("@_APPROVE_DATETO", SqlDbType.VarChar, AppDateTo);
                }
                if (ReportType == "1")
                {
                    sql += " and r.to_pos_id=@_POS_ID " + Environment.NewLine;
                    //sql += " and r.to_lev=@_TO_LEV " + Environment.NewLine;
                    p[5] = OPM_BL.setParameter("@_POS_ID", SqlDbType.VarChar, PosID);

                    if (CurLev != null)
                    {
                        sql += " and lev.level_name like '%' + @_TO_LEV + '%'" + Environment.NewLine;
                        p[6] = OPM_BL.setParameter("@_TO_LEV", SqlDbType.VarChar, CurLev);
                    }
                }
                sql += " order by r.req_by_name, r.req_by_surname, po.pos_name, lev.level_name, po.pos_id";

                DataTable dt = OPM_BL.GetDatatable(sql, p);
                if (dt.Rows.Count > 0)
                {
                    string DateFrom = (!string.IsNullOrEmpty(ReqDateFrom))?OPM_BL.GetTHDate(ReqDateFrom):"-";
                    string DateTo = (!string.IsNullOrEmpty(ReqDateTo)) ? OPM_BL.GetTHDate(ReqDateTo) : "-";
                    string ProjectStatus = "";
                    switch (ApproveStatus)
                    {
                        case null:
                            ProjectStatus = "อยู่ระหว่างพิจารณา";
                            break;
                        case "1":
                            ProjectStatus = "ผ่าน";
                            break;
                        case "0":
                            ProjectStatus = "ไม่ผ่าน";
                            break;
                    }

                    if (vReportFormat == "EXCEL")
                    {
                        lblErrorMessage.Text = "";
                        ExcelReportENG.ExportReportSMM_RP13(ProjectStatus, PosName, DateFrom, DateTo, UserName, UserOrg, dt, Response);
                    }
                    else if (vReportFormat == "PDF")
                    {
                        rpt.Load(Server.MapPath(home_floder + "/" + reportname + ".rpt"));
                        rpt.DataDefinition.FormulaFields["UserName"].Text = "'" + UserName + "'";
                        rpt.DataDefinition.FormulaFields["UserOrg"].Text = "'" + UserOrg + "'";
                        rpt.DataDefinition.FormulaFields["DateFrom"].Text = "'" + DateFrom + "'";
                        rpt.DataDefinition.FormulaFields["DateTo"].Text = "'" + DateTo + "'";
                        rpt.DataDefinition.FormulaFields["PosName"].Text = "'" + PosName + "'";
                        rpt.DataDefinition.FormulaFields["ProjectStatus"].Text = "'" + ProjectStatus + "'";
                        rpt.DataDefinition.FormulaFields["PrintDate"].Text = "'" + printDate + "'";
                        rpt.SetDataSource(dt);
                        lblErrorMessage.Text = "";

                        CreatePDFReport(rpt, reportname);
                    }
                }
                else
                {
                    lblErrorMessage.Text = "ไม่พบข้อมูล";
                    CrystalReportViewer1.Visible = false;
                }
                dt.Dispose();

                #endregion
            }
            else if (reportname == "SMM_UT0402_CONCEPT")
            {
                #region "SMM_UT0402_CONCEPT"

                SqlParameter[] param = new SqlParameter[1];
                string REQ_ID = Request.QueryString["REQ_ID"];
                string UserID = Request.QueryString["UserID"];
                //REQ_ID = "9";
                DataTable uDT = OPM_BL.GetUserNameByID(UserID);

                string sql = "";
                sql += "  SELECT REQ_ID,REQ_BY_NAME + ' ' + REQ_BY_SURNAME REQ_NAME,isnull(FPST.POS_NAME,'-') FROM_POS_NAME," + Environment.NewLine;
                sql += "  CASE ISNULL(FPST.ORG_NAME_LEVEL3,'') + ' ' + ISNULL(FPST.ORG_NAME_LEVEL4,'') WHEN '' THEN '-' END FROM_ORG_NAME," + Environment.NewLine;
                sql += "  (" + Environment.NewLine;
                sql += "  select distinct Stuff((SELECT ', ' + l.ACTION_DETAIL" + Environment.NewLine;
                sql += "  FROM SMM_PROMOTE_ACTION l " + Environment.NewLine;
                sql += "  where l.REQ_ID =REQ.REQ_ID " + Environment.NewLine;
                sql += "  FOR XML PATH('')),1,1,'') ACTION_DETAIL " + Environment.NewLine;
                sql += "  from SMM_PROMOTE_ACTION) ACTION_DETAIL,REQ.CONCEPT,(select dbo.ThaiDateAbbrMonth(REQ.PROCESS_DATE_FROM))  +'-'+" + Environment.NewLine;
                sql += "  (select dbo.ThaiDateAbbrMonth(REQ.PROCESS_DATE_TO))  PROCESS_PERIOD," + Environment.NewLine;
                sql += "  (" + Environment.NewLine;
                sql += "  select distinct Stuff((SELECT ', ' + l.PROJ_NAME" + Environment.NewLine;
                sql += "  FROM SMM_PROJECT l " + Environment.NewLine;
                sql += "  where l.REQ_ID =REQ.REQ_ID " + Environment.NewLine;
                sql += "  FOR XML PATH('')),1,1,'') PROJ_NAME " + Environment.NewLine;
                sql += "  from SMM_PROJECT) PROJ_NAME" + Environment.NewLine;
                sql += "  from SMM_PER_PROMOTE_REQ REQ" + Environment.NewLine;
                sql += "  LEFT JOIN vw_CMN_PERSON FPST ON REQ.FROM_POS_ID=FPST.POS_ID " + Environment.NewLine;
                sql += "  AND REQ.FROM_PER_TYPE=FPST.PER_TYPE AND REQ.FROM_LEV=FPST.CUR_LEV" + Environment.NewLine;

                sql += " where 1=1" + Environment.NewLine;
                if (REQ_ID != null)
                {
                    sql += " and REQ_ID=@_REQ_ID";
                    param[0] = OPM_BL.setParameter("@_REQ_ID", SqlDbType.VarChar, REQ_ID);
                }



                DataTable dt = OPM_BL.GetDatatable(sql, param);
                if (dt.Rows.Count > 0)
                {

                    if (vReportFormat == "EXCEL")
                    {
                        lblErrorMessage.Text = "";
                        //ExcelReportENG.ExportReportSMM_RP04(dt, DTS2, DTS3, DTS4, DTS5, Response);
                    }
                    else if (vReportFormat == "PDF")
                    {
                        rpt.Load(Server.MapPath(home_floder + "/" + reportname + ".rpt"));
                        rpt.DataDefinition.FormulaFields["UserName"].Text = "'" + UserName + "'";
                        rpt.DataDefinition.FormulaFields["UserOrg"].Text = "'" + UserOrg + "'";
                        rpt.DataDefinition.FormulaFields["PrintDate"].Text = "'" + printDate + "'";

                        rpt.SetDataSource(dt);
                        lblErrorMessage.Text = "";

                        CreatePDFReport(rpt, reportname);
                    }



                }
                else
                {
                    lblErrorMessage.Text = "ไม่พบข้อมูล";
                    CrystalReportViewer1.Visible = false;
                }
                #endregion
            }
            else if (reportname == "SMM_UT0402_SCORE")
            {
                #region "SMM_UT0402_SCORE"

                SqlParameter[] param = new SqlParameter[1];
                string REQ_ID = Request.QueryString["REQ_ID"];

                //REQ_ID = "11";


                string sql = "";
                sql += "  select REQ_CODE,REQ_ID,REQ_BY_ID,FPST.LINE_NAME FROM_LINE_NAME,FPST.POS_NAME FROM_POS_NAME,REQ_BY_NAME + ' ' + REQ_BY_SURNAME REQ_NAME,FROM_POS_ID" + Environment.NewLine;
                sql += "  ,ISNULL(FPST.ORG_NAME_LEVEL3,'') + ' ' + ISNULL(FPST.ORG_NAME_LEVEL4,'') FROM_ORG_NAME" + Environment.NewLine;
                sql += "  ,TO_POS_ID,TPST.POS_NAME TO_POS_NAME,ISNULL(TPST.ORG_NAME_LEVEL3,'') + ' ' + ISNULL(TPST.ORG_NAME_LEVEL4,'') TO_ORG_NAME" + Environment.NewLine;
                sql += "  from SMM_PER_PROMOTE_REQ REQ" + Environment.NewLine;
                sql += "  LEFT JOIN vw_CMN_PERSON FPST ON REQ.FROM_POS_ID=FPST.POS_ID AND REQ.FROM_PER_TYPE=FPST.PER_TYPE AND REQ.FROM_LEV=FPST.CUR_LEV" + Environment.NewLine;
                sql += "  LEFT JOIN vw_CMN_PERSON TPST ON REQ.TO_POS_ID=TPST.POS_ID AND REQ.TO_PER_TYPE=TPST.PER_TYPE AND REQ.TO_LEV=TPST.CUR_LEV" + Environment.NewLine;

                sql += " where 1=1" + Environment.NewLine;
                if (REQ_ID != null)
                {
                    sql += " and REQ_ID=@_REQ_ID";
                    param[0] = OPM_BL.setParameter("@_REQ_ID", SqlDbType.VarChar, REQ_ID);
                }



                DataTable dt = OPM_BL.GetDatatable(sql, param);
                if (dt.Rows.Count > 0)
                {
                    sql = " SELECT p.PROJ_ID,p.REQ_ID,p.PROJ_SEQ,p.PROJ_NAME,p.PROJ_DESCRIPTION,r.CONCEPT" + Environment.NewLine;
                    sql += " FROM SMM_PROJECT p inner join SMM_PER_PROMOTE_REQ r on p.REQ_ID = r.REQ_ID" + Environment.NewLine;
                    DataTable DTS2 = new DataTable();
                    DTS2 = OPM_BL.GetDatatable(sql, null);


                    sql = " SELECT LS.REQ_ID,LC.LINE_CODE ,LC.PER_TYPE ,LC.COND_SEQ ,COND_NAME,GET_SCORE,MAX_SCORE" + Environment.NewLine;
                    sql += " FROM SMM_PROMOTE_LINE_COND LC INNER JOIN SMM_PROMOTE_LINE_SCORE LS ON LC.LINE_CODE=LS.LINE_CODE AND  LC.PER_TYPE=LS.PER_TYPE AND LC.COND_SEQ = LS.COND_SEQ" + Environment.NewLine;
                    DataTable DTS3 = new DataTable();
                    DTS3 = OPM_BL.GetDatatable(sql, null);


                    if (vReportFormat == "EXCEL")
                    {
                        lblErrorMessage.Text = "";
                        //ExcelReportENG.ExportReportSMM_RP04(dt, DTS2, DTS3, DTS4, DTS5, Response);
                    }
                    else if (vReportFormat == "PDF")
                    {
                        rpt.Load(Server.MapPath(home_floder + "/" + reportname + ".rpt"));
                        rpt.DataDefinition.FormulaFields["UserName"].Text = "'" + UserName + "'";
                        rpt.DataDefinition.FormulaFields["UserOrg"].Text = "'" + UserOrg + "'";
                        rpt.DataDefinition.FormulaFields["PrintDate"].Text = "'" + printDate + "'";
                        rpt.Subreports["SMM_UT0402_PROJECT"].SetDataSource(DTS2);
                        rpt.Subreports["SMM_UT0402_SCORE"].SetDataSource(DTS3);

                        rpt.SetDataSource(dt);
                        lblErrorMessage.Text = "";

                        CreatePDFReport(rpt, reportname);
                    }



                }
                else
                {
                    lblErrorMessage.Text = "ไม่พบข้อมูล";
                    CrystalReportViewer1.Visible = false;
                }
                #endregion
            }
            else if (reportname == "SMM_UT0403")
            {
                #region "SMM_UT0403"

                SqlParameter[] param = new SqlParameter[7];
                string REQ_ID = Request.QueryString["REQ_ID"];
                string TypeName = Request.QueryString["TypeName"];
                string POSName = Request.QueryString["POSName"];
                string REQ_BY_ID = Request.QueryString["ReqBy"];
                string REQ_DATE_From = Request.QueryString["REQ_DATE_From"];
                string REQ_DATE_To = Request.QueryString["REQ_DATE_To"];
                string Lev = Request.QueryString["Lev"];
                string strTitle = Request.QueryString["strTitle"];

                //REQ_ID = "9";
                //TypeName = "11";
                //ORG = "2";
                //POSName = "ปลัดสำนักนายกรัฐมนตรี";
                //POSName = "";
                //Per_FullName = "นพปฎล";

                string sql = "";
                sql += " SELECT REQ_ID,REQ_BY_ID,FROM_POS_ID,FPST.POS_NAME FROM_POS_NAME,FROM_LEV,FPST.LEVEL_NAME FROM_LEVEL_NAME,FROM_PER_TYPE,FPST.PER_TYPE_NAME FROM_PER_TYPE_NAME," + Environment.NewLine;
                sql += " FROM_LENGTH_CODE,FROM_ORG_SERIAL,ISNULL(FPST.ORG_NAME_LEVEL3,'') + ' ' + ISNULL(FPST.ORG_NAME_LEVEL4,'') FROM_ORG_NAME," + Environment.NewLine;
                sql += " FPST.ADMIN_CODE FROM_ADMIN_CODE,FPST.ADMIN_NAME FROM_ADMIN_NAME,FPST.LINE_CODE FROM_LINE_CODE,FPST.LINE_NAME FROM_LINE_NAME," + Environment.NewLine;
                sql += " TO_POS_ID,TPST.POS_NAME TO_POS_NAME,TO_LEV,TPST.LEVEL_NAME TO_LEVEL_NAME,TO_PER_TYPE,TPST.PER_TYPE_NAME TO_PER_TYPE_NAME," + Environment.NewLine;
                sql += " TO_LENGTH_CODE,TO_ORG_SERIAL,ISNULL(TPST.ORG_NAME_LEVEL3,'') + ' ' + ISNULL(TPST.ORG_NAME_LEVEL4,'') TO_ORG_NAME," + Environment.NewLine;
                sql += " TPST.ADMIN_CODE TO_ADMIN_CODE,TPST.ADMIN_NAME TO_ADMIN_NAME,TPST.LINE_CODE TO_LINE_CODE,TPST.LINE_NAME TO_LINE_NAME," + Environment.NewLine;
                sql += " CONCEPT,PROCESS_DATE_FROM,PROCESS_DATE_TO,(select dbo.Thaidate(APPROVE_DATE)) APPROVE_DATE,(select dbo.Thaidate(REQ_DATE)) REQ_DATE,REQ_CODE," + Environment.NewLine;
                sql += " PST.NAME + ' ' + PST.SURNAME FullName,(select dbo.Thaidate(PST.ENTER_DATE)) ENTER_DATE, (select dbo.Thaidate(PST.POS_DATE)) POS_DATE,PST.PREFIX_CODE,PST.NAME,PST.SURNAME,PST.PER_TYPE,''enter_date_period,''pos_date_period,''line_age " + Environment.NewLine;
                sql += " from SMM_PER_PROMOTE_REQ REQ" + Environment.NewLine;
                sql += " LEFT JOIN vw_CMN_PERSON FPST ON REQ.FROM_POS_ID=FPST.POS_ID AND REQ.FROM_PER_TYPE=FPST.PER_TYPE AND REQ.FROM_LEV=FPST.CUR_LEV" + Environment.NewLine;
                sql += " LEFT JOIN vw_CMN_PERSON TPST ON REQ.TO_POS_ID=TPST.POS_ID AND REQ.TO_PER_TYPE=TPST.PER_TYPE AND REQ.TO_LEV=TPST.CUR_LEV" + Environment.NewLine;
                sql += " LEFT JOIN vw_CMN_PERSON PST ON REQ.REQ_BY_ID=PST.ID " + Environment.NewLine;

                sql += " where 1=1" + Environment.NewLine;
                if (REQ_ID != null)
                {
                    sql += " and REQ_ID=@_REQ_ID";
                    param[0] = OPM_BL.setParameter("@_REQ_ID", SqlDbType.VarChar, REQ_ID);
                }

                if (TypeName != null)
                {
                    sql += "  and FROM_PER_TYPE =@_PER_TYPE" + Environment.NewLine;
                    param[1] = OPM_BL.setParameter("@_PER_TYPE", SqlDbType.VarChar, TypeName);
                }
                if (POSName != null)
                {
                    //sql += " and (ISNULL(AMC.ADMIN_NAME,'') like '%' + @_POSNAME + '%'  or ISNULL(LINE.LINE_NAME,'') like '%' + @_POSNAME + '%')" + Environment.NewLine;
                    sql += "and (FROM_POS_ID Like '%' + @_POSNAME + '%' or FPST.POS_NAME Like '%' + @_POSNAME + '%')";
                    param[2] = OPM_BL.setParameter("@_POSNAME", SqlDbType.VarChar, POSName);
                }

                if (REQ_BY_ID != null)
                {
                    sql += " and REQ_BY_ID = @_REQ_BY_ID" + Environment.NewLine;
                    param[3] = OPM_BL.setParameter("@_REQ_BY_ID", SqlDbType.VarChar, REQ_BY_ID);
                }
                if (REQ_DATE_From != null && REQ_DATE_To != null)
                {
                    param[4] = OPM_BL.setParameter("@_REQ_DATE_From", SqlDbType.VarChar, REQ_DATE_From.Replace("'", "''"));
                    param[5] = OPM_BL.setParameter("@_REQ_DATE_To", SqlDbType.VarChar, REQ_DATE_To.Replace("'", "''"));
                    sql += " AND (Convert(varchar(8),REQ_DATE,112) between @_REQ_DATE_From and @_REQ_DATE_To)" + Environment.NewLine;
                }
                if (Lev != null)
                {
                    sql += "and (FROM_LEV Like '%' + @_LEV + '%' or   FPST.LEVEL_NAME Like '%' + @_LEV + '%')";
                    param[6] = OPM_BL.setParameter("@_LEV", SqlDbType.VarChar, Lev);
                }

                DataTable dt = OPM_BL.GetDatatable(sql, param);
                if (dt.Rows.Count > 0)
                {

                    //for (int i = 0; i <= dt.Rows.Count -1; i++)
                    //{ 


                    DateTime enter_date;
                    DateTime pos_date;
                    string display_enter_date = "";
                    string enter_date_period = "";
                    string pos_date_period = "";
                    //string _req_id = dt.Rows[i]["REQ_ID"].ToString();
                    try
                    {
                        if (Convert.IsDBNull(dt.Rows[0]["ENTER_DATE"]) == false)
                        {
                            enter_date = Convert.ToDateTime(dt.Rows[0]["ENTER_DATE"]);
                            enter_date_period = OPM_BL.getyearmonthday_DatediffFromCurrentDate(enter_date);
                            display_enter_date = enter_date.ToString("dd MMMM yyyy", new System.Globalization.CultureInfo("th-TH"));
                        }
                        if (Convert.IsDBNull(dt.Rows[0]["POS_DATE"]) == false)
                        {
                            pos_date = Convert.ToDateTime(dt.Rows[0]["POS_DATE"]);
                            pos_date_period = OPM_BL.getyearmonthday_DatediffFromCurrentDate(pos_date);
                        }
                        dt.Rows[0]["enter_date_period"] = enter_date_period;
                        dt.Rows[0]["pos_date_period"] = pos_date_period;
                    }
                    catch (Exception ex)
                    {
                    }


                    //section2 PROJECT
                    sql = " SELECT p.PROJ_ID,p.REQ_ID,p.PROJ_SEQ,p.PROJ_NAME,p.PROJ_DESCRIPTION,p.RECOMMENT ," + Environment.NewLine;
                    sql += " t.PROJ_ID,str(t.TEAM_ID )as Seq,t.TEAM_NAME" + Environment.NewLine;
                    sql += " FROM SMM_PROJECT p left join SMM_PROJECT_TEAM t on p.PROJ_ID=t.PROJ_ID" + Environment.NewLine;
                    sql += " WHERE p.REQ_ID is not null " + Environment.NewLine;
                    //sql += " and p.REQ_ID =@_REQ_ID";
                    DataTable DTS2 = new DataTable();
                    //SqlParameter[] p2 = new SqlParameter[1];
                    //p2[0] = OPM_BL.setParameter("@_REQ_ID", SqlDbType.VarChar, REQ_ID);
                    DTS2 = OPM_BL.GetDatatable(sql, null);

                    //section3 ACTION
                    sql = " SELECT a.REQ_ID,str(a.ACTION_ID) as Seq,a.ACTION_DETAIL,r.CONCEPT,(select dbo.Thaidate(r.PROCESS_DATE_FROM)) PROCESS_DATE_FROM,(select dbo.Thaidate(r.PROCESS_DATE_TO)) PROCESS_DATE_TO " + Environment.NewLine;
                    sql += " FROM SMM_PROMOTE_ACTION  a right join SMM_PER_PROMOTE_REQ r on a.REQ_ID = r.REQ_ID " + Environment.NewLine;
                    sql += " WHERE a.REQ_ID is not null";
                    //sql += " and a.REQ_ID =@_REQ_ID";
                    DataTable DTS3 = new DataTable();
                    //SqlParameter[] p3 = new SqlParameter[1];
                    //p3[0] = OPM_BL.setParameter("@_REQ_ID", SqlDbType.VarChar, REQ_ID);
                    DTS3 = OPM_BL.GetDatatable(sql, null);

                    //section4 SCORE
                    sql = " select * from (" + Environment.NewLine;
                    sql += " SELECT REQ_ID,TPST.LINE_CODE TO_LINE_CODE,REQ.TO_PER_TYPE,case PASSED when 'true' then 'ผ่าน' when 'false' then'ไม่ผ่าน' else 'รอการประเมิน' end PASSED,RECOMMENT,REMARK,APPROVE_STATUS,(select dbo.Thaidate(APPROVE_DATE)) APPROVE_DATE" + Environment.NewLine;
                    sql += " from SMM_PER_PROMOTE_REQ REQ " + Environment.NewLine;
                    sql += " LEFT JOIN vw_CMN_PERSON TPST ON REQ.TO_POS_ID=TPST.POS_ID AND REQ.TO_PER_TYPE=TPST.PER_TYPE AND REQ.TO_LEV=TPST.CUR_LEV" + Environment.NewLine;
                    sql += " WHERE REQ_ID is not null  " + Environment.NewLine;
                    //sql += " and REQ_ID =@_REQ_ID";
                    sql += " ) T1 left join " + Environment.NewLine;
                    sql += " (SELECT LS.REQ_ID,LC.LINE_CODE ,LC.PER_TYPE ,LC.COND_SEQ ,COND_NAME,GET_SCORE,MAX_SCORE" + Environment.NewLine;
                    sql += " FROM SMM_PROMOTE_LINE_COND LC INNER JOIN SMM_PROMOTE_LINE_SCORE LS ON LC.LINE_CODE=LS.LINE_CODE AND  LC.PER_TYPE=LS.PER_TYPE AND LC.COND_SEQ = LS.COND_SEQ" + Environment.NewLine;
                    sql += " WHERE LS.REQ_ID is not null " + Environment.NewLine;
                    //sql += " and REQ_ID =@_REQ_ID";
                    sql += " ) T2 on T1.REQ_ID=T2.REQ_ID" + Environment.NewLine;
                    sql += " ORDER BY COND_SEQ" + Environment.NewLine;
                    DataTable DTS4 = new DataTable();
                    //SqlParameter[] p4 = new SqlParameter[1];
                    //p4[0] = OPM_BL.setParameter("@_REQ_ID", SqlDbType.VarChar, REQ_ID);
                    DTS4 = OPM_BL.GetDatatable(sql, null);



                    //}

                    if (vReportFormat == "EXCEL")
                    {
                        lblErrorMessage.Text = "";
                        ExcelReportENG.ExportReportSMM_UT0403(dt, DTS2, DTS3, DTS4, Response);
                    }
                    else if (vReportFormat == "PDF")
                    {
                        rpt.Load(Server.MapPath(home_floder + "/" + reportname + ".rpt"));
                        rpt.DataDefinition.FormulaFields["UserName"].Text = "'" + UserName + "'";
                        rpt.DataDefinition.FormulaFields["UserOrg"].Text = "'" + UserOrg + "'";
                        rpt.DataDefinition.FormulaFields["PrintDate"].Text = "'" + printDate + "'";
                        rpt.DataDefinition.FormulaFields["strTitle"].Text = "'" + strTitle + "'";

                        rpt.Subreports["SMM_UT0403_PROJECT"].SetDataSource(DTS2);
                        rpt.Subreports["SMM_UT0403_RECOMENT"].SetDataSource(DTS3);
                        rpt.Subreports["SMM_UT0403_SCORE"].SetDataSource(DTS4);

                        rpt.SetDataSource(dt);
                        lblErrorMessage.Text = "";

                        CreatePDFReport(rpt, reportname);
                    }



                }
                else
                {
                    lblErrorMessage.Text = "ไม่พบข้อมูล";
                    CrystalReportViewer1.Visible = false;
                }
                #endregion
            }
            else if (reportname == "SMM_UT0405")
            {
                #region "SMM_UT0405"
                string Project_Name = Request.QueryString["Project_Name"];
                string Project_Description = Request.QueryString["Project_Description"];
                string Req_Code = Request.QueryString["Req_Code"];
                string Req_Name = Request.QueryString["Req_Name"];
                string Req_Date_From = Request.QueryString["Req_Date_From"];
                string Req_Date_To = Request.QueryString["Req_Date_To"];
                string Appv_Date_From = Request.QueryString["Appv_Date_From"];
                string Appv_Date_To = Request.QueryString["Appv_Date_To"];

                string sql = "SELECT DISTINCT REQ.REQ_ID,REQ.REQ_CODE,PROJ_NAME,PROJ_DESCRIPTION,FRE.PREFIX_NAME + '' + REQ_BY_NAME + ' ' + REQ_BY_SURNAME AS REQ_NAME," + Environment.NewLine;
                sql += " (select distinct Stuff((SELECT ', ' + l.TEAM_NAME" + Environment.NewLine;
                sql += " FROM SMM_PROJECT_TEAM l " + Environment.NewLine;
                sql += " where l.PROJ_ID =PRO.PROJ_ID" + Environment.NewLine;
                sql += " FOR XML PATH('')),1,1,'') TEAM_NAME " + Environment.NewLine;
                sql += " from SMM_PROJECT_TEAM) PROJECT_TEAM,(select dbo.ThaiDateAbbrMonth(REQ_DATE)) REQ_DATE, (select dbo.ThaiDateAbbrMonth(APPROVE_DATE)) APPROVE_DATE " + Environment.NewLine;
                sql += " FROM SMM_PER_PROMOTE_REQ  REQ " + Environment.NewLine;
                sql += " INNER JOIN SMM_PROJECT PRO ON REQ.REQ_ID = PRO.REQ_ID" + Environment.NewLine;
                sql += " LEFT JOIN SMM_PROJECT_TEAM PJT ON PRO.PROJ_ID = PJT.PROJ_ID" + Environment.NewLine;
                sql += " LEFT JOIN CTLT_PREFIX_CODE FRE ON REQ.REQ_BY_PREFIX_CODE = FRE.PREFIX_CODE";
                sql += " Where ISNULL(APPROVE_STATUS,'') ='1' ";

                SqlParameter[] param = new SqlParameter[8];
                if (Project_Name != null)
                {
                    param[0] = OPM_BL.setParameter("@_PROJ_NAME", SqlDbType.VarChar, Project_Name.Replace("'", "''"));
                    sql += " AND PROJ_NAME like '%' + @_PROJ_NAME + '%'";
                }
                if (Project_Description != null)
                {
                    param[1] = OPM_BL.setParameter("@_PROJ_DESCRIPTION", SqlDbType.VarChar, Project_Description.Replace("'", "''"));
                    sql += " AND PROJ_DESCRIPTION like '%' + @_PROJ_DESCRIPTION + '%'";
                }
                if (Req_Code != null)
                {
                    param[2] = OPM_BL.setParameter("@_REQ_CODE", SqlDbType.VarChar, Req_Code.Replace("'", "''"));
                    sql += " AND REQ.REQ_CODE like '%' + @_REQ_CODE + '%'";
                }
                if (Req_Name != null)
                {
                    param[3] = OPM_BL.setParameter("@_REQ_NAME", SqlDbType.VarChar, Req_Name.Replace("'", "''"));
                    sql += " AND (FRE.PREFIX_NAME like '%' + @_REQ_NAME + '%' or REQ_BY_NAME  like '%' + @_REQ_NAME + '%' or REQ_BY_SURNAME like '%' + @_REQ_NAME + '%' OR ( FRE.PREFIX_NAME + '' + REQ_BY_NAME + ' ' + REQ_BY_SURNAME LIKE  '%' + @_REQ_NAME + '%' ) )";
                }
                if (Req_Date_From != null && Req_Date_To != null)
                {
                    param[4] = OPM_BL.setParameter("@_REQ_DATE_FROM", SqlDbType.VarChar, Req_Date_From.Replace("'", "''"));
                    param[5] = OPM_BL.setParameter("@_REQ_DATE_TO", SqlDbType.VarChar, Req_Date_To.Replace("'", "''"));
                    sql += " AND (Convert(varchar(8),REQ_DATE,112) between @_REQ_DATE_FROM and @_REQ_DATE_TO )" + Environment.NewLine;
                }
                if (Appv_Date_From != null && Appv_Date_To != null)
                {
                    param[6] = OPM_BL.setParameter("@_APPROVE_DATE", SqlDbType.VarChar, Appv_Date_From.Replace("'", "''"));
                    param[7] = OPM_BL.setParameter("@_APPROVE_TO", SqlDbType.VarChar, Appv_Date_To.Replace("'", "''"));
                    sql += " AND (Convert(varchar(8),APPROVE_DATE,112) between @_APPROVE_DATE and @_APPROVE_TO)" + Environment.NewLine;
                }

                DataTable dt = OPM_BL.GetDatatable(sql, param);
                if (dt.Rows.Count > 0)
                {

                    if (vReportFormat == "PDF")
                    {
                        string DateFrom = "-";
                        string DateTo = "-";
                        if (Appv_Date_From != null)
                        {
                            DateFrom = OPM_BL.GetTHDate(Appv_Date_From);
                        }
                        if (Appv_Date_To != null)
                        {
                            DateTo = OPM_BL.GetTHDate(Appv_Date_To);
                        }
                        rpt.Load(Server.MapPath(home_floder + "/" + reportname + ".rpt"));
                        rpt.DataDefinition.FormulaFields["UserName"].Text = "'" + UserName + "'";
                        rpt.DataDefinition.FormulaFields["UserOrg"].Text = "'" + UserOrg + "'";
                        rpt.DataDefinition.FormulaFields["DateFrom"].Text = "'" + DateFrom + "'";
                        rpt.DataDefinition.FormulaFields["DateTo"].Text = "'" + DateTo + "'";
                        rpt.DataDefinition.FormulaFields["PrintDate"].Text = "'" + printDate + "'";
                        rpt.SetDataSource(dt);
                        lblErrorMessage.Text = "";

                        CreatePDFReport(rpt, reportname);
                    }


                }
                else
                {
                    lblErrorMessage.Text = "ไม่พบข้อมูล";
                    CrystalReportViewer1.Visible = false;
                }
                #endregion
            }
            else if (reportname == "SMM_UT0119")
            {
                #region "SMM_UT0119"

                string ADMIN_CODE = Request.QueryString["ADMIN_CODE"];
                string LINE_CODE = Request.QueryString["LINE_CODE"];
                string PER_TYPE = Request.QueryString["PER_TYPE"];
                string CUR_LEV = Request.QueryString["CUR_LEV"];

                //ADMIN_CODE = "";
                //LINE_CODE = "511013";
                //PER_TYPE = "11";
                //CUR_LEV = "16";   

                string sql = "";
                sql += " select S.ITEM_ID,Item.ITEM_NAME,PASS_SCORE,TYPE.TYPE_NAME,ITEM.TYPE_ID,S.ADMIN_CODE,S.LINE_CODE,S.PER_TYPE,S.CUR_LEV,V.POS_NAME,V.LEVEL_NAME_FULL " + Environment.NewLine;
                sql += " from SMM_JD_EVAL_SCORE S " + Environment.NewLine;
                sql += " INNER JOIN SMM_JD_EVAL_ITEM ITEM ON S.ITEM_ID=ITEM.ITEM_ID " + Environment.NewLine;
                sql += " INNER JOIN SMM_JD_EVAL_TYPE TYPE ON ITEM.TYPE_ID = TYPE.TYPE_ID " + Environment.NewLine;
                sql += " INNER JOIN vw_SMM_JD_REQUIRE V on ISNULL(V.ADMIN_CODE,0) = ISNULL(S.ADMIN_CODE,0) AND V.LINE_CODE = S.LINE_CODE AND V.PER_LEVEL = S.CUR_LEV AND PER_TYPE=@_PER_TYPE " + Environment.NewLine;
                sql += " where 1=1  ";//and ISNULL(S.ADMIN_CODE,'')='' and S.LINE_CODE='511013' and S.PER_TYPE ='11'  and S.CUR_LEV ='16'";

                SqlParameter[] param = new SqlParameter[4];
                if (ADMIN_CODE != null)
                {
                    sql += " and ISNULL(S.ADMIN_CODE,'')=@_ADMIN_CODE";
                    param[0] = OPM_BL.setParameter("@_ADMIN_CODE", SqlDbType.VarChar, ADMIN_CODE);
                }

                if (LINE_CODE != null)
                {
                    sql += "  and S.LINE_CODE =@_LINE_CODE" + Environment.NewLine;
                    param[1] = OPM_BL.setParameter("@_LINE_CODE", SqlDbType.VarChar, LINE_CODE);
                }
                if (PER_TYPE != null)
                {
                    sql += "  and S.PER_TYPE =@_PER_TYPE" + Environment.NewLine;
                    param[2] = OPM_BL.setParameter("@_PER_TYPE", SqlDbType.VarChar, PER_TYPE);
                }
                if (CUR_LEV != null)
                {
                    sql += "  and S.CUR_LEV =@_CUR_LEV" + Environment.NewLine;
                    param[3] = OPM_BL.setParameter("@_CUR_LEV", SqlDbType.VarChar, CUR_LEV);
                }

                DataTable dt = OPM_BL.GetDatatable(sql, param);
                if (dt.Rows.Count > 0)
                {
                    rpt.Load(Server.MapPath(home_floder + "/" + reportname + ".rpt"));
                    rpt.DataDefinition.FormulaFields["UserName"].Text = "'" + UserName + "'";
                    rpt.DataDefinition.FormulaFields["UserOrg"].Text = "'" + UserOrg + "'";
                    rpt.DataDefinition.FormulaFields["PrintDate"].Text = "'" + printDate + "'";

                    rpt.SetDataSource(dt);
                    lblErrorMessage.Text = "";
                    CrystalReportViewer1.Visible = true;
                    CreatePDFReport(rpt, reportname);
                }
                else
                {
                    lblErrorMessage.Text = "ไม่พบข้อมูล";
                    CrystalReportViewer1.Visible = false;
                }
                #endregion
            }
            #endregion //"__SMM"

            #region "__ODS"
            else if (reportname == "ODS_RP01")
            {
                #region "ODS_RP01"

                string SysCode = Request.QueryString["SysCode"];
                string POSName = Request.QueryString["POSName"];
                string ORGName = Request.QueryString["ORGName"];
                string StaffName = Request.QueryString["StaffName"];


                //SysCode = "ODS";//*****Req
                //POSName = "";//"429";
                //ORGName = "สำนักแผนงานและกิจการพิเศษ";
                //StaffName = "อดิศร";
                //UserName = "99999ชื่อสกุล99999";
                //UserOrg = "xxxxxหน่วยงานxxxxx";



                SqlParameter[] param = new SqlParameter[4];
                string sql = " " + Environment.NewLine;
                sql += " select  p.prefix_name, p.name, p.surname, p.pos_name +''+ isnull(Level_Name,'') pos_name, p.pos_id," + Environment.NewLine;
                sql += " p.org_serial, p.org_name_level3, isnull(p.org_name_level4,p.org_name_level3) org_name_level4 ,p.org_abbr, s.sys_code,s.sys_name," + Environment.NewLine;
                sql += " p.email,p.user_code, sr.role_name ,(select [dbo].[ThaiDateAbbrMonth](p.user_efft_date)) user_efft_date ,(select [dbo].[ThaiDateAbbrMonth](p.user_exp_date)) user_exp_date" + Environment.NewLine;
                sql += " from ODS_USER_ROLE ur" + Environment.NewLine;
                sql += " inner join ODS_SYSTEM s on s.sys_code=ur.SYS_CODE" + Environment.NewLine;
                sql += " inner join ODS_SYSTEM_ROLE sr on sr.sys_code=ur.sys_code and sr.role_id=ur.role_id" + Environment.NewLine;
                sql += " inner join vw_CMN_PERSON p on p.user_code=ur.user_code" + Environment.NewLine;
                sql += " where 1=1" + Environment.NewLine;
                if (SysCode != null)
                {
                    sql += " and ur.sys_code=@_SYS_CODE" + Environment.NewLine;
                    param[0] = OPM_BL.setParameter("@_SYS_CODE", SqlDbType.VarChar, SysCode);
                }

                if (POSName != null)
                {
                    sql += " and (p.pos_id like '%' + @_POS_NAME + '%' or p.pos_name like '%' + @_POS_NAME + '%')" + Environment.NewLine;
                    param[1] = OPM_BL.setParameter("@_POS_NAME", SqlDbType.VarChar, POSName);
                }

                if (ORGName != null)
                {
                    sql += " and (p.org_name_level3 like '%' + @_ORG_NAME + '%' or  p.org_name_level4 like '%' + @_ORG_NAME + '%')" + Environment.NewLine;
                    param[2] = OPM_BL.setParameter("@_ORG_NAME", SqlDbType.VarChar, ORGName);
                }
                if (StaffName != null)
                {
                    sql += " and isnull(p.prefix_name,'')+isnull(p.name,'') + ' ' + isnull(p.surname,'') like '%' + @_STAFF_NAME + '%'";
                    param[3] = OPM_BL.setParameter("@_STAFF_NAME", SqlDbType.VarChar, StaffName);
                }
                sql += " order by p.org_name_level3,p.org_name_level4,p.name, p.surname";


                DataTable dt = OPM_BL.GetDatatable(sql, param);
                DataRow dr;


                if (dt.Rows.Count > 0)
                {

                    if (vReportFormat == "EXCEL")
                    {
                        lblErrorMessage.Text = "";
                        ExcelReportENG.ExportReportODS_RP01(UserName, UserOrg, dt, Response);
                    }
                    else if (vReportFormat == "PDF")
                    {
                        rpt.Load(Server.MapPath(home_floder + "/" + reportname + ".rpt"));
                        rpt.DataDefinition.FormulaFields["UserName"].Text = "'" + UserName + "'";
                        rpt.DataDefinition.FormulaFields["UserOrg"].Text = "'" + UserOrg + "'";
                        //string printDate = DateTime.Now.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("th-TH"));
                        rpt.DataDefinition.FormulaFields["PrintDate"].Text = "'" + printDate + "'";
                        rpt.SetDataSource(dt);
                        lblErrorMessage.Text = "";

                        CreatePDFReport(rpt, reportname);
                    }
                }
                else
                {
                    lblErrorMessage.Text = "ไม่พบข้อมูล";
                    CrystalReportViewer1.Visible = false;
                }
                #endregion

            }
            else if (reportname == "ODS_RP02")
            {
                #region "ODS_RP02"
                string POSName = Request.QueryString["POSName"];
                string ORGName = Request.QueryString["ORGName"];
                string StaffName = Request.QueryString["StaffName"];


                //POSName = "";//"นักวิชาการคอมพิวเตอร์";
                //ORGName = "";//"สารสนเทศ";
                //StaffName = "";//"จันธิรา";
                //UserName = "99999ชื่อสกุล99999";
                //UserOrg = "xxxxxหน่วยงานxxxxx";

                SqlParameter[] param = new SqlParameter[3];
                string sql = " " + Environment.NewLine;
                sql += " select  p.prefix_name, p.name, p.surname, p.org_serial, p.org_name_level3, p.org_name_level4 ," + Environment.NewLine;
                sql += " p.pos_name, p.pos_id,p.level_name, p.per_type_name," + Environment.NewLine;
                sql += " isnull(p.USER_LOGIN,p.user_code) as user_code, p.email,(select dbo.Thaidate(p.user_efft_date)) user_efft_date,(select dbo.Thaidate(p.user_exp_date)) user_exp_date," + Environment.NewLine;
                sql += " s.sys_code,s.sys_name, org.org_name sys_org_name, sr.role_name" + Environment.NewLine;
                sql += " from ODS_USER_ROLE ur" + Environment.NewLine;
                sql += " inner join ODS_SYSTEM s on s.sys_code=ur.SYS_CODE" + Environment.NewLine;
                sql += " inner join ODS_SYSTEM_ROLE sr on sr.sys_code=ur.sys_code and sr.role_id=ur.role_id" + Environment.NewLine;
                sql += " inner join vw_CMN_PERSON p on p.user_code=ur.user_code" + Environment.NewLine;
                sql += " inner join CTLT_ORGANIZE org on org.org_serial=ur.org_serial" + Environment.NewLine;
                //sql += " inner join CTLT_ORGANIZE pOrg on pOrg.org_serial=p.org_serial" + Environment.NewLine;
                sql += " where 1=1" + Environment.NewLine;
                if (StaffName != null)
                {
                    sql += " and (isnull(p.prefix_name,'')+isnull(p.name,'') + ' ' + isnull(p.surname,'') like '%' + @_STAFF_NAME + '%' or s.sys_name like '%' + @_STAFF_NAME + '%')" + Environment.NewLine;
                    param[0] = OPM_BL.setParameter("@_STAFF_NAME", SqlDbType.VarChar, StaffName);
                }
                if (POSName != null)
                {
                    sql += " and (p.pos_id like '%' + @_POS_NAME + '%' or p.pos_name like '%' + @_POS_NAME + '%')" + Environment.NewLine;
                    param[1] = OPM_BL.setParameter("@_POS_NAME", SqlDbType.VarChar, POSName);
                }
                if (ORGName != null)
                {
                    sql += " and (p.org_name_level3 like '%' + @_ORG_NAME + '%' or  p.org_name_level4 like '%' + @_ORG_NAME + '%')" + Environment.NewLine;
                    param[2] = OPM_BL.setParameter("@_ORG_NAME", SqlDbType.VarChar, ORGName);
                }

                DataTable dt = OPM_BL.GetDatatable(sql, param);
                DataRow dr;


                if (dt.Rows.Count > 0)
                {
                    if (vReportFormat == "EXCEL")
                    {
                        lblErrorMessage.Text = "";
                        ExcelReportENG.ExportReportODS_RP02(UserName, UserOrg, dt, Response);
                    }
                    else if (vReportFormat == "PDF")
                    {
                        rpt.Load(Server.MapPath(home_floder + "/" + reportname + ".rpt"));
                        rpt.DataDefinition.FormulaFields["UserName"].Text = "'" + UserName + "'";
                        rpt.DataDefinition.FormulaFields["UserOrg"].Text = "'" + UserOrg + "'";
                        rpt.DataDefinition.FormulaFields["ORGName"].Text = "'" + ORGName + "'";
                        //string printDate = DateTime.Now.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("th-TH"));
                        rpt.DataDefinition.FormulaFields["PrintDate"].Text = "'" + printDate + "'";
                        rpt.SetDataSource(dt);
                        lblErrorMessage.Text = "";

                        CreatePDFReport(rpt, reportname);
                    }

                }
                else
                {
                    lblErrorMessage.Text = "ไม่พบข้อมูล";
                    CrystalReportViewer1.Visible = false;
                }
                #endregion

            }
            else if (reportname == "ODS_RP03")
            {
                #region "ODS_RP03"

                string StaffName = Request.QueryString["StaffName"];
                string PERTYPE = Request.QueryString["PERTYPE"];
                string FromPOS = Request.QueryString["FromPOS"];
                string ToPOS = Request.QueryString["ToPOS"];
                string noPosition = Request.QueryString["noPosition"];


                //StaffName = "บงกช";
                //UserName = "99999ชื่อสกุล99999";
                //UserOrg = "xxxxxหน่วยงานxxxxx";
                //noPosition = "จากเลขที่ตำแหน่งXXXถึงเลขที่ตำแหน่งXXX";

                SqlParameter[] param = new SqlParameter[4];
                string msql = " " + Environment.NewLine;
                //sql += " select  isnull(p.prefix_name,'') prefix_name, isnull(p.name,'') name, isnull(p.surname,'') surname, p.org_serial, p.org_name_level3, p.org_name_level4 ," + Environment.NewLine;
                //sql += " p.pos_name, p.pos_id,p.level_name, p.per_type_name," + Environment.NewLine;
                //sql += " p.user_code, p.user_pwd, p.email,(select dbo.Thaidate(p.user_efft_date)) user_efft_date,(select dbo.Thaidate(p.user_exp_date)) user_exp_date" + Environment.NewLine;
                //sql += " from vw_CMN_PERSON p " + Environment.NewLine;
                //sql += " where 1=1 " + Environment.NewLine;

                msql += " select isnull(p.prefix_name,'')prefix_name, isnull(p.name,'')name, isnull(p.surname,'')surname, p.org_serial, p.org_name_level3, p.org_name_level4 ," + Environment.NewLine;
                msql += " p.pos_name, p.pos_id,p.level_name, p.per_type_name," + Environment.NewLine;
                msql += " isnull(p.USER_LOGIN,p.user_code) as user_code,p.user_pwd,p.email,(select dbo.Thaidate(p.user_efft_date)) user_efft_date,(select dbo.Thaidate(p.user_exp_date)) user_exp_date," + Environment.NewLine;
                msql += " s.sys_code,s.sys_name, org.org_name sys_org_name, sr.role_name , s.is_external" + Environment.NewLine;
                msql += " from ODS_USER_ROLE ur" + Environment.NewLine;
                msql += " inner join ODS_SYSTEM s on s.sys_code=ur.SYS_CODE" + Environment.NewLine;
                msql += " inner join ODS_SYSTEM_ROLE sr on sr.sys_code=ur.sys_code and sr.role_id=ur.role_id" + Environment.NewLine;
                msql += " inner join vw_CMN_PERSON p on p.user_code=ur.user_code" + Environment.NewLine;
                msql += " inner join CTLT_ORGANIZE org on org.org_serial=ur.org_serial" + Environment.NewLine;
                msql += " where 1=1" + Environment.NewLine;
                msql += "###" + Environment.NewLine;
                //******************************************//
                msql += " union all " + Environment.NewLine;
                msql += " select isnull(p.prefix_name,'')prefix_name, isnull(p.name,'')name, isnull(p.surname,'')surname, p.org_serial, p.org_name_level3,p.org_name_level4 , " + Environment.NewLine;
                msql += " p.pos_name, p.pos_id,p.level_name, p.per_type_name, " + Environment.NewLine;
                msql += " p.user_code,p.user_pwd,p.email,(select dbo.Thaidate(p.user_efft_date)) user_efft_date,(select dbo.Thaidate(p.user_exp_date)) user_exp_date, " + Environment.NewLine;
                msql += " s.sys_code,s.sys_name, org.org_name sys_org_name, '' role_name , s.is_external " + Environment.NewLine;
                msql += " from ODS_SYSTEM s " + Environment.NewLine;
                msql += " cross join vw_CMN_PERSON p  " + Environment.NewLine;  //ขอแนะนำให้รู้จักกับคำสั่ง cross join
                msql += " inner join CTLT_ORGANIZE org on org.org_serial=p.org_serial " + Environment.NewLine;
                msql += " where s.is_external=1 and is_hidden =1" + Environment.NewLine;
                msql += "###" + Environment.NewLine;

                string wh = "";
                if (StaffName != null && StaffName != "")
                {
                    wh = " and (isnull(p.prefix_name,'')+isnull(p.name,'') + ' ' + isnull(p.surname,'') like '%' + @_STAFF_NAME + '%'" + Environment.NewLine;
                    wh += " or p.user_code like '%' + @_STAFF_NAME + '%'" + Environment.NewLine;
                    wh += " or p.email like '%' + @_STAFF_NAME + '%')" + Environment.NewLine;

                    param[0] = OPM_BL.setParameter("@_STAFF_NAME", SqlDbType.VarChar, StaffName);
                }
                if (PERTYPE != null)
                {
                    wh += " and p.per_type = @_PER_TYPE " + Environment.NewLine;
                    param[1] = OPM_BL.setParameter("@_PER_TYPE", SqlDbType.VarChar, PERTYPE);
                }
                if (FromPOS != "" && ToPOS != "" && FromPOS != null && ToPOS != null)
                {
                    wh += " and p.pos_id between @_FROM_POS and @_TO_POS " + Environment.NewLine;
                    param[2] = OPM_BL.setParameter("@_FROM_POS", SqlDbType.VarChar, FromPOS);
                    param[3] = OPM_BL.setParameter("@_TO_POS", SqlDbType.VarChar, ToPOS);
                }
                if (FromPOS != ""  && FromPOS != null )
                {
                    wh += " and p.pos_id = @_FROM_POS  " + Environment.NewLine;
                    param[2] = OPM_BL.setParameter("@_FROM_POS", SqlDbType.VarChar, FromPOS);
                    param[3] = OPM_BL.setParameter("@_TO_POS", SqlDbType.VarChar, ToPOS);
                }
                if (ToPOS != ""   && ToPOS != null)
                {
                    wh += " and p.pos_id = @_TO_POS " + Environment.NewLine;
                    param[2] = OPM_BL.setParameter("@_FROM_POS", SqlDbType.VarChar, FromPOS);
                    param[3] = OPM_BL.setParameter("@_TO_POS", SqlDbType.VarChar, ToPOS);
                }
                msql = msql.Replace("###", wh);
                msql += " order by  name, surname";

                DataTable dt = OPM_BL.GetDatatable(msql, param);
                DataRow dr;

                if (dt.Rows.Count > 0)
                {
                    dt.DefaultView.Sort = "org_name_level3, org_name_level4, name, is_external";
                    dt = dt.DefaultView.ToTable().Copy();
                    if (vReportFormat == "EXCEL")
                    {
                        lblErrorMessage.Text = "";
                        ExcelReportENG.ExportReportODS_RP03(UserName, UserOrg, dt, Response);
                    }
                    else if (vReportFormat == "PDF")
                    {
                        rpt.Load(Server.MapPath(home_floder + "/" + reportname + ".rpt"));
                        rpt.DataDefinition.FormulaFields["UserName"].Text = "'" + UserName + "'";
                        rpt.DataDefinition.FormulaFields["UserOrg"].Text = "'" + UserOrg + "'";
                        //string printDate = DateTime.Now.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("th-TH"));
                        rpt.DataDefinition.FormulaFields["PrintDate"].Text = "'" + printDate + "'";
                        rpt.DataDefinition.FormulaFields["noPosition"].Text = "'" + noPosition + "'";
                        rpt.SetDataSource(dt);
                        lblErrorMessage.Text = "";

                        CreatePDFReport(rpt, reportname);
                    }
                }
                else
                {
                    lblErrorMessage.Text = "ไม่พบข้อมูล";
                    CrystalReportViewer1.Visible = false;
                }
                #endregion

            }
            else if (reportname == "ODS_RP04")
            {
                #region "ODS_RP04"

                string ORG = Request.QueryString["ORG"]; //***
                string PERSON_NAME = Request.QueryString["PERSON_NAME"];
                string START_DATE = Request.QueryString["START_DATE"];//**
                string END_DATE = Request.QueryString["END_DATE"];//**
                string LEVELNAME = Request.QueryString["LevelName"];
                string ORGName = Request.QueryString["ORGName"] + " ระดับ : " + LEVELNAME;

                //ORG = "8";
                //START_DATE = "20030101";
                //END_DATE = "20031231";
                //PERSON_NAME = "อุบล";
                //LEVELNAME = "งาน";
                //UserName = "99999ชื่อสกุล99999";
                //UserOrg = "xxxxxหน่วยงานxxxxx";


                SqlParameter[] param = new SqlParameter[5];
                string sql = " " + Environment.NewLine;
                sql += " select ph.org_serial, org.name_level3 org_name, org.org_abbr, " + Environment.NewLine;
                sql += " isnull(p.prefix_name,'') + isnull(p.name,'') + ' ' + isnull(p.surname,'') person_name,p.pos_name,p.level_name,p.user_code, " + Environment.NewLine;
                sql += " case when left(mc.MOVEMENT_FLAG,1)='1' then 'เพิ่ม' when left(mc.MOVEMENT_FLAG,1)='2' then 'ลบ' end emp_status, " + Environment.NewLine;
                sql += " dbo.Thaidate(ph.EFFECT_DATE) last_date " + Environment.NewLine;
                sql += " from PSST_HIS_POSITION ph " + Environment.NewLine;
                sql += " inner join PSST_MOVEMENT_CODE mc on ph.MOVEMENT_CODE=mc.MOVEMENT_CODE " + Environment.NewLine;
                sql += " inner join vw_CMN_PERSON p on p.id=ph.id " + Environment.NewLine;
                sql += " left join CTLT_PREFIX_CODE pe on pe.PREFIX_CODE=p.PREFIX_CODE " + Environment.NewLine;
                sql += " left join CTLT_EMP emp on emp.EMP_ID=p.id " + Environment.NewLine;
                sql += " inner join CTLT_ORGANIZE org on org.org_serial=ph.org_serial " + Environment.NewLine;
                sql += " where  left(mc.MOVEMENT_FLAG,1) in ('1','2')" + Environment.NewLine;

                if (ORG != null)
                {
                    sql += " and ph.org_serial=@_ORG_SERIAL" + Environment.NewLine;
                    param[0] = OPM_BL.setParameter("@_ORG_SERIAL", SqlDbType.VarChar, ORG);
                }

                if (PERSON_NAME != null && PERSON_NAME != "")
                {
                    sql += " and (isnull(p.prefix_name,'') + isnull(p.name,'') + ' ' + isnull(p.surname,'') like '%' + @_PERSON_NAME + '%' or p.pos_name like '%' + @_PERSON_NAME + '%')" + Environment.NewLine;
                    param[1] = OPM_BL.setParameter("@_PERSON_NAME", SqlDbType.VarChar, PERSON_NAME);
                }
                if (START_DATE != null && END_DATE != null)
                {
                    sql += " and convert(varchar(8),ph.EFFECT_DATE,112) between @_START_DATE and @_END_DATE" + Environment.NewLine;
                    param[2] = OPM_BL.setParameter("@_START_DATE", SqlDbType.VarChar, START_DATE);
                    param[3] = OPM_BL.setParameter("@_END_DATE", SqlDbType.VarChar, END_DATE);
                }
                if (LEVELNAME != null && LEVELNAME != "")
                {
                    sql += " and isnull(p.level_name,'') like '%' + @_LEVEL_NAME + '%'";
                    param[4] = OPM_BL.setParameter("@_LEVEL_NAME", SqlDbType.VarChar, LEVELNAME);
                }

                sql += " order by org.org_name, p.name" + Environment.NewLine;


                DataTable dt = OPM_BL.GetDatatable(sql, param);
                DataRow dr;

                if (dt.Rows.Count > 0)
                {
                    string DateFrom = OPM_BL.GetTHDate(START_DATE);
                    string DateTo = OPM_BL.GetTHDate(END_DATE);
                    if (vReportFormat == "EXCEL")
                    {
                        lblErrorMessage.Text = "";
                        ExcelReportENG.ExportReportODS_RP04(DateFrom, DateTo, UserName, UserOrg, dt, Response);
                    }
                    else if (vReportFormat == "PDF")
                    {
                        rpt.Load(Server.MapPath(home_floder + "/" + reportname + ".rpt"));
                        rpt.DataDefinition.FormulaFields["UserName"].Text = "'" + UserName + "'";
                        rpt.DataDefinition.FormulaFields["UserOrg"].Text = "'" + UserOrg + "'";
                        rpt.DataDefinition.FormulaFields["DateFrom"].Text = "'" + DateFrom + "'";
                        rpt.DataDefinition.FormulaFields["DateTo"].Text = "'" + DateTo + "'";
                        //string printDate = DateTime.Now.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("th-TH"));
                        rpt.DataDefinition.FormulaFields["PrintDate"].Text = "'" + printDate + "'";
                        rpt.DataDefinition.FormulaFields["ORGName"].Text = "'" + ORGName + "'";
                        rpt.SetDataSource(dt);
                        lblErrorMessage.Text = "";

                        CreatePDFReport(rpt, reportname);
                    }
                }
                else
                {
                    lblErrorMessage.Text = "ไม่พบข้อมูล";
                    CrystalReportViewer1.Visible = false;
                }
                #endregion
            }
            else if (reportname == "ODS_RP0404")
            {
                #region "ODS_RP04"

                string USER_CODE = Request.QueryString["USER_CODE"]; //***
                string PERSON_NAME = Request.QueryString["PERSON_NAME"];
                string Log_Type = Request.QueryString["LOG_TYPE"];
                string ORG = Request.QueryString["ORG"];
                //ORG = "8";
                //START_DATE = "20030101";
                //END_DATE = "20031231";
                //PERSON_NAME = "อุบล";
                //LEVELNAME = "งาน";
                //UserName = "99999ชื่อสกุล99999";
                //UserOrg = "xxxxxหน่วยงานxxxxx";


                SqlParameter[] param = new SqlParameter[2];
                string sql = " " + Environment.NewLine;
                sql += "SELECT USER_CODE,RH.SEQ " + Environment.NewLine;
                sql += "      ,RH.ACTION_USER_CODE " + Environment.NewLine;
                sql += "      ,RH.EVENT_TIME " + Environment.NewLine;
                sql += "      ,RH.SYS_TYPE " + Environment.NewLine;
                sql += "      ,RH.ACTION " + Environment.NewLine;
                sql += "      ,RH.USER_CODE " + Environment.NewLine;
                sql += "      ,RH.USER_NAME " + Environment.NewLine;
                sql += "      ,RH.SYS_CODE " + Environment.NewLine;
                sql += "      ,RH.SYS_NAME " + Environment.NewLine;
                sql += "      ,RH.ROLE_ID " + Environment.NewLine;
                sql += "      ,RH.ROLE_NAME " + Environment.NewLine;
                sql += "      ,RH.ORG_SERIAL " + Environment.NewLine;
                sql += "   ,Org.ORG_NAME " + Environment.NewLine;
                sql += "  FROM ODS_USER_ROLE_HIS RH inner join CTLT_ORGANIZE Org on RH.ORG_SERIAL = Org.ORG_SERIAL  ORDER BY RH.EVENT_TIME  DESC " + Environment.NewLine;


                if (USER_CODE != null)
                {
                    sql += " where USER_CODE = @_USER_CODE";
                    param[0] = OPM_BL.setParameter("@_USER_CODE", SqlDbType.VarChar, USER_CODE);
                }

                if (Log_Type != null)
                {
                    if (Log_Type == "NEW")
                    {
                        sql += " and SYS_TYPE = 'NEW'" + Environment.NewLine;
                    }
                    else
                    {
                        sql += " and SYS_TYPE <> 'NEW'" + Environment.NewLine;
                    }
                }



                DataTable dt = OPM_BL.GetDatatable(sql, param);
                DataRow dr;

                if (dt.Rows.Count > 0)
                {

                    //if (vReportFormat == "EXCEL")
                    //{
                    //    lblErrorMessage.Text = "";
                    //    //ExcelReportENG.ExportReportODS_RP04(DateFrom, DateTo, UserName, UserOrg, dt, Response);
                    //}
                    //else if (vReportFormat == "PDF")
                    //{

                    if (Log_Type == "NEW")
                    {
                        rpt.Load(Server.MapPath(home_floder + "/ODS_UT0404_SYS_NEW.rpt"));
                    }
                    else
                    {
                        rpt.Load(Server.MapPath(home_floder + "/ODS_UT0404_SYS_OLD.rpt"));
                    }
                    //rpt.Load(Server.MapPath(home_floder + "/" + reportname + ".rpt"));
                    rpt.DataDefinition.FormulaFields["UserName"].Text = "'" + UserName + "'";
                    rpt.DataDefinition.FormulaFields["UserOrg"].Text = "'" + UserOrg + "'";
                    //string printDate = DateTime.Now.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("th-TH"));
                    rpt.DataDefinition.FormulaFields["PrintDate"].Text = "'" + printDate + "'";
                    rpt.SetDataSource(dt);
                    lblErrorMessage.Text = "";

                    rpt.SetParameterValue("PERSON_NAME", PERSON_NAME);
                    rpt.SetParameterValue("ORG", ORG);
                    CreatePDFReport(rpt, reportname);
                    //}
                }
                else
                {
                    lblErrorMessage.Text = "ไม่พบข้อมูล";
                    CrystalReportViewer1.Visible = false;
                }
                #endregion
            }
            else if (reportname == "ODS_RP05")
            {
                #region "ODS_RP05"
                string ORG = Request.QueryString["ORG"]; //***
                string PERSON_NAME = Request.QueryString["PERSON_NAME"];
                string START_DATE = Request.QueryString["START_DATE"];//**
                string END_DATE = Request.QueryString["END_DATE"];//**
                string LEVELNAME = Request.QueryString["LevelName"];
                string ORGName = Request.QueryString["ORGName"] + " ระดับ : " + LEVELNAME;

                //ORG = "65";
                //START_DATE = "20160116";
                //END_DATE = "20160116";
                //UserName = "99999ชื่อสกุล99999";
                //UserOrg = "xxxxxหน่วยงานxxxxx";

                SqlParameter[] param = new SqlParameter[5];
                string sql = " " + Environment.NewLine;
                sql += " select distinct org_name,PREFIX_CODE_NEW.PREFIX_NAME +''+ [NH_NAME_NEW]+ ' ' +[NH_SURNAME_NEW] as new_person_name," + Environment.NewLine;
                sql += " PREFIX_CODE.PREFIX_NAME+''+ [NH_NAME]+' ' +[NH_SURNAME] as old_person_name,pos_name,isnull(level_name,'') level_name,ISNULL((select [dbo].[ThaiDateAbbrMonth](NH_DATE)),'') as created_date" + Environment.NewLine;
                sql += " FROM [HRM_NAMEHIS]" + Environment.NewLine;
                sql += " LEFT JOIN CTLT_PREFIX_CODE PREFIX_CODE ON PREFIX_CODE.PN_CODE=HRM_NAMEHIS.PN_CODE" + Environment.NewLine;
                sql += " LEFT JOIN CTLT_PREFIX_CODE PREFIX_CODE_NEW ON PREFIX_CODE_NEW.PN_CODE=HRM_NAMEHIS.PN_CODE_NEW" + Environment.NewLine;
                sql += " LEFT JOIN vw_CMN_PERSON P ON HRM_NAMEHIS.ID = P.ID" + Environment.NewLine;
                sql += " where 1=1" + Environment.NewLine;
                if (ORG != null)
                {
                    sql += " and org_serial=@_ORG_SERIAL" + Environment.NewLine;
                    param[0] = OPM_BL.setParameter("@_ORG_SERIAL", SqlDbType.VarChar, ORG);
                }

                if (PERSON_NAME != null && PERSON_NAME != "")
                {
                    sql += " and (isnull(PREFIX_CODE.PREFIX_NAME,'') + isnull(NH_NAME,'') + ' ' + isnull(NH_SURNAME,'') like '%' + @_PERSON_NAME + '%' or pos_name like '%' + @_PERSON_NAME + '%'" + Environment.NewLine;
                    sql += " or isnull(PREFIX_CODE_NEW.PREFIX_NAME,'') + isnull(NH_NAME_NEW,'') + ' ' + isnull(NH_SURNAME_NEW,'') like '%' + @_PERSON_NAME + '%')" + Environment.NewLine;
                    param[1] = OPM_BL.setParameter("@_PERSON_NAME", SqlDbType.VarChar, PERSON_NAME);
                }
                if (START_DATE != null && END_DATE != null)
                {
                    sql += " and convert(varchar(8),NH_DATE,112) between @_START_DATE and @_END_DATE" + Environment.NewLine;
                    param[2] = OPM_BL.setParameter("@_START_DATE", SqlDbType.VarChar, START_DATE);
                    param[3] = OPM_BL.setParameter("@_END_DATE", SqlDbType.VarChar, END_DATE);
                }
                if (LEVELNAME != null && LEVELNAME != "")
                {
                    sql += " and isnull(level_name,'') like '%' + @_LEVEL_NAME + '%'";
                    param[4] = OPM_BL.setParameter("@_LEVEL_NAME", SqlDbType.VarChar, LEVELNAME);
                }

                sql += " order by org_name, new_person_name,old_person_name  " + Environment.NewLine;


                DataTable dt = OPM_BL.GetDatatable(sql, param);
                if (dt.Rows.Count > 0)
                {

                    string DateFrom = "-";
                    string DateTo = "-";
                    if (START_DATE != null)
                    {
                        DateFrom = OPM_BL.GetTHDate(START_DATE);
                    }
                    if (END_DATE != null)
                    {
                        DateTo = OPM_BL.GetTHDate(END_DATE);
                    }
                    if (vReportFormat == "EXCEL")
                    {
                        lblErrorMessage.Text = "";
                        ExcelReportENG.ExportReportODS_RP05(DateFrom, DateTo, UserName, UserOrg, dt, Response);
                    }
                    else if (vReportFormat == "PDF")
                    {
                        rpt.Load(Server.MapPath(home_floder + "/" + reportname + ".rpt"));
                        rpt.DataDefinition.FormulaFields["UserName"].Text = "'" + UserName + "'";
                        rpt.DataDefinition.FormulaFields["UserOrg"].Text = "'" + UserOrg + "'";
                        rpt.DataDefinition.FormulaFields["DateFrom"].Text = "'" + DateFrom + "'";
                        rpt.DataDefinition.FormulaFields["DateTo"].Text = "'" + DateTo + "'";
                        //string printDate = DateTime.Now.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("th-TH"));
                        rpt.DataDefinition.FormulaFields["PrintDate"].Text = "'" + printDate + "'";
                        rpt.DataDefinition.FormulaFields["ORGName"].Text = "'" + ORGName + "'";
                        rpt.SetDataSource(dt);
                        lblErrorMessage.Text = "";

                        CreatePDFReport(rpt, reportname);
                    }

                }
                else
                {
                    lblErrorMessage.Text = "ไม่พบข้อมูล";
                    CrystalReportViewer1.Visible = false;
                }
                #endregion
            }
            else if (reportname == "ODS_RP06")
            {
                #region "ODS_RP06"

                string ORG = Request.QueryString["ORG"];
                string PERSON_NAME = Request.QueryString["PERSON_NAME"];
                string START_DATE = Request.QueryString["START_DATE"];//**
                string END_DATE = Request.QueryString["END_DATE"];//**
                string LEVEL = Request.QueryString["Level"];
                string EDITTYPE = Request.QueryString["EditType"];
                string rdLevel = Request.QueryString["rdLevel"];
                string cmbLev = Request.QueryString["cmbLev"];
                string ORGName = Request.QueryString["ORGName"] + " ระดับ : " + cmbLev + " การแก้ไข : " + rdLevel;
                string PER_STATUS = Request.QueryString["PER_STATUS"];
                //ORG = "2";
                //START_DATE = "20021001";
                //END_DATE = "20021030";
                //UserName = "99999ชื่อสกุล99999";
                //UserOrg = "xxxxxหน่วยงานxxxxx";
                //EDITTYPE = "3";

                SqlParameter[] param = new SqlParameter[7];
                string sql = " " + Environment.NewLine;
                sql += " select isnull(px.prefix_name,'') + isnull(p.name,'') + ' ' + isnull(p.surname,'') as person_name," + Environment.NewLine;
                sql += " ISNULL(oad.ADMIN_NAME,oln.LINE_NAME) old_pos_name, olev.LEVEL_NAME_FULL old_level_name, oorg.org_name old_org_name," + Environment.NewLine;
                sql += " norg.org_name new_org_name, isnull(nad.admin_code,nln.line_name) new_pos_name, nlev.LEVEL_NAME_FULL new_level_name," + Environment.NewLine;
                sql += " (select dbo.ThaiDateAbbrMonth(ph.effect_date)) start_date, (select dbo.Thaidate(p.last_date)) created_date" + Environment.NewLine;
                sql += " from PSST_HIS_POSITION ph" + Environment.NewLine;
                sql += " inner join PSST_PERSON p on p.id=ph.id" + Environment.NewLine;
                sql += " left join CTLT_PREFIX_CODE px on p.PREFIX_CODE=px.PREFIX_CODE" + Environment.NewLine;
                sql += " left join PSST_POSITION opo on opo.POS_ID=ph.OLD_POS_ID and opo.PER_TYPE=ph.PER_TYPE" + Environment.NewLine;
                sql += " LEFT JOIN vw_SMM_ADMIN_CODE oad ON opo.ADMIN_CODE=oad.ADMIN_CODE" + Environment.NewLine;
                sql += " LEFT JOIN PSST_LINE_CODE oln ON opo.LINE_CODE=oln.LINE_CODE AND opo.PER_TYPE=oln.PER_TYPE" + Environment.NewLine;
                sql += " left join CTLT_ORGANIZE oorg on oorg.org_serial=ph.old_org_serial" + Environment.NewLine;
                sql += " left join PSST_PER_LEVEL olev on olev.CUR_LEV=ph.OLD_CUR_LEV and olev.PER_TYPE=ph.PER_TYPE" + Environment.NewLine;
                sql += " left join PSST_POSITION npo on npo.POS_ID=ph.OLD_POS_ID and npo.PER_TYPE=ph.PER_TYPE" + Environment.NewLine;
                sql += " LEFT JOIN vw_SMM_ADMIN_CODE nad ON npo.ADMIN_CODE=nad.ADMIN_CODE" + Environment.NewLine;
                sql += " LEFT JOIN PSST_LINE_CODE nln ON npo.LINE_CODE=nln.LINE_CODE AND npo.PER_TYPE=nln.PER_TYPE" + Environment.NewLine;
                sql += " left join CTLT_ORGANIZE norg on norg.org_serial=ph.org_serial" + Environment.NewLine;
                sql += " left join PSST_PER_LEVEL nlev on nlev.CUR_LEV=ph.CUR_LEV and nlev.PER_TYPE=ph.PER_TYPE" + Environment.NewLine;
                sql += " where 1=1 " + Environment.NewLine;


                if (ORG != null)
                {
                    sql += " and (ph.old_org_serial=@_ORG_SERIAL or ph.org_serial=@_ORG_SERIAL)" + Environment.NewLine;
                    param[0] = OPM_BL.setParameter("@_ORG_SERIAL", SqlDbType.VarChar, ORG);
                }

                if (PERSON_NAME != null && PERSON_NAME != "")
                {
                    sql += " and (isnull(px.prefix_name,'') like '%' + @_PERSON_NAME + '%' or isnull(p.name,'') like '%' + @_PERSON_NAME + '%' or isnull(p.surname,'')  like '%' + @_PERSON_NAME + '%' or ISNULL(oad.ADMIN_NAME,oln.LINE_NAME) like '%' + @_PERSON_NAME + '%' or isnull(nad.admin_code,nln.line_name) like '%' + @_PERSON_NAME + '%')" + Environment.NewLine;
                    param[1] = OPM_BL.setParameter("@_PERSON_NAME", SqlDbType.VarChar, PERSON_NAME);
                }
                if (START_DATE != null && END_DATE != null)
                {
                    sql += " and convert(varchar(8),ph.effect_date,112) between @_START_DATE and @_END_DATE" + Environment.NewLine;
                    param[2] = OPM_BL.setParameter("@_START_DATE", SqlDbType.VarChar, START_DATE);
                    param[3] = OPM_BL.setParameter("@_END_DATE", SqlDbType.VarChar, END_DATE);
                }
                if (LEVEL != null && LEVEL != "")
                {
                    sql += " and (olev.cur_lev= @_LEVEL or nlev.cur_lev = @_LEVEL)";
                    param[4] = OPM_BL.setParameter("@_LEVEL", SqlDbType.VarChar, LEVEL);
                }
                if (EDITTYPE != null)
                {
                    if (EDITTYPE == "1")
                    {
                        sql += " and  ISNULL(CONVERT(varchar,ph.CUR_LEV),'') <>ISNULL(CONVERT(varchar,ph.OLD_CUR_LEV),'')" + Environment.NewLine;
                    }
                    if (EDITTYPE == "2")
                    {
                        sql += " and  ISNULL(CONVERT(varchar,ph.POS_ID),'') <>ISNULL(CONVERT(varchar,ph.OLD_POS_ID),'')" + Environment.NewLine;
                    }
                    if (EDITTYPE == "3")
                    {
                        sql += "  and  ISNULL(CONVERT(varchar,ph.ORG_SERIAL),'') <>ISNULL(CONVERT(varchar,ph.OLD_ORG_SERIAL),'')" + Environment.NewLine;
                    }
                }

                if (PER_STATUS != null)
                {
                    sql += " and p.PER_STATUS = @_PER_STATUS" + Environment.NewLine;
                    param[6] = OPM_BL.setParameter("@_PER_STATUS", SqlDbType.VarChar, PER_STATUS);
                }

                sql += " order by  isnull(px.prefix_name,'') , isnull(p.name,'') , isnull(p.surname,'')" + Environment.NewLine;


                DataTable dt = OPM_BL.GetDatatable(sql, param);
                DataRow dr;

                if (dt.Rows.Count > 0)
                {
                    string DateFrom = OPM_BL.GetTHDate(START_DATE);
                    string DateTo = OPM_BL.GetTHDate(END_DATE);
                    if (vReportFormat == "EXCEL")
                    {
                        lblErrorMessage.Text = "";
                        if (EDITTYPE == "1")
                        {
                            ExcelReportENG.ExportReportODS_RP06_1(DateFrom, DateTo, UserName, UserOrg, dt, Response);
                        }
                        else if (EDITTYPE == "2")
                        {
                            ExcelReportENG.ExportReportODS_RP06_2(DateFrom, DateTo, UserName, UserOrg, dt, Response);
                        }
                        else if (EDITTYPE == "3")
                        {
                            ExcelReportENG.ExportReportODS_RP06_3(DateFrom, DateTo, UserName, UserOrg, dt, Response);
                        }

                    }
                    else if (vReportFormat == "PDF")
                    {
                        reportname = reportname + "_" + EDITTYPE;
                        rpt.Load(Server.MapPath(home_floder + "/" + reportname + ".rpt"));
                        rpt.DataDefinition.FormulaFields["UserName"].Text = "'" + UserName + "'";
                        rpt.DataDefinition.FormulaFields["UserOrg"].Text = "'" + UserOrg + "'";
                        rpt.DataDefinition.FormulaFields["DateFrom"].Text = "'" + DateFrom + "'";
                        rpt.DataDefinition.FormulaFields["DateTo"].Text = "'" + DateTo + "'";
                        //string printDate = DateTime.Now.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("th-TH"));
                        rpt.DataDefinition.FormulaFields["PrintDate"].Text = "'" + printDate + "'";
                        rpt.DataDefinition.FormulaFields["ORGName"].Text = "'" + ORGName + "'";
                        rpt.SetDataSource(dt);
                        lblErrorMessage.Text = "";

                        CreatePDFReport(rpt, reportname);
                    }


                }
                else
                {
                    lblErrorMessage.Text = "ไม่พบข้อมูล";
                    CrystalReportViewer1.Visible = false;
                }
                #endregion
            }
            else if (reportname == "ODS_RP07")
            {
                #region "ODS_RP07"
                string ORG = Request.QueryString["ORG"];
                string USER_CODE = Request.QueryString["USER_CODE"];
                string CLIENT_IP = Request.QueryString["CLIENT_IP"];
                string PERSON_NAME = Request.QueryString["PERSON_NAME"];
                string LEVELNAME = Request.QueryString["LevelName"];
                string DATE_FROM = Request.QueryString["DATE_FROM"];//**
                string DATE_TO = Request.QueryString["DATE_TO"];//**

                string strLevel = Request.QueryString["txtLevel"];//**
                string ORGName = Request.QueryString["ORGName"] + " ระดับ : " + strLevel;
                string sys_code = Request.QueryString["sys_code"];


                //DATE_FROM = "20160301";
                //DATE_TO = "20160430";
                //UserName = "99999ชื่อสกุล99999";
                //UserOrg = "xxxxxหน่วยงานxxxxx";

                SqlParameter[] param = new SqlParameter[8];
                string sql = " " + Environment.NewLine;
                sql += " select distinct p.ORG_SERIAL,p.org_name, p.org_abbr, s.user_code, isnull(p.prefix_name,'') + isnull(p.name,'') + ' ' + isnull(p.surname,'') person_name," + Environment.NewLine;
                sql += " p.POS_NAME, p.LEVEL_NAME_FULL, s.client_ip, (select dbo.ThaiDateAbbrMonth(s.created_date))  +' ' + convert(varchar(5),created_date,108) created_date,s.sys_code" + Environment.NewLine;
                sql += " from SSO_SESSION_INFO s" + Environment.NewLine;
                sql += " inner join vw_CMN_PERSON p on s.user_code=p.user_code" + Environment.NewLine;
                sql += " where 1=1  and sys_code <> ''" + Environment.NewLine;





                if (ORG != null && ORG != "76")
                {
                    sql += " and p.org_serial=@_ORG_SERIAL" + Environment.NewLine;
                    param[0] = OPM_BL.setParameter("@_ORG_SERIAL", SqlDbType.VarChar, ORG);
                }
                if (USER_CODE != null)
                {
                    sql += " and s.user_code like '%'+@_USER_CODE +'%'" + Environment.NewLine;
                    param[1] = OPM_BL.setParameter("@_USER_CODE", SqlDbType.VarChar, USER_CODE);
                }
                if (CLIENT_IP != null)
                {
                    sql += " and s.client_ip like '%'+@_CLIENT_IP+'%'" + Environment.NewLine;
                    param[2] = OPM_BL.setParameter("@_CLIENT_IP", SqlDbType.VarChar, CLIENT_IP);
                }
                if (PERSON_NAME != null && PERSON_NAME != "")
                {
                    sql += " and (isnull(p.prefix_name,'') + isnull(p.name,'') + ' ' + isnull(p.surname,'') like '%' + @_PERSON_NAME + '%' or p.pos_name like '%'+@_PERSON_NAME+'%')" + Environment.NewLine;
                    param[3] = OPM_BL.setParameter("@_PERSON_NAME", SqlDbType.VarChar, PERSON_NAME);
                }
                if (LEVELNAME != null && LEVELNAME != "")
                {
                    sql += " and p.level_name_full like '%'+@_LEVEL_NAME+'%'" + Environment.NewLine;
                    param[4] = OPM_BL.setParameter("@_LEVEL_NAME", SqlDbType.VarChar, LEVELNAME);
                }
                if (DATE_FROM != null && DATE_TO != null)
                {
                    sql += " and convert(varchar(8),s.created_date,112) between @_DATE_FROM and @_DATE_TO" + Environment.NewLine;
                    param[5] = OPM_BL.setParameter("@_DATE_FROM", SqlDbType.VarChar, DATE_FROM);
                    param[6] = OPM_BL.setParameter("@_DATE_TO", SqlDbType.VarChar, DATE_TO);
                }
                if (sys_code != null)
                {
                    sql += " and sys_code = @_sys_code" + Environment.NewLine;
                    param[7] = OPM_BL.setParameter("@_sys_code", SqlDbType.VarChar, sys_code);
                }

                DataTable dt = OPM_BL.GetDatatable(sql, param);
                DataRow dr;

                if (dt.Rows.Count > 0)
                {
                    string DateFrom = OPM_BL.GetTHDate(DATE_FROM);
                    string DateTo = OPM_BL.GetTHDate(DATE_TO);
                    if (vReportFormat == "EXCEL")
                    {
                        lblErrorMessage.Text = "";
                        ExcelReportENG.ExportReportODS_RP07(DateFrom, DateTo, UserName, UserOrg, dt, Response);
                    }
                    else if (vReportFormat == "PDF")
                    {
                        rpt.Load(Server.MapPath(home_floder + "/" + reportname + ".rpt"));
                        rpt.DataDefinition.FormulaFields["UserName"].Text = "'" + UserName + "'";
                        rpt.DataDefinition.FormulaFields["UserOrg"].Text = "'" + UserOrg + "'";
                        rpt.DataDefinition.FormulaFields["DateFrom"].Text = "'" + DateFrom + "'";
                        rpt.DataDefinition.FormulaFields["DateTo"].Text = "'" + DateTo + "'";
                        //string printDate = DateTime.Now.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("th-TH"));
                        rpt.DataDefinition.FormulaFields["PrintDate"].Text = "'" + printDate + "'";
                        rpt.DataDefinition.FormulaFields["CriteriaHeader"].Text = "'" + ORGName + "'";
                        rpt.SetDataSource(dt);
                        lblErrorMessage.Text = "";

                        CreatePDFReport(rpt, reportname);
                    }

                }
                else
                {
                    lblErrorMessage.Text = "ไม่พบข้อมูล";
                    CrystalReportViewer1.Visible = false;
                }
                #endregion
            }
            else if (reportname == "ODS_RP08")
            {
                #region "ODS_RP08"
                string SysCode = Request.QueryString["SysCode"];
                string DATE_FROM = Request.QueryString["DATE_FROM"];//**
                string DATE_TO = Request.QueryString["DATE_TO"];//**
                string strSystems = Request.QueryString["strSystems"];

                //DATE_FROM = "20160116";
                //DATE_TO = "20160501";
                //UserName = "99999ชื่อสกุล99999";
                //UserOrg = "xxxxxหน่วยงานxxxxx";

                SqlParameter[] param = new SqlParameter[3];
                string sql = " " + Environment.NewLine;
                sql += " select s.sys_code, st.sys_name_eng, st.sys_name,  count(s.id) login_qty" + Environment.NewLine;
                sql += " from SSO_SESSION_INFO s" + Environment.NewLine;
                sql += " inner join ODS_SYSTEM st on st.sys_code=s.sys_code" + Environment.NewLine;
                sql += " where 1=1" + Environment.NewLine;
                if (SysCode != null)
                {
                    //sql += " and (s.sys_code like '%'+@_SYS_CODE+'%' or st.sys_name like '%' + @_SYS_NAME + '%' or st.sys_name_eng like '%' + @_SYS_NAME + '%')" + Environment.NewLine;
                    sql += " and s.sys_code = @_SYS_CODE" + Environment.NewLine;
                    param[0] = OPM_BL.setParameter("@_SYS_CODE", SqlDbType.VarChar, SysCode);
                }
                if (DATE_FROM != null && DATE_TO != null)
                {
                    sql += " and convert(varchar(8),s.created_date,112) between @_DATE_FROM and @_DATE_TO" + Environment.NewLine;
                    param[1] = OPM_BL.setParameter("@_DATE_FROM", SqlDbType.VarChar, DATE_FROM);
                    param[2] = OPM_BL.setParameter("@_DATE_TO", SqlDbType.VarChar, DATE_TO);
                }

                sql += " group by s.sys_code, st.sys_name_eng, st.sys_name" + Environment.NewLine;

                DataTable dt = OPM_BL.GetDatatable(sql, param);
                DataRow dr;

                if (dt.Rows.Count > 0)
                {
                    string DateFrom = OPM_BL.GetTHDate(DATE_FROM);
                    string DateTo = OPM_BL.GetTHDate(DATE_TO);
                    if (vReportFormat == "EXCEL")
                    {
                        lblErrorMessage.Text = "";
                        ExcelReportENG.ExportReportODS_RP08(DateFrom, DateTo, UserName, UserOrg, dt, Response);
                    }
                    else if (vReportFormat == "PDF")
                    {
                        rpt.Load(Server.MapPath(home_floder + "/" + reportname + ".rpt"));
                        rpt.DataDefinition.FormulaFields["UserName"].Text = "'" + UserName + "'";
                        rpt.DataDefinition.FormulaFields["UserOrg"].Text = "'" + UserOrg + "'";
                        rpt.DataDefinition.FormulaFields["DateFrom"].Text = "'" + DateFrom + "'";
                        rpt.DataDefinition.FormulaFields["DateTo"].Text = "'" + DateTo + "'";
                        //string printDate = DateTime.Now.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("th-TH"));
                        rpt.DataDefinition.FormulaFields["PrintDate"].Text = "'" + printDate + "'";
                        rpt.DataDefinition.FormulaFields["strSystems"].Text = "'" + strSystems + "'";
                        rpt.SetDataSource(dt);
                        lblErrorMessage.Text = "";

                        CreatePDFReport(rpt, reportname);
                    }
                }
                else
                {
                    lblErrorMessage.Text = "ไม่พบข้อมูล";
                    CrystalReportViewer1.Visible = false;
                }
                #endregion
            }
            else if (reportname == "ODS_RP09")
            {
                #region "ODS_RP09"
                string ORG = Request.QueryString["ORG"];
                string PERSON_NAME = Request.QueryString["PERSON_NAME"];
                string POSNAME = Request.QueryString["POSNAME"];
                string DATE_FROM = Request.QueryString["DATE_FROM"];//**
                string DATE_TO = Request.QueryString["DATE_TO"];//**               
                string ORGName = Request.QueryString["ORGName"];//**

                //DATE_FROM = "20160101";
                //DATE_TO = "20160530";
                //UserName = "99999ชื่อสกุล99999";
                //UserOrg = "xxxxxหน่วยงานxxxxx";

                SqlParameter[] param = new SqlParameter[7];
                string sql = " " + Environment.NewLine;
                sql += " select distinct p.org_name_level3 org_name, p.org_abbr, isnull(p.prefix_name,'') + isnull(p.name,'') + ' ' + isnull(p.surname,'') person_name,p.POS_NAME," + Environment.NewLine;
                sql += " HRM = (select count(ss.id) login_qty" + Environment.NewLine;
                sql += " from SSO_SESSION_INFO ss" + Environment.NewLine;
                sql += " where ss.user_code = s.user_code" + Environment.NewLine;
                sql += " and convert(varchar(8),ss.created_date,112) between @_DATE_FROM and @_DATE_TO" + Environment.NewLine;
                sql += " and ss.sys_code='HRM') ," + Environment.NewLine;
                sql += " PDM = (select count(ss.id) login_qty" + Environment.NewLine;
                sql += " from SSO_SESSION_INFO ss" + Environment.NewLine;
                sql += " where ss.user_code = s.user_code" + Environment.NewLine;
                sql += " and convert(varchar(8),ss.created_date,112) between @_DATE_FROM and @_DATE_TO" + Environment.NewLine;
                sql += " and ss.sys_code='PDM') ," + Environment.NewLine;
                sql += " SMM = (select count(ss.id) login_qty" + Environment.NewLine;
                sql += " from SSO_SESSION_INFO ss" + Environment.NewLine;
                sql += " where ss.user_code = s.user_code" + Environment.NewLine;
                sql += " and convert(varchar(8),ss.created_date,112) between @_DATE_FROM and @_DATE_TO" + Environment.NewLine;
                sql += " and ss.sys_code='SMM') ," + Environment.NewLine;
                sql += " ODS = (select count(ss.id) login_qty" + Environment.NewLine;
                sql += " from SSO_SESSION_INFO ss" + Environment.NewLine;
                sql += " where ss.user_code = s.user_code" + Environment.NewLine;
                sql += " and convert(varchar(8),ss.created_date,112) between @_DATE_FROM and @_DATE_TO" + Environment.NewLine;
                sql += " and ss.sys_code='ODS') ," + Environment.NewLine;
                sql += " CRS = (select count(ss.id) login_qty" + Environment.NewLine;
                sql += " from SSO_SESSION_INFO ss" + Environment.NewLine;
                sql += " where ss.user_code = s.user_code" + Environment.NewLine;
                sql += " and convert(varchar(8),ss.created_date,112) between @_DATE_FROM and @_DATE_TO" + Environment.NewLine;
                sql += " and ss.sys_code='CRS') ," + Environment.NewLine;
                sql += " PDM = (select count(ss.id) login_qty" + Environment.NewLine;
                sql += " from SSO_SESSION_INFO ss" + Environment.NewLine;
                sql += " where ss.user_code = s.user_code" + Environment.NewLine;
                sql += " and convert(varchar(8),ss.created_date,112) between @_DATE_FROM and @_DATE_TO" + Environment.NewLine;
                sql += " and ss.sys_code='PDM') ," + Environment.NewLine;
                sql += " CCB = (select count(ss.id) login_qty" + Environment.NewLine;
                sql += " from SSO_SESSION_INFO ss" + Environment.NewLine;
                sql += " where ss.user_code = s.user_code" + Environment.NewLine;
                sql += " and convert(varchar(8),ss.created_date,112) between @_DATE_FROM and @_DATE_TO" + Environment.NewLine;
                sql += " and ss.sys_code='CCB') ," + Environment.NewLine;
                sql += " MRM = (select count(ss.id) login_qty" + Environment.NewLine;
                sql += " from SSO_SESSION_INFO ss" + Environment.NewLine;
                sql += " where ss.user_code = s.user_code" + Environment.NewLine;
                sql += " and convert(varchar(8),ss.created_date,112) between @_DATE_FROM and @_DATE_TO" + Environment.NewLine;
                sql += " and ss.sys_code='MRM') ," + Environment.NewLine;
                sql += " RCS = (select count(ss.id) login_qty" + Environment.NewLine;
                sql += " from SSO_SESSION_INFO ss" + Environment.NewLine;
                sql += " where ss.user_code = s.user_code" + Environment.NewLine;
                sql += " and convert(varchar(8),ss.created_date,112) between @_DATE_FROM and @_DATE_TO" + Environment.NewLine;
                sql += " and ss.sys_code='RCS') ," + Environment.NewLine;
                sql += " PRS = (select count(ss.id) login_qty" + Environment.NewLine;
                sql += " from SSO_SESSION_INFO ss" + Environment.NewLine;
                sql += " where ss.user_code = s.user_code" + Environment.NewLine;
                sql += " and convert(varchar(8),ss.created_date,112) between @_DATE_FROM and @_DATE_TO" + Environment.NewLine;
                sql += " and ss.sys_code='PRS') ," + Environment.NewLine;
                sql += " BIS = (select count(ss.id) login_qty" + Environment.NewLine;
                sql += " from SSO_SESSION_INFO ss" + Environment.NewLine;
                sql += " where ss.user_code = s.user_code" + Environment.NewLine;
                sql += " and convert(varchar(8),ss.created_date,112) between @_DATE_FROM and @_DATE_TO" + Environment.NewLine;
                sql += " and ss.sys_code='BIS') ," + Environment.NewLine;
                sql += " EIS = (select count(ss.id) login_qty" + Environment.NewLine;
                sql += " from SSO_SESSION_INFO ss" + Environment.NewLine;
                sql += " where ss.user_code = s.user_code" + Environment.NewLine;
                sql += " and convert(varchar(8),ss.created_date,112) between @_DATE_FROM and @_DATE_TO" + Environment.NewLine;
                sql += " and ss.sys_code='EIS') ," + Environment.NewLine;
                sql += " RMS = (select count(ss.id) login_qty" + Environment.NewLine;
                sql += " from SSO_SESSION_INFO ss" + Environment.NewLine;
                sql += " where ss.user_code = s.user_code" + Environment.NewLine;
                sql += " and convert(varchar(8),ss.created_date,112) between @_DATE_FROM and @_DATE_TO" + Environment.NewLine;
                sql += " and ss.sys_code='RMS') ," + Environment.NewLine;
                sql += " PFS = (select count(ss.id) login_qty" + Environment.NewLine;
                sql += " from SSO_SESSION_INFO ss" + Environment.NewLine;
                sql += " where ss.user_code = s.user_code" + Environment.NewLine;
                sql += " and convert(varchar(8),ss.created_date,112) between @_DATE_FROM and @_DATE_TO" + Environment.NewLine;
                sql += " and ss.sys_code='PFS') ," + Environment.NewLine;
                sql += " PLN = (select count(ss.id) login_qty" + Environment.NewLine;
                sql += " from SSO_SESSION_INFO ss" + Environment.NewLine;
                sql += " where ss.user_code = s.user_code" + Environment.NewLine;
                sql += " and convert(varchar(8),ss.created_date,112) between @_DATE_FROM and @_DATE_TO" + Environment.NewLine;
                sql += " and ss.sys_code='PLN') ," + Environment.NewLine;
                sql += " FTS = (select count(ss.id) login_qty" + Environment.NewLine;
                sql += " from SSO_SESSION_INFO ss" + Environment.NewLine;
                sql += " where ss.user_code = s.user_code" + Environment.NewLine;
                sql += " and convert(varchar(8),ss.created_date,112) between @_DATE_FROM and @_DATE_TO" + Environment.NewLine;
                sql += " and ss.sys_code='FTS')" + Environment.NewLine;
                sql += " from SSO_SESSION_INFO s" + Environment.NewLine;
                sql += " inner join vw_CMN_PERSON p on s.user_code=p.user_code" + Environment.NewLine;
                sql += " where 1=1" + Environment.NewLine;

                param[0] = OPM_BL.setParameter("@_DATE_FROM", SqlDbType.VarChar, DATE_FROM);
                param[1] = OPM_BL.setParameter("@_DATE_TO", SqlDbType.VarChar, DATE_TO);
                if (ORG != null)
                {
                    sql += " and p.org_serial=@_ORG_SERIAL" + Environment.NewLine;
                    param[2] = OPM_BL.setParameter("@_ORG_SERIAL", SqlDbType.VarChar, ORG);
                }
                if (PERSON_NAME != null && PERSON_NAME != "")
                {
                    sql += " and isnull(p.prefix_name,'') + isnull(p.name,'') + ' ' + isnull(p.surname,'') like '%' + @_PERSON_NAME + '%'" + Environment.NewLine;
                    param[3] = OPM_BL.setParameter("@_PERSON_NAME", SqlDbType.VarChar, PERSON_NAME);
                }
                if (POSNAME != null && POSNAME != "")
                {
                    sql += " and p.pos_name like '%'+@_POS_NAME+'%'" + Environment.NewLine;
                    param[4] = OPM_BL.setParameter("@_POS_NAME", SqlDbType.VarChar, POSNAME);
                }

                sql += " and convert(varchar(8),s.created_date,112) between @_DATE_FROM and @_DATE_TO" + Environment.NewLine;

                DataTable dt = OPM_BL.GetDatatable(sql, param);
                DataRow dr;

                if (dt.Rows.Count > 0)
                {
                    string DateFrom = OPM_BL.GetTHDate(DATE_FROM);
                    string DateTo = OPM_BL.GetTHDate(DATE_TO);
                    if (vReportFormat == "EXCEL")
                    {
                        lblErrorMessage.Text = "";
                        ExcelReportENG.ExportReportODS_RP09(DateFrom, DateTo, UserName, UserOrg, dt, Response);
                    }
                    else if (vReportFormat == "PDF")
                    {
                        rpt.Load(Server.MapPath(home_floder + "/" + reportname + ".rpt"));
                        rpt.DataDefinition.FormulaFields["UserName"].Text = "'" + UserName + "'";
                        rpt.DataDefinition.FormulaFields["UserOrg"].Text = "'" + UserOrg + "'";
                        rpt.DataDefinition.FormulaFields["DateFrom"].Text = "'" + DateFrom + "'";
                        rpt.DataDefinition.FormulaFields["DateTo"].Text = "'" + DateTo + "'";
                        //string printDate = DateTime.Now.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("th-TH"));
                        rpt.DataDefinition.FormulaFields["PrintDate"].Text = "'" + printDate + "'";
                        rpt.DataDefinition.FormulaFields["strOrganize"].Text = "'" + ORGName + "'";
                        rpt.SetDataSource(dt);
                        lblErrorMessage.Text = "";

                        CreatePDFReport(rpt, reportname);
                    }
                }
                else
                {
                    lblErrorMessage.Text = "ไม่พบข้อมูล";
                    CrystalReportViewer1.Visible = false;
                }
                #endregion
            }
   //         else if ((reportname == "ODS_UT0403") || (reportname == "ODS_UT0404"))
 //           {
//                #region "ODS_UT0403&ODS_UT0404"
//                string UserCode = Request.QueryString["USER_CODE"];
//                //UserCode = "chantiki"; 

//                SqlParameter[] param = new SqlParameter[1];
//                string sql = " " + Environment.NewLine;
//                sql += " select isnull(p.prefix_name,'')prefix_name, isnull(p.name,'')name, isnull(p.surname,'')surname,p.user_code,p.user_pwd,p.email,s.sys_code,s.sys_name" + Environment.NewLine;
//                sql += " from ODS_USER_ROLE ur" + Environment.NewLine;
//                sql += " inner join ODS_SYSTEM s on s.sys_code=ur.SYS_CODE" + Environment.NewLine;
//                sql += " inner join ODS_SYSTEM_ROLE sr on sr.sys_code=ur.sys_code and sr.role_id=ur.role_id" + Environment.NewLine;
//                sql += " inner join vw_CMN_PERSON p on p.user_code=ur.user_code" + Environment.NewLine;
//                sql += " inner join CTLT_ORGANIZE org on org.org_serial=ur.org_serial" + Environment.NewLine;
//                sql += " where 1=1" + Environment.NewLine;
//                sql += "###" + Environment.NewLine;
//                //******************************************// 
//                sql += " union all " + Environment.NewLine;
//                sql += " select isnull(p.prefix_name,'')prefix_name, isnull(p.name,'')name, isnull(p.surname,'')surname, " + Environment.NewLine;
//                sql += " p.user_code,p.user_pwd,p.email, " + Environment.NewLine;
//                sql += " s.sys_code,s.sys_name" + Environment.NewLine;
//                sql += " from ODS_SYSTEM s " + Environment.NewLine;
//                sql += " cross join vw_CMN_PERSON p " + Environment.NewLine; //ขอแนะนำให้รู้จักกับคำสั่ง cross join 
//                sql += " inner join CTLT_ORGANIZE org on org.org_serial=p.org_serial " + Environment.NewLine;
//                sql += " where s.is_external=1 and is_hidden =1" + Environment.NewLine;
//                sql += "###" + Environment.NewLine;

//                string wh = "";
//                if (UserCode != null && UserCode != "")
//                {
//                    wh = " and p.user_code = @_User_Code" + Environment.NewLine;
//                    param[0] = OPM_BL.setParameter("@_User_Code", SqlDbType.VarChar, UserCode);
//                }
//                sql = sql.Replace("###", wh);
//                DataTable dt = OPM_BL.GetDatatable(sql, param);
//                DataRow dr;



//                if (dt.Rows.Count > 0)
//                {
//                    rpt.Load(Server.MapPath(home_floder + "/" + reportname + ".rpt"));
//                    rpt.SetDataSource(dt);
//                    lblErrorMessage.Text = "";
//                    CreatePDFReport(rpt, reportname);
//                }
//                else
//<<<<<<< HEAD
//                {
//                    sql = "";
//                    sql += " select isnull(p.prefix_name,'')prefix_name, isnull(p.name,'')name, isnull(p.surname,'')surname,p.user_code,p.user_pwd,p.email,'' sys_code,'' sys_name " + Environment.NewLine;
//                    sql += " from CTLT_EMP e " + Environment.NewLine;
//                    sql += " left join vw_CMN_PERSON p on e.EMP_ID=p.ID " + Environment.NewLine;
//=======
//                {               
//                    sql = "";
//                    sql += " select isnull(p.prefix_name,'')prefix_name, isnull(p.name,'')name, isnull(p.surname,'')surname,p.user_code,p.user_pwd,p.email,'' sys_code,'' sys_name " + Environment.NewLine;
//                    sql += " from CTLT_EMP e " + Environment.NewLine;
//                    sql += " left join vw_CMN_PERSON p on e.EMP_ID=p.ID " + Environment.NewLine;
//>>>>>>> b536d31e6740edc1c62a2b386dfb28cea0a91dd6
//                    sql += " where p.user_code ='" + UserCode + "' " + Environment.NewLine;
//                    DataTable dt_Emp = OPM_BL.GetDatatable(sql, null);
//                    if (dt_Emp.Rows.Count > 0)
//                    {
//                        rpt.Load(Server.MapPath(home_floder + "/" + reportname + ".rpt"));
//<<<<<<< HEAD
//                        rpt.SetDataSource(dt_Emp);
//=======
//                        rpt.SetDataSource(dt);
//>>>>>>> b536d31e6740edc1c62a2b386dfb28cea0a91dd6
//                        lblErrorMessage.Text = "";
//                        CreatePDFReport(rpt, reportname);
//                    }
//                    else
//                    {
//                        lblErrorMessage.Text = "ไม่พบข้อมูล";
//                        CrystalReportViewer1.Visible = false;
//                    }
//                }
//                #endregion
 //           }
            else if ((reportname == "ODS_UT0403") || (reportname == "ODS_UT0404"))
            {
                #region "ODS_UT0403&ODS_UT0404"
                string UserCode = Request.QueryString["USER_CODE"];
                //UserCode = "chantiki";

                SqlParameter[] param = new SqlParameter[1];
                string sql = " " + Environment.NewLine;
                sql += " select isnull(p.prefix_name,'')prefix_name, isnull(p.name,'')name, isnull(p.surname,'')surname,p.user_code,p.user_pwd,p.email,s.sys_code,s.sys_name" + Environment.NewLine;
                sql += " from ODS_USER_ROLE ur" + Environment.NewLine;
                sql += " inner join ODS_SYSTEM s on s.sys_code=ur.SYS_CODE" + Environment.NewLine;
                sql += " inner join ODS_SYSTEM_ROLE sr on sr.sys_code=ur.sys_code and sr.role_id=ur.role_id" + Environment.NewLine;
                sql += " inner join vw_CMN_PERSON p on p.user_code=ur.user_code" + Environment.NewLine;
                sql += " inner join CTLT_ORGANIZE org on org.org_serial=ur.org_serial" + Environment.NewLine;
                sql += " where 1=1" + Environment.NewLine;
                sql += "###" + Environment.NewLine;
                //******************************************//
                sql += " union all " + Environment.NewLine;
                sql += " select isnull(p.prefix_name,'')prefix_name, isnull(p.name,'')name, isnull(p.surname,'')surname, " + Environment.NewLine;
                sql += " p.user_code,p.user_pwd,p.email, " + Environment.NewLine;
                sql += " s.sys_code,s.sys_name" + Environment.NewLine;
                sql += " from ODS_SYSTEM s " + Environment.NewLine;
                sql += " cross join vw_CMN_PERSON p  " + Environment.NewLine;  //ขอแนะนำให้รู้จักกับคำสั่ง cross join
                sql += " inner join CTLT_ORGANIZE org on org.org_serial=p.org_serial " + Environment.NewLine;
                sql += " where s.is_external=1 and is_hidden =1" + Environment.NewLine;
                sql += "###" + Environment.NewLine;

                string wh = "";
                if (UserCode != null && UserCode != "")
                {
                    wh = " and p.user_code = @_User_Code" + Environment.NewLine;
                    param[0] = OPM_BL.setParameter("@_User_Code", SqlDbType.VarChar, UserCode);
                }
                sql = sql.Replace("###", wh);
                DataTable dt = OPM_BL.GetDatatable(sql, param);
                DataRow dr;



                if (dt.Rows.Count > 0)
                {
                    rpt.Load(Server.MapPath(home_floder + "/" + reportname + ".rpt"));
                    rpt.SetDataSource(dt);
                    lblErrorMessage.Text = "";
                    CreatePDFReport(rpt, reportname);
                }
                else
                {
                    sql = "";
                    sql += " select isnull(p.prefix_name,'')prefix_name, isnull(p.name,'')name, isnull(p.surname,'')surname,p.user_code,p.user_pwd,p.email,'' sys_code,'' sys_name " + Environment.NewLine;
                    sql += " from CTLT_EMP e " + Environment.NewLine;
                    sql += " left join vw_CMN_PERSON p on e.EMP_ID=p.ID " + Environment.NewLine;
                    sql += " where p.user_code ='" + UserCode + "' " + Environment.NewLine;
                    DataTable dt_Emp = OPM_BL.GetDatatable(sql, null);
                    if (dt_Emp.Rows.Count > 0)
                    {
                        rpt.Load(Server.MapPath(home_floder + "/" + reportname + ".rpt"));
                        rpt.SetDataSource(dt_Emp);
                        lblErrorMessage.Text = "";
                        CreatePDFReport(rpt, reportname);
                    }
                    else
                    {
                        lblErrorMessage.Text = "ไม่พบข้อมูล";
                        CrystalReportViewer1.Visible = false;
                    }
                }
                #endregion
            }

            else if (reportname == "ODS_UT0409")
            {
                #region "ODS_UT0409"
                string SEARCH = Request.QueryString["SEARCH"];
                string sys_code_SEARCH = Request.QueryString["sys_code_SEARCH"];
                string prg_id_SEARCH = Request.QueryString["prg_id_SEARCH"];
                string TPK_SEARCH = Request.QueryString["TPK_SEARCH"];
                string IP_SEARCH = Request.QueryString["IP_SEARCH"];
                string action_SEARCH = Request.QueryString["action_SEARCH"];
                string DATE_FROM = Request.QueryString["DATE_FROM"];
                string DATE_TO = Request.QueryString["DATE_TO"];

                //ORG = "65";
                //START_DATE = "20160116";
                //END_DATE = "20160116";
                //UserName = "99999ชื่อสกุล99999";
                //UserOrg = "xxxxxหน่วยงานxxxxx";


                SqlParameter[] param = new SqlParameter[8];
                string sql = " " + Environment.NewLine;
                sql = " SELECT AL.id,AL.created_by,AL.created_date,AL.updated_by,AL.updated_date,AL.sys_code,AL.prg_id " + Environment.NewLine;
                sql += " ,AL.token,AL.action,AL.table_name,AL.pk,AL.action_desc,AL.active_status,AL.client_ip,AL.client_browser,AL.server_url " + Environment.NewLine;
                sql += " ,PS.NAME+' '+SURNAME as user_code " + Environment.NewLine;
                sql += " FROM  ODS_APP_LOG AL join vw_CMN_PERSON PS on  AL.user_code = PS.USER_CODE " + Environment.NewLine;

                sql += " WHERE 1=1 ";
                if (SEARCH != null)
                {
                    sql += " AND PS.NAME+' '+SURNAME LIKE  '%' + @_SEARCH + '%' " + Environment.NewLine;
                    param[0] = OPM_BL.setParameter("@_SEARCH", SqlDbType.VarChar, SEARCH);
                }
                if (sys_code_SEARCH != null)
                {
                    sql += " AND AL.sys_code LIKE '%' + @_sys_code_SEARCH + '%'" + Environment.NewLine;
                    param[1] = OPM_BL.setParameter("@_sys_code_SEARCH", SqlDbType.VarChar, sys_code_SEARCH);
                }
                if (prg_id_SEARCH != null)
                {
                    sql += " AND AL.prg_id LIKE '%' + @_prg_id_SEARCH + '%'" + Environment.NewLine;
                    param[2] = OPM_BL.setParameter("@_prg_id_SEARCH", SqlDbType.VarChar, prg_id_SEARCH);
                }
                if (TPK_SEARCH != null)
                {
                    sql += " AND ( AL.action_desc LIKE '%' + @_TPK_SEARCH + '%'  " + Environment.NewLine;
                    sql += "            OR AL.table_name LIKE '%' + @_TPK_SEARCH + '%' " + Environment.NewLine;
                    sql += "            OR AL.pk LIKE '%' + @_TPK_SEARCH + '%' " + Environment.NewLine;
                    sql += "          )" + Environment.NewLine;
                    param[3] = OPM_BL.setParameter("@_TPK_SEARCH", SqlDbType.VarChar, TPK_SEARCH);
                }
                if (IP_SEARCH != null)
                {
                    sql += " AND AL.client_ip LIKE '%' + @_IP_SEARCH + '%' " + Environment.NewLine;
                    param[4] = OPM_BL.setParameter("@_IP_SEARCH", SqlDbType.VarChar, IP_SEARCH);
                }
                if (action_SEARCH != null)
                {
                    sql += " AND AL.action LIKE '%' + @_action_SEARCH + '%' " + Environment.NewLine;
                    param[5] = OPM_BL.setParameter("@_action_SEARCH", SqlDbType.VarChar, action_SEARCH);
                }
                if (DATE_FROM != null && DATE_TO != null)
                {
                    sql += " AND (Convert(varchar(8),AL.created_date,112) between  @_DATE_FROM  and  @_DATE_TO  )" + Environment.NewLine;
                    param[6] = OPM_BL.setParameter("@_DATE_FROM", SqlDbType.VarChar, DATE_FROM);
                    param[7] = OPM_BL.setParameter("@_DATE_TO", SqlDbType.VarChar, DATE_TO);
                }
                sql += " order by created_date desc";

                DataTable dt = OPM_BL.GetDatatable(sql, param);
                if (dt.Rows.Count > 0)
                {

                    string DateFrom = "-";
                    string DateTo = "-";
                    if (DATE_FROM != null)
                    {
                        DateFrom = OPM_BL.GetTHDate(DATE_FROM);
                    }
                    if (DATE_TO != null)
                    {
                        DateTo = OPM_BL.GetTHDate(DATE_TO);
                    }

                    if (vReportFormat == "PDF")
                    {
                        rpt.Load(Server.MapPath(home_floder + "/" + reportname + ".rpt"));
                        rpt.DataDefinition.FormulaFields["UserName"].Text = "'" + UserName + "'";
                        rpt.DataDefinition.FormulaFields["UserOrg"].Text = "'" + UserOrg + "'";
                        rpt.DataDefinition.FormulaFields["DateFrom"].Text = "'" + DateFrom + "'";
                        rpt.DataDefinition.FormulaFields["DateTo"].Text = "'" + DateTo + "'";
                        rpt.DataDefinition.FormulaFields["PrintDate"].Text = "'" + printDate + "'";
                        rpt.SetDataSource(dt);
                        lblErrorMessage.Text = "";

                        CreatePDFReport(rpt, reportname);
                    }

                }
                else
                {
                    lblErrorMessage.Text = "ไม่พบข้อมูล";
                    CrystalReportViewer1.Visible = false;
                }
                #endregion
            }
            #endregion

            #region "DPIS"
            else if (reportname == "DPIS_RP01")
            {
                #region "DPIS_RP01"

                SqlParameter[] param = new SqlParameter[1];
                string sys_code = Request.QueryString["sys_code"];
                //string UserName = Request.QueryString["UserName"];
                //string UserOrg = Request.QueryString["UserOrg"];
                
                string sql = "select system_code,db_type,serverHost,ServerPort,databasename,databaseuserid,databasepassword,";
                sql += " case active_status when 1 then 'ใช้งาน' else 'ไม่ใช้งาน' end active_status from CF_DB_CONFIG";
                sql += " where 1=1 ";
                if (sys_code != null)
                {
                    sql += " and system_code = @_sys_code";
                    param[0] = OPM_BL.setParameter("@_sys_code", SqlDbType.VarChar, sys_code);
                }

                sql += " order by system_code ";

                DataTable dt = OPM_BL.GetDatatable(sql, param);
                if (dt.Rows.Count > 0)
                {
                    if (vReportFormat == "EXCEL")
                    {
                        lblErrorMessage.Text = "";
                        ExcelReportENG.ExportReportDPIS_RP01(UserName, UserOrg, dt, Response);
                    }
                    else if (vReportFormat == "PDF")
                    {
                        rpt.Load(Server.MapPath(home_floder + "/" + reportname + ".rpt"));
                        rpt.DataDefinition.FormulaFields["UserName"].Text = "'" + UserName + "'";
                        rpt.DataDefinition.FormulaFields["UserOrg"].Text = "'" + UserOrg + "'";
                        //string printDate = DateTime.Now.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("th-TH"));
                        rpt.DataDefinition.FormulaFields["PrintDate"].Text = "'" + printDate + "'";
                        rpt.SetDataSource(dt);
                        lblErrorMessage.Text = "";

                        CreatePDFReport(rpt, reportname);
                    }
                }
                else
                {
                    lblErrorMessage.Text = "ไม่พบข้อมูล";
                    CrystalReportViewer1.Visible = false;
                }
                #endregion
            }

            else if (reportname == "DPIS_RP02")
            {
                #region "DPIS_RP02"

                SqlParameter[] param = new SqlParameter[3];
                string sys_code = Request.QueryString["sys_code"];
                string DateFrom = Request.QueryString["DateFrom"];
                string DateTo = Request.QueryString["DateTo"];
                //string UserName = Request.QueryString["UserName"];
                //string UserOrg = Request.QueryString["UserOrg"];

                //string sql = "select (select [dbo].[ThaiDateAbbrMonth](created_date)) created_date,sys_code,to_sys_code,action,action_desc from ODS_APP_LOG";
                string sql = "select (select [dbo].[ThaiDateAbbrMonth](created_date)) + ' ' + convert(varchar(10),created_date,108) created_date,sys_code,to_sys_code,pk action_desc, ";
                sql += " case table_name when 'PSST_PERSON' then 'บุคลากร' when 'CTLT_ORGANIZE' then 'หน่วยงาน' when 'PSST_POSITION' then 'ตำแหน่ง' end  action";
                sql += " from ODS_APP_LOG";
                sql += " where isnull(to_sys_code,'') <> ''";
                sql += " and table_name in('PSST_PERSON','CTLT_ORGANIZE','PSST_POSITION')";

                if (sys_code != null)
                {
                    sql += " and (sys_code like '%' + @_sys_code + '%' or to_sys_code like '%' + @_sys_code + '%') ";
                    param[0] = OPM_BL.setParameter("@_sys_code", SqlDbType.VarChar, sys_code);
                }
                sql += " and convert(varchar(8),created_date,112) between @_DATE_FROM and @_DATE_TO " + Environment.NewLine;
                param[1] = OPM_BL.setParameter("@_DATE_FROM", SqlDbType.VarChar, DateFrom);
                param[2] = OPM_BL.setParameter("@_DATE_TO", SqlDbType.VarChar, DateTo);

                sql += " order by created_date ";

                DataTable dt = OPM_BL.GetDatatable(sql, param);
                if (dt.Rows.Count > 0)
                {
                    string strDateFrom = OPM_BL.GetTHDate(DateFrom);
                    string strDateTo = OPM_BL.GetTHDate(DateTo);
                    if (vReportFormat == "EXCEL")
                    {
                        lblErrorMessage.Text = "";
                        ExcelReportENG.ExportReportDPIS_RP02(strDateFrom, strDateTo, UserName, UserOrg, dt, Response);
                    }
                    else if (vReportFormat == "PDF")
                    {
                        rpt.Load(Server.MapPath(home_floder + "/" + reportname + ".rpt"));
                        rpt.DataDefinition.FormulaFields["DateFrom"].Text = "'" + strDateFrom + "'";
                        rpt.DataDefinition.FormulaFields["DateTo"].Text = "'" + strDateTo + "'";
                        rpt.DataDefinition.FormulaFields["UserName"].Text = "'" + UserName + "'";
                        rpt.DataDefinition.FormulaFields["UserOrg"].Text = "'" + UserOrg + "'";
                        rpt.DataDefinition.FormulaFields["PrintDate"].Text = "'" + printDate + "'";
                        rpt.SetDataSource(dt);
                        lblErrorMessage.Text = "";

                        CreatePDFReport(rpt, reportname);
                    }
                }
                else
                {
                    lblErrorMessage.Text = "ไม่พบข้อมูล";
                    CrystalReportViewer1.Visible = false;
                }
                #endregion
            }
            #endregion
            else if (reportname == "xxx")
            {
                #region "xxx"
                #endregion
            }

            //rpt.Database.Tables[0].ApplyLogOnInfo(tbLogon);
            //CrystalReportViewer1.LogOnInfo.Add(tbLogon);
            CrystalReportViewer1.ReportSource = rpt;
        }
        catch (Exception ex)
        {
            lblErrorMessage.Text = ex.ToString();
            CrystalReportViewer1.Visible = false;
        }

    }



    private void CreatePDFReport(ReportDocument rpt, string ReportName) {
        try {
            System.IO.Stream oStream = null;
            byte[] byteArray = null;
            oStream = rpt.ExportToStream(ExportFormatType.PortableDocFormat);

            byteArray = new byte[oStream.Length];
            oStream.Read(byteArray, 0, Convert.ToInt32(oStream.Length - 1));
            Response.ClearContent();
            Response.ClearHeaders();
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Disposition", "inline; filename=" + ReportName + ".pdf");
            Response.BinaryWrite(byteArray);
            Response.Flush();
            Response.Close();
            rpt.Close();
            rpt.Dispose();
            
        }
        catch (Exception ex) { 
        
        }
    
    }

    private void CreateWordReport(ReportDocument rpt, string ReportName)
    {
        try
        {
            rpt.ExportToHttpResponse(ExportFormatType.WordForWindows, Response, true, ReportName);
        }
        catch (Exception ex)
        {

        }
    }

    private void CreateExcelReport(ReportDocument rpt, string ReportName)
    {
        //Export Excel เขียนโค้ดแค่ 2 บรรทัด 
        try
        {
            //ใส่ Try ... catch ไปอย่างนั้นเอง ยังไงก็เข้า catch อยู่ดี แต่ก็ออก Excel ได้ ไม่รู้ทำไมเหมือนกัน
            rpt.ExportToHttpResponse(ExportFormatType.Excel, Response, true, ReportName);
        }
        catch (Exception ex) { 
        
        }
        

        




        //try
        //{
        //    ExportOptions CrExportOptions;

        //    DiskFileDestinationOptions CrDiskFileDestinationOptions = new DiskFileDestinationOptions();
        //    ExcelFormatOptions CrFormatTypeOptions = new ExcelFormatOptions();
        //    CrDiskFileDestinationOptions.DiskFileName = "D:\\1.xls";
        //    CrExportOptions = rpt.ExportOptions;
        //    CrExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
        //    CrExportOptions.ExportFormatType = ExportFormatType.Excel;
        //    CrExportOptions.DestinationOptions = CrDiskFileDestinationOptions;
        //    CrExportOptions.FormatOptions = CrFormatTypeOptions;
        //    rpt.Export();
        //}
        //catch (Exception ex)
        //{
        //    //MessageBox.Show(ex.ToString());
        //}

        //try
        //{
        //    System.IO.Stream oStream = null;
        //    byte[] byteArray = null;
        //    oStream = rpt.ExportToStream(ExportFormatType.Excel);

        //    byteArray = new byte[oStream.Length];
        //    oStream.Read(byteArray, 0, Convert.ToInt32(oStream.Length - 1));
        //    Response.ClearContent();
        //    Response.ClearHeaders();
        //    Response.ContentType = "application/vnd.ms-excel";
        //    //Response.ContentType = "application/msexcel";
        //    Response.AddHeader("Content-Disposition", "inline; filename=" + ReportName + ".xls");

        //    //Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        //    //Response.AddHeader("Content-Disposition", "inline; filename=" + ReportName + ".xlsx");
        //    Response.BinaryWrite(byteArray);
        //    Response.Flush();
        //    Response.Close();
        //    rpt.Close();
        //    rpt.Dispose();
        //}
        //catch (Exception ex)
        //{

        //}

    }
    
}
