using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.DirectoryServices;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Web.UI.WebControls;

	public class textControlLib
	{


        OPM_BL BL = new OPM_BL();
		public void ImplementJavaMoneyText(TextBox Obj)
		{
			Obj.Attributes["OnChange"] = "this.value=formatmoney(this.value,'0','999999999999');";
			Obj.Style["Text-Align"] = "Right";
		}

		public void ImplementJavaIntegerText(TextBox Obj)
		{
			Obj.Attributes["OnChange"] = "this.value=formatinteger(this.value,'0','999999999999');";
			Obj.Style["Text-Align"] = "Right";
		}

		public void ImplementJavaFloatText(TextBox Obj)
		{
			Obj.Attributes["OnChange"] = "this.value=formatfloat(this.value,'0','999999999999');";
			Obj.Style["Text-Align"] = "Right";
		}

		public void ImplementJavaOnlyNumberText(TextBox Obj)
		{
			Obj.Attributes["OnChange"] = "this.value=formatonlynumber(this.value);";
		}



	}

