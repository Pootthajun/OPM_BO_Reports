﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.DirectoryServices;
using System.Text.RegularExpressions;
using System.Globalization;
 
public class GenericLib
{
   
    public char chr0 = '0';

    public char CCHR(string str)
    {
        if (str != "")
        {
            return Convert.ToChar(str.Substring(0, 1));
        }
        else
        {
            return Convert.ToChar("");
        }
    }

    public string ReportProgrammingDate(DateTime Input)
    {
        return Input.Year + "-" + Input.Month.ToString().PadLeft(2, chr0) + "-" + Input.Day.ToString().PadLeft(2, chr0);
    }

    public string ReportProgrammingDotDate(DateTime Input)
    {
        return Input.Day.ToString().PadLeft(2, chr0) + "." + Input.Month.ToString().PadLeft(2, chr0) + "." + Input.Year;
    }

    public string ReportProgrammingFullDate(DateTime Input)
    {
        string StrDate = Input.Month.ToString();
        StrDate = ReportShortMonthEnglish(Input.Month);
        return Input.Day.ToString().PadLeft(2, chr0) + "-" + StrDate + "-" + Input.Year;
    }

    public bool IsProgrammingDate(string Input)
    {
        try
        {
            System.DateTime Temp = DateTime.Parse(Input);
            return true;
        }
        catch (Exception ex)
        {
            return false;
        }
    }

    public bool IsProgrammingDate(string Input, string Format)
    {
        try
        {
            CultureInfo Provider = CultureInfo.GetCultureInfo("en-US");
            DateTime Temp = DateTime.ParseExact(Input, Format, Provider);
            return true;
        }
        catch (Exception ex)
        {
            return false;
        }
    }


    public bool IsValidEmailFormat(string input)
    {
        return Regex.IsMatch(input, "^([0-9a-zA-Z]([-\\.\\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$");
    }

    public string OriginalFileName(string FullPath)
    {
        return FullPath.Substring(FullPath.LastIndexOf("\\") + 1);
    }

    public string ReportMonthThai(int MonthID)
    {
        switch (MonthID)
        {
            case 1:
                return "มกราคม";
            case 2:
                return "กุมภาพันธ์";
            case 3:
                return "มีนาคม";
            case 4:
                return "เมษายน";
            case 5:
                return "พฤษภาคม";
            case 6:
                return "มิถุนายน";
            case 7:
                return "กรกฎาคม";
            case 8:
                return "สิงหาคม";
            case 9:
                return "กันยายน";
            case 10:
                return "ตุลาคม";
            case 11:
                return "พฤศจิกายน";
            case 12:
                return "ธันวาคม";
            default:
                return "";
        }
    }

    public string ReportMonthThaiShort(int MonthID)
    {
        switch (MonthID)
        {
            case 1:
                return "ม.ค.";
            case 2:
                return "ก.พ.";
            case 3:
                return "มี.ค.";
            case 4:
                return "เม.ย.";
            case 5:
                return "พ.ค.";
            case 6:
                return "มิ.ย.";
            case 7:
                return "ก.ค.";
            case 8:
                return "ส.ค.";
            case 9:
                return "ก.ย.";
            case 10:
                return "ต.ค.";
            case 11:
                return "พ.ย.";
            case 12:
                return "ธ.ค.";
            default:
                return "";
        }
    }

    public string ReportMonthEnglish(int MonthID)
    {
        switch (MonthID)
        {
            case 1:
                return "January";
            case 2:
                return "February";
            case 3:
                return "March";
            case 4:
                return "April";
            case 5:
                return "May";
            case 6:
                return "June";
            case 7:
                return "July";
            case 8:
                return "August";
            case 9:
                return "September";
            case 10:
                return "October";
            case 11:
                return "November";
            case 12:
                return "December";
            default:
                return "";
        }
    }

    public string ReportShortMonthEnglish(int MonthID)
    {
        switch (MonthID)
        {
            case 1:
                return "Jan";
            case 2:
                return "Feb";
            case 3:
                return "Mar";
            case 4:
                return "Apr";
            case 5:
                return "May";
            case 6:
                return "Jun";
            case 7:
                return "Jul";
            case 8:
                return "Aug";
            case 9:
                return "Sep";
            case 10:
                return "Oct";
            case 11:
                return "Nov";
            case 12:
                return "Dec";
            default:
                return "";
        }
    }


    public int ReportMonthEnglishToNumber(string DateFullName)
    {
        switch (DateFullName)
        {
            case "January":
                return 1;
            case "February":
                return 2;
            case "March":
                return 3;
            case "April":
                return 4;
            case "May":
                return 5;
            case "June":
                return 6;
            case "July":
                return 7;
            case "August":
                return 8;
            case "September":
                return 9;
            case "October":
                return 10;
            case "November":
                return 11;
            case "December":
                return 12;
            default:
                return 0;
        }
    }

    public string ReportThaiDate(DateTime TheDate)
    {
        string Result = TheDate.Day + " " + ReportMonthThaiShort(TheDate.Month) + " " + (TheDate.Year + 543);
        return Result;
    }
    public string ReportThaiDateTime(DateTime TheDate)
    {
        string Result = ReportThaiDate(TheDate) + " " + TheDate.Hour.ToString().PadLeft(2, chr0) + ":" + TheDate.Minute.ToString().PadLeft(2, chr0);
        return Result;
    }


    ////Dim LDAP_Path คือ ชื่อ Domain หรือชื่อ Window ที่ต้องการตรวจสอบสิทธิ์-----------
    //public string AuthenticateUser(string LDAP_Path, string user, string pass)
    //{
    //    System.DirectoryServices.DirectoryEntry de = new System.DirectoryServices.DirectoryEntry(LDAP_Path, user, pass, AuthenticationTypes.Secure);
    //    try
    //    {
    //        DirectorySearcher ds = new DirectorySearcher(de);
    //        ds.FindOne();
    //        return "";
    //    }
    //    catch (Exception ex)
    //    {
    //        return ex.Message;
    //    }
    //}

    //public bool AuthenticateUser(string Domain, string Username, string Password, string LDAP_Path, ref string Errmsg)
    //{

    //    Errmsg = "";
    //    string domainAndUsername = Domain + "\\" + Username;
    //    DirectoryEntry entry = new DirectoryEntry(LDAP_Path, domainAndUsername, Password);
    //    entry.AuthenticationType = AuthenticationTypes.Secure;
    //    try
    //    {
    //        DirectorySearcher search = new DirectorySearcher(entry);

    //        search.Filter = "(SAMAccountName=" + Username + ")";

    //        search.PropertiesToLoad.Add("cn");

    //        SearchResult result = search.FindOne();

    //        if (result == null)
    //        {
    //            return false;
    //        }
    //        // Update the new path to the user in the directory

    //        LDAP_Path = result.Path;
    //        string _filterAttribute = (String)result.Properties["cn"][0];
    //    }
    //    catch (Exception ex)
    //    {
    //        Errmsg = ex.Message;
    //        return false;
    //        throw new Exception("Error authenticating user." + ex.Message);
    //    }

    //    return true;

    //}

    public string CompactDecimal(string Number)
    {
        return CDBL(Number).ToString();
    }

    public void PushArray_String(string[] TheArray, string AppendedValue)
    {
        Array.Resize(ref TheArray, TheArray.Length + 1);
        TheArray[TheArray.Length - 1] = AppendedValue;
    }

    public void PushArray_Integer(int[] TheArray, int AppendedValue)
    {
        Array.Resize(ref TheArray, TheArray.Length + 1);
        TheArray[TheArray.Length - 1] = AppendedValue;
    }

    public string[] SplitString(string InputStr, string Spliter)
    {
        string[] splt = { Spliter };
        return InputStr.Split(splt, StringSplitOptions.None);
    }

    public bool IsEqualNull(object Obj)
    {
        return object.Equals(Obj, DBNull.Value);
    }

    public string StringFormatNumber(string Str, int DecimalPlace)
    {
        double dbl = Convert.ToDouble(Str);
        string Format = "{0:#,0";
        if (DecimalPlace > 0)
        {
            Format += ".";
        }
        for (int i = 1; i <= DecimalPlace; i++)
        {
            Format += "0";
        }
        Format += "}";
        return string.Format(Format, dbl);
    }

    public string StringFormatNumber(string Str)
    {
        return StringFormatNumber(Str, 2);
    }

    public string StringFormatNumber(object Obj, int DecimalPlace)
    {
        return StringFormatNumber(Obj.ToString(), DecimalPlace);
    }

    public string StringFormatNumber(object Obj)
    {
        return StringFormatNumber(Obj, 2);
    }

    public double CDBL(string str)
    {
        return Convert.ToDouble(str);
    }

    public double CDBL(int input)
    {
        return Convert.ToDouble(input);
    }

    public double CDBL(object Obj)
    {
        return Convert.ToDouble(Obj);
    }

    //ทศนิยม 2 ตำแหน่ง
    public string StringFormatNumber(Double val)
    {
        return String.Format("{0:F2}", val);
    }

    public int CINT(string str)
    {
        try
        {
            return Convert.ToInt32(str);
        }
        catch
        {
            return 0;
        }
    }

    public string CSTR(object str)
    {
        try
        {
            return str.ToString();
        }
        catch
        {
            return "";
        }
    }

    public int CINT(double input)
    {
        return Convert.ToInt32(input);
    }

    public int CINT(object Obj)
    {
        return Convert.ToInt32(Obj);
    }

    public bool CBOOL(string str)
    {
        return Convert.ToBoolean(str);
    }

    public bool CBOOL(object str)
    {
        if (str != null)
        {
            return Convert.ToBoolean(str.ToString());
        }
        else
        {
            return false;
        }
    }
    
    //ทศนิยม ปัดขึ้น .5  0
    public string StringFormatNumber_1P(Double val)
    {
        return String.Format("{0:00.0}", val);
    }
}