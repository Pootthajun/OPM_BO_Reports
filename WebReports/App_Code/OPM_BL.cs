﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Data;
using System.Data.SqlClient;
//using DevExpress.Web.ASPxEditors;
//using System.Data.OracleClient;

public class OPM_BL
{
    private static string DefaultConnectionString = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
    public static string DefaultConnectionString_Oracle = WebConfigurationManager.ConnectionStrings["DefaultConnection_Oracle"].ToString();
    public static string OracleConnectionString = WebConfigurationManager.ConnectionStrings["OracleConnectionString"].ToString();


    GenericLib GL = new GenericLib();
    System.Globalization.CultureInfo ct_en = new System.Globalization.CultureInfo("en-US");
    public class PersonalInfo
    {
        public string EMAIL = "";
        
    }

    public class Location
    {
        public string DIST = "";
        public string AMPER = "";
        public string PROVINCE = "";        
    }

    public PersonalInfo GetNewEmail(string NAME_ENG, string SURNAME_ENG)
    {
        PersonalInfo Result = new PersonalInfo();

        string SQL = "";
        SQL += " SELECT NAME_ENG,SURNAME_ENG,EMAIL FROM PSST_PERSON WHERE NAME_ENG LIKE '" + NAME_ENG + "'";
        SqlDataAdapter DA = new SqlDataAdapter(SQL, DefaultConnectionString );
        DataTable DT = new DataTable();
        DA.Fill(DT);

        if (DT.Rows.Count > 0)
        {
            for (int i = 0; i <= DT.Rows.Count - 1; i++)
            {
                for (int j = 0; j <= SURNAME_ENG.Length - 1; j++)
                {
                    String NewEmail = NAME_ENG + "." + SURNAME_ENG.Substring(0, j + 1) + "@opm.go.th";
                    DT.DefaultView.RowFilter = " EMAIL LIKE '%" + NAME_ENG + "." + SURNAME_ENG.Substring(0, j + 1) + "@opm.go.th'";
                    if (DT.DefaultView.Count == 0)
                    {
                        Result.EMAIL = NewEmail;
                        break;
                    }

                }
            }
        }
        else {
            String NewEmail = NAME_ENG + "." + SURNAME_ENG.Substring(0,1) + "@opm.go.th";
            Result.EMAIL = NewEmail;
        }
       
        return Result;
    }
    public Location GetSERIAL(string LOC_SERIAL)
    {
        Location Result = new Location();

        string SQL = " SELECT LOC_SERIAL,PROV_CODE,AMP_CODE,DIST_CODE,LOC_NAME,LOC_FULL_NAME FROM CTLT_LOCATION WHERE LOC_SERIAL='" + LOC_SERIAL + "' ORDER BY LOC_NAME";
        SqlDataAdapter DA = new SqlDataAdapter(SQL, DefaultConnectionString);
        DataTable DT = new DataTable();
        DA.Fill(DT);

        if (DT.Rows.Count > 0)
        {
            //-------------------DIST_CODE-----------------
            if (DT.Rows[0]["DIST_CODE"] != DBNull.Value && DT.Rows[0]["DIST_CODE"] != "00")
            {
                Result.DIST = DT.Rows[0]["LOC_SERIAL"].ToString();
            }
            DataTable DT_Filter = new DataTable();
            string SQL_Filter = " SELECT LOC_SERIAL,PROV_CODE,AMP_CODE,DIST_CODE,LOC_NAME,LOC_FULL_NAME FROM CTLT_LOCATION ORDER BY LOC_NAME";
            SqlDataAdapter DA_Filter = new SqlDataAdapter(SQL_Filter, DefaultConnectionString);
            DA_Filter.Fill(DT_Filter);

            //-------------------AMP_CODE-----------------
            DT_Filter.DefaultView.RowFilter =" PROV_CODE='" + DT.Rows[0]["PROV_CODE"] +"'	AND AMP_CODE='"+ DT.Rows[0]["AMP_CODE"] +"' AND DIST_CODE IN ('00') ";
            if (DT_Filter.DefaultView.Count>0)
            Result.AMPER = DT_Filter.DefaultView[0]["LOC_SERIAL"].ToString();
              

            DT_Filter.DefaultView.RowFilter =" PROV_CODE='" + DT.Rows[0]["PROV_CODE"] +"'	AND AMP_CODE  IN ('00')  AND DIST_CODE IN ('00') ";
            if (DT_Filter.DefaultView.Count > 0)
            Result.PROVINCE  = DT_Filter.DefaultView[0]["LOC_SERIAL"].ToString();
            
        }       
       
        return Result;
    }
    //public int GetNewID(string ID, string Table_Name)
    //{
    //    string SQL = "SELECT ISNULL(MAX(" + ID + "),0)+1 " + ID  + "";
    //    SQL += " FROM " + Table_Name + "";

    //    SqlDataAdapter DA = new SqlDataAdapter(SQL, DefaultConnectionString );
    //    DataTable DT = new DataTable();
    //    DA.Fill(DT);
    //    return GL.CINT(DT.Rows[0][0]);

    //}

    public String CalculateAge(string ID)
    {
        return CalculateAge(ID, "");
    }

    public String CalculateAge(string ID,String BORN_DATE)
    {
        string SQL = "";
        SQL += " SELECT ID,BORN_DATE,START_DATE \n";
        switch (BORN_DATE.ToString())
        {
            case "":
                SQL += " ,( YEAR( GETDATE()) - YEAR(BORN_DATE)) AS AGE \n";
                SQL += " ,(MONTH( GETDATE()) - MONTH(BORN_DATE)) AS MONTH \n";
                break;
            default:
                SQL += " ,( YEAR( GETDATE()) - YEAR('" + BORN_DATE + "')) AS AGE \n";
                SQL += " ,(MONTH( GETDATE()) - MONTH('" + BORN_DATE + "')) AS MONTH \n";
                break;
        }        
        SQL += " FROM PSST_PERSON \n";
        SQL += " where ID='" + ID  + "' \n";

        SqlDataAdapter DA = new SqlDataAdapter(SQL, DefaultConnectionString );
        DataTable DT = new DataTable();
        DA.Fill(DT);
        String lblAGE = "";

        if (DT.Rows.Count==1){
            if ((DT.Rows[0]["BORN_DATE"] != null && DT.Rows[0]["BORN_DATE"] != DBNull.Value )|| BORN_DATE != "")
            {
                if (DT.Rows[0]["AGE"] != DBNull.Value && DT.Rows[0]["AGE"].ToString() != "0")
                {
                    lblAGE = " " + GL.CINT(DT.Rows[0]["AGE"]).ToString() + " ปี ";
                }
                if (DT.Rows[0]["MONTH"] != DBNull.Value && DT.Rows[0]["MONTH"].ToString() != "0")
                {
                    if (GL.CINT(DT.Rows[0]["MONTH"]) < 0)
                    {
                        lblAGE += GL.CINT(DT.Rows[0]["MONTH"]) * -1 + " เดือน";
                    }
                    else { 
                        lblAGE += GL.CINT(DT.Rows[0]["MONTH"]).ToString() + " เดือน";                    
                    }
                }
            }
        }
        return lblAGE;
    }

    public DataTable getORGInfo(int ORG_SERIAL)
    {
        string sql = "SELECT ORG.ORG_SERIAL,ORG_LEVEL,ORG_NAME,ORG_ABBR,ORG_CODE_DPIS,UPPER_ORG_SERIAL\n";
        sql += " FROM vw_SMM_ORG ORG\n";
        sql += " WHERE ORG_SERIAL=" + ORG_SERIAL;
        SqlDataAdapter da = new SqlDataAdapter(sql, DefaultConnectionString);
        DataTable dt = new DataTable();
        da.Fill(dt);
        return dt;
    }

    public static String GetPSN_ID_DPIS(string ID) {
        SqlParameter[] param = new SqlParameter[1];
        string sql = " SELECT PER_ID_DPIS FROM PSST_PERSON WHERE ID = @_ID";
        param[0] = OPM_BL.setParameter("@_ID", SqlDbType.VarChar, ID);
        DataTable dt = OPM_BL.GetDatatable(sql,param);
        string PSN_ID_DPIS = "";
        if (dt.Rows.Count > 0)
        {
            PSN_ID_DPIS = dt.Rows[0]["PER_ID_DPIS"].ToString();
        }
        else {
            PSN_ID_DPIS = "-1";
        }
        return PSN_ID_DPIS;
    }

    //public DataTable getOracleDatable()
    //{
    //    string sql = "";

    //    sql += " SELECT 	POH_ID \n";
    //    sql += "           , POH_EFFECTIVEDATE, POH_ENDDATE, POH_ORG2, POH_ORG3, POH_ASS_ORG \n";
    //    sql += "           ,PL_CODE, PM_CODE, PT_CODE, PN_CODE, EP_CODE, TP_CODE \n";
    //    sql += "           , PER_POSITIONHIS.LEVEL_NO \n";
    //    sql += "           ,PER_LEVEL.LEVEL_NAME POH_LEVEL_NAME    \n";
    //    sql += "           , PER_LEVEL.POSITION_LEVEL \n";
    //    sql += "           ,NVL(POH_POS_NO, '-') POH_POS_NO \n";
    //    sql += "           ,POH_POS_NO_NAME                \n";
    //    sql += "           ,PER_POSITIONHIS.MOV_CODE,PER_MOVMENT.MOV_NAME \n";
    //    sql += "           ,POH_DOCNO, POH_PM_NAME, POH_PL_NAME, POH_LAST_POSITION, POH_LEVEL_NO, POH_SEQ_NO    \n";
    //    sql += "                     \n";
    //    sql += "  FROM PER_POSITIONHIS  \n";
    //    sql += "  LEFT JOIN  PER_LEVEL ON (Per_Level.Level_No = Per_Positionhis.Level_No) \n";
    //    sql += "  LEFT JOIN PER_MOVMENT ON (PER_MOVMENT.MOV_CODE=PER_POSITIONHIS.Mov_Code)    \n";
    //    sql += "  WHERE   PER_ID='4' \n";

    //    DataTable DT = new DataTable();
    //    OracleDataAdapter DA = new OracleDataAdapter(sql, OracleConnectionString);
    //    DA.Fill(DT);
    //    return DT;

    //}


    public static DataTable getHeaderDetailInfo(string ID)
    {
        string sql = "";
        sql += "  SELECT * FROM( \n";
        sql += " SELECT  \n";
        sql += " POS.ID,PER.PER_ID_DPIS, \n";
        sql += " COMLINE.COM_CODE \n";
        sql += " ,COMLINE.POS_ID \n";
        sql += " ,COMLINE.PER_TYPE \n";
        sql += " ,POS.ORG_SERIAL \n";
        sql += " ,ISNULL(ISNULL(ADM.ADMIN_NAME,LINE.LINE_NAME),ODS.POS_NAME) + ' (' + ORG_NAME + ') ' NODE_NAME \n";

        sql += " ,ORG_NAME,ISNULL(ISNULL(ADM.ADMIN_NAME,LINE.LINE_NAME),ODS.POS_NAME) POS_NAME	   \n";
        sql += " ,ISNULL(ADM.ADMIN_NAME,'') ADMIN_NAME	   \n";
        sql += " ,ISNULL(LINE.LINE_NAME,'') LINE_NAME	  \n";
        sql += " ,ISNULL(ISNULL(PREFIX.PREFIX_NAME,'') + PER.NAME + ' ' + PER.SURNAME,'(ว่าง)') PER_FULLNAME \n";
        sql += " ,TYPE.PER_TYPE_NAME  PER_TYPE_Name, TYPE .PER_TYPE_COLOR \n";
        sql += " ,CASE WHEN CUR_LEVEL.LEVEL_GROUP_NAME IS NOT NULL AND CUR_LEVEL.LEVEL_TYPE_NAME IS NOT NULL THEN  ' '+REPLACE(CUR_LEVEL.LEVEL_GROUP_NAME,'ประเภท','')+REPLACE(CUR_LEVEL.LEVEL_TYPE_NAME,'','')  ELSE '' END LEVEL_NAME_FULL \n";
        sql += " ,PER.BORN_DATE,PER.START_DATE,PER.RETIRE_DATE \n";
        sql += " FROM vw_SMM_ORG ORG  \n";
        sql += " INNER JOIN PSST_POSITION POS ON ORG.ORG_SERIAL=POS.ORG_SERIAL \n";
        sql += " 			                AND POS.POS_STATUS IN (1,2,3) \n";
        sql += " INNER JOIN SMM_PER_TYPE SMM ON POS.PER_TYPE=SMM.PER_TYPE \n";
        sql += " 			                OR POS.PER_TYPE='-1' \n";
        sql += " INNER JOIN CTLT_COMMAND_LINE COMLINE ON COMLINE.POS_ID=POS.POS_ID  \n";
        sql += " 			                AND COMLINE.PER_TYPE=POS.PER_TYPE \n";
        sql += " LEFT JOIN PSST_LINE_CODE LINE ON POS.LINE_CODE=LINE.LINE_CODE \n";
        sql += " 			                AND POS.PER_TYPE=LINE.PER_TYPE \n";
        sql += " LEFT JOIN PSST_ADMIN_CODE ADM ON POS.ADMIN_CODE=ADM.ADMIN_CODE \n";
        sql += " LEFT JOIN ODS_COMMAND_LINE ODS ON COMLINE.COM_CODE=ODS.COM_CODE \n";
        sql += " LEFT JOIN PSST_PERSON PER ON POS.ID=PER.ID  \n";
        sql += " LEFT JOIN CTLT_PREFIX_CODE PREFIX ON PER.PREFIX_CODE=PREFIX.PREFIX_CODE \n";
        sql += " LEFT JOIN SMM_PER_TYPE TYPE ON PER.PER_TYPE=TYPE.PER_TYPE \n";
        sql += " LEFT JOIN vw_CMN_PER_LEVEL CUR_LEVEL ON POS.CUR_LEV=CUR_LEVEL.CUR_LEV \n";
        sql += " WHERE PER.ID IS NOT NULL  AND PER.PER_STATUS =1  \n";
        sql += " AND  PER.PER_ORG_SERIAL NOT IN (SELECT ORG_SERIAL FROM CTLT_ORGANIZE where ORG_STATUS='C')  \n";
        sql += " AND PER.ID=@_ID \n";
        sql += " ) AS PER  \n";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = OPM_BL.setParameter("@_ID", SqlDbType.VarChar, ID);
        DataTable dt = OPM_BL.GetDatatable(sql, param);
        return dt;
    }


    public static string GetNewID(string TableName, string ColName)
    {
        string id = "";
        string SQL = "";
        SQL = "select isnull(MAX(" + ColName + " + 1),1) as id from " + TableName.Replace("'", "''");
        DataTable DT = new DataTable();
        DT = GetDatatable(SQL, null);
        if (DT.Rows.Count > 0)
        {
            DataRow dr = DT.Rows[0];
            DataRowView drv = DT.DefaultView[DT.Rows.IndexOf(dr)];
            id = drv["id"].ToString();
        }
        return id;
    }

    public static SqlDataAdapter GetNewSqlDataAdapter(string sql) {
        SqlDataAdapter ret = new SqlDataAdapter(sql, DefaultConnectionString);
        return ret;
    }

    public static DataTable GetDatatable(string sql, SqlParameter[] param)
    {
        DataTable dt = new DataTable();
        try
        {
            SqlConnection conn = new SqlConnection(DefaultConnectionString);
            conn.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandText = sql;
            cmd.CommandType = CommandType.Text;
            cmd.CommandTimeout = 240;

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;

            if (param != null) {
                foreach (SqlParameter p in param) {
                    if (p != null)
                    {
                        cmd.Parameters.Add(p);
                    }
                }
            }

            da.Fill(dt);
            da.Dispose();            
        }
        catch (Exception ex)
        {
            dt = new DataTable();
        }

        return dt;
    }

    public static string ExecuteNoneQuery(string sql, SqlParameter[] param)
    {
        string ret = "false";
        try {
            SqlConnection conn = new SqlConnection(DefaultConnectionString);
            conn.Open();
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.CommandType = CommandType.Text;
            cmd.CommandTimeout = 240;

            if (param != null)
            {
                foreach (SqlParameter p in param)
                {
                    if (p != null)
                    {
                        cmd.Parameters.Add(p);
                    }
                }
            }

            cmd.ExecuteNonQuery();
            ret = "true";
        }
        catch (Exception ex) {
            ret = "false|Exception " + ex.Message + "\n" + ex.StackTrace.ToString();
        }

        return ret;
    }

    //public static void SetDDLPerType(ASPxComboBox ddl) {
    //    try
    //    {
    //        string sql = "select PER_TYPE,PER_TYPE_NAME from PSST_PER_TYPE where ORDER_SEQ is not null order by PER_TYPE";
    //        DataTable dt = GetDatatable(sql,null);
    //        if (dt.Rows.Count > 0) {
    //            ddl.ValueField = "PER_TYPE";
    //            ddl.TextField = "PER_TYPE_NAME";
                
    //            ddl.DataSource = dt;
    //            ddl.DataBind();
    //        }
    //    }
    //    catch (Exception ex) {
            
    //    }
    //}

    //public static void SetDDLTimeRule(ASPxComboBox ddl) {
    //    try
    //    {
    //        string sql = "select time_code,time_name from PSST_TIME_RULE where TIME_STATUS=1 order by TIME_NAME";
    //        DataTable dt = GetDatatable(sql,null);
    //        if(dt.Rows.Count>0){
    //            ddl.ValueField = "time_code";
    //            ddl.TextField = "time_name";
                
    //            ddl.DataSource = dt;
    //            ddl.DataBind();
    //        }
    //    }
    //    catch (Exception ex)
    //    {

    //    }
    //}

    //public static void SetDDLLeaveTypeRemark(ASPxComboBox ddl)
    //{
    //    //กำหนดค่าให้กับ Dropdownlist หมายเหตุการลงเวลา
    //    try
    //    {
    //        string sql = " select leave_id,leave_name from PDM_MS_LEAVE where leave_type_id=2 order by leave_id";
    //        DataTable dt = GetDatatable(sql, null);
    //        if (dt.Rows.Count > 0)
    //        {
    //            ddl.ValueField = "leave_id";
    //            ddl.TextField = "leave_name";

    //            ddl.DataSource = dt;
    //            ddl.DataBind();
    //        }
    //    }
    //    catch (Exception ex)
    //    {

    //    }
    //}

    //public static void SetDDLORG(ASPxComboBox ddl)
    //{
    //    try
    //    {
    //        string sql = "SELECT ORG_SERIAL,ORG_NAME,ORG_ABBR FROM vw_SMM_ORG";
    //        DataTable dt = GetDatatable(sql,null);

    //        if (dt.Rows.Count > 0)
    //        {
    //            ddl.Columns.Clear();
    //            ddl.Columns.Add("ORG_NAME", "ชื่อหน่วยงาน");
    //            ddl.Columns.Add("ORG_ABBR", "ชื่อย่อ");

    //            ddl.ValueField = "ORG_SERIAL";
    //            ddl.TextField = "ORG_NAME";

    //            ddl.DataSource = dt;
    //            ddl.DataBind();
    //        }
    //    }
    //    catch (Exception ex)
    //    {
            
    //    }
    //}

    public static string getLoginUserID() {
        return "admin";
    }

    public static SqlParameter setParameter(string pName, SqlDbType pType, object pValue) {
        SqlParameter p = new SqlParameter(pName, pType);
        if (pValue != null)
            p.Value = pValue;
        else
            p.Value = DBNull.Value;

        return p;
    }

    public static int setBudgetYear(DateTime vDate)
    {
        System.Globalization.CultureInfo ct_th = new System.Globalization.CultureInfo("th-TH");
        int ret = Convert.ToInt16(vDate.ToString("yyyy", ct_th));
        try
        {
            //ถ้าวันที่เป็นวันที่อยู่ในช่วง 1 ตค. - 30 ธค.  ให้บวกปีเพิ่มอีก 1 ปี
            if (Convert.ToInt16(vDate.ToString("MMdd")) >= 1001 && Convert.ToInt16(vDate.ToString("MMdd")) <= 1231)
            {
                ret = ret + 1;
            }
        }
        catch (Exception ex)
        {
            ret = Convert.ToInt16(DateTime.Now.ToString("yyyy", ct_th));
        }
        return ret;
    }

    //public static void SetAlertComboBox(Control page, string msg, DevExpress.Web.ASPxEditors.ASPxComboBox ddl, string key = "Validation")
    //{
    //    ScriptManager.RegisterStartupScript(page, typeof(string), key, "alert('" + msg + "');", true);
    //    ddl.Focus();
    //}

    //public static void SetAlertTextBox(Control page, string msg, DevExpress.Web.ASPxEditors.ASPxTextBox txt, string key = "Validation")
    //{
    //    ScriptManager.RegisterStartupScript(page, typeof(string), key, "alert('" + msg + "');", true);
    //    txt.Focus();
    //}

    //public static void SetAlertTextBox(Control page, string msg, DevExpress.Web.ASPxEditors.ASPxSpinEdit txt, string key = "Validation")
    //{
    //    ScriptManager.RegisterStartupScript(page, typeof(string), key, "alert('" + msg + "');", true);
    //    txt.Focus();
    //}
    //public static void SetAlertTextDate(Control page,  string msg, DevExpress.Web.ASPxEditors.ASPxDateEdit txt, string key = "Validation")
    //{
    //    ScriptManager.RegisterStartupScript(page, typeof(string), key, "alert('" + msg + "');", true);
    //    txt.Focus();
    //}

    public static string GetSysconfig(string ConfigName) {
        string ret = "";
        try {
            string sql = "select config_value from cf_sysconfig where config_name = @_CONFIG_NAME";
            SqlParameter[] param = new SqlParameter[1];
            param[0] = setParameter("@_CONFIG_NAME", SqlDbType.VarChar, ConfigName);

            DataTable dt = GetDatatable(sql, param);
            if (dt.Rows.Count > 0) {
                ret = dt.Rows[0]["config_value"].ToString();
            }
            dt.Dispose();
        }
        catch (Exception ex) {
            ret = "";
        }
        return ret;
    }


    public static string GetDayPeriod(DateTime StartDate, DateTime EndDate)
    {
        string ret = "";
        try
        {
            if (StartDate > EndDate)
            {
                return "";
            }

            int vYear = 0;
            int vMonth = 0;
            int vDay = 0;

            vYear = EndDate.Year - StartDate.Year;
            if (vYear == 0)
            {
                //กรณีอยู่ในปีเดียวกัน
                vMonth = EndDate.Month - StartDate.Month;
                vDay = EndDate.Day - StartDate.Day;
                if (vDay < 0)
                {
                    vDay += 30;
                    vMonth -= 1;
                }
            }
            else
            {
                //กรณีข้ามปี
                vMonth = EndDate.Month - StartDate.Month;
                vDay = EndDate.Day - StartDate.Day;
                if (vDay < 0)
                {
                    vDay += 30;
                    vMonth -= 1;
                }
                if (vMonth < 0)
                {
                    vMonth += 12;
                    vYear -= 1;
                }
            }

            if (vYear == 0 && vMonth == 0)
            {
                vDay += 1;
            }

            ret = vYear.ToString() + "#" + vMonth.ToString() + "#" + vDay.ToString();
        }
        catch (Exception ex) { }

        return ret;
    }

    public static DataTable GetUserNameByID(string pUserID)
    {
        DataTable dt = new DataTable();
        try
        {
            string sql = "select prefix_name +''+name +' '+ surname as fullname,org_full_abbr org_full,isnull(abbr_level3,'') +' '+ isnull(abbr_level4,'') org_abbr34 from vw_CMN_PERSON p";
            sql += " inner join  ctlt_organize o on p.ORG_SERIAL = o.org_serial";
            sql += " where id=@_USERID";
            SqlParameter[] param = new SqlParameter[1];
            param[0] = OPM_BL.setParameter("@_USERID", SqlDbType.VarChar, pUserID);
            dt = OPM_BL.GetDatatable(sql, param);
        }
        catch (Exception ex)
        {

        }
        return dt;
    }

    public static string GetMonthTH(string pMonth)
    {
        string strMonth = "";
        string NumMonth = pMonth.Substring(4, 2);
        if (NumMonth == "01")
        {
            strMonth = "มกราคม";
        }
        else if (NumMonth == "02")
        {
            strMonth = "กุมภาพันธ์";
        }
        else if (NumMonth == "03")
        {
            strMonth = "มีนาคม";
        }
        else if (NumMonth == "04")
        {
            strMonth = "เมษายน";
        }
        else if (NumMonth == "05")
        {
            strMonth = "พฤษภาคม";
        }
        else if (NumMonth == "06")
        {
            strMonth = "มิถุนายน";
        }
        else if (NumMonth == "07")
        {
            strMonth = "กรกฎาคม";
        }
        else if (NumMonth == "08")
        {
            strMonth = "สิงหาคม";
        }
        else if (NumMonth == "09")
        {
            strMonth = "กันยายน";
        }
        else if (NumMonth == "10")
        {
            strMonth = "ตุลาคม";
        }
        else if (NumMonth == "11")
        {
            strMonth = "พฤศจิกายน";
        }
        else if (NumMonth == "12")
        {
            strMonth = "ธันวาคม";
        }

        long year = 0;
        try
        {
            year = Convert.ToInt64(pMonth.Substring(0, 4)) + 543;
        }
        catch (Exception ex)
        {

        }

        strMonth = strMonth + " " + year.ToString();

        return strMonth;

    }

    public static string GetTHDate(string pDate)
    {
        string strDate;//yyyyMMdd
        string strMonth = "";
        string NumMonth = pDate.Substring(4, 2);
        int intDay = 1;
        try
        {
            intDay = Convert.ToInt16(pDate.Substring(6, 2));
        }
        catch (Exception ex)
        { }

        if (NumMonth == "01")
        {
            strMonth = "มกราคม";
        }
        else if (NumMonth == "02")
        {
            strMonth = "กุมภาพันธ์";
        }
        else if (NumMonth == "03")
        {
            strMonth = "มีนาคม";
        }
        else if (NumMonth == "04")
        {
            strMonth = "เมษายน";
        }
        else if (NumMonth == "05")
        {
            strMonth = "พฤษภาคม";
        }
        else if (NumMonth == "06")
        {
            strMonth = "มิถุนายน";
        }
        else if (NumMonth == "07")
        {
            strMonth = "กรกฎาคม";
        }
        else if (NumMonth == "08")
        {
            strMonth = "สิงหาคม";
        }
        else if (NumMonth == "09")
        {
            strMonth = "กันยายน";
        }
        else if (NumMonth == "10")
        {
            strMonth = "ตุลาคม";
        }
        else if (NumMonth == "11")
        {
            strMonth = "พฤศจิกายน";
        }
        else if (NumMonth == "12")
        {
            strMonth = "ธันวาคม";
        }

        long year = 0;
        try
        {
            year = Convert.ToInt64(pDate.Substring(0, 4)) + 543;
        }
        catch (Exception ex)
        {

        }

        strDate = intDay.ToString() + " " + strMonth + " " + year.ToString();

        return strDate;
    }

    public static string GetTHDateAbbr(string pDate)
    {
        string strDate;//yyyyMMdd
        string strMonth = "";
        string NumMonth = pDate.Substring(4, 2);
        int intDay = 1;
        try
        {
            intDay = Convert.ToInt16(pDate.Substring(6, 2));
        }
        catch (Exception ex)
        { }

        if (NumMonth == "01")
        {
            strMonth = "ม.ค.";
        }
        else if (NumMonth == "02")
        {
            strMonth = "ก.พ.";
        }
        else if (NumMonth == "03")
        {
            strMonth = "มี.ค.";
        }
        else if (NumMonth == "04")
        {
            strMonth = "เม.ย.";
        }
        else if (NumMonth == "05")
        {
            strMonth = "พ.ค.";
        }
        else if (NumMonth == "06")
        {
            strMonth = "มิ.ย.";
        }
        else if (NumMonth == "07")
        {
            strMonth = "ก.ค.";
        }
        else if (NumMonth == "08")
        {
            strMonth = "ส.ค.";
        }
        else if (NumMonth == "09")
        {
            strMonth = "ก.ย.";
        }
        else if (NumMonth == "10")
        {
            strMonth = "ต.ค.";
        }
        else if (NumMonth == "11")
        {
            strMonth = "พ.ย.";
        }
        else if (NumMonth == "12")
        {
            strMonth = "ธ.ค.";
        }

        long year = 0;
        try
        {
            year = Convert.ToInt64(pDate.Substring(0, 4)) + 543;
        }
        catch (Exception ex)
        {

        }

        strDate = intDay.ToString() + " " + strMonth + " " + year.ToString();

        return strDate;
    }

    public static string getyearmonthday_DatediffFromCurrentDate(DateTime fromdate)
    {

        string result = "";

        result = (DateTime.Now.Year - fromdate.Year).ToString() + " ปี " + (DateTime.Now.Month - fromdate.Month).ToString().Replace("-", "") + " เดือน " + (DateTime.Now.Day - fromdate.Day).ToString().Replace("-", "") + " วัน";

        return result;
    }

    // ใช้ตรวจสอบสิทธิ์จาก Token ว่ามีสิทธิ์เข้าถึงหน้านี้หรือไม่ หรือ Token หมดอายุ
    //public static bool canAccess(string prg_id, string token, string org_serial)
    //{
    //    WSAuthen.BappRights right = ThisMenuRole(prg_id, token, org_serial);
    //    return right.view_flag | right.insert_flag | right.update_flag | right.delete_flag | right.print_flag | right.verify_flag | right.approve_flag | right.process_flag;
    //}

    //// ใช้ตรวจสอบว่า Token มีสิทธิ์เข้าถึงหน้านี้ด้วยสิทธิ์อะไรบ้าง
    //private static WSAuthen.BappRights ThisMenuRole(string prg_id, string token, string org_serial)
    //{
    //    WSAuthen.BappAuthenticate WS = new WSAuthen.BappAuthenticate();
    //    WS.Url = GetSysconfig("WSAuthenURL");
    //    //string token = "chantiki"; // Token ได้จาก SSO ตอน Login แต่ตอนนี้ใส่ชื่อ User Name ตรงๆไปก่อน
    //    WSAuthen.BappRights r = WS.BappGetProgramRight(token, "HRM", org_serial, prg_id);
    //    return r;
    //}

}