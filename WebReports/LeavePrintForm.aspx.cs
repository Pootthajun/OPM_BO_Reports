﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

public partial class LeavePrintForm : System.Web.UI.Page 
{
    string Type_leave_his = string.Empty;
    string sick_Last_Day = string.Empty;
    string sick_Current_Day = string.Empty;
    string sick_Sum_Day = string.Empty;
    string leave_Last_Day = string.Empty;
    string leave_Current_Day = string.Empty;
    string leave_Sum_Day = string.Empty;
    string confined_Last_Day = string.Empty;
    string confined_Current_Day = string.Empty;
    string confined_Sum_Day = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request["id"] != null)
        {
            string reportname = "";
            string sql = " select leave_id from hrm_leave_his where id=@_ID";
            SqlParameter[] param = new SqlParameter[1];
            param[0] = OPM_BL.setParameter("@_ID", SqlDbType.BigInt, Convert.ToInt64(Request["id"]));

            DataTable dt = OPM_BL.GetDatatable(sql, param);
            if (dt.Rows.Count > 0)
            {
                Type_leave_his = dt.Rows[0]["leave_id"].ToString();
                switch (Type_leave_his)
                { 
                    case "10":
                    case "92":
                    case "93":
                    case "94":
                    case "95":
                        reportname = "LeaveGovAid";
                        break;
                    case "1":
                    case "2":
                    case "3":
                        reportname = "LeaveSick";
                        break;
                    case "4":
                        reportname = "LeaveVacation";
                        break;
                    case "13":
                        reportname = "LeaveHajj";
                        break;
                    case "29":
                        reportname = "LeaveHelpWife";
                        break;
                    case "5":
                        reportname = "LeaveOrdain";
                        break;
                    case "9":
                        reportname = "LeaveSpouse";
                        break;
                    case "7" :
                        reportname = "LeaveTrain";
                        break;
                    case "97":
                        reportname = "LeaveTrain";
                        break;
                    case "98":
                        reportname = "LeaveTrain";
                        break;
                    case "99":
                        reportname = "LeaveTrain";
                        break;
                    case "8":
                        reportname = "LeaveWorkAbroad";
                        break;
                    case "101":
                        reportname = "LeaveRehabilitationProfessional";
                        break;
                    case "6":
                        reportname = "LeaveSoldier";
                        break;
                    default:
                        break;
                }
                if (Request["IsCancelForm"] != null)
                {

                    if (Request["IsCancelForm"].ToString() == "Y")
                    {
                        reportname = "LeaveLeaveCanceled";
                    }
                }

                PrintReport(reportname);
            }
            else
            {
                lblErrorMessage.Text = "No data found";
                CrystalReportViewer1.Visible = false;
            }
            dt.Dispose();
        }
    }

    protected void PrintReport(string reportname)
    {
        ReportDocument  rpt = new ReportDocument();
        string home_floder = System.Web.HttpContext.Current.Request.ApplicationPath + "/";

        try
        {
            string sql = "select lh.id,lh.person_id,o.org_code,lh.org_serial,lh.time_code, isnull(pt.per_type_name,'') per_type_name,lh.leave_reason, " + Environment.NewLine;
            //ลาไปราชการ 1 ลาป่วย 2 ลาคลอดบุตร  3 ลากิจ
            sql += " o.name_level2, o.name_level4, o.name_level5,o.name_level3, ";
            sql += " isnull(lh.leaveno_type,'') + '/' + isnull(lh.leaveno_seq,'') + '/'+ isnull(lh.leaveno_year,'') doc_no, ";
            sql += " isnull(p.prefix_name,'') + isnull(p.name,'') + ' ' + isnull(p.surname,'') staff_name,";
            sql += " lh.leave_id,lh.created_date,lh.start_date,lh.start_flag, lh.end_date,lh.end_flag, " + Environment.NewLine;
            sql += " lh.gov_aid_place,lh.leave_time,p.pos_name,p.level_name,";

            sql += " lh.day_qty,lh.leave_day,lh.leave_reason, lh.leaveno_type,lh.leaveno_seq,lh.leaveno_year, lh.ordain_flag,  " + Environment.NewLine;
            sql += " lh.ordain_date,lh.ordain_temple,lh.ordain_temple_addr,lh.stay_temple,lh.stay_temple_addr, " + Environment.NewLine;
            sql += " lh.hajj_flag, lh.calve_date,lh.contact_addr,lh.contact_telno, " + Environment.NewLine;
            sql += " lh.soldier_summons, lh.soldier_summons_no,lh.soldier_summons_date,lh.soldier_summons_begin,lh.soldier_summons_place, " + Environment.NewLine;
            sql += " lh.train_course_name,lh.train_place,tcc.country_name as train_country,lh.train_cost,traing_degree as train_degree, " + Environment.NewLine;
            sql += " lh.leave_status,p.enter_date, p.born_date birth_date, " + Environment.NewLine;
            sql += " p.salary_recv, cc.country_name spouse_country_name, lh.spouse_name, lh.spouse_position, lh.spouse_level_name, " + Environment.NewLine;
            sql += " lh.spouse_org_name, lh.spouse_country_code, lh.spouse_year, lh.spouse_month, lh.spouse_day " + Environment.NewLine;
            sql += " ,CASE WHEN lh.leave_id = 1  THEN 'ขออนุญาตลาป่วย' WHEN lh.leave_id = 2  THEN 'ขออนุญาตลาคลอดบุตร' WHEN lh.leave_id = 3  THEN 'ขออนุญาตลากิจ' END  as Leave_Title " + Environment.NewLine;
            sql += " ,CASE WHEN pl.COUNT_LEAVE = 1 THEN lh.leave_day ELSE lh.day_qty END as Current_Leave_qty" + Environment.NewLine;
            sql += " , CONVERT(Bit, 0) as COUNT_LEAVE" + Environment.NewLine;
            sql += " from hrm_leave_his lh  " + Environment.NewLine;
            sql += " inner join CTLT_ORGANIZE o on o.org_serial=lh.org_serial" + Environment.NewLine;
            sql += " inner join vw_CMN_PERSON p on p.id=lh.person_id" + Environment.NewLine;
            sql += " left join CTLT_COUNTRY_CODE cc on cc.country_code=lh.spouse_country_code" + Environment.NewLine;
            sql += " left join CTLT_COUNTRY_CODE tcc on tcc.country_code=lh.train_country" + Environment.NewLine;
            sql += " left join psst_per_type pt on lh.per_type=pt.per_type" + Environment.NewLine;
            sql += " left join PDM_MS_LEAVE pl on pl.LEAVE_ID = lh.leave_id" + Environment.NewLine;
            sql += " where lh.id=@_ID";

            SqlParameter[] param = new SqlParameter[1];
            param[0] = OPM_BL.setParameter("@_ID", SqlDbType.BigInt, Convert.ToInt64(Request["id"]));

            DataTable dt = OPM_BL.GetDatatable(sql, param);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                rpt.Load(Server.MapPath(home_floder + "/" + reportname + ".rpt"));

                if (reportname == "LeaveSick")
                {
                    #region "LeaveSick"
                    //หาข้อมูลการลาป่วย ลาคลอดบุตร ลากิจส่วนตัวครั้งล่าสุด
                    sql = "select top 1 start_date, end_date, day_qty,leave_day";
                    sql += " from HRM_LEAVE_HIS ";
                    sql += " where person_id=@_PERSON_ID";
                    sql += " and convert(varchar(8),start_date,112)<@_START_DATE";
                    sql += " order by start_date desc";
                    param = new SqlParameter[2];
                    param[0] = OPM_BL.setParameter("@_PERSON_ID", SqlDbType.BigInt, Convert.ToInt64(dr["person_id"]));
                    param[1] = OPM_BL.setParameter("@_START_DATE", SqlDbType.VarChar, Convert.ToDateTime(dr["start_date"]).ToString("yyyyMMdd",new System.Globalization.CultureInfo("en-US")));
                    DataTable lDt = OPM_BL.GetDatatable(sql, param);
                    if (lDt.Rows.Count > 0) {
                        DataRow lDr = lDt.Rows[0];

                        System.Globalization.CultureInfo cu_th = new System.Globalization.CultureInfo("th-TH");

                        rpt.DataDefinition.FormulaFields["last_start_date"].Text = "'" + Convert.ToDateTime(lDr["start_date"]).ToString("d MMMM yyyy", cu_th) + "'";
                        rpt.DataDefinition.FormulaFields["last_end_date"].Text = "'" + Convert.ToDateTime(lDr["end_date"]).ToString("d MMMM yyyy", cu_th) + "'";
                        rpt.DataDefinition.FormulaFields["last_day_qty"].Text = "'" + Convert.ToInt16(lDr["day_qty"]).ToString() + "'";
                        rpt.DataDefinition.FormulaFields["last_leave_day"].Text = "'" + Convert.ToInt16(lDr["leave_day"]).ToString() + "'";

                        
                    }
                    lDt.Dispose();

                    int budgetYear = Convert.ToInt16(dr["leaveno_year"]) - 543;
                    string DateFrom = new DateTime(budgetYear - 1, 10, 1).ToString("yyyyMMdd", new System.Globalization.CultureInfo("en-US"));
                    string DateTo = new DateTime(budgetYear, 9, 30).ToString("yyyyMMdd", new System.Globalization.CultureInfo("en-US"));

                    //DataTable sDt = GetLeaveHis(Convert.ToDouble(dr["leave_day"]).ToString("#,##0.0"), Convert.ToInt64(dr["person_id"]), DateFrom, DateTo, Convert.ToInt64(dr["id"]));
                   

                    DataTable sDt = get_HRM_LEAVE_HIS_STATICT(Request["id"]);

                    if (sDt.Rows.Count > 0)
                    {
                        // 1ลาป่วย
                        // 2ลาคลอด
                        // 3 ลากิจส่วนตัว
                        if (Type_leave_his == "1")
                        {
                            sick_Last_Day = sDt.Rows[0]["Last_Day"].ToString();
                            sick_Current_Day = sDt.Rows[0]["Current_Day"].ToString();
                            sick_Sum_Day = sDt.Rows[0]["Sum_Day"].ToString();

                        }
                        if (Type_leave_his == "2")
                        {
                            confined_Last_Day = sDt.Rows[0]["Last_Day"].ToString();
                            confined_Current_Day = sDt.Rows[0]["Current_Day"].ToString();
                            confined_Sum_Day = sDt.Rows[0]["Sum_Day"].ToString();

                            
                        }
                        if (Type_leave_his == "3")
                        {
                            leave_Last_Day = sDt.Rows[0]["Last_Day"].ToString();
                            leave_Current_Day = sDt.Rows[0]["Current_Day"].ToString();
                            leave_Sum_Day = sDt.Rows[0]["Sum_Day"].ToString();

                          
                        }
                    }
                  


                    //if (sDt.Rows.Count > 0)
                    //{
                    //    rpt.Subreports[1].SetDataSource(sDt);
                    //    rpt.Subreports[1].DataDefinition.FormulaFields["LeaveID"].Text = Convert.ToInt16(dr["leave_id"]).ToString();
                    //}

                    //for (int i = 0; i < dt.Rows.Count-1; i++)
                    //{   
                    //    string Leave_id = dt.Rows[i]["leave_id"].ToString();
                    //    string his_day = "0.0", cur_day = "0.0", tot_day = "0.0";

                    //    his_day = dt.Rows[i]["his_day"].ToString();
                    //    cur_day = dt.Rows[i]["cur_day"].ToString();
                    //    tot_day = dt.Rows[i]["tot_day"].ToString();




                    //    if (Leave_id == "1")
                    //    {
                    //        rpt.Subreports[0].SetParameterValue("his_day_1", his_day);
                    //        rpt.Subreports[0].SetParameterValue("cur_day_1", cur_day);
                    //        rpt.Subreports[0].SetParameterValue("tot_day_1", tot_day);
                    //    }
                    //    else if (Leave_id == "2")
                    //    {
                    //        rpt.Subreports[0].SetParameterValue("his_day_2", his_day);
                    //        rpt.Subreports[0].SetParameterValue("cur_day_2", cur_day);
                    //        rpt.Subreports[0].SetParameterValue("tot_day_2", tot_day);
                    //    }
                    //    else
                    //    {
                    //        rpt.Subreports[0].SetParameterValue("his_day_3", his_day);
                    //        rpt.Subreports[0].SetParameterValue("cur_day_3", cur_day);
                    //        rpt.Subreports[0].SetParameterValue("tot_day_3", tot_day);
                    //    }
                    //}
                  
                    //แบ่งครึ่งปี
                    //string DateFrom = new DateTime(budgetYear-1, 10, 1).ToString("yyyyMMdd", new System.Globalization.CultureInfo("en-US"));
                    //string DateTo = new DateTime(budgetYear, 3, 31).ToString("yyyyMMdd", new System.Globalization.CultureInfo("en-US"));

                    //DataTable fDt = GetLeaveHis(Convert.ToDouble(dr["leave_day"]).ToString("#,##0.0"), Convert.ToInt64(dr["person_id"]), DateFrom, DateTo, Convert.ToInt64(dr["id"]));
                    //if (fDt.Rows.Count > 0) { 
                    //    //ข้อมูลสถิติการลาครึ่งปีแรก
                    //    rpt.Subreports[0].SetDataSource(fDt);
                    //    rpt.Subreports[0].DataDefinition.FormulaFields["LeaveID"].Text = Convert.ToInt16(dr["leave_id"]).ToString();
                    //}

                    //DateFrom = new DateTime(budgetYear, 4, 1).ToString("yyyyMMdd", new System.Globalization.CultureInfo("en-US"));
                    //DateTo = new DateTime(budgetYear, 9, 30).ToString("yyyyMMdd", new System.Globalization.CultureInfo("en-US"));
                    //DataTable sDt = GetLeaveHis(dr["leave_day"].ToString(), Convert.ToInt64(dr["person_id"]), DateFrom, DateTo, Convert.ToInt64(dr["id"]));
                    //if (sDt.Rows.Count > 0) {
                    //    rpt.Subreports[1].SetDataSource(sDt);
                    //    rpt.Subreports[1].DataDefinition.FormulaFields["LeaveID"].Text = Convert.ToInt16(dr["leave_id"]).ToString();
                    //}
                    //

                    budgetYear = budgetYear + 543;
                    string FirstHalfYear = "1 ต.ค." + (budgetYear - 1).ToString() + " - 31 มี.ค. " + budgetYear.ToString();
                    string SecondHalfYear = "1 เม.ย." + budgetYear.ToString() + " - 30 ก.ย. " + budgetYear.ToString();
                    rpt.DataDefinition.FormulaFields["YearFirstHalf"].Text = "'" + FirstHalfYear + "'";
                    rpt.DataDefinition.FormulaFields["YearSecondHalf"].Text = "'" + SecondHalfYear + "'";
                    #endregion
                }
                else if (reportname == "LeaveGovAid")
                {
                    #region "LeaveGovAid"


                    #endregion
                }
                else if (reportname == "LeaveVacation") {
                    //ข้อมูลใบลาพักผ่อน
                    #region "LeaveVacation"

                    int budgetYear = Convert.ToInt16(dr["leaveno_year"]);

                    sql = "select sum_rest,this_rest,leave_rest ";
                    sql += " from HRM_SUM_VACATION ";
                    sql += " where budget_year=@_BUDGET_YEAR";
                    sql += " and person_id=@_PERSON_ID";
                    param = new SqlParameter[2];
                    param[0] = OPM_BL.setParameter("@_BUDGET_YEAR", SqlDbType.Int, budgetYear);
                    param[1] = OPM_BL.setParameter("@_PERSON_ID", SqlDbType.BigInt, Convert.ToInt64(dr["person_id"]));

                    DataTable vDt = OPM_BL.GetDatatable(sql, param);
                    if (vDt.Rows.Count > 0) {
                        DataRow vDr = vDt.Rows[0];
                        double tot_rest = 0.0; //จำนวนวันลาพักผ่อนสะสม
                        double tot_leave_rest = 0.0;   //จำนวนวันลาพักผ่อนที่ใช้ไปแล้ว
                        if (Convert.IsDBNull(vDr["sum_rest"]) == false)
                        {
                            rpt.DataDefinition.FormulaFields["sum_rest"].Text = Convert.ToDouble(vDr["sum_rest"]).ToString();
                            tot_rest += Convert.ToDouble(vDr["sum_rest"]);
                        }
                        if (Convert.IsDBNull(vDr["this_rest"]) == false) {
                            rpt.DataDefinition.FormulaFields["this_rest"].Text = Convert.ToDouble(vDr["this_rest"]).ToString();
                            tot_rest += Convert.ToDouble(vDr["this_rest"]);
                        }
                        if(Convert.IsDBNull(vDr["leave_rest"])==false){
                            double leave_rest = 0.0;
                            
                            if (dr["leave_status"].ToString() == "3")
                            {
                                leave_rest = Convert.ToDouble(vDr["leave_rest"]) - Convert.ToDouble(dr["leave_day"]);
                            }
                            else {
                                leave_rest = Convert.ToDouble(vDr["leave_rest"]);
                            }
                            rpt.DataDefinition.FormulaFields["leave_rest"].Text = leave_rest.ToString();
                            tot_leave_rest = leave_rest + Convert.ToDouble(dr["leave_day"]);
                        }

                        rpt.DataDefinition.FormulaFields["tot_rest"].Text = tot_rest.ToString();
                        rpt.DataDefinition.FormulaFields["tot_leave_rest"].Text = tot_leave_rest.ToString();
                       
                    }
                    vDt.Dispose();

                    
                    #endregion
                }
                else if (reportname == "LeaveHajj")
                {
                    //ข้อมูลใบลาไปประกอบพิธีฮัจย์
                    #region "LeaveHajj"

                    #endregion
                }
                else if (reportname == "LeaveSpouse")
                {
                    // ข้อมูลใบลาติดตามคู่สมรส
                    #region "Spouse"
                    //ครั้งนี้
                    string[] vSpouseYear = OPM_BL.GetDayPeriod(Convert.ToDateTime(dr["start_date"]), Convert.ToDateTime(dr["end_date"])).Split('#');

                    if (vSpouseYear.Length == 3)
                    {
                        if (Convert.ToInt16(vSpouseYear[0]) > 0)
                        {
                            rpt.DataDefinition.FormulaFields["leave_year"].Text = vSpouseYear[0];
                        }
                        if (Convert.ToInt16(vSpouseYear[1]) > 0)
                        {
                            rpt.DataDefinition.FormulaFields["leave_month"].Text = vSpouseYear[1];
                        }
                        if (Convert.ToInt16(vSpouseYear[2]) > 0)
                        {
                            rpt.DataDefinition.FormulaFields["leave_day"].Text = vSpouseYear[2];
                        }
                    }

                    GetSpouseLastYear(Convert.ToInt64(dr["id"]), Convert.ToInt16(dr["leave_id"]), Convert.ToInt64(dr["person_id"]), vSpouseYear, rpt);
                    #endregion
                }
                else if (reportname == "LeaveTrain")
                {
                    //ข้อมูลใบลาไปศึกษา ฝึกอบรม ปฏิบัติการวิจัย หรือดูงาน
                    #region "LeaveTrain"
                    string[] TotalAllDay = { };
                    TotalAllDay = OPM_BL.GetDayPeriod(Convert.ToDateTime(dr["start_date"]), Convert.ToDateTime(dr["end_date"])).Split('#');

                    string total_day = "0";
                    string total_month = "0";
                    string total_year = "0";
                    if (TotalAllDay.Length == 3)
                    {
                        total_day = TotalAllDay[2];
                        total_month = TotalAllDay[1];
                        total_year = TotalAllDay[0];
                    }

                    rpt.DataDefinition.FormulaFields["total_day"].Text = "'" + total_day + "'";
                    rpt.DataDefinition.FormulaFields["total_month"].Text = "'" + total_month + "'";
                    rpt.DataDefinition.FormulaFields["total_year"].Text = "'" + total_year + "'";

                    #endregion
                }
                else if (reportname == "LeaveWorkAbroad")
                {
                    //ข้อมูลใบลาไปปฏิบัติงานในองค์การระหว่างประเทศ
                    #region "LeaveWorkAbroad"
                    sql = "select lh.id,lh.person_id,o.name_level2, o.name_level4, o.name_level5,o.name_level3," + Environment.NewLine;
                    sql += " isnull(lh.leaveno_type,'') + '/' + isnull(lh.leaveno_seq,'') + '/'+ isnull(lh.leaveno_year,'') doc_no," + Environment.NewLine;
                    sql += " isnull(p.prefix_name,'') + isnull(p.name,'') + ' ' + isnull(p.surname,'') staff_name_th," + Environment.NewLine;
                    sql += " isnull(p.prefix_eng,'') +' ' + isnull(p.name_eng,'') + ' ' + isnull(p.surname_eng,'') staff_name_eng," + Environment.NewLine;
                    sql += " lh.start_date,lh.end_date, p.born_date birth_date,datediff(YY,p.born_date,getdate()) [age]," + Environment.NewLine;
                    sql += " ''bachelor_degree,'' bachelor_degree_from,'' bachelor_degree_contry,'' bachelor_degree_year," + Environment.NewLine;
                    sql += " ''masters_degree,'' masters_degree_from,'' masters_degree_contry,'' masters_degree_year,  " + Environment.NewLine;
                    sql += " ''doctoral_degree,'' doctoral_degree_from,'' doctoral_degree_contry,'' doctoral_degree_year, " + Environment.NewLine;
                    sql += " p.enter_date,now_per_type_name,now_posname,now_level,now_org_level4,now_org_level3,now_org_dept,now_salary,ever_go_inter," + Environment.NewLine;
                    sql += " '' inter_to_org_1,'' inter_to_contry_1,0 period_year_1,0 period_month_1,0 period_day_1,getdate() inter_to_start_date_1,getdate() inter_to_end_date_1," + Environment.NewLine;
                    sql += " '' inter_to_org_2,'' inter_to_contry_2,0 period_year_2,0 period_month_2,0 period_day_2,getdate() inter_to_start_date_2,getdate() inter_to_end_date_2," + Environment.NewLine;
                    sql += " inter_to_org,inter_country_code,cc.country_name inter_country_name,inter_type,inter_org_deal_name1,inter_org_deal_name2,inter_regis_org_name," + Environment.NewLine;
                    sql += " inter_contact_org_name,inter_other,i.posname inter_pos_name,i.level_name inter_level_name,job_desc,period_year,period_month,period_day," + Environment.NewLine;
                    sql += " schedule_departure_date,chk_salary,case salary_type when 'M' then 'เดือนละ' when 'Y' then 'ปีละ' end salary_type," + Environment.NewLine;
                    sql += " salary_money,chk_hostel_exp,hostel_money,chk_travel_exp,travel_money,chk_other_exp,other_money,i.contact_addr,i.contact_telno,p.pos_name,p.level_name,lh.created_date" + Environment.NewLine;
                    sql += " from hrm_leave_his lh" + Environment.NewLine;
                    sql += " inner join CTLT_ORGANIZE o on o.org_serial=lh.org_serial" + Environment.NewLine;
                    sql += " inner join vw_CMN_PERSON p on p.id=lh.person_id" + Environment.NewLine;
                    sql += " left join HRM_LEAVE_HIS_GO_INTER i on lh.id= i.leave_his_id" + Environment.NewLine;
                    sql += " left join CTLT_COUNTRY_CODE cc on cc.country_code=i.inter_country_code" + Environment.NewLine;
                    sql += " where lh.id=@_ID";
                    param = new SqlParameter[1];
                    param[0] = OPM_BL.setParameter("@_ID", SqlDbType.BigInt, Convert.ToInt64(Request["id"]));
                    dt = OPM_BL.GetDatatable(sql, param);

                    if (dt.Rows.Count > 0)
                    {
                        sql = " select top 2 hg.inter_to_org, isnull(cc.country_name,'') country_name, hg.period_year,hg.period_month, hg.period_day,";
                        sql += " lh.start_date, lh.end_date ";
                        sql += " from hrm_leave_his_go_inter hg ";
                        sql += " inner join hrm_leave_his lh on lh.id=hg.leave_his_id ";
                        sql += " left join ctlt_country_code cc on cc.country_code=hg.inter_country_code ";
                        sql += " where lh.person_id=@_PERSON_ID ";
                        sql += " and lh.start_date<=@_START_DATE";

                        int person_id = Convert.ToInt16(dt.Rows[0]["person_id"]);
                        DateTime start_date = Convert.ToDateTime(dt.Rows[0]["start_date"]);

                        param = new SqlParameter[2];
                        param[0] = OPM_BL.setParameter("@_PERSON_ID", SqlDbType.BigInt, person_id);
                        param[1] = OPM_BL.setParameter("@_START_DATE", SqlDbType.Date, start_date);

                        DataTable dtinter = OPM_BL.GetDatatable(sql, param);
                        if (dtinter.Rows.Count > 0)
                        {
                            for (int i = 0; i <= dtinter.Rows.Count - 1; i++)
                            {
                               
                                if (i == 0)
                                {
                                    dt.Rows[0]["inter_to_org_1"] = dtinter.Rows[i]["inter_to_org"];
                                    dt.Rows[0]["inter_to_contry_1"] = dtinter.Rows[i]["country_name"];
                                    dt.Rows[0]["period_year_1"] = dtinter.Rows[i]["period_year"];
                                    dt.Rows[0]["period_month_1"] = dtinter.Rows[i]["period_month"];
                                    dt.Rows[0]["period_day_1"] = dtinter.Rows[i]["period_day"];
                                    dt.Rows[0]["inter_to_start_date_1"] = dtinter.Rows[i]["start_date"];
                                    dt.Rows[0]["inter_to_end_date_1"] = dtinter.Rows[i]["end_date"];
                                }
                                if (i == 1)
                                {
                                    dt.Rows[0]["inter_to_org_2"] = dtinter.Rows[i]["inter_to_org"];
                                    dt.Rows[0]["inter_to_contry_2"] = dtinter.Rows[i]["country_name"];
                                    dt.Rows[0]["period_year_2"] = dtinter.Rows[i]["period_year"];
                                    dt.Rows[0]["period_month_2"] = dtinter.Rows[i]["period_month"];
                                    dt.Rows[0]["period_day_2"] = dtinter.Rows[i]["period_day"];
                                    dt.Rows[0]["inter_to_start_date_2"] = dtinter.Rows[i]["start_date"];
                                    dt.Rows[0]["inter_to_end_date_2"] = dtinter.Rows[i]["end_date"];
                                }
                            }//for

                        }//if dtinter.Rows.Count > 0


                    }// if dt.Rows.Count > 0

                    string[] TotalEnterToStart = { };
                    TotalEnterToStart = OPM_BL.GetDayPeriod(Convert.ToDateTime(dr["enter_date"]), Convert.ToDateTime(dr["start_date"])).Split('#');

                    string total_entstart_day = "0";
                    string total_entstart_month = "0";
                    string total_entstart_year = "0";
                    if (TotalEnterToStart.Length == 3)
                    {
                        total_entstart_day = TotalEnterToStart[2];
                        total_entstart_month = TotalEnterToStart[1];
                        total_entstart_year = TotalEnterToStart[0];
                    }

                    rpt.DataDefinition.FormulaFields["total_entstart_day"].Text = "'" + total_entstart_day + "'";
                    rpt.DataDefinition.FormulaFields["total_entstart_month"].Text = "'" + total_entstart_month + "'";
                    rpt.DataDefinition.FormulaFields["total_entstart_year"].Text = "'" + total_entstart_year + "'";


                    #endregion
                }
                else if (reportname == "LeaveLeaveCanceled")
                {
                    //ข้อมูลใบลาขอยกเลิกวันลา
                    #region "LeaveLeaveCanceled"
                    sql =" select lh.id,lh.created_date,lh.person_id,o.name_level2, o.name_level4, o.name_level5,o.name_level3," +Environment.NewLine;
                    sql += " isnull(lh.leaveno_type,'') + '/' + isnull(lh.leaveno_seq,'') + '/'+ isnull(lh.leaveno_year,'') doc_no," + Environment.NewLine;
                    sql += " isnull(p.prefix_name,'') + isnull(p.name,'') + ' ' + isnull(p.surname,'') staff_name_th," + Environment.NewLine;
                    sql += " p.pos_name,p.level_name,l.LEAVE_NAME,lh.start_date,lh.end_date,leave_day,cancel_remarks," + Environment.NewLine;
                    sql += " 0 cancel_qty,getdate() cancel_start_date,getdate() cancel_end_date" + Environment.NewLine;
                    sql += " from hrm_leave_his lh" + Environment.NewLine;
                    sql += " inner join CTLT_ORGANIZE o on o.org_serial=lh.org_serial" + Environment.NewLine;
                    sql += " inner join vw_CMN_PERSON p on p.id=lh.person_id" + Environment.NewLine;
                    sql += " inner join PDM_MS_LEAVE l on lh.leave_id = l.leave_id";
                    sql += " where lh.id=@_ID";
                    param = new SqlParameter[1];
                    param[0] = OPM_BL.setParameter("@_ID", SqlDbType.BigInt, Convert.ToInt64(Request["id"]));
                    dt = OPM_BL.GetDatatable(sql, param);

                    if (dt.Rows.Count > 0)
                    {
                        sql = "select id,count(*) cancel_qty,min(canceldate) cancel_start_date,max(canceldate) cancel_end_date ";
                        sql += " from hrm_leave_his_cancel where id=@_ID";
                        sql += " group by id";
                        param = new SqlParameter[1];
                        param[0] = OPM_BL.setParameter("@_ID", SqlDbType.BigInt, Convert.ToInt64(Request["id"]));
                        DataTable dtcancel = new DataTable();
                        dtcancel = OPM_BL.GetDatatable(sql, param);
                        if (dtcancel.Rows.Count > 0)
                        {
                            dt.Rows[0]["cancel_qty"] = dtcancel.Rows[0]["cancel_qty"];
                            dt.Rows[0]["cancel_start_date"] = dtcancel.Rows[0]["cancel_start_date"];
                            dt.Rows[0]["cancel_end_date"] = dtcancel.Rows[0]["cancel_end_date"];                       
                        }

                    }


                    #endregion
                }
                else if (reportname == "LeaveSoldier")
                {
                    //ข้อมูลใบลาเข้ารับการตรวจเลือก หรือเข้ารับการเตรียมพล
                    #region "LeaveSoldier"
                    

                    #endregion
                }
                else if (reportname == "LeaveRehabilitationProfessional")
                {
                    //ข้อมูลใบลาไปฟื้นฟูสมรรถภาพด้านอาชีพ
                    #region "LeaveRehabilitationProfessional"
                    sql =" select lh.id,lh.person_id,lh.created_date," + Environment.NewLine;
                    sql += "isnull(p.prefix_name,'') + isnull(p.name,'') + ' ' + isnull(p.surname,'') staff_name," + Environment.NewLine;
                    sql += " o.name_level2, o.name_level4, o.name_level5,o.name_level3,p.level_name,p.pos_name," + Environment.NewLine;
                    sql += " sick_posname	,sick_level	,sick_org_name	,sick_ministry	,sick_officiate	," + Environment.NewLine;
                    sql += " sick_start_date,sick_end_date,now_posname,now_level,now_org_name,now_ministry," + Environment.NewLine;
                    sql += " now_officiate,now_start_date,now_end_date,now_addr,now_telno,incidence_at,incidence_pro_code," + Environment.NewLine;
                    sql += " incidence_am_code,incidence_dist_code,incidence_date,reason_sick_from_duty,reason_wrong," + Environment.NewLine;
                    sql += " reason_other,reason_other_detail,brief_facts,doctor_name,doctor_position,hospital_name," + Environment.NewLine;
                    sql += " doctor_comment,leave_purpost_to,course_name,course_by,course_period_hour,course_period_day," + Environment.NewLine;
                    sql += " course_period_month,course_period_year,course_start_date,course_end_date,course_expense,course_document_qty," + Environment.NewLine;
                    sql += " pr.loc_name prov_name,am.loc_name amp_name,dt.loc_name dist_name" + Environment.NewLine;
                    sql += " from hrm_leave_his lh" + Environment.NewLine;
                    sql += " inner join CTLT_ORGANIZE o on o.org_serial=lh.org_serial" + Environment.NewLine;
                    sql += " inner join vw_CMN_PERSON p on p.id=lh.person_id" + Environment.NewLine;
                    sql += " left join HRM_LEAVE_HIS_REPAIR_CAREER hc on lh.id = hc.hrm_leave_his_id" + Environment.NewLine;
                    sql += " left join CTLT_LOCATION pr on pr.PROV_CODE = hc.incidence_pro_code" + Environment.NewLine;
                    sql += " left join CTLT_LOCATION am on am.PROV_CODE=hc.incidence_pro_code and am.AMP_CODE=hc.incidence_am_code" + Environment.NewLine;
                    sql += " left join CTLT_LOCATION dt on dt.PROV_CODE=hc.incidence_pro_code and dt.AMP_CODE=hc.incidence_am_code and dt.DIST_CODE=hc.incidence_dist_code" + Environment.NewLine;
                    sql += " where lh.id=@_ID";
                    param = new SqlParameter[1];
                    param[0] = OPM_BL.setParameter("@_ID", SqlDbType.BigInt, Convert.ToInt64(Request["id"]));
                    dt = OPM_BL.GetDatatable(sql, param);

                    #endregion
                }
                else if (reportname == "xxx")
                {
                    #region "xxx"
                    #endregion
                }
                rpt.SetDataSource(dt);
                lblErrorMessage.Text = "";

                if (reportname == "LeaveSick")
                {
                    rpt.SetParameterValue("sick_Last_Day", sick_Last_Day);
                    rpt.SetParameterValue("sick_Current_Day", sick_Current_Day);
                    rpt.SetParameterValue("sick_Sum_Day", sick_Sum_Day);
                    rpt.SetParameterValue("leave_Last_Day", leave_Last_Day);
                    rpt.SetParameterValue("leave_Current_Day", leave_Current_Day);
                    rpt.SetParameterValue("leave_Sum_Day", leave_Sum_Day);
                    rpt.SetParameterValue("confined_Last_Day", confined_Last_Day);
                    rpt.SetParameterValue("confined_Current_Day", confined_Current_Day);
                    rpt.SetParameterValue("confined_Sum_Day", confined_Sum_Day);
                }
                CrystalReportViewer1.ReportSource = rpt;
                CrystalReportViewer1.Visible = true;
                
            }
            else {
                lblErrorMessage.Text = "ไม่พบข้อมูล";
                CrystalReportViewer1.Visible = false;
            }
        }
        catch (Exception ex)
        {
            lblErrorMessage.Text = ex.ToString();
            CrystalReportViewer1.Visible = false;
        }

    }

    private void GetSpouseLastYear(long vHisID, int vLeaveID, long vPersonID, string[] vSpouseYear, ReportDocument rpt)
    {
        string sql = "select top 1 h.id, cc.country_name,  h.start_date, h.end_date ";
        sql += " from HRM_LEAVE_HIS h";
        sql += " inner join CTLT_COUNTRY_CODE cc on cc.country_code=spouse_country_code";
        sql += " where h.person_id=@_PERSON_ID";
        sql += " and h.leave_id=@_LEAVE_ID";
        sql += " and h.leave_status='3'"; //อนุญาตแล้ว
        sql += " and h.id<>@_ID";
        sql += " order by h.created_date desc";

        SqlParameter[] param = new SqlParameter[3];
        param[0] = OPM_BL.setParameter("@_PERSON_ID", SqlDbType.BigInt, vPersonID);
        param[1] = OPM_BL.setParameter("@_LEAVE_ID", SqlDbType.Int, vLeaveID);
        param[2] = OPM_BL.setParameter("@_ID", SqlDbType.BigInt, vHisID);

        string[] vSpouseLastYear = { };

        DataTable dt = OPM_BL.GetDatatable(sql, param);
        if (dt.Rows.Count > 0)
        {
            DataRow dr = dt.Rows[0];
            rpt.DataDefinition.FormulaFields["last_country_name"].Text = "'" + (Convert.IsDBNull(dr["country_name"]) == false ? dr["country_name"].ToString() : "") + "'";
            rpt.DataDefinition.FormulaFields["last_start_day"].Text = "'" + Convert.ToDateTime(dr["start_date"]).Day.ToString() + "'";
            rpt.DataDefinition.FormulaFields["last_start_month"].Text = "'" + Convert.ToDateTime(dr["start_date"]).ToString("MMMM", new System.Globalization.CultureInfo("th-TH")) + "'";
            rpt.DataDefinition.FormulaFields["last_start_year"].Text = "'" + Convert.ToDateTime(dr["start_date"]).ToString("yyyy", new System.Globalization.CultureInfo("th-TH")) + "'";

            rpt.DataDefinition.FormulaFields["last_end_day"].Text = "'" + Convert.ToDateTime(dr["end_date"]).Day.ToString() + "'";
            rpt.DataDefinition.FormulaFields["last_end_month"].Text = "'" + Convert.ToDateTime(dr["end_date"]).ToString("MMMM", new System.Globalization.CultureInfo("th-TH")) + "'";
            rpt.DataDefinition.FormulaFields["last_end_year"].Text = "'" + Convert.ToDateTime(dr["end_date"]).ToString("yyyy", new System.Globalization.CultureInfo("th-TH")) + "'";

            if (Convert.IsDBNull(dr["start_date"]) == false && Convert.IsDBNull(dr["end_date"]) == false)
            {
                vSpouseLastYear = OPM_BL.GetDayPeriod(Convert.ToDateTime(dr["start_date"]), Convert.ToDateTime(dr["end_date"])).Split('#');
                if (vSpouseLastYear.Length == 3)
                {
                    if (Convert.ToInt16(vSpouseLastYear[0]) > 0)
                    {
                        rpt.DataDefinition.FormulaFields["last_year"].Text = vSpouseLastYear[0];
                    }

                    if (Convert.ToInt16(vSpouseLastYear[1]) > 0)
                    {
                        rpt.DataDefinition.FormulaFields["last_month"].Text = vSpouseLastYear[1];
                    }

                    if (Convert.ToInt16(vSpouseLastYear[2]) > 0)
                    {
                        rpt.DataDefinition.FormulaFields["last_day"].Text = vSpouseLastYear[2];
                    }
                }
            }
        }
        dt.Dispose();

        //รวมกับครั้งนี้
        if (vSpouseYear.Length == 3 && vSpouseLastYear.Length == 3)
        {
            int vYear = Convert.ToInt16(vSpouseYear[0]) + Convert.ToInt16(vSpouseLastYear[0]);
            int vMonth = Convert.ToInt16(vSpouseYear[1]) + Convert.ToInt16(vSpouseLastYear[1]);
            int vDay = Convert.ToInt16(vSpouseYear[2]) + Convert.ToInt16(vSpouseLastYear[2]);

            if (vDay > 30)
            {
                vDay -= 30;
                vMonth += 1;
            }

            if (vMonth > 12)
            {
                vMonth -= 12;
                vYear += 1;
            }

            if (vYear > 0)
            {
                rpt.DataDefinition.FormulaFields["total_year"].Text = vYear.ToString();
            }
            if (vMonth > 0)
            {
                rpt.DataDefinition.FormulaFields["total_month"].Text = vMonth.ToString();
            }
            if (vDay > 0)
            {
                rpt.DataDefinition.FormulaFields["total_day"].Text = vDay.ToString();
            }
        }
    }


    private DataTable GetLeaveHis(string CurrDay, long person_id, string DateFrom, string DateTo, long vLeaveID) {
        DataTable dt = null;
        try
        {
            //หาข้อมูลสถิติการลา
            string sql = "select lh.leave_id,l.leave_name, count(lh.id) his_qty, sum(lh.leave_day) his_day, " + Environment.NewLine;
            sql += " 1 cur_qty, '" + CurrDay + "' cur_day,  " + Environment.NewLine;
            sql += " case when lh.leave_id=" + vLeaveID + " then count(lh.id) + 1 else count(lh.id) end tot_qty," + Environment.NewLine;
            sql += " case when lh.leave_id=" + vLeaveID + " then sum(lh.leave_day) + " + CurrDay + " else sum(lh.leave_day) end tot_day";
            sql += " from PDM_MS_LEAVE l " + Environment.NewLine;
            sql += " inner join HRM_LEAVE_HIS lh on l.leave_id=lh.leave_id " + Environment.NewLine;
            sql += " where lh.person_id=@_PERSON_ID  " + Environment.NewLine;
            sql += " and convert(varchar(8), lh.start_date,112) between @_DATE_FROM and @_DATE_TO " + Environment.NewLine;
            sql += " and lh.id <> @_ID ";  //ไม่รวมจำนวนของการลาครั้งนี้
            sql += " and l.leave_id in (1,2,3)";
            sql += " group by lh.leave_id,l.leave_name";

            SqlParameter[] param = new SqlParameter[4];
            param[0] = OPM_BL.setParameter("@_PERSON_ID", SqlDbType.BigInt, person_id);
            param[1] = OPM_BL.setParameter("@_DATE_FROM", SqlDbType.VarChar, DateFrom);
            param[2] = OPM_BL.setParameter("@_DATE_TO", SqlDbType.VarChar, DateTo);
            param[3] = OPM_BL.setParameter("@_ID", SqlDbType.BigInt, vLeaveID);

            dt = OPM_BL.GetDatatable(sql, param);
            if (dt.Rows.Count == 0) {
                sql = " select  leave_id, leave_name, 0 his_qty, 0 his_day, 0 cur_qty, 0 cur_day, 0 tot_qty, 0 tot_day " + Environment.NewLine;
                sql += " from PDM_MS_LEAVE";
                sql += " where leave_id in (1,2,3)";
                dt = OPM_BL.GetDatatable(sql, null);
            }
        }
        catch (Exception ex) { 
            dt = new DataTable();
        }
        return dt;
    }

    public static DataTable get_HRM_LEAVE_HIS_STATICT(string ID)
    {
        string sql = "";
        sql += "  select leave_his_id,Last_Day,Current_Day,Sum_Day from HRM_LEAVE_HIS_STATICT";
        sql += " where leave_his_id=@_ID  \n";

        SqlParameter[] param = new SqlParameter[1];
        param[0] = OPM_BL.setParameter("@_ID", SqlDbType.VarChar, ID);
        DataTable dt = OPM_BL.GetDatatable(sql, param);
        return dt;
    }

    //protected string GetMonthTH(string pMonth) { 
    //    string strMonth ="";
    //    string NumMonth = pMonth.Substring(4,2);
    //    if (NumMonth =="01"){
    //        strMonth = "มกราคม";
    //    }else if (NumMonth =="02"){
    //        strMonth = "กุมภาพันธ์";
    //    }else if (NumMonth =="03"){
    //        strMonth = "มีนาคม";
    //    }else if (NumMonth =="04"){
    //        strMonth = "เมษายน";
    //    }else if (NumMonth =="05"){
    //        strMonth = "พฤษภาคม";
    //    }else if (NumMonth =="06"){
    //        strMonth = "มิถุนายน";
    //    }else if (NumMonth =="07"){
    //        strMonth = "กรกฎาคม";
    //    }else if (NumMonth =="08"){
    //        strMonth = "สิงหาคม";
    //    }else if (NumMonth =="09"){
    //        strMonth = "กันยายน";
    //    }else if (NumMonth =="10"){
    //        strMonth = "ตุลาคม";
    //    }else if (NumMonth =="11"){
    //        strMonth = "พฤศจิกายน";
    //    }else if (NumMonth =="12"){
    //        strMonth = "ธันวาคม";
    //    }

    //    long year =0;
    //    try{
    //        year = Convert.ToInt64(pMonth.Substring(0, 4))+ 543;
    //    }
    //    catch(Exception ex){

    //    }

    //    strMonth = strMonth + " " + year.ToString();

    //    return strMonth;                            
                                            
    //}

    //protected string GetTHDate(string pDate) {
    //    string strDate;//yyyyMMdd
    //    string strMonth = "";
    //    string NumMonth = pDate.Substring(4, 2);
    //    string strDay = pDate.Substring(6,2);
    //    if (NumMonth == "01")
    //    {
    //        strMonth = "มกราคม";
    //    }
    //    else if (NumMonth == "02")
    //    {
    //        strMonth = "กุมภาพันธ์";
    //    }
    //    else if (NumMonth == "03")
    //    {
    //        strMonth = "มีนาคม";
    //    }
    //    else if (NumMonth == "04")
    //    {
    //        strMonth = "เมษายน";
    //    }
    //    else if (NumMonth == "05")
    //    {
    //        strMonth = "พฤษภาคม";
    //    }
    //    else if (NumMonth == "06")
    //    {
    //        strMonth = "มิถุนายน";
    //    }
    //    else if (NumMonth == "07")
    //    {
    //        strMonth = "กรกฎาคม";
    //    }
    //    else if (NumMonth == "08")
    //    {
    //        strMonth = "สิงหาคม";
    //    }
    //    else if (NumMonth == "09")
    //    {
    //        strMonth = "กันยายน";
    //    }
    //    else if (NumMonth == "10")
    //    {
    //        strMonth = "ตุลาคม";
    //    }
    //    else if (NumMonth == "11")
    //    {
    //        strMonth = "พฤศจิกายน";
    //    }
    //    else if (NumMonth == "12")
    //    {
    //        strMonth = "ธันวาคม";
    //    }

    //    long year = 0;
    //    try
    //    {
    //        year = Convert.ToInt64(pDate.Substring(0, 4)) + 543;
    //    }
    //    catch (Exception ex)
    //    {

    //    }

    //    strDate = strDay + " " + strMonth + " " + year.ToString();

    //    return strDate;
    //}

    
    
}
