﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Web.UI.DataVisualization.Charting;

public partial class PreviewChartReports : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string reportname = "RPT_HRM_UT0612";
        string FormatType = "BAR_GRAPH";

        if (Request.QueryString["ReportName"] != null)
        {
            reportname = Request.QueryString["ReportName"];
        }
        if (Request["FormatType"] != null)
        {
            FormatType = Request["FormatType"];
        }

        PrintReportChart(reportname, FormatType);
    }

    private void PrintReportChart(string reportname, string FormatType)
    {
        try {
            if (reportname == "RPT_HRM_UT0612")
            {
                #region "RPT_HRM_UT0612"
                //string TypeName = Request.QueryString["TypeName"];
                //string ORG = Request.QueryString["ORG"];
                string DateFrom = Request.QueryString["DateFrom"];
                string DateTo = Request.QueryString["DateTo"];
                //string MsLeaveType = Request.QueryString["leave_type_id"];
                //string UserName = Request.QueryString["UserName"];
                //string UserOrg = Request.QueryString["UserOrg"];
                string BudgetYear = Request.QueryString["BudgetYear"];

                //string TypeName = "11";
                //string DateFrom = "20121001";//"20140101";
                //string DateTo = "20140930"; //"20140501";
                //string MsLeaveType = "2";
                //string UserName = "99999ชื่อสกุล99999";
                //string UserOrg = "xxxxxหน่วยงานxxxxx";
                //string ORG = "3";//"36";
                //string BudgetYear = "2556";  //กรณีเลือกเดือนเดียว ไม่ต้องระบุปีงบประมาณ
                //FormatType = "BAR_GRAPH"; //BAR_GRAPH, LINE_GRAPH

                DataTable dt = ReportsEngine.WebReportsENG.GetQueryReportHRM_UT0612(Request);
                if (dt.Rows.Count > 0) {
                    Document pdfDoc = CreatePDFDocument();

                    string per_type_name = "";
                    string leave_type_name = "";
                    string org_name = "";

                    per_type_name = dt.Rows[0]["per_type_name"].ToString();
                    leave_type_name = dt.Rows[0]["leave_type_name"].ToString();

                    if (Request.QueryString["ORG"] != null)
                    {
                        org_name = " หน่วยงาน " + dt.Rows[0]["org_name"].ToString();
                    }

                    SetReportHeaderStyle(pdfDoc, "สำนักงานปลัดสำนักนายกรัฐมนตรี");
                    SetReportHeaderStyle(pdfDoc, "รายงานสถิติการลาแยกตามหน่วยงาน");
                    SetReportHeaderStyle(pdfDoc, "ประเภทบุคลากร " + per_type_name + " ประเภท " + leave_type_name + org_name);

                    if (BudgetYear.Trim() == "")
                    {
                        SetReportHeaderStyle(pdfDoc, "ตั้งแต่วันที่ " + ReportsEngine.WebReportsENG.SetDisplayDateFormat(DateFrom) + " ถึงวันที่ " + ReportsEngine.WebReportsENG.SetDisplayDateFormat(DateTo));
                    }
                    else
                    {
                        SetReportHeaderStyle(pdfDoc, "ปีงบประมาณ " + BudgetYear);
                    }

                    SetReportHeaderStyle(pdfDoc, "พบรายการทั้งหมด " + dt.Rows.Count.ToString() + " รายการ");


                    DataTable oDt = new DataTable();
                    oDt = dt.DefaultView.ToTable(true, "org_name").Copy();   //ชื่อหน่วยงาน
                    
                    DataTable lDt = new DataTable();
                    lDt = dt.DefaultView.ToTable(true, "leave_name").Copy();  //ชื่อการลาทั้งหมด

                    DataTable nDt = CreateGraphDataTable(dt, lDt, oDt, "org_name");
                    GenerateGraph(FormatType, nDt, "org_name");
                    pdfDoc = AddChartToDocument(pdfDoc);
                    pdfDoc.Close();

                    CreatePDFOutput(reportname, pdfDoc);
                }
                dt.Dispose();
                #endregion
            }
            else if (reportname == "RPT_HRM_UT0613")
            {
                #region "RPT_HRM_UT0613"
                //string TypeName = Request.QueryString["TypeName"];
                string ORG = Request.QueryString["ORG"];
                string DateFrom = Request.QueryString["DateFrom"];
                string DateTo = Request.QueryString["DateTo"];
                //string MsLeaveType = Request.QueryString["leave_type_id"];
                //string UserName = Request.QueryString["UserName"];
                //string UserOrg = Request.QueryString["UserOrg"];
                string BudgetYear = Request.QueryString["BudgetYear"];

                DataTable dt = ReportsEngine.WebReportsENG.GetQueryReportHRM_UT0613(Request);
                if (dt.Rows.Count > 0) {
                    Document pdfDoc = CreatePDFDocument();

                    string per_type_name = "";
                    string leave_type_name = "";
                    string org_name = "";
                    if (dt.Rows.Count > 0)
                    {
                        per_type_name = dt.Rows[0]["per_type_name"].ToString();
                        leave_type_name = dt.Rows[0]["leave_type_name"].ToString();
                        if (ORG != null)
                        {
                            org_name = " หน่วยงาน " + dt.Rows[0]["org_name"].ToString();
                        }
                    }

                    SetReportHeaderStyle(pdfDoc, "สำนักงานปลัดสำนักนายกรัฐมนตรี");
                    SetReportHeaderStyle(pdfDoc, "รายงานสถิติการลาแยกตามเดือน/ปี");
                    SetReportHeaderStyle(pdfDoc, "ประเภทบุคลากร " + per_type_name + " ประเภท " + leave_type_name + org_name);

                    if (BudgetYear.Trim() == "")
                    {
                        SetReportHeaderStyle(pdfDoc, "ตั้งแต่วันที่ " + ReportsEngine.WebReportsENG.SetDisplayDateFormat(DateFrom) + " ถึงวันที่ " + ReportsEngine.WebReportsENG.SetDisplayDateFormat(DateTo));
                    }
                    else
                    {
                        SetReportHeaderStyle(pdfDoc, "ปีงบประมาณ " + BudgetYear);
                    }
                    SetReportHeaderStyle(pdfDoc, "พบรายการทั้งหมด " + dt.Rows.Count.ToString() + " รายการ");

                    DataTable lDt = new DataTable();
                    lDt = dt.DefaultView.ToTable(true, "leave_name").Copy();

                    DataTable oDt = new DataTable();
                    oDt = dt.DefaultView.ToTable(true, "str_start_date").Copy();

                    DataTable nDt = CreateGraphDataTable(dt, lDt, oDt, "str_start_date");
                    GenerateGraph(FormatType, nDt, "str_start_date");

                    pdfDoc = AddChartToDocument(pdfDoc);
                    pdfDoc.Close();

                    CreatePDFOutput(reportname, pdfDoc);
                }
                dt.Dispose();

                #endregion
            }
            else if (reportname == "RPT_HRM_UT0614")
            {
                #region "RPT_HRM_UT0614"
                string TypeName = Request.QueryString["TypeName"];
                string ORG = Request.QueryString["ORG"];
                string differentiate = Request.QueryString["differentiate"];
                string differentiate_name = Request.QueryString["differentiate_name"];
                string PerTypeName = Request.QueryString["PerTypeName"];
                string UserName = Request.QueryString["UserName"];
                string UserOrg = Request.QueryString["UserOrg"];
                //string FormatType = Request.QueryString["FormatType"];

                DataTable dt = ReportsEngine.WebReportsENG.GetQueryReportHRM_UT0614(Request);
                if (dt.Rows.Count > 0) {
                    Document pdfDoc = CreatePDFDocument();

                    string org_name = "";
                    if (ORG != null)
                    {
                        org_name = " หน่วยงาน " + dt.Rows[0]["org_name"].ToString();
                    }
                    string thDate = DateTime.Now.ToString("dd MMMM yyyy", new System.Globalization.CultureInfo("th-TH"));
                    SetReportHeaderStyle(pdfDoc, "สำนักงานปลัดสำนักนายกรัฐมนตรี");
                    SetReportHeaderStyle(pdfDoc, "รายงานแสดงจำนวนบุคคลากรใน สปน. จำแนกตาม" + differentiate_name);
                    SetReportHeaderStyle(pdfDoc, "ประเภท " + PerTypeName + " " + org_name);
                    SetReportHeaderStyle(pdfDoc, "ณ วันที่ " + thDate);
                    SetReportHeaderStyle(pdfDoc, "พบรายการทั้งหมด " + dt.Rows.Count.ToString() + " รายการ");

                    DataTable lDt = new DataTable();
                    lDt = dt.DefaultView.ToTable(true, "differentiate").Copy();

                    DataTable oDt = new DataTable();
                    oDt = dt.DefaultView.ToTable(true, "org_name").Copy();

                    //สร้าง Datatable ใหม่ให้มีชื่อคอลัมน์ differentiate
                    DataTable nDt = new DataTable();
                    nDt.Columns.Add("org_name", typeof(string));

                    foreach (DataRow lDr in lDt.Rows)
                    {
                        nDt.Columns.Add(lDr["differentiate"].ToString(), typeof(int));
                    }

                    for (int o = 0; o < oDt.Rows.Count; o++)
                    {
                        DataRow oDr = oDt.Rows[o];

                        DataRow nDr = nDt.NewRow();
                        nDr["org_name"] = oDr["org_name"].ToString();
                        for (int l = 0; l < lDt.Rows.Count; l++)
                        {
                            DataRow lDr = lDt.Rows[l];

                            dt.DefaultView.RowFilter = "org_name='" + oDr["org_name"].ToString() + "' and differentiate='" + lDr["differentiate"].ToString() + "'";
                            DataView dv = dt.DefaultView;
                            int person_qty = 0;
                            if (dv.Count > 0)
                            {
                                person_qty = Convert.ToInt16(dv[0]["person_qty"]);
                            }
                            dt.DefaultView.RowFilter = "";

                            nDr[lDr["differentiate"].ToString()] = person_qty;
                        }

                        nDt.Rows.Add(nDr);
                    }


                    GenerateGraph(FormatType, nDt, "org_name");

                    pdfDoc = AddChartToDocument(pdfDoc);
                    pdfDoc.Close();

                    CreatePDFOutput(reportname, pdfDoc);
                }
                dt.Dispose();

                #endregion
            }
        }
        catch (Exception ex) { }
    }

    #region "Set Report Style"
    private  void SetReportHeaderStyle(Document pdfDoc, string txt)
    {
        using (MemoryStream bmStream = new MemoryStream())
        {
            System.Drawing.Bitmap bm = CreateTextToImage(txt, "TH SarabunPSK", 20, System.Drawing.FontStyle.Bold);
            bm.Save(bmStream, System.Drawing.Imaging.ImageFormat.Png);

            iTextSharp.text.Image txtImage = iTextSharp.text.Image.GetInstance(bmStream.GetBuffer());
            txtImage.ScalePercent(80f);
            txtImage.Alignment = Element.ALIGN_MIDDLE;
            pdfDoc.Add(txtImage);
        }
    }

    private System.Drawing.Bitmap CreateTextToImage(string txt, string fontName, int fontSize, System.Drawing.FontStyle fontStyle) {
        System.Drawing.Bitmap bm = new System.Drawing.Bitmap(1, 1);
        System.Drawing.Font font = new System.Drawing.Font(fontName, fontSize, fontStyle, System.Drawing.GraphicsUnit.Pixel);
        System.Drawing.Graphics graphics = System.Drawing.Graphics.FromImage(bm);
        try {
            int width = (int)graphics.MeasureString(txt, font).Width;
            int height = (int)graphics.MeasureString(txt, font).Height;
            bm = new System.Drawing.Bitmap(bm, new System.Drawing.Size(width, height));
            
            graphics = System.Drawing.Graphics.FromImage(bm);
            graphics.Clear(System.Drawing.Color.White);
            graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            graphics.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
            graphics.DrawString(txt, font, new System.Drawing.SolidBrush(System.Drawing.Color.Black), 0, 0);
            graphics.Flush();
            graphics.Dispose();

            //bm.Save(@"D:\TxtImg.png", System.Drawing.Imaging.ImageFormat.Png);
        }
        catch (Exception ex) {
            bm = new System.Drawing.Bitmap(1, 1);
        }

        return bm;
    }
    #endregion

    private Document AddChartToDocument(Document pdfDoc)
    {
        using (MemoryStream stream = new MemoryStream())
        {
            Chart1.SaveImage(stream, ChartImageFormat.Png);
            iTextSharp.text.Image chartImage = iTextSharp.text.Image.GetInstance(stream.GetBuffer());
            //chartImage.ScalePercent(75f);
            chartImage.Alignment = Element.ALIGN_MIDDLE;
            pdfDoc.Add(chartImage);
        }

        return pdfDoc;
    }

    private DataTable CreateGraphDataTable(DataTable dt,DataTable lDt, DataTable oDt, string LegendFieldName)
    {
        //สร้าง Datatable ใหม่ให้มีชื่อคอลัมน์ตามชื่อการลา
        DataTable nDt = new DataTable();
        //nDt.Columns.Add("str_start_date", typeof(string));
        nDt.Columns.Add(LegendFieldName, typeof(string));

        try {
            foreach (DataRow lDr in lDt.Rows)
            {
                nDt.Columns.Add(lDr["leave_name"].ToString(), typeof(int));
            }

            for (int o = 0; o < oDt.Rows.Count; o++)
            {
                DataRow oDr = oDt.Rows[o];

                DataRow nDr = nDt.NewRow();
                nDr[LegendFieldName] = oDr[LegendFieldName].ToString();
                for (int l = 0; l < lDt.Rows.Count; l++)
                {
                    DataRow lDr = lDt.Rows[l];

                    dt.DefaultView.RowFilter = LegendFieldName + "='" + oDr[LegendFieldName].ToString() + "' and leave_name='" + lDr["leave_name"].ToString() + "'";
                    DataView dv = dt.DefaultView;
                    int person_qty = 0;
                    if (dv.Count > 0)
                    {
                        person_qty = Convert.ToInt16(dv[0]["person_qty"]);
                    }
                    dt.DefaultView.RowFilter = "";

                    nDr[lDr["leave_name"].ToString()] = person_qty;
                }

                nDt.Rows.Add(nDr);
            }
        }
        catch (Exception ex) {
            nDt = new DataTable();
        }
        return nDt;
    }

    private void GenerateGraph(string FormatType, DataTable nDt, string LegendFieldName) {
        try {
            //Generate Graph
            for (int i = 1; i < nDt.Columns.Count; i++)
            {  //ข้อมูลเริ่มที่คอลัมน์ที่สอง
                Series series = new Series();
                foreach (DataRow dr in nDt.Rows)
                {
                    int y = Convert.ToInt16(dr[i]);
                    //series.Points.AddXY(dr["str_start_date"].ToString(), y);
                    series.Points.AddXY(dr[LegendFieldName].ToString(), y);
                }

                if (FormatType == "BAR_GRAPH")
                {
                    series.ChartType = SeriesChartType.Column;
                }
                else if (FormatType == "LINE_GRAPH")
                {
                    series.ChartType = SeriesChartType.Line;
                }

                series.Name = nDt.Columns[i].ColumnName;
                series.IsValueShownAsLabel = true;
                Chart1.Series.Add(series);
            }
        }
        catch (Exception ex) { }
    }

    private Document CreatePDFDocument() {
        Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 15f, 25f);
        try {
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
        }
        catch (Exception ex) { }
        

        return pdfDoc;
    }

    private void CreatePDFOutput(string reportname, Document pdfDoc)
    {
        //Response.ClearContent();
        //Response.ClearHeaders();
        Response.ContentType = "application/pdf";
        Response.AddHeader("content-disposition", "inline; filename=" + reportname + ".pdf");
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.Write(pdfDoc);
        Response.Flush();
        Response.Close();
    }
}