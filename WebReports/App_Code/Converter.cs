using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Globalization;

public class Converter
	{

        //public enum EncodeType
        //{
        //    _DEFAULT = 0,
        //    _ASCII = 1,
        //    _UNICODE = 2,
        //    _UTF32 = 3,
        //    _UTF7 = 4,
        //    _UTF8 = 5
        //}

		
		public System.IO.MemoryStream ByteToStream(byte[] Buffer)
		{
			// Convert Byte To Stream
			return new System.IO.MemoryStream(Buffer);
		}

        public byte[] StreamToByte(System.IO.Stream Stream)
        {
            Int32 length = Stream.Length > Int32.MaxValue ? Int32.MaxValue : Convert.ToInt32(Stream.Length);
            Byte[] buffer = new Byte[length];
            Stream.Read(buffer, 0, length);
            return buffer;
        }

		public string ToMonthNameEN(int MonthID)
		{
			switch (MonthID) {
				case 1:
					return "January";
				case 2:
					return "February";
				case 3:
					return "March";
				case 4:
					return "April";
				case 5:
					return "May";
				case 6:
					return "June";
				case 7:
					return "July";
				case 8:
					return "August";
				case 9:
					return "September";
				case 10:
					return "October";
				case 11:
					return "November";
				case 12:
					return "December";
				default:
					return "Unknow";
			}
		}

		public string ToMonthNameTH(int MonthID)
		{
			switch (MonthID) {
				case 1:
					return "มกราคม";
				case 2:
					return "กุมภาพันธ์";
				case 3:
					return "มีนาคม";
				case 4:
					return "เมษายน";
				case 5:
					return "พฤษภาคม";
				case 6:
					return "มิถุนายน";
				case 7:
					return "กรกฎาคม";
				case 8:
					return "สิงหาคม";
				case 9:
					return "กันยายน";
				case 10:
					return "ตุลาคม";
				case 11:
					return "พฤศจิกายน";
				case 12:
					return "ธันวาคม";
				default:
					return "...";
			}
		}

		public string ToMonthShortTH(int MonthID)
		{
			switch (MonthID) {
				case 1:
					return "ม.ค.";
				case 2:
					return "ก.พ.";
				case 3:
					return "มี.ค.";
				case 4:
					return "เม.ย.";
				case 5:
					return "พ.ค.";
				case 6:
					return "มิ.ย.";
				case 7:
					return "ก.ค.";
				case 8:
					return "ส.ค.";
				case 9:
					return "ก.ย.";
				case 10:
					return "ต.ค.";
				case 11:
					return "พ.ย.";
				case 12:
					return "ธ.ค.";
				default:
					return "...";
			}
		}

		public DateTime StringToDate(string InputString, string Format)
		{
			CultureInfo Provider = CultureInfo.GetCultureInfo("en-US");
			return DateTime.ParseExact(InputString, Format, Provider);
		}

		public string DateToString(DateTime InputDate, string Format)
		{
			return InputDate.ToString(Format);
		}

	}