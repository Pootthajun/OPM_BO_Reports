﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using OfficeOpenXml;
using OfficeOpenXml.Drawing.Chart;
using System.IO;





/// <summary>
/// Summary description for ExcelReportENG
/// </summary>
public class ExcelReportENG
{

    static string printDate = OPM_BL.GetTHDateAbbr(DateTime.Now.ToString("yyyyMMdd", new System.Globalization.CultureInfo("en-US"))) + " " + DateTime.Now.ToString("HH:mm", new System.Globalization.CultureInfo("en-US"));
    

    #region "__HRM"
    

    #region "Export Report RPT_HRM_UT0601"
    public static void ExportReportRPT_HRM_UT0601(string BudgetYear, string UserName, string UserOrg, DataTable dt, HttpResponse res)
    {
        #region "RPT_HRM_UT0601"
        using (ExcelPackage ep = new ExcelPackage()) {
            ExcelWorksheet ws = ep.Workbook.Worksheets.Add("HRM_RP01");
            int cntRow = 2;
            SetReportHeaderStyle(ws, "สำนักงานปลัดสำนักนายกรัฐมนตรี", "A" + cntRow, "A" + cntRow + ":F" + cntRow);
            cntRow += 1;
            
            SetReportHeaderStyle(ws, "รายงานสรุปรายชื่อบุคลากร ในปีงบประมาณ พ.ศ." + BudgetYear, "A" + cntRow, "A" + cntRow + ":F" + cntRow);
            cntRow += 1;
            
            SetReportHeaderStyle(ws, "พบรายการทั้งหมด" + dt.Rows.Count.ToString() + "รายการ", "A" + cntRow, "A" + cntRow + ":F" + cntRow);
            cntRow += 1;

            SetRowHeaderStyle(ws, "ลำดับ", cntRow, 1, "A"+cntRow);
            SetRowHeaderStyle(ws, "ชื่อ-สกุล", cntRow, 2, "B" + cntRow);
            SetRowHeaderStyle(ws, "ตำแหน่ง", cntRow, 3, "C" + cntRow);
            SetRowHeaderStyle(ws, "ระดับ", cntRow, 4, "D" + cntRow);
            SetRowHeaderStyle(ws, "ประเภทบุคคลากร", cntRow, 5, "E" + cntRow);
            SetRowHeaderStyle(ws, "ความเคลื่อนไหว", cntRow, 6, "F" + cntRow);
            cntRow += 1;


            //Set Column Width
            ws.Column(1).Width = 10;
            ws.Column(2).Width = 30;
            ws.Column(3).Width = 30;
            ws.Column(4).Width = 10;
            ws.Column(5).Width = 20;
            ws.Column(6).Width = 30;

            DataTable dt_org = new DataTable();
            dt_org = dt.DefaultView.ToTable(true, "org_serial","org_name").Copy();
            for (int i = 0; i <= dt_org.Rows.Count - 1; i++)
            {
                string org_serial = dt_org.Rows[i]["org_serial"].ToString();
                DataTable dt_org_detail = new DataTable();
                dt.DefaultView.RowFilter = "org_serial ='" + org_serial + "'";
                dt_org_detail = dt.DefaultView.ToTable(true, "name_level").Copy();

                string org_name = dt_org.Rows[i]["org_name"].ToString();
                SetRowGroupStyle(ws, org_name, cntRow, 1, "A" + cntRow + ":F" + cntRow, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);

                cntRow += 1;

                for (int j = 0; j <= dt_org_detail.Rows.Count - 1; j++)
                {
                    string name_level = dt_org_detail.Rows[j]["name_level"].ToString();
                    DataTable dt_level_detail = new DataTable();
                    dt.DefaultView.RowFilter = "org_serial ='" + org_serial + "' and name_level ='" + name_level + "'";
                    dt_level_detail = dt.DefaultView.ToTable().Copy();

                    SetRowGroupStyle(ws, name_level, cntRow, 2, "B" + cntRow + ":F" + cntRow, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    cntRow += 1;

                    int seq = 1;
                    foreach (DataRow dr in dt_level_detail.Rows)
                    {
                        string staff_name = dr["staff_name"].ToString();
                        string pos_name = dr["pos_name"].ToString();
                        string level_name_full = dr["level_name_full"].ToString();
                        string per_type_name = dr["per_type_name"].ToString();
                        string movement_name = dr["movement_name"].ToString();

                        SetDetailStyle(ws, seq.ToString(), cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                        SetDetailStyle(ws, staff_name, cntRow, 2, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                        SetDetailStyle(ws, pos_name, cntRow, 3, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                        SetDetailStyle(ws, level_name_full, cntRow, 4, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                        SetDetailStyle(ws, per_type_name, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                        SetDetailStyle(ws, movement_name, cntRow, 6, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);

                        cntRow += 1;
                        seq += 1;

                    }//dt_org_detail
                    dt_level_detail.Dispose();
                    dt_org_detail.Dispose();

                }//dt_org_detail
                dt_org_detail.Dispose();
                dt.DefaultView.RowFilter = "";

            }//dt_org
            dt_org.Dispose();

            cntRow += 3;
            //SetNormalStyle(ws, "วันที่พิมพ์ " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " น.", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            //SetNormalStyle(ws, "ผู้พิมพ์ " + UserName , cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            SetNormalStyle(ws, "วันที่พิมพ์ " + printDate + " น.", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            SetNormalStyle(ws, "ผู้พิมพ์ " + UserName + " " + UserOrg, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);

            ExportExcelToClient(res, ep, "RPT_HRM_UT0601");

        }
        #endregion
        
    }
    #endregion

    #region "Export Report RPT_HRM_UT0602"
    public static void ExportReportRPT_HRM_UT0602(string BudgetYear, string UserName, string UserOrg, DataTable dt, HttpResponse res)
    {

        #region "RPT_HRM_UT0602 Backup"
        //using (ExcelPackage ep = new ExcelPackage())
        //{
        //    ExcelWorksheet ws = ep.Workbook.Worksheets.Add("HRM_RP02");
        //    ws.Cells["A1"].Value = "PSS7R050";
        //    ws.Cells["A1"].Style.Font.Bold = true;

        //    SetReportHeaderStyle(ws, "สำนักงานปลัดสำนักนายกรัฐมนตรี", "A2", "A2:E2");
        //    //SetReportHeaderStyle(ws, "รายงานจำนวนข้าราชการที่บรรจุตั้งแต่วันที่ " + StartDate + " ถึง " + EndDate + "/ ในปีงบประมาณ " + BudgetYear, "A3", "A3:E3");

        //    SetRowHeaderStyle(ws, "ส่วนราชการ", 4, 1, "A4");
        //    SetRowHeaderStyle(ws, "บรรจุใหม่", 4, 2, "B4");
        //    SetRowHeaderStyle(ws, "รับโอน", 4, 3, "C4");
        //    SetRowHeaderStyle(ws, "อื่นๆ", 4, 4, "D4");
        //    SetRowHeaderStyle(ws, "รวม", 4, 5, "E4");


        //    //Set Column Width
        //    ws.Column(1).Width = 50;
        //    ws.Column(2).Width = 20;
        //    ws.Column(3).Width = 20;
        //    ws.Column(4).Width = 20;
        //    ws.Column(5).Width = 20;

        //    int sumNewEmp = 0;
        //    int sumTransferEmp = 0;
        //    int sumOtherEmp = 0;
        //    int sumTot = 0;

        //    int iRow = 5;
        //    foreach (DataRow dr in dt.Rows)
        //    {
        //        int newEmp = Convert.ToInt16(dr["new_emp"]);
        //        int transferEmp = Convert.ToInt16(dr["transfer_emp"]);
        //        int otherEmp = Convert.ToInt16(dr["other_emp"]);
        //        int tot = Convert.ToInt16(dr["new_emp"]) + Convert.ToInt16(dr["transfer_emp"]) + Convert.ToInt16(dr["other_emp"]);

        //        SetDetailStyle(ws, dr["org_name"].ToString(), iRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
        //        SetDetailStyle(ws, newEmp.ToString(), iRow, 2, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
        //        SetDetailStyle(ws, transferEmp.ToString(), iRow, 3, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
        //        SetDetailStyle(ws, otherEmp.ToString(), iRow, 4, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
        //        SetDetailStyle(ws, tot.ToString(), iRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);

        //        sumNewEmp += newEmp;
        //        sumTransferEmp += transferEmp;
        //        sumOtherEmp += otherEmp;
        //        sumTot += tot;

        //        iRow += 1;
        //    }

        //    SetRowHeaderStyle(ws, "รวมทั้งหมด", iRow, 1, "A" + iRow.ToString());
        //    SetRowHeaderStyle(ws, sumNewEmp.ToString(), iRow, 2, "B" + iRow.ToString());
        //    SetRowHeaderStyle(ws, sumTransferEmp.ToString(), iRow, 3, "C" + iRow.ToString());
        //    SetRowHeaderStyle(ws, sumOtherEmp.ToString(), iRow, 4, "D" + iRow.ToString());
        //    SetRowHeaderStyle(ws, sumTot.ToString(), iRow, 5, "E" + iRow.ToString());

        //    SetNormalStyle(ws, "ผู้พิมพ์ " + UserName + " " + UserOrg, iRow + 3, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
        //    SetNormalStyle(ws, "เวลาที่พิมพ์ " + DateTime.Now.ToString("HH:mm:ss") + " น.", iRow + 3, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);

        //    try
        //    {
        //        //Write it back to the client
        //        res.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        //        res.AddHeader("content-disposition", "attachment;  filename=HRM_RP02.xlsx");
        //        res.BinaryWrite(ep.GetAsByteArray());
        //        res.End();
        //    }
        //    catch (Exception ex) { }
        //}
        #endregion

        using (ExcelPackage ep = new ExcelPackage())
        {
            string per_type_name = "";
            if (dt.Rows.Count > 0)
            {
                per_type_name = dt.Rows[0]["per_type_name"].ToString();
            }
            ExcelWorksheet ws = ep.Workbook.Worksheets.Add("RPT_HRM_UT0602");
            int cntRow = 2;
            SetReportHeaderStyle(ws, "สำนักงานปลัดสำนักนายกรัฐมนตรี", "A" + cntRow, "A" + cntRow + ":F" + cntRow);
            cntRow += 1;

            SetReportHeaderStyle(ws, "รายงานจำนวน" + per_type_name + " ที่เข้าสู่ส่วนราชการ พ.ศ." + BudgetYear, "A" + cntRow, "A" + cntRow + ":F" + cntRow);
            cntRow += 1;

            SetReportHeaderStyle(ws, "พบรายการทั้งหมด" + dt.Rows.Count.ToString() + "รายการ", "A" + cntRow, "A" + cntRow + ":F" + cntRow);
            cntRow += 1;

            SetRowHeaderStyle(ws, "เลขที่", cntRow, 1, "A"+cntRow);
            SetRowHeaderStyle(ws, "ส่วนราชการ", cntRow, 2, "B" + cntRow);
            SetRowHeaderStyle(ws, "บรรจุใหม่", cntRow, 3, "C" + cntRow);
            SetRowHeaderStyle(ws, "รับโอน", cntRow, 4, "D" + cntRow);
            SetRowHeaderStyle(ws, "อื่นๆ", cntRow, 5, "E" + cntRow);
            SetRowHeaderStyle(ws, "รวม", cntRow, 6, "F" + cntRow);
            cntRow += 1;

            //Set Column Width
            ws.Column(1).Width = 10;
            ws.Column(2).Width = 50;
            ws.Column(3).Width = 20;
            ws.Column(4).Width = 20;
            ws.Column(5).Width = 20;
            ws.Column(6).Width = 20;

            int sumNewEmp = 0;
            int sumTransferEmp = 0;
            int sumOtherEmp = 0;
            int sumTot = 0;

            int seq = 1;
            foreach (DataRow dr in dt.Rows)
            {
                int newEmp = Convert.ToInt16(dr["new_emp"]);
                int transferEmp = Convert.ToInt16(dr["transfer_emp"]);
                int otherEmp = Convert.ToInt16(dr["other_emp"]);
                int tot = Convert.ToInt16(dr["new_emp"]) + Convert.ToInt16(dr["transfer_emp"]) + Convert.ToInt16(dr["other_emp"]);

                SetDetailStyle(ws, seq.ToString(), cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetDetailStyle(ws, dr["org_name"].ToString(), cntRow, 2, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                SetDetailStyle(ws, newEmp.ToString(),  cntRow, 3, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetDetailStyle(ws, transferEmp.ToString(),  cntRow, 4, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetDetailStyle(ws, otherEmp.ToString(),  cntRow , 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetDetailStyle(ws, tot.ToString(),  cntRow, 6, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);

                sumNewEmp += newEmp;
                sumTransferEmp += transferEmp;
                sumOtherEmp += otherEmp;
                sumTot += tot;

                seq += 1;
                cntRow += 1;
            }

            SetRowHeaderStyle(ws, "รวมทั้งหมด", cntRow, 1, "A" + cntRow + ":B" + cntRow);
            SetRowHeaderStyle(ws, sumNewEmp.ToString(), cntRow, 3, "C" + cntRow);
            SetRowHeaderStyle(ws, sumTransferEmp.ToString(), cntRow, 4, "D" + cntRow);
            SetRowHeaderStyle(ws, sumOtherEmp.ToString(), cntRow, 5, "E" + cntRow);
            SetRowHeaderStyle(ws, sumTot.ToString(), cntRow, 6, "F" + cntRow);
            cntRow += 4;


            //SetNormalStyle(ws, "วันที่พิมพ์ " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " น.", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            //SetNormalStyle(ws, "ผู้พิมพ์ " + UserName, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);

            SetNormalStyle(ws, "วันที่พิมพ์ " + printDate + " น.", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            SetNormalStyle(ws, "ผู้พิมพ์ " + UserName + " " + UserOrg, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);

            ExportExcelToClient(res, ep, "RPT_HRM_UT0602");

          
        }

    }
    #endregion

    #region "Export Report RPT_HRM_UT0603"
    public static void ExportReportRPT_HRM_UT0603(string BudgetYear, string UserName, string UserOrg, DataTable dt, HttpResponse res)
    {
        using (ExcelPackage ep = new ExcelPackage())
        {
            ExcelWorksheet ws = ep.Workbook.Worksheets.Add("RPT_HRM_UT0603");

            string per_type_name = "";
            if (dt.Rows.Count > 0)
            {
                per_type_name = dt.Rows[0]["per_type_name"].ToString();
            }
            int cntRow = 2;
            SetReportHeaderStyle(ws, "สำนักงานปลัดสำนักนายกรัฐมนตรี", "A" + cntRow, "A" + cntRow + ":H" + cntRow);
            cntRow += 1;

            SetReportHeaderStyle(ws, "รายงานจำนวน" + per_type_name + " ที่พ้นจากส่วนราชการ ในปีงบประมาณ พ.ศ." + BudgetYear, "A" + cntRow, "A" + cntRow + ":H" + cntRow);
            cntRow += 1;

            SetReportHeaderStyle(ws, "พบรายการทั้งหมด" + dt.Rows.Count.ToString() + "รายการ", "A" + cntRow, "A" + cntRow + ":H" + cntRow);
            cntRow += 1;

            SetRowHeaderStyle(ws, "เลขที่", cntRow, 1, "A" + cntRow);
            SetRowHeaderStyle(ws, "ส่วนราชการ", cntRow, 2, "B" + cntRow);
            SetRowHeaderStyle(ws, "เกษียณ", cntRow, 3, "C" + cntRow);
            SetRowHeaderStyle(ws, "ลาออก", cntRow, 4, "D" + cntRow);
            SetRowHeaderStyle(ws, "โอนออก", cntRow, 5, "E" + cntRow);
            SetRowHeaderStyle(ws, "วินัย", cntRow, 6, "F" + cntRow);
            SetRowHeaderStyle(ws, "อื่น ๆ", cntRow, 7, "G" + cntRow);
            SetRowHeaderStyle(ws, "รวม", cntRow, 8, "H" + cntRow);
            cntRow += 1;

            //Set Column Width
            ws.Column(1).Width = 10;
            ws.Column(2).Width = 50;
            ws.Column(3).Width = 10;
            ws.Column(4).Width = 10;
            ws.Column(5).Width = 10;
            ws.Column(6).Width = 10;
            ws.Column(7).Width = 10;
            ws.Column(8).Width = 10;

            int sumcmd_retire = 0;
            int sumcmd_resign = 0;
            int sumcmd_transfer = 0;
            int sumcmd_disciplinary = 0;
            int sumcmd_other = 0;
            int sumtot = 0;

            int seq = 1;
            foreach (DataRow dr in dt.Rows)
            {
                int cmd_retire = Convert.ToInt16(dr["cmd_retire"]);
                int cmd_resign = Convert.ToInt16(dr["cmd_resign"]);
                int cmd_transfer = Convert.ToInt16(dr["cmd_transfer"]);
                int cmd_disciplinary = Convert.ToInt16(dr["cmd_disciplinary"]);
                int cmd_other = Convert.ToInt16(dr["cmd_other"]);
                int tot = Convert.ToInt16(dr["cmd_retire"]) + Convert.ToInt16(dr["cmd_resign"]) + Convert.ToInt16(dr["cmd_transfer"]) + Convert.ToInt16(dr["cmd_disciplinary"]) + Convert.ToInt16(dr["cmd_other"]);

                SetDetailStyle(ws, seq.ToString(), cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetDetailStyle(ws, dr["org_name"].ToString(), cntRow, 2, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                SetDetailStyle(ws, cmd_retire.ToString(), cntRow, 3, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetDetailStyle(ws, cmd_resign.ToString(), cntRow, 4, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetDetailStyle(ws, cmd_transfer.ToString(), cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetDetailStyle(ws, cmd_disciplinary.ToString(), cntRow, 6, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetDetailStyle(ws, cmd_other.ToString(), cntRow, 7, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetDetailStyle(ws, tot.ToString(), cntRow, 8, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);

                sumcmd_retire += cmd_retire;
                sumcmd_resign += cmd_resign;
                sumcmd_transfer += cmd_transfer;
                sumcmd_disciplinary += cmd_disciplinary;
                sumcmd_other += cmd_other;
                sumtot += tot;
              
                seq += 1;
                cntRow += 1;
            }

            SetRowHeaderStyle(ws, "รวมทั้งหมด", cntRow, 1, "A" + cntRow + ":B" + cntRow);
            SetRowHeaderStyle(ws, sumcmd_retire.ToString(), cntRow, 3, "C" + cntRow);
            SetRowHeaderStyle(ws, sumcmd_resign.ToString(), cntRow, 4, "D" + cntRow);
            SetRowHeaderStyle(ws, sumcmd_transfer.ToString(), cntRow, 5, "E" + cntRow);
            SetRowHeaderStyle(ws, sumcmd_disciplinary.ToString(), cntRow, 6, "F" + cntRow);
            SetRowHeaderStyle(ws, sumcmd_other.ToString(), cntRow, 7, "G" + cntRow);
            SetRowHeaderStyle(ws, sumtot.ToString(), cntRow, 8, "H" + cntRow);

            cntRow += 4;
            //SetNormalStyle(ws, "วันที่พิมพ์ " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " น.", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            //SetNormalStyle(ws, "ผู้พิมพ์ " + UserName, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            SetNormalStyle(ws, "วันที่พิมพ์ " + printDate + " น.", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            SetNormalStyle(ws, "ผู้พิมพ์ " + UserName + " " + UserOrg, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);


            ExportExcelToClient(res, ep, "RPT_HRM_UT0603");
            
        }
    }
    #endregion

    #region "Export Report RPT_HRM_UT0604"
    public static void ExportReportRPT_HRM_UT0604(string BudgetYear, string UserName, string UserOrg, DataTable dt, HttpResponse res)
    {
        using (ExcelPackage ep = new ExcelPackage())
        {
            ExcelWorksheet ws = ep.Workbook.Worksheets.Add("RPT_HRM_UT0604");

            string per_type_name = "";
            if (dt.Rows.Count > 0) { per_type_name = dt.Rows[0]["per_type_name"].ToString(); }
            string BudgetYearOld = "";
            BudgetYearOld = (Convert.ToInt16(BudgetYear) - 1).ToString();

            int cntRow = 2;
            SetReportHeaderStyle(ws, "สำนักงานปลัดสำนักนายกรัฐมนตรี", "A" + cntRow, "A" + cntRow + ":H" + cntRow);
            cntRow += 1;

            SetReportHeaderStyle(ws, "รายงานอัตราเข้า-ออก ของ" + per_type_name + " ในปีงบประมาณ พ.ศ." + BudgetYear, "A" + cntRow, "A" + cntRow + ":H" + cntRow);
            cntRow += 1;

            SetReportHeaderStyle(ws, "พบรายการทั้งหมด" + dt.Rows.Count.ToString() + "รายการ", "A" + cntRow, "A" + cntRow + ":H" + cntRow);
            cntRow += 1;

            SetRowHeaderStyle(ws, "ส่วนราชการ", cntRow, 1, "A" + cntRow + ":A" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "จำนวน(คน)", cntRow, 2, "B" + cntRow + ":E" + cntRow);
            SetRowHeaderStyle(ws, "ปีงบประมาณ พ.ศ." + BudgetYearOld, (cntRow + 1), 2, "B" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "เข้า", (cntRow + 1), 3, "C" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "ออก", (cntRow + 1), 4, "D" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "ปีงบประมาณ พ.ศ." + BudgetYear, (cntRow + 1), 5, "E" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "อัตราการ", cntRow, 6, "F" + cntRow + ":G" + cntRow);
            SetRowHeaderStyle(ws, "เข้า", (cntRow + 1), 6, "F" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "ออก", (cntRow + 1), 7, "G" + (cntRow + 1).ToString());
            cntRow += 2;

            //Set Column Width
            ws.Column(1).Width = 50;
            ws.Column(2).Width = 10;
            ws.Column(3).Width = 10;
            ws.Column(4).Width = 10;
            ws.Column(5).Width = 10;
            ws.Column(6).Width = 10;
            ws.Column(7).Width = 10;

            int seq = 1;
            foreach (DataRow dr in dt.Rows)
            {
                int budget_year_old_qty = Convert.ToInt16(dr["budget_year_old_qty"]);
                int in_qty = Convert.ToInt16(dr["in_qty"]);
                int out_qty = Convert.ToInt16(dr["out_qty"]);
                int budget_year_now_qty = Convert.ToInt16(dr["budget_year_now_qty"]);
                int in_percent = Convert.ToInt16(dr["in_percent"]);
                int out_percent = Convert.ToInt16(dr["out_percent"]);

                SetDetailStyle(ws, dr["org_name"].ToString(), cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                SetDetailStyle(ws, budget_year_old_qty.ToString(), cntRow, 2, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetDetailStyle(ws, in_qty.ToString(), cntRow, 3, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetDetailStyle(ws, out_qty.ToString(), cntRow, 4, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetDetailStyle(ws, budget_year_now_qty.ToString(), cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetDetailStyle(ws, in_percent.ToString(), cntRow, 6, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetDetailStyle(ws, out_percent.ToString(), cntRow, 7, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);

                seq += 1;
                cntRow += 1;
            }


            cntRow += 3;
            //SetNormalStyle(ws, "วันที่พิมพ์ " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " น.", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            //SetNormalStyle(ws, "ผู้พิมพ์ " + UserName, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            SetNormalStyle(ws, "วันที่พิมพ์ " + printDate + " น.", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            SetNormalStyle(ws, "ผู้พิมพ์ " + UserName + " " + UserOrg, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);

         
            ExportExcelToClient(res, ep, "RPT_HRM_UT0604");
        }
    }
    #endregion

    #region "Export Report RPT_HRM_UT0605"
    public static void ExportReportRPT_HRM_UT0605_ORG(DataTable dt, string UserName, HttpResponse res)
    {
        using (ExcelPackage ep = new ExcelPackage())
        {

            try
            { 
            ExcelWorksheet ws = ep.Workbook.Worksheets.Add("RPT_HRM_UT0605_ORG");

            DataTable ptDt = new DataTable();
            ptDt = dt.DefaultView.ToTable(true, "per_type_name").Copy();
            if (ptDt.Rows.Count > 0) { 
                //Per type ประเภทบุคคลากร


                //Set Column Width
                ws.Column(1).Width = 10;
                ws.Column(2).Width = 50;
                ws.Column(3).Width = 25;
                ws.Column(4).Width = 20;
                ws.Column(5).Width = 20;
                ws.Column(6).Width = 30;
                
                int iRow = 2;

                foreach (DataRow ptDr in ptDt.Rows) {

                    DataTable o3Dt = new DataTable();
                    dt.DefaultView.RowFilter = "per_type_name='" + ptDr["per_type_name"].ToString() + "'";
                    o3Dt = dt.DefaultView.ToTable(true, "per_type_name", "name_level3").Copy();
                    if (o3Dt.Rows.Count > 0) {
                        foreach (DataRow o3Dr in o3Dt.Rows) {

                            DataTable o4Dt = new DataTable();
                            dt.DefaultView.RowFilter = "per_type_name='" + ptDr["per_type_name"].ToString() + "' and name_level3='" + o3Dr["name_level3"].ToString() + "'";
                            o4Dt = dt.DefaultView.ToTable(true, "per_type_name", "name_level3", "name_level4").Copy();
                            if (o4Dt.Rows.Count > 0) {
                                foreach (DataRow o4Dr in o4Dt.Rows) {

                                    DataTable o5Dt = new DataTable();
                                    dt.DefaultView.RowFilter = "per_type_name='" + ptDr["per_type_name"].ToString() + "' and name_level3='" + o3Dr["name_level3"].ToString() + "' and name_level4='" + o4Dr["name_level4"].ToString() + "'";
                                    o5Dt = dt.DefaultView.ToTable(true, "per_type_name", "name_level3", "name_level4", "name_level5").Copy();
                                    if (o5Dt.Rows.Count > 0) {
                                        foreach (DataRow o5Dr in o5Dt.Rows) {

                                            //ประจำวันที่
                                            DataTable dDt = new DataTable();
                                            dt.DefaultView.RowFilter = "per_type_name='" + o4Dr["per_type_name"].ToString() + "' and name_level3='" + o3Dr["name_level3"].ToString() + "' and name_level4='" + o4Dr["name_level4"].ToString() + "' and name_level5 = '" + o5Dr["name_level5"].ToString() + "'";
                                            dDt = dt.DefaultView.ToTable(true, "per_type_name", "name_level3", "name_level4", "name_level5", "downtime_date").Copy();
                                            if (dDt.Rows.Count > 0) {
                                                foreach (DataRow dDr in dDt.Rows) {
                                                    SetReportHeaderStyle(ws, "สำนักงานปลัดสำนักนายกรัฐมนตรี", "A" + iRow.ToString(), "A" + iRow.ToString() + ":F" + iRow.ToString());
                                                    iRow += 1;

                                                    string NameLevel3 = o3Dr["name_level3"].ToString();
                                                    string NameLevel4 = o4Dr["name_level4"].ToString();
                                                    string NameLevel5 = o5Dr["name_level5"].ToString();

                                                    if (NameLevel5 == NameLevel4)
                                                    {
                                                        NameLevel5 = "";
                                                    }
                                                    else
                                                    {
                                                        NameLevel5 = " / " + NameLevel5;
                                                    }

                                                    if (NameLevel4 == NameLevel3)
                                                    {
                                                        NameLevel4 = "";
                                                    }
                                                    else
                                                    {
                                                        NameLevel4 = " / " + NameLevel4;
                                                    }


                                                    SetReportHeaderStyle(ws, "บัญชีลงเวลาการปฏิบัติราชการ " + o4Dr["per_type_name"].ToString() + " " + NameLevel3 + NameLevel4 + NameLevel5, "A" + iRow.ToString(), "A" + iRow.ToString() + ":F" + iRow.ToString());
                                                    iRow += 1;

                                                    SetReportHeaderStyle(ws, "ประจำวันที่ " + dDr["downtime_date"].ToString(), "A" + iRow.ToString(), "A" + iRow.ToString() + ":F" + iRow.ToString());
                                                    iRow += 1;


                                                    dt.DefaultView.RowFilter = "per_type_name='" + o4Dr["per_type_name"].ToString() + "' and name_level3='" + o3Dr["name_level3"].ToString() + "' and name_level4='" + o4Dr["name_level4"].ToString() + "' and name_level5 = '" + o5Dr["name_level5"] + "' and downtime_date='" + dDr["downtime_date"].ToString() + "'";
                                                    SetReportHeaderStyle(ws, "พบรายการทั้งหมด " + dt.DefaultView.Count.ToString() + " รายการ", "A" + iRow.ToString(), "A" + iRow.ToString() + ":F" + iRow.ToString());
                                                    iRow += 1;

                                                    SetRowHeaderStyle(ws, "ลำดับที่", iRow, 1, "A" + iRow.ToString());
                                                    SetRowHeaderStyle(ws, "ชื่อสกุล", iRow, 2, "B" + iRow.ToString());
                                                    SetRowHeaderStyle(ws, "รอบการลงเวลา", iRow, 3, "C" + iRow.ToString());
                                                    SetRowHeaderStyle(ws, "เวลามา", iRow, 4, "D" + iRow.ToString());
                                                    SetRowHeaderStyle(ws, "เวลากลับ", iRow, 5, "E" + iRow.ToString());
                                                    SetRowHeaderStyle(ws, "หมายเหตุ", iRow, 6, "F" + iRow.ToString());
                                                    iRow += 1;

                                                    string org_name = "หน่วยงาน  " + NameLevel3 + NameLevel4 + NameLevel5;
                                                    SetRowGroupStyle(ws, org_name, iRow, 1, "A" + iRow.ToString() + ":F" + iRow.ToString(), OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);

                                                    //SetTextStyle(ws, org_name, "A" + iRow.ToString(), true, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                                                    ws.Cells["A" + iRow.ToString()].Style.Font.UnderLine = true;
                                                    iRow += 1;
                                                   

                                                    int i = 1;
                                                    foreach (DataRowView dr in dt.DefaultView)
                                                    {
                                                        SetNormalStyle(ws, i.ToString(), iRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                                                        SetCellBorder(ws, "A" + iRow.ToString(), false);

                                                        SetNormalStyle(ws, dr["staff_name"].ToString(), iRow, 2, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                                                        SetCellBorder(ws, "B" + iRow.ToString(), false);

                                                        SetNormalStyle(ws, dr["time_name"].ToString(), iRow, 3, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                                                        SetCellBorder(ws, "C" + iRow.ToString(), false);

                                                        SetNormalStyle(ws, dr["time_come"].ToString(), iRow, 4, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                                                        SetCellBorder(ws, "D" + iRow.ToString(), false);

                                                        SetNormalStyle(ws, dr["time_back"].ToString(), iRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                                                        SetCellBorder(ws, "E" + iRow.ToString(), false);

                                                        SetNormalStyle(ws, dr["remarks"].ToString(), iRow, 6, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                                                        SetCellBorder(ws, "F" + iRow.ToString(), false);

                                                        i += 1;
                                                        iRow += 1;
                                                    }
                                                    iRow += 3;
                                                    dt.DefaultView.RowFilter = "";

                                                    //SetNormalStyle(ws, "วันที่พิมพ์ " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " น.", iRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                                                    //SetNormalStyle(ws, "ผู้พิมพ์ " + UserName, iRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                                                    SetNormalStyle(ws, "วันที่พิมพ์ " + printDate + " น.", iRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                                                    SetNormalStyle(ws, "ผู้พิมพ์ " + UserName, iRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                                                    
                                                    iRow += 3;
                                                }
                                            }
                                            dDt.Dispose();
                                            dt.DefaultView.RowFilter = "";
                                        }
                                    }
                                    o5Dt.Dispose();
                                    dt.DefaultView.RowFilter = "";
                                }
                            }
                            o4Dt.Dispose();
                            dt.DefaultView.RowFilter = "";
                        }
                    }
                    o3Dt.Dispose();
                    dt.DefaultView.RowFilter = "";
                }




            }
            ptDt.Dispose();

            ExportExcelToClient(res, ep, "RPT_HRM_UT0605_ORG");
            }
            catch (Exception ex)
            { }
        }
            
    }

    public static void ExportReportRPT_HRM_UT0605_PERSON(DataTable dt,string UserName, HttpResponse res)
    {
        using (ExcelPackage ep = new ExcelPackage())
        {
            ExcelWorksheet ws = ep.Workbook.Worksheets.Add("RPT_HRM_UT0605_PERSON");

            string staff_name = "";
            string NameLevel3 = "";
            string NameLevel4 = "";
            string NameLevel5 = "";
            if (dt.Rows.Count > 0)
            {
                staff_name = dt.Rows[0]["staff_name"].ToString();
                NameLevel3 = dt.Rows[0]["name_level3"].ToString();
                NameLevel4 = dt.Rows[0]["name_level4"].ToString();
                NameLevel5 = dt.Rows[0]["name_level5"].ToString();

                if (NameLevel5 == NameLevel4)
                {
                    NameLevel5 = "";
                }
                else
                {
                    NameLevel5 = " / " + NameLevel5;
                }

                if (NameLevel4 == NameLevel3)
                {
                    NameLevel4 = "";
                }
                else
                {
                    NameLevel4 = " / " + NameLevel4;
                }
            }
            int cntRow = 2;
            SetReportHeaderStyle(ws, "สำนักงานปลัดสำนักนายกรัฐมนตรี", "A" + cntRow, "A" + cntRow + ":F" + cntRow);
            cntRow += 1;

            SetReportHeaderStyle(ws, "บัญชีลงเวลาการปฏิบัติราชการ ของนาย" + staff_name, "A" + cntRow, "A" + cntRow + ":F" + cntRow);
            cntRow += 1;

            SetReportHeaderStyle(ws, NameLevel3 + NameLevel4 + NameLevel5, "A" + cntRow, "A" + cntRow + ":F" + cntRow);
            cntRow += 1;

            SetReportHeaderStyle(ws, "พบรายการทั้งหมด" + dt.Rows.Count.ToString() + "รายการ", "A" + cntRow, "A" + cntRow + ":F" + cntRow);
            cntRow += 1;

            SetRowHeaderStyle(ws, "เลขที่", cntRow, 1, "A" + cntRow);
            SetRowHeaderStyle(ws, "วันที่", cntRow, 2, "B" + cntRow);
            SetRowHeaderStyle(ws, "เวลามา", cntRow, 3, "C" + cntRow);
            SetRowHeaderStyle(ws, "เวลากลับ", cntRow, 4, "D" + cntRow);
            SetRowHeaderStyle(ws, "จำนวนชั่วโมง", cntRow, 5, "E" + cntRow);
            SetRowHeaderStyle(ws, "หมายเหตุ", cntRow, 6, "F" + cntRow);
            cntRow += 1;

            //Set Column Width
            ws.Column(1).Width = 10;
            ws.Column(2).Width = 20;
            ws.Column(3).Width = 20;
            ws.Column(4).Width = 20;
            ws.Column(5).Width = 20;
            ws.Column(6).Width = 30;

            int seq = 1;
            foreach (DataRow dr in dt.Rows)
            {
                string downtime_date = dr["downtime_date"].ToString();
                string time_come = dr["time_come"].ToString();
                string time_out = dr["time_back"].ToString();
                string timeformat = dr["timeformat"].ToString();
                string remark = dr["remarks"].ToString();

                SetDetailStyle(ws, seq.ToString(), cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetDetailStyle(ws, downtime_date, cntRow, 2, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetDetailStyle(ws, time_come, cntRow, 3, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetDetailStyle(ws, time_out, cntRow, 4, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetDetailStyle(ws, timeformat, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetDetailStyle(ws, remark, cntRow, 6, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                
                seq += 1;
                cntRow += 1;
            }

            cntRow += 3;
            SetNormalStyle(ws, "วันที่พิมพ์ " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " น.", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            SetNormalStyle(ws, "ผู้พิมพ์ " + UserName, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            ExportExcelToClient(res, ep, "RPT_HRM_UT0605_PERSON");

        }
    }
    #endregion

    #region "Export Report RPT_HRM_UT0606"
    public static void ExportReportRPT_HRM_UT0606_ORG(DataTable dt, string UserName, HttpResponse res)
    {
        using (ExcelPackage ep = new ExcelPackage())
        {

            try
            {
                ExcelWorksheet ws = ep.Workbook.Worksheets.Add("RPT_HRM_UT0606_ORG");

                DataTable ptDt = new DataTable();
                ptDt = dt.DefaultView.ToTable(true, "per_type_name").Copy();
                if (ptDt.Rows.Count > 0)
                {
                    //Per type ประเภทบุคคลากร


                    //Set Column Width
                    ws.Column(1).Width = 10;
                    ws.Column(2).Width = 50;
                    ws.Column(3).Width = 25;
                    ws.Column(4).Width = 20;
                    ws.Column(5).Width = 20;
                    ws.Column(6).Width = 30;

                    int iRow = 2;

                    foreach (DataRow ptDr in ptDt.Rows)
                    {

                        DataTable o3Dt = new DataTable();
                        dt.DefaultView.RowFilter = "per_type_name='" + ptDr["per_type_name"].ToString() + "'";
                        o3Dt = dt.DefaultView.ToTable(true, "per_type_name", "name_level3").Copy();
                        if (o3Dt.Rows.Count > 0)
                        {
                            foreach (DataRow o3Dr in o3Dt.Rows)
                            {

                                DataTable o4Dt = new DataTable();
                                dt.DefaultView.RowFilter = "per_type_name='" + ptDr["per_type_name"].ToString() + "' and name_level3='" + o3Dr["name_level3"].ToString() + "'";
                                o4Dt = dt.DefaultView.ToTable(true, "per_type_name", "name_level3", "name_level4").Copy();
                                if (o4Dt.Rows.Count > 0)
                                {
                                    foreach (DataRow o4Dr in o4Dt.Rows)
                                    {

                                        DataTable o5Dt = new DataTable();
                                        dt.DefaultView.RowFilter = "per_type_name='" + ptDr["per_type_name"].ToString() + "' and name_level3='" + o3Dr["name_level3"].ToString() + "' and name_level4='" + o4Dr["name_level4"].ToString() + "'";
                                        o5Dt = dt.DefaultView.ToTable(true, "per_type_name", "name_level3", "name_level4", "name_level5").Copy();
                                        if (o5Dt.Rows.Count > 0)
                                        {
                                            foreach (DataRow o5Dr in o5Dt.Rows)
                                            {

                                                //ประจำวันที่
                                                DataTable dDt = new DataTable();
                                                dt.DefaultView.RowFilter = "per_type_name='" + o4Dr["per_type_name"].ToString() + "' and name_level3='" + o3Dr["name_level3"].ToString() + "' and name_level4='" + o4Dr["name_level4"].ToString() + "' and name_level5 = '" + o5Dr["name_level5"].ToString() + "'";
                                                dDt = dt.DefaultView.ToTable(true, "per_type_name", "name_level3", "name_level4", "name_level5", "downtime_date").Copy();
                                                if (dDt.Rows.Count > 0)
                                                {
                                                    foreach (DataRow dDr in dDt.Rows)
                                                    {
                                                        //remark
                                                        DataTable rDt = new DataTable();
                                                        dt.DefaultView.RowFilter = "per_type_name='" + o4Dr["per_type_name"].ToString() + "' and name_level3='" + o3Dr["name_level3"].ToString() + "' and name_level4='" + o4Dr["name_level4"].ToString() + "' and name_level5 = '" + o5Dr["name_level5"].ToString() + "' and downtime_date='" + dDr["downtime_date"].ToString() +"'";
                                                        rDt = dt.DefaultView.ToTable(true, "per_type_name", "name_level3", "name_level4", "name_level5", "downtime_date","remarks").Copy();
                                                        if (rDt.Rows.Count > 0)
                                                        {
                                                            foreach (DataRow rDr in rDt.Rows)
                                                            { 

                                                                SetReportHeaderStyle(ws, "สำนักงานปลัดสำนักนายกรัฐมนตรี", "A" + iRow.ToString(), "A" + iRow.ToString() + ":F" + iRow.ToString());
                                                                iRow += 1;

                                                                SetReportHeaderStyle(ws, "รายงานบัญชีลงเวลามาปฏิบัติราชการประจำวัน (แยกตามหมายเหตุการลงเวลา)" + rDr["remarks"].ToString(), "A" + iRow.ToString(), "A" + iRow.ToString() + ":F" + iRow.ToString());
                                                                iRow += 1;

                                                                string NameLevel3 = o3Dr["name_level3"].ToString();
                                                                string NameLevel4 = o4Dr["name_level4"].ToString();
                                                                string NameLevel5 = o5Dr["name_level5"].ToString();

                                                                if (NameLevel5 == NameLevel4)
                                                                {
                                                                    NameLevel5 = "";
                                                                }
                                                                else
                                                                {
                                                                    NameLevel5 = " / " + NameLevel5;
                                                                }

                                                                if (NameLevel4 == NameLevel3)
                                                                {
                                                                    NameLevel4 = "";
                                                                }
                                                                else
                                                                {
                                                                    NameLevel4 = " / " + NameLevel4;
                                                                }


                                                                SetReportHeaderStyle(ws, NameLevel3 + NameLevel4 + NameLevel5, "A" + iRow.ToString(), "A" + iRow.ToString() + ":F" + iRow.ToString());
                                                                iRow += 1;

                                                                SetReportHeaderStyle(ws, "ประจำวันที่ " + dDr["downtime_date"].ToString(), "A" + iRow.ToString(), "A" + iRow.ToString() + ":F" + iRow.ToString());
                                                                iRow += 1;


                                                                dt.DefaultView.RowFilter = "per_type_name='" + o4Dr["per_type_name"].ToString() + "' and name_level3='" + o3Dr["name_level3"].ToString() + "' and name_level4='" + o4Dr["name_level4"].ToString() + "' and name_level5 = '" + o5Dr["name_level5"] + "' and downtime_date='" + dDr["downtime_date"].ToString() + "' and remarks ='"+ rDr["remarks"].ToString() +"'";
                                                                SetReportHeaderStyle(ws, "พบรายการทั้งหมด " + dt.DefaultView.Count.ToString() + " รายการ", "A" + iRow.ToString(), "A" + iRow.ToString() + ":F" + iRow.ToString());
                                                                iRow += 1;

                                                                SetRowHeaderStyle(ws, "ลำดับที่", iRow, 1, "A" + iRow.ToString());
                                                                SetRowHeaderStyle(ws, "ชื่อสกุล", iRow, 2, "B" + iRow.ToString());
                                                                SetRowHeaderStyle(ws, "รอบการลงเวลา", iRow, 3, "C" + iRow.ToString());
                                                                SetRowHeaderStyle(ws, "เวลามา", iRow, 4, "D" + iRow.ToString());
                                                                SetRowHeaderStyle(ws, "เวลากลับ", iRow, 5, "E" + iRow.ToString());
                                                                SetRowHeaderStyle(ws, "หมายเหตุ", iRow, 6, "F" + iRow.ToString());
                                                                iRow += 1;

                                                                string org_name = "หน่วยงาน  " + NameLevel3 + NameLevel4 + NameLevel5;
                                                                SetRowGroupStyle(ws, org_name, iRow, 1, "A" + iRow.ToString() + ":F" + iRow.ToString(), OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);

                                                                //SetTextStyle(ws, org_name, "A" + iRow.ToString(), true, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                                                                ws.Cells["A" + iRow.ToString()].Style.Font.UnderLine = true;
                                                                iRow += 1;


                                                                int i = 1;
                                                                foreach (DataRowView dr in dt.DefaultView)
                                                                {
                                                                    SetNormalStyle(ws, i.ToString(), iRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                                                                    SetCellBorder(ws, "A" + iRow.ToString(), false);

                                                                    SetNormalStyle(ws, dr["staff_name"].ToString(), iRow, 2, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                                                                    SetCellBorder(ws, "B" + iRow.ToString(), false);

                                                                    SetNormalStyle(ws, dr["time_name"].ToString(), iRow, 3, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                                                                    SetCellBorder(ws, "C" + iRow.ToString(), false);

                                                                    SetNormalStyle(ws, dr["time_come"].ToString(), iRow, 4, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                                                                    SetCellBorder(ws, "D" + iRow.ToString(), false);

                                                                    SetNormalStyle(ws, dr["time_back"].ToString(), iRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                                                                    SetCellBorder(ws, "E" + iRow.ToString(), false);

                                                                    SetNormalStyle(ws, dr["remarks"].ToString(), iRow, 6, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                                                                    SetCellBorder(ws, "F" + iRow.ToString(), false);

                                                                    i += 1;
                                                                    iRow += 1;
                                                                }
                                                                iRow += 3;
                                                                dt.DefaultView.RowFilter = "";

                                                                //SetNormalStyle(ws, "วันที่พิมพ์ " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " น.", iRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                                                                //SetNormalStyle(ws, "ผู้พิมพ์ " + UserName, iRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                                                                SetNormalStyle(ws, "วันที่พิมพ์ " + printDate + " น.", iRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                                                                SetNormalStyle(ws, "ผู้พิมพ์ " + UserName , iRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                                                                
                                                                iRow += 3;
                                                            }//detail

                                                            
                                                            }
                                                        rDt.Dispose();
                                                        dt.DefaultView.RowFilter = "";

                                                        }

                                                   
                                                }

                                                dDt.Dispose();
                                                dt.DefaultView.RowFilter = "";
                                            }
                                        }
                                        o5Dt.Dispose();
                                        dt.DefaultView.RowFilter = "";
                                    }
                                }
                                o4Dt.Dispose();
                                dt.DefaultView.RowFilter = "";
                            }
                        }
                        o3Dt.Dispose();
                        dt.DefaultView.RowFilter = "";
                    }




                }
                ptDt.Dispose();

                ExportExcelToClient(res, ep, "RPT_HRM_UT0606_ORG");
            }
            catch (Exception ex)
            { }
        }

    }

    public static void ExportReportRPT_HRM_UT0606_PERSON(string DateFrom ,string DateTo,DataTable dt, string UserName, HttpResponse res)
    {
        try
        {
            using (ExcelPackage ep = new ExcelPackage())
            {
                ExcelWorksheet ws = ep.Workbook.Worksheets.Add("RPT_HRM_UT0605_PERSON");
                int cntRow = 2;

                DataTable rDt = new DataTable();
                rDt = dt.DefaultView.ToTable(true, "remarks").Copy();
                if (rDt.Rows.Count > 0)
                {
                    for (int i = 0; i <= rDt.Rows.Count - 1; i++)
                    {

                        string remarks = rDt.Rows[i]["remarks"].ToString();

                        DataTable dDt = new DataTable();
                        dt.DefaultView.RowFilter = "remarks='" + remarks + "'";
                        dDt = dt.DefaultView.ToTable().Copy();
                        if (dDt.Rows.Count > 0)
                        {
                            string staff_name = "";
                            string NameLevel3 = "";
                            string NameLevel4 = "";
                            string NameLevel5 = "";
                            if (dt.Rows.Count > 0)
                            {
                                staff_name = dDt.Rows[0]["staff_name"].ToString();
                                NameLevel3 = dDt.Rows[0]["name_level3"].ToString();
                                NameLevel4 = dDt.Rows[0]["name_level4"].ToString();
                                NameLevel5 = dDt.Rows[0]["name_level5"].ToString();

                                if (NameLevel5 == NameLevel4)
                                {
                                    NameLevel5 = "";
                                }
                                else
                                {
                                    NameLevel5 = " / " + NameLevel5;
                                }

                                if (NameLevel4 == NameLevel3)
                                {
                                    NameLevel4 = "";
                                }
                                else
                                {
                                    NameLevel4 = " / " + NameLevel4;
                                }
                            }
                            
                            SetReportHeaderStyle(ws, "สำนักงานปลัดสำนักนายกรัฐมนตรี", "A" + cntRow, "A" + cntRow + ":F" + cntRow);
                            cntRow += 1;

                            SetReportHeaderStyle(ws, "รายงานบัญชีการลงเวลามาปฏิบัติราชการประจำวัน (แยกตามหมายเหตุการลงเวลา) " + remarks, "A" + cntRow, "A" + cntRow + ":F" + cntRow);
                            cntRow += 1;

                            SetReportHeaderStyle(ws, "ของ" + staff_name, "A" + cntRow, "A" + cntRow + ":F" + cntRow);
                            cntRow += 1;

                            SetReportHeaderStyle(ws, NameLevel3 + NameLevel4 + NameLevel5, "A" + cntRow, "A" + cntRow + ":F" + cntRow);
                            cntRow += 1;

                            SetReportHeaderStyle(ws, "ตั้งแต่วันที่  " + DateFrom + "  ถึงวันที่  " + DateTo, "A" + cntRow, "A" + cntRow + ":F" + cntRow);
                            cntRow += 1;

                            SetReportHeaderStyle(ws, "พบรายการทั้งหมด" + dDt.Rows.Count.ToString() + "รายการ", "A" + cntRow, "A" + cntRow + ":F" + cntRow);
                            cntRow += 1;

                            SetRowHeaderStyle(ws, "เลขที่", cntRow, 1, "A" + cntRow);
                            SetRowHeaderStyle(ws, "วันที่", cntRow, 2, "B" + cntRow);
                            SetRowHeaderStyle(ws, "เวลามา", cntRow, 3, "C" + cntRow);
                            SetRowHeaderStyle(ws, "เวลากลับ", cntRow, 4, "D" + cntRow);
                            SetRowHeaderStyle(ws, "จำนวนชั่วโมง", cntRow, 5, "E" + cntRow);
                            SetRowHeaderStyle(ws, "หมายเหตุ", cntRow, 6, "F" + cntRow);
                            cntRow += 1;

                            //Set Column Width
                            ws.Column(1).Width = 10;
                            ws.Column(2).Width = 20;
                            ws.Column(3).Width = 20;
                            ws.Column(4).Width = 20;
                            ws.Column(5).Width = 20;
                            ws.Column(6).Width = 30;

                            int seq = 1;
                            foreach (DataRow dr in dDt.Rows)
                            {
                                string downtime_date = dr["downtime_date"].ToString();
                                string time_come = dr["time_come"].ToString();
                                string time_out = dr["time_back"].ToString();
                                string timeformat = dr["timeformat"].ToString();
                                string remark = dr["remarks"].ToString();

                                SetDetailStyle(ws, seq.ToString(), cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                                SetDetailStyle(ws, downtime_date, cntRow, 2, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                                SetDetailStyle(ws, time_come, cntRow, 3, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                                SetDetailStyle(ws, time_out, cntRow, 4, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                                SetDetailStyle(ws, timeformat, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                                SetDetailStyle(ws, remark, cntRow, 6, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);

                                seq += 1;
                                cntRow += 1;
                            }

                            cntRow += 3;
                            //SetNormalStyle(ws, "วันที่พิมพ์ " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " น.", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                            //SetNormalStyle(ws, "ผู้พิมพ์ " + UserName, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                            SetNormalStyle(ws, "วันที่พิมพ์ " + printDate + " น.", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                            SetNormalStyle(ws, "ผู้พิมพ์ " + UserName , cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);

                            cntRow += 3;


                        }//if dDt

                        dt.DefaultView.RowFilter = "";

                    }//for rDt
                    ExportExcelToClient(res, ep, "RPT_HRM_UT0605_PERSON");
                }
            } 
        }
        catch (Exception ex) { 
        
        }
    }
    #endregion

    #region "Export Report RPT_HRM_UT0607"
    public static void ExportReportRPT_HRM_UT0607(string strMonth, string UserName, string UserOrg, DataTable dt, HttpResponse res)
    {
        using (ExcelPackage ep = new ExcelPackage())
        {
            ExcelWorksheet ws = ep.Workbook.Worksheets.Add("RPT_HRM_UT0607");

            string name_level3 = "";
            if (dt.Rows.Count > 0) { name_level3 = dt.Rows[0]["name_level3"].ToString(); }
           
            int cntRow = 2;
            SetReportHeaderStyle(ws, "รายงานสรุปการลาของส่วนราชการ", "A" + cntRow, "A" + cntRow + ":P" + cntRow);
            cntRow += 1;

            SetReportHeaderStyle(ws,name_level3 + " สำนักงานปลัดสำนักนายกรัฐมนตรี", "A" + cntRow, "A" + cntRow + ":P" + cntRow);
            cntRow += 1;

            SetReportHeaderStyle(ws, "ประจำเดือน " + strMonth, "A" + cntRow, "A" + cntRow + ":P" + cntRow);
            cntRow += 1;

            SetReportHeaderStyle(ws, "พบรายการทั้งหมด" + dt.Rows.Count.ToString() + "รายการ", "A" + cntRow, "A" + cntRow + ":P" + cntRow);
            cntRow += 1;

            SetRowHeaderStyle(ws, "ลำดับ", cntRow, 1, "A" + cntRow + ":A" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "ชื่อ-สกุล", cntRow, 2, "B" + cntRow + ":B" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "เลขที่ตำแหน่ง", cntRow, 3, "C" + cntRow + ":C" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "ลาป่วย", cntRow, 4, "D" + cntRow + ":E" + cntRow);
            SetRowHeaderStyle(ws, "วันที่", (cntRow + 1), 4, "D" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "รวม", (cntRow + 1), 5, "E" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "ลากิจ", cntRow, 6, "F" + cntRow + ":G" + cntRow);
            SetRowHeaderStyle(ws, "วันที่", (cntRow + 1), 6, "F" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "รวม", (cntRow + 1), 7, "G" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "มาสาย", cntRow, 8, "H" + cntRow + ":I" + cntRow);
            SetRowHeaderStyle(ws, "วันที่", (cntRow + 1), 8, "H" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "รวม", (cntRow + 1), 9, "I" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "กลับก่อน", cntRow, 10, "J" + cntRow + ":K" + cntRow);
            SetRowHeaderStyle(ws, "วันที่", (cntRow + 1), 10, "J" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "รวม", (cntRow + 1), 11, "K" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "ลาพักผ่อน", cntRow, 12, "L" + cntRow + ":M" + cntRow);
            SetRowHeaderStyle(ws, "วันที่", (cntRow + 1), 12, "L" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "รวม", (cntRow + 1), 13, "M" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "ลาอื่นๆ", cntRow, 14, "N" + cntRow + ":O" + cntRow);
            SetRowHeaderStyle(ws, "วันที่", (cntRow + 1), 14, "N" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "รวม", (cntRow + 1), 15, "O" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "หมายเหตุ", cntRow, 16, "P" + cntRow + ":P" + (cntRow + 1).ToString());
            cntRow += 2;

            //Set Column Width
            ws.Column(1).Width = 10;
            ws.Column(2).Width = 20;
            ws.Column(3).Width = 10;
            ws.Column(4).Width = 10;
            ws.Column(5).Width = 10;
            ws.Column(6).Width = 10;
            ws.Column(7).Width = 10;
            ws.Column(8).Width = 10;
            ws.Column(9).Width = 10;
            ws.Column(10).Width = 10;
            ws.Column(11).Width = 10;
            ws.Column(12).Width = 10;
            ws.Column(13).Width = 10;
            ws.Column(14).Width = 10;
            ws.Column(15).Width = 10;
            ws.Column(16).Width = 20;


            DataTable l4Dt = new DataTable();
            l4Dt = dt.DefaultView.ToTable(true, "name_level4").Copy();
            if (l4Dt.Rows.Count > 0)
            {
                for (int i = 0; i <= l4Dt.Rows.Count - 1; i++)
                {
                    string name_level4 = l4Dt.Rows[i]["name_level4"].ToString();
                    SetRowGroupStyle(ws, name_level4, cntRow, 1, "A" + cntRow.ToString() + ":P" + cntRow.ToString(), OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    ws.Cells["A" + cntRow.ToString()].Style.Font.UnderLine = true;
                    cntRow += 1;
                    
                    DataTable dDt = new DataTable();
                    dt.DefaultView.RowFilter = "name_level4 ='" + name_level4 + "'";
                    dDt = dt.DefaultView.ToTable().Copy();

                    int seq = 1;
                    foreach (DataRow dr in dDt.Rows)
                    {
                        string staff_name = dr["staff_name"].ToString();
                        string pos_id = dr["pos_id"].ToString();
                        string sick_date = dr["sick_date"].ToString();
                        string private_date = dr["private_date"].ToString();
                        string late_date =dr["late_date"].ToString();
                        string back_before_date = dr["back_before_date"].ToString();
                        string vacation_date =dr["vacation_date"].ToString();
                        string other_date =dr["other_date"].ToString();
                        string sick_qty =dr["sick_qty"].ToString();
                        string private_qty = dr["private_qty"].ToString();
                        string late_qty = dr["late_qty"].ToString();
                        string back_before_qty = dr["back_before_qty"].ToString();
                        string vacation_qty =dr["vacation_qty"].ToString();
                        string other_qty = dr["other_qty"].ToString();

                        SetDetailStyle(ws, seq.ToString(), cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                        SetDetailStyle(ws, staff_name, cntRow, 2, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                        SetDetailStyle(ws, pos_id, cntRow, 3, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                        SetDetailStyle(ws, sick_date, cntRow, 4, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                        SetDetailStyle(ws, sick_qty, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                        SetDetailStyle(ws, private_date, cntRow, 6, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                        SetDetailStyle(ws, private_qty, cntRow, 7, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                        SetDetailStyle(ws, late_date, cntRow, 8, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                        SetDetailStyle(ws, late_qty, cntRow, 9, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                        SetDetailStyle(ws, back_before_date, cntRow, 10, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                        SetDetailStyle(ws, back_before_qty, cntRow, 11, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                        SetDetailStyle(ws, vacation_date, cntRow, 12, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                        SetDetailStyle(ws, vacation_qty, cntRow, 13, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                        SetDetailStyle(ws, other_date, cntRow, 14, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                        SetDetailStyle(ws, other_qty, cntRow, 15, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                        SetDetailStyle(ws, "", cntRow, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);

                        seq += 1;
                        cntRow += 1;
                    }

                }//for l4Dt
                dt.DefaultView.RowFilter = "";
            }

            cntRow += 3;
            //SetNormalStyle(ws, "วันที่พิมพ์ " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " น.", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            //SetNormalStyle(ws, "ผู้พิมพ์ " + UserName, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            SetNormalStyle(ws, "วันที่พิมพ์ " + printDate + " น.", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            SetNormalStyle(ws, "ผู้พิมพ์ " + UserName + " " + UserOrg, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);

            ExportExcelToClient(res, ep, "RPT_HRM_UT0607");
           
        }
    }
    #endregion

    #region "Export Report RPT_HRM_UT0608"
    public static void ExportReportRPT_HRM_UT0608(string cmd_budg, string DateFrom, string DateTo, string UserName, string UserOrg, DataTable dt, HttpResponse res)
    {
        using (ExcelPackage ep = new ExcelPackage())
        {
            ExcelWorksheet ws = ep.Workbook.Worksheets.Add("RPT_HRM_UT0607");

            string name_level3 = "";
            if (dt.Rows.Count > 0) { name_level3 = dt.Rows[0]["name_level3"].ToString(); }

            int cntRow = 2;
            SetReportHeaderStyle(ws, name_level3 + " สำนักงานปลัดสำนักนายกรัฐมนตรี", "A" + cntRow, "A" + cntRow + ":H" + cntRow);
            cntRow += 1;

            SetReportHeaderStyle(ws, "สถิติการลาของข้าราชการและลูกจ้างประจำปี ประจำปีงบประมาณ" + cmd_budg, "A" + cntRow, "A" + cntRow + ":H" + cntRow);
            cntRow += 1;

            SetReportHeaderStyle(ws, "ตั้งแต่วันที่ " + DateFrom +" ถึงวันที่ " + DateTo, "A" + cntRow, "A" + cntRow + ":H" + cntRow);
            cntRow += 1;

            SetReportHeaderStyle(ws, name_level3, "A" + cntRow, "A" + cntRow + ":H" + cntRow);
            cntRow += 1;

            SetReportHeaderStyle(ws, "พบรายการทั้งหมด" + dt.Rows.Count.ToString() + "รายการ", "A" + cntRow, "A" + cntRow + ":H" + cntRow);
            cntRow += 1;

            SetRowHeaderStyle(ws, "ลำดับ", cntRow, 1, "A" + cntRow);
            SetRowHeaderStyle(ws, "ชื่อ-สกุล", cntRow, 2, "B" + cntRow);
            SetRowHeaderStyle(ws, "ลากิจ(วัน)", cntRow, 3, "C" + cntRow);
            SetRowHeaderStyle(ws, "ลาป่วย(วัน)", cntRow, 4, "D" + cntRow);
            SetRowHeaderStyle(ws, "รวมลาป่วย ลากิจ(ครั้ง)", cntRow, 5, "E" + cntRow);
            SetRowHeaderStyle(ws, "มาสาย (ครั้ง)", cntRow, 6, "F" + cntRow);
            SetRowHeaderStyle(ws, "กลับก่อน (ครั้ง)", cntRow, 7, "G" + cntRow);
            SetRowHeaderStyle(ws, "หมายเหตุ", cntRow, 8, "H" + cntRow);                      
            cntRow += 1;

            //Set Column Width
            ws.Column(1).Width = 10;
            ws.Column(2).Width = 30;
            ws.Column(3).Width = 20;
            ws.Column(4).Width = 20;
            ws.Column(5).Width = 20;
            ws.Column(6).Width = 20;
            ws.Column(7).Width = 20;
            ws.Column(8).Width = 20;

            DataTable l4Dt = new DataTable();
            l4Dt = dt.DefaultView.ToTable(true, "name_level4").Copy();
            if (l4Dt.Rows.Count > 0)
            {
                for (int i = 0; i <= l4Dt.Rows.Count - 1; i++)
                {
                    string name_level4 = l4Dt.Rows[i]["name_level4"].ToString();
                    SetRowGroupStyle(ws, name_level4, cntRow, 1, "A" + cntRow.ToString() + ":H" + cntRow.ToString(), OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    ws.Cells["A" + cntRow.ToString()].Style.Font.UnderLine = true;
                    cntRow += 1;

                    DataTable dDt = new DataTable();
                    dt.DefaultView.RowFilter = "name_level4 ='" + name_level4 + "'";
                    dDt = dt.DefaultView.ToTable().Copy();

                    int seq = 1;
                    foreach (DataRow dr in dDt.Rows)
                    {
                        string staff_name = dr["staff_name"].ToString();
                        string private_date = dr["private_date"].ToString();
                        string sick_date = dr["sick_date"].ToString();
                        string sum_private_sick = dr["sum_private_sick"].ToString();
                        string late_qty = dr["late_qty"].ToString();
                        string back_before_qty = dr["back_before_qty"].ToString();

                        SetDetailStyle(ws, seq.ToString(), cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                        SetDetailStyle(ws, staff_name, cntRow, 2, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                        SetDetailStyle(ws, private_date, cntRow, 3, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                        SetDetailStyle(ws, sick_date, cntRow, 4, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                        SetDetailStyle(ws, sum_private_sick, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                        SetDetailStyle(ws, late_qty, cntRow, 6, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                        SetDetailStyle(ws, back_before_qty, cntRow, 7, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                        SetDetailStyle(ws, "", cntRow, 8, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                        seq += 1;
                        cntRow += 1;
                    }

                }//for l4Dt
                dt.DefaultView.RowFilter = "";
            }

            cntRow += 3;
            //SetNormalStyle(ws, "วันที่พิมพ์ " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " น.", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            //SetNormalStyle(ws, "ผู้พิมพ์ " + UserName, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            SetNormalStyle(ws, "วันที่พิมพ์ " + printDate + " น.", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            SetNormalStyle(ws, "ผู้พิมพ์ " + UserName + " " + UserOrg, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);

            ExportExcelToClient(res, ep, "RPT_HRM_UT0608");

        }
    }
    #endregion

    #region "Export Report RPT_HRM_UT0609"
    public static void ExportReportRPT_HRM_UT0609(string cmd_budg, string MonthPeriod,string UserName, string UserOrg, DataTable dt, HttpResponse res)
    {
        using (ExcelPackage ep = new ExcelPackage())
        {
            ExcelWorksheet ws = ep.Workbook.Worksheets.Add("RPT_HRM_UT0609");

            string pos_id = "";
            string person_name ="";
            string org_name = "";
            if (dt.Rows.Count > 0) { 
                pos_id = dt.Rows[0]["pos_id"].ToString(); 
                person_name = dt.Rows[0]["person_name"].ToString();
                org_name = dt.Rows[0]["org_name"].ToString();
            }

            int cntRow = 2;
            SetReportHeaderStyle(ws, org_name + " สำนักงานปลัดสำนักนายกรัฐมนตรี", "A" + cntRow, "A" + cntRow + ":P" + cntRow);
            cntRow += 1;

            SetReportHeaderStyle(ws, "บัญชีการลาป่วย ลากิจ มาสาย ลาพักผ่อน กลับก่อน ประจำปีงบประมาณ " + cmd_budg  + " รอบ " +@MonthPeriod, "A" + cntRow, "A" + cntRow + ":P" + cntRow);
            cntRow += 1;

            SetReportHeaderStyle(ws, "ชื่อ" + person_name, "A" + cntRow, "A" + cntRow + ":P" + cntRow);
            cntRow += 1;

            SetReportHeaderStyle(ws, org_name +" เลขที่ตำแหน่ง "+ pos_id, "A" + cntRow, "A" + cntRow + ":P" + cntRow);
            cntRow += 1;

            SetReportHeaderStyle(ws, "พบรายการทั้งหมด" + dt.Rows.Count.ToString() + "รายการ", "A" + cntRow, "A" + cntRow + ":P" + cntRow);
            cntRow += 1;
       
            SetRowHeaderStyle(ws, "ลำดับ", cntRow, 1, "A" + cntRow + ":A" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "วัน เดือน ปี", cntRow, 2, "B" + cntRow + ":B" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "เหตุที่ลา", cntRow, 3, "C" + cntRow + ":C" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "ลาป่วย", cntRow, 4, "D" + cntRow + ":E" + cntRow);
            SetRowHeaderStyle(ws, "วัน", (cntRow + 1), 4, "D" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "รวม", (cntRow + 1), 5, "E" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "ลากิจ", cntRow, 6, "F" + cntRow + ":G" + cntRow);
            SetRowHeaderStyle(ws, "วัน", (cntRow + 1), 6, "F" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "รวม", (cntRow + 1), 7, "G" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "มาสาย", cntRow, 8, "H" + cntRow + ":J" + cntRow);
            SetRowHeaderStyle(ws, "วัน เดือน ปี", (cntRow + 1), 8, "H" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "วัน", (cntRow + 1), 9, "I" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "รวม", (cntRow + 1), 10, "J" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "กลับก่อน", cntRow, 11, "K" + cntRow + ":M" + cntRow);
            SetRowHeaderStyle(ws, "วัน เดือน ปี", (cntRow + 1), 11, "K" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "วัน", (cntRow + 1), 12, "L" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "รวม", (cntRow + 1), 13, "M" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "ลาพักผ่อน", cntRow, 14, "N" + cntRow + ":P" + cntRow);
            SetRowHeaderStyle(ws, "วัน เดือน ปี", (cntRow + 1), 14, "N" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "วัน", (cntRow + 1), 15, "O" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "รวม", (cntRow + 1), 16, "P" + (cntRow + 1).ToString());
            cntRow += 2;

            //Set Column Width
            ws.Column(1).Width = 10;
            ws.Column(2).Width = 20;
            ws.Column(3).Width = 20;
            ws.Column(4).Width = 10;
            ws.Column(5).Width = 10;
            ws.Column(6).Width = 10;
            ws.Column(7).Width = 10;
            ws.Column(8).Width = 20;
            ws.Column(9).Width = 10;
            ws.Column(10).Width = 10;
            ws.Column(11).Width = 20;
            ws.Column(12).Width = 10;
            ws.Column(13).Width = 10;
            ws.Column(14).Width = 20;
            ws.Column(15).Width = 10;
            ws.Column(16).Width = 20;

            int seq = 1;
            foreach (DataRow dr in dt.Rows)
            {
                string created_date = dr["created_date"].ToString();
                string leave_id = dr["leave_id"].ToString().Trim();
                string leave_reason = dr["leave_reason"].ToString();
                string start_date = dr["start_date"].ToString();
                string end_date = dr["end_date"].ToString();
                string leave_day = dr["leave_day"].ToString();
                string leave_sum = dr["leave_sum"].ToString();

                string sick_day = "";
                string sick_sum = "";
                string private_day = "";
                string private_sum = "";
                string late_date = "";
                string late_day = "";
                string late_qty = "";
                string back_before_date = "";
                string back_before_day = "";
                string back_before_qty = "";
                string relax_date = "";
                string relax_day = "";
                string relax_qty = "";

                if (leave_id == "1")
                {
                    sick_day = leave_day;
                    sick_sum = leave_sum;
                }
                if (leave_id == "3")
                {
                    private_day = leave_day;
                    private_sum = leave_sum;
                }
                if (leave_id == "91")
                {
                    late_date = start_date;
                    late_day = leave_day;
                    late_qty = leave_sum;
                }
                if (leave_id == "92")
                {
                    back_before_date = start_date;
                    back_before_day = leave_day;
                    back_before_qty = leave_sum;
                }
                if (leave_id == "4")
                {
                    relax_date = start_date;
                    relax_day = leave_day;
                    relax_qty = leave_sum;
                }

                SetDetailStyle(ws, seq.ToString(), cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetDetailStyle(ws, created_date, cntRow, 2, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                SetDetailStyle(ws, leave_reason, cntRow, 3, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetDetailStyle(ws, sick_day, cntRow, 4, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetDetailStyle(ws, sick_sum, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetDetailStyle(ws, private_day, cntRow, 6, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetDetailStyle(ws, private_sum, cntRow, 7, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetDetailStyle(ws, late_date, cntRow, 8, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetDetailStyle(ws, late_day, cntRow, 9, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetDetailStyle(ws, late_qty, cntRow, 10, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetDetailStyle(ws, back_before_date, cntRow, 11, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetDetailStyle(ws, back_before_day, cntRow, 12, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetDetailStyle(ws, back_before_qty, cntRow, 13, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetDetailStyle(ws, relax_date, cntRow, 14, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetDetailStyle(ws, relax_day, cntRow, 15, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetDetailStyle(ws, relax_qty, cntRow, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);

                seq += 1;
                cntRow += 1;
            }

            cntRow += 3;
            //SetNormalStyle(ws, "วันที่พิมพ์ " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " น.", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            //SetNormalStyle(ws, "ผู้พิมพ์ " + UserName, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            SetNormalStyle(ws, "วันที่พิมพ์ " + printDate + " น.", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            SetNormalStyle(ws, "ผู้พิมพ์ " + UserName + " " + UserOrg, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);

            ExportExcelToClient(res, ep, "RPT_HRM_UT0609");

        }
    }
    #endregion

    #region "Export Report RPT_HRM_UT0610"
    public static void ExportReportRPT_HRM_UT0610(string cmd_budg, string DateFrom, string DateTo, string UserName, string UserOrg, DataTable dt, HttpResponse res)
    {
        using (ExcelPackage ep = new ExcelPackage())
        {
            ExcelWorksheet ws = ep.Workbook.Worksheets.Add("RPT_HRM_UT0610");

            string name_level3 = "";
            if (dt.Rows.Count > 0) { name_level3 = dt.Rows[0]["name_level3"].ToString(); }

            int cntRow = 2;
            SetReportHeaderStyle(ws, name_level3 + " สำนักงานปลัดสำนักนายกรัฐมนตรี", "A" + cntRow, "A" + cntRow + ":G" + cntRow);
            cntRow += 1;

            SetReportHeaderStyle(ws, "สถิติการลากิจ ลาป่วย มาสาย ประจำปีงบประมาณ " + cmd_budg, "A" + cntRow, "A" + cntRow + ":G" + cntRow);
            cntRow += 1;

            SetReportHeaderStyle(ws, "ตั้งแต่วันที่ " + DateFrom + " ถึงวันที่ " + DateTo, "A" + cntRow, "A" + cntRow + ":G" + cntRow);
            cntRow += 1;

            SetReportHeaderStyle(ws, name_level3, "A" + cntRow, "A" + cntRow + ":G" + cntRow);
            cntRow += 1;

            SetReportHeaderStyle(ws, "พบรายการทั้งหมด" + dt.Rows.Count.ToString() + "รายการ", "A" + cntRow, "A" + cntRow + ":G" + cntRow);
            cntRow += 1;

            SetRowHeaderStyle(ws, "ลำดับ", cntRow, 1, "A" + cntRow);
            SetRowHeaderStyle(ws, "ชื่อ-สกุล", cntRow, 2, "B" + cntRow);
            SetRowHeaderStyle(ws, "เลขที่ตำแหน่ง", cntRow, 3, "C" + cntRow);
            SetRowHeaderStyle(ws, "ลาป่วย", cntRow, 4, "D" + cntRow);
            SetRowHeaderStyle(ws, "ลากิจ", cntRow, 5, "E" + cntRow);
            SetRowHeaderStyle(ws, "สาย", cntRow, 6, "F" + cntRow);
            SetRowHeaderStyle(ws, "หมายเหตุ ", cntRow, 7, "G" + cntRow);
            cntRow += 1;

            //Set Column Width
            ws.Column(1).Width = 10;
            ws.Column(2).Width = 30;
            ws.Column(3).Width = 20;
            ws.Column(4).Width = 20;
            ws.Column(5).Width = 20;
            ws.Column(6).Width = 20;
            ws.Column(7).Width = 20;

            DataTable l4Dt = new DataTable();
            l4Dt = dt.DefaultView.ToTable(true, "name_level4").Copy();
            if (l4Dt.Rows.Count > 0)
            {
                for (int i = 0; i <= l4Dt.Rows.Count - 1; i++)
                {
                    string name_level4 = l4Dt.Rows[i]["name_level4"].ToString();
                    SetRowGroupStyle(ws, name_level4, cntRow, 1, "A" + cntRow.ToString() + ":G" + cntRow.ToString(), OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    ws.Cells["A" + cntRow.ToString()].Style.Font.UnderLine = true;
                    cntRow += 1;

                    DataTable dDt = new DataTable();
                    dt.DefaultView.RowFilter = "name_level4 ='" + name_level4 + "'";
                    dDt = dt.DefaultView.ToTable().Copy();

                    int seq = 1;
                    foreach (DataRow dr in dDt.Rows)
                    {
                        string staff_name = dr["staff_name"].ToString();
                        string pos_id = dr["pos_id"].ToString();
                        string sick_date = dr["sick_date"].ToString();
                        string private_date = dr["private_date"].ToString();
                        string late_date = dr["late_date"].ToString();

                        SetDetailStyle(ws, seq.ToString(), cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                        SetDetailStyle(ws, staff_name, cntRow, 2, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                        SetDetailStyle(ws, pos_id, cntRow, 3, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                        SetDetailStyle(ws, sick_date, cntRow, 4, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                        SetDetailStyle(ws, private_date, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                        SetDetailStyle(ws, late_date, cntRow, 6, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                        SetDetailStyle(ws, "", cntRow, 7, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                        seq += 1;
                        cntRow += 1;
                    }

                }//for l4Dt
                dt.DefaultView.RowFilter = "";
            }

            cntRow += 3;
            //SetNormalStyle(ws, "วันที่พิมพ์ " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " น.", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            //SetNormalStyle(ws, "ผู้พิมพ์ " + UserName, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            SetNormalStyle(ws, "วันที่พิมพ์ " + printDate + " น.", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            SetNormalStyle(ws, "ผู้พิมพ์ " + UserName + " " + UserOrg, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);

            ExportExcelToClient(res, ep, "RPT_HRM_UT0610");

        }
    }
    #endregion

    #region "Export Report RPT_HRM_UT0611"
    public static void ExportReportRPT_HRM_UT0611(string cmd_budg, string UserName, string UserOrg, DataTable dt, HttpResponse res)
    {
        using (ExcelPackage ep = new ExcelPackage())
        {
            ExcelWorksheet ws = ep.Workbook.Worksheets.Add("RPT_HRM_UT0611");

            string per_type_name = "";
            if (dt.Rows.Count > 0) { per_type_name = dt.Rows[0]["per_type_name"].ToString(); }

            int cntRow = 2;
            SetReportHeaderStyle(ws, " รายงานสรุปยอดการลาพักผ่อนของ " + per_type_name, "A" + cntRow, "A" + cntRow + ":G" + cntRow);
            cntRow += 1;

            SetReportHeaderStyle(ws, "ปีงบประมาณ พ.ศ. " + cmd_budg, "A" + cntRow, "A" + cntRow + ":G" + cntRow);
            cntRow += 1;

            SetReportHeaderStyle(ws, "พบรายการทั้งหมด" + dt.Rows.Count.ToString() + "รายการ", "A" + cntRow, "A" + cntRow + ":G" + cntRow);
            cntRow += 1;

            SetRowHeaderStyle(ws, "ลำดับ", cntRow, 1, "A" + cntRow + ":A" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "ชื่อ-สกุล", cntRow, 2, "B" + cntRow + ":B" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "จำนวนวันลาพักผ่อน(วัน)", cntRow, 3, "C" + cntRow + ":G" + cntRow);
            SetRowHeaderStyle(ws, "สะสม", (cntRow + 1), 3, "C" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "ปีนี้", (cntRow + 1), 4, "D" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "รวม", (cntRow + 1), 5, "E" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "ลาไปแล้ว", (cntRow + 1), 6, "F" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "คงเหลือ", (cntRow + 1), 7, "G" + (cntRow + 1).ToString());
            cntRow += 2;

            //Set Column Width
            ws.Column(1).Width = 10;
            ws.Column(2).Width = 30;
            ws.Column(3).Width = 20;
            ws.Column(4).Width = 20;
            ws.Column(5).Width = 20;
            ws.Column(6).Width = 20;
            ws.Column(7).Width = 20;

            DataTable l4Dt = new DataTable();
            l4Dt = dt.DefaultView.ToTable(true, "name_level4").Copy();
            if (l4Dt.Rows.Count > 0)
            {
                for (int i = 0; i <= l4Dt.Rows.Count - 1; i++)
                {
                    string name_level4 = l4Dt.Rows[i]["name_level4"].ToString();
                    SetRowGroupStyle(ws, name_level4, cntRow, 1, "A" + cntRow.ToString() + ":G" + cntRow.ToString(), OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    ws.Cells["A" + cntRow.ToString()].Style.Font.UnderLine = true;
                    cntRow += 1;

                    DataTable dDt = new DataTable();
                    dt.DefaultView.RowFilter = "name_level4 ='" + name_level4 + "'";
                    dDt = dt.DefaultView.ToTable().Copy();

                    int seq = 1;
                    foreach (DataRow dr in dDt.Rows)
                    {
                        string staff_name = dr["staff_name"].ToString();
                        string sum_rest = dr["sum_rest"].ToString();
                        string this_rest = dr["this_rest"].ToString();
                        string tot_rest = dr["tot_rest"].ToString();
                        string leave_rest = dr["leave_rest"].ToString();
                        string remain_rest = dr["remain_rest"].ToString();

                        SetDetailStyle(ws, seq.ToString(), cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                        SetDetailStyle(ws, staff_name, cntRow, 2, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                        SetDetailStyle(ws, sum_rest, cntRow, 3, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                        SetDetailStyle(ws, this_rest, cntRow, 4, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                        SetDetailStyle(ws, tot_rest, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                        SetDetailStyle(ws, leave_rest, cntRow, 6, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                        SetDetailStyle(ws, remain_rest, cntRow, 7, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                        seq += 1;
                        cntRow += 1;
                    }

                }//for l4Dt
                dt.DefaultView.RowFilter = "";
            }

            cntRow += 3;
            //SetNormalStyle(ws, "วันที่พิมพ์ " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " น.", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            //SetNormalStyle(ws, "ผู้พิมพ์ " + UserName, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            SetNormalStyle(ws, "วันที่พิมพ์ " + printDate + " น.", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            SetNormalStyle(ws, "ผู้พิมพ์ " + UserName + " " + UserOrg, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);

            ExportExcelToClient(res, ep, "RPT_HRM_UT0611");

        }
    }
    #endregion

    #region "Export Report RPT_HRM_UT0612"
    public static void ExportReportRPT_HRM_UT0612(string cmd_budg, string ORG, string DateFrom, string DateTo, string UserName, string UserOrg, DataTable dt, HttpResponse res, string ReportFormat)
    {
        using (ExcelPackage ep = new ExcelPackage()) {
            ExcelWorksheet ws = ep.Workbook.Worksheets.Add("RPT_HRM_UT0612");

            string per_type_name = "";
            string leave_type_name = "";
            string org_name = "";
            if (dt.Rows.Count > 0) { 
                per_type_name = dt.Rows[0]["per_type_name"].ToString();
                leave_type_name = dt.Rows[0]["leave_type_name"].ToString();

                if (ORG != null)
                {
                    org_name = " หน่วยงาน " + dt.Rows[0]["org_name"].ToString();
                }
            }

            int cntRow = 2;
            SetReportHeaderStyle(ws, "สำนักงานปลัดสำนักนายกรัฐมนตรี", "A" + cntRow, "A" + cntRow + ":E" + cntRow);
            cntRow += 1;

            SetReportHeaderStyle(ws, "รายงานสถิติการลาแยกตามหน่วยงาน", "A" + cntRow, "A" + cntRow + ":E" + cntRow);
            cntRow += 1;

            SetReportHeaderStyle(ws, "ประเภทบุคลากร " + per_type_name + " ประเภท " + leave_type_name + org_name, "A" + cntRow, "A" + cntRow + ":E" + cntRow);
            cntRow += 1;

            if (cmd_budg.Trim() == "")
            {
                SetReportHeaderStyle(ws, "ตั้งแต่วันที่ " + ReportsEngine.WebReportsENG.SetDisplayDateFormat(DateFrom) + " ถึงวันที่ " + ReportsEngine.WebReportsENG.SetDisplayDateFormat(DateTo), "A" + cntRow, "A" + cntRow + ":E" + cntRow);
                cntRow += 1;
            }
            else {
                SetReportHeaderStyle(ws, "ปีงบประมาณ " + cmd_budg, "A" + cntRow, "A" + cntRow + ":E" + cntRow);
                cntRow += 1;
            }
            SetReportHeaderStyle(ws, "พบรายการทั้งหมด " + dt.Rows.Count.ToString() + " รายการ", "A" + cntRow, "A" + cntRow + ":E" + cntRow);
            cntRow += 1;

            DataTable lDt = new DataTable();
            lDt = dt.DefaultView.ToTable(true, "leave_name").Copy();

            DataTable oDt = new DataTable();
            oDt = dt.DefaultView.ToTable(true, "org_name").Copy();

            if (ReportFormat == "TABLE")
            {
                #region "Report TABLE"
                //Set Column Width
                ws.Column(1).Width = 40;
                ws.Column(2).Width = 30;
                ws.Column(3).Width = 20;
                ws.Column(4).Width = 20;
                ws.Column(5).Width = 20;

                //Header Row
                SetRowHeaderStyle(ws, "หน่วยงาน", cntRow, 1, "A" + cntRow);
                SetRowHeaderStyle(ws, "ประเภทการลา", cntRow, 2, "B" + cntRow);
                SetRowHeaderStyle(ws, "จำนวนคนที่ลา", cntRow, 3, "C" + cntRow);
                SetRowHeaderStyle(ws, "จำนวนวัน", cntRow, 4, "D" + cntRow);
                SetRowHeaderStyle(ws, "จำนวนครั้ง", cntRow, 5, "E" + cntRow);
                cntRow += 1;

                if (oDt.Rows.Count > 0)
                {
                    foreach (DataRow oDr in oDt.Rows)
                    {
                        dt.DefaultView.RowFilter = "org_name='" + oDr["org_name"].ToString() + "'";

                        string CellRange = "A" + cntRow.ToString() + ":" + "A" + (cntRow + lDt.Rows.Count - 1).ToString();
                        SetDetailStyle(ws, oDr["org_name"].ToString(), cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                        SetCellBorder(ws, CellRange, true);
                        ws.Cells[CellRange].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Top;

                        if (lDt.Rows.Count > 0)
                        {
                            //Data Row
                            foreach (DataRow lDr in lDt.Rows)
                            {
                                dt.DefaultView.RowFilter = "org_name='" + oDr["org_name"].ToString() + "' and leave_name='" + lDr["leave_name"].ToString() + "'";
                                DataView dv = dt.DefaultView;

                                SetDetailStyle(ws, lDr["leave_name"].ToString(), cntRow, 2, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);

                                string person_qty = "0";
                                string day_qty = "0";
                                string leave_qty = "0";
                                if (dv.Count > 0)
                                {
                                    person_qty = dv[0]["person_qty"].ToString();
                                    day_qty = dv[0]["day_qty"].ToString();
                                    leave_qty = dv[0]["leave_qty"].ToString();
                                }

                                SetDetailStyle(ws, person_qty, cntRow, 3, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                                SetDetailStyle(ws, day_qty, cntRow, 4, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                                SetDetailStyle(ws, leave_qty, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);

                                cntRow += 1;
                            }
                        }
                        lDt.Dispose();

                        dt.DefaultView.RowFilter = "";
                    }
                }
                oDt.Dispose();
                #endregion
            }
            else if (ReportFormat == "BAR_GRAPH") {
                cntRow = ExportGraph_RPT_HRM_UT0612(ep, oDt, lDt, dt, cntRow, ws, eChartType.ColumnClustered);
            }
            else if (ReportFormat == "LINE_GRAPH") {
                cntRow = ExportGraph_RPT_HRM_UT0612(ep, oDt, lDt, dt, cntRow, ws, eChartType.Line);
            }

            //Footer
            cntRow += 3;
            //SetNormalStyle(ws, "วันที่พิมพ์ " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " น.", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            //SetNormalStyle(ws, "ผู้พิมพ์ " + UserName, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            SetNormalStyle(ws, "วันที่พิมพ์ " + printDate + " น.", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            SetNormalStyle(ws, "ผู้พิมพ์ " + UserName + " " + UserOrg, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);

            ExportExcelToClient(res, ep, "RPT_HRM_UT0612");
        }   
    }

    private static int ExportGraph_RPT_HRM_UT0612(ExcelPackage ep, DataTable oDt, DataTable lDt, DataTable dt, int cntRow, ExcelWorksheet ws, eChartType GraphType)
    {
        //Set Column Width
        ws.Column(1).Width = 40;  //ชื่อหน่วยงาน

        int cntCol = 2;  //ลำดับของคอลัมน์

        //สำหรับใช้แสดง Graph
        int DataRowStart = cntRow;
        int DataRowEnd = 0;
        int DataColStart = cntCol;
        int DataColEnd = 0;

        //Header Row
        SetRowHeaderStyle(ws, "หน่วยงาน", cntRow, 1, "A" + cntRow);
        foreach (DataRow lDr in lDt.Rows) {
            ws.Column(cntCol).Width = 30;
            SetRowHeaderStyle(ws, lDr["leave_name"].ToString(), cntRow, cntCol);

            DataColEnd = cntCol;
            cntCol += 1;
        }
        cntRow += 1;
        
        //คอลัมน์แรก เริ่มรันทุก Row
        foreach (DataRow oDr in oDt.Rows) {
            SetDetailStyle(ws, oDr["org_name"].ToString(), cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            cntRow += 1;
        }
        DataRowEnd = cntRow - 1;

        string xSerieAddr = ws.Cells[DataRowStart + 1, 1].Address + ":" + ws.Cells[DataRowEnd, 1].Address;
        
        //สร้าง Graph
        if (GraphType == eChartType.ColumnClustered)
        {
            ExcelBarChart ec = (ExcelBarChart)ws.Drawings.AddChart("", eChartType.ColumnClustered);
            ec.SetPosition(DataRowEnd, 0, 0, 0);  //เริ่มแสดง Graph ที่ Row สุดท้าย
            ec.SetSize(900, 600);
            ec.Legend.Position = eLegendPosition.Bottom;
            ec.DataLabel.ShowValue = true;
            ec.YAxis.Title.Text="จำนวนคน";
            ec.YAxis.Font.Size = 10;

            cntCol = DataColStart;
            foreach (DataRow lDr in lDt.Rows)
            {
                string StartAddr = ws.Cells[DataRowStart+1, cntCol].Address;
                string EndAddr = ws.Cells[DataRowEnd, cntCol].Address;

                //Add Series ของ Graph
                ExcelChartSerie sie = ec.Series.Add(StartAddr + ":" + EndAddr, xSerieAddr);
                sie.Header = lDr["leave_name"].ToString();

                cntCol += 1;
            }
        }
        else if (GraphType == eChartType.Line) {
            ExcelLineChart ec = (ExcelLineChart)ws.Drawings.AddChart("", eChartType.Line);
            ec.SetPosition(DataRowEnd, 0, 0, 0);  //เริ่มแสดง Graph ที่ Row สุดท้าย
            //ec.SetSize(1125, 700);
            ec.SetSize(900, 600);
            ec.Legend.Position = eLegendPosition.Bottom;
            ec.DataLabel.ShowValue = true;
            ec.YAxis.Title.Text = "จำนวนคน";
            ec.YAxis.Font.Size = 10;

            cntCol = DataColStart;
            foreach (DataRow lDr in lDt.Rows)
            {
                string StartAddr = ws.Cells[DataRowStart + 1, cntCol].Address;
                string EndAddr = ws.Cells[DataRowEnd, cntCol].Address;

                //Add Series ของ Graph
                ExcelChartSerie sie = ec.Series.Add(StartAddr + ":" + EndAddr, xSerieAddr);
                sie.Header = lDr["leave_name"].ToString();

                cntCol += 1;
            }
        }
        

        //Data Column
        cntCol = DataColStart;
        foreach (DataRow lDr in lDt.Rows)
        {
            cntRow = DataRowStart + 1; //Row แรก เป็น Header
            foreach (DataRow oDr in oDt.Rows)
            {
                dt.DefaultView.RowFilter = "org_name='" + oDr["org_name"].ToString() + "' and leave_name='" + lDr["leave_name"].ToString() + "'";
                DataView dv = dt.DefaultView;

                int person_qty = 0;
                if (dv.Count > 0)
                {
                    person_qty = Convert.ToInt16(dv[0]["person_qty"]);
                }

                SetNumberValueStyle(ws, person_qty, cntRow, cntCol, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);

                cntRow += 1;
                dt.DefaultView.RowFilter = "";
            }
            cntCol += 1;
        }

        //ซ่อนแถว
        for (int i = 0; i < (oDt.Rows.Count + 1); i++)
        {
            ExcelRow er = ws.Row(i + DataRowStart);
            er.Height = 0.1;
        }

        return cntRow + 35;  //เผื่อพื้นที่สำหรับแสดงกราฟด้วย
    }


    //if (Directory.Exists(TempFolder) == false)
    //        {
    //            Directory.CreateDirectory(TempFolder);
    //        }
    //        //ตรงนี้จะได้ไฟล์เป็น Excel 2007
    //        FileInfo fInfo = new FileInfo(TempFolder + "RPT_HRM_UT0612.xlsx");
    //        ep.SaveAs(fInfo);

    //        Workbook wb = new Workbook();
    //        wb.LoadFromFile(fInfo.FullName);
    //        wb.SaveToFile(TempFolder + "RPT_HRM_UT0612.xls", ExcelVersion.Version97to2003);

    //        //XLSXDocument xlsx2007 = new XLSXDocument(fInfo.FullName);
    //        //xlsx2007.ConvertToDocument(RasterEdge.Imaging.Basic.DocumentType.XLS, TempFolder + "RPT_HRM_UT0612.xls");
            
    //        ////แปลงไฟล์เป็น Excel 2003
    //        //string ex2013File = TempFolder + "RPT_HRM_UT0612.xls";
    //        //Workbook wb = new Workbook(exOutStre);
    //        //wb.Save(ex2013File, SaveFormat.Excel97To2003);
    //        //MemoryStream ex2013Stream = wb.SaveToStream();

    //        ////แปลง MemoryStream เป็น byte[]
    //        //byte[] ex2013Byte = new byte[ex2013Stream.Length];
    //        //ex2013Stream.Position = 0;
    //        //ex2013Stream.Read(ex2013Byte, 0, (int)ex2013Stream.Length);

    //        ////ลบ Sheet ที่สองก่อน
    //        //XLSDocument ex2013 = new XLSDocument(ex2013File);
    //        //ex2013.DeletePage(1);

    //        //แปลงไฟล์เป็น PDF
    //        ExcelToPdf x = new ExcelToPdf();
    //        x.OutputFormat = ExcelToPdf.eOutputFormat.Pdf;
    //        x.PageStyle.PageSize.A4();
    //        x.PageStyle.PageMarginBottom.mm(10.0f);
    //        x.PageStyle.PageMarginTop.mm(10.0f);
    //        x.PageStyle.PageMarginLeft.mm(10.0f);
    //        x.PageStyle.PageMarginRight.mm(10.0f);

    //        try
    //        {
    //            byte[] pdfBytes = null;
    //            pdfBytes = x.ConvertFiletoBytes(fInfo.FullName);

    //            if (pdfBytes != null)
    //            {
    //                res.Buffer = true;
    //                res.Clear();
    //                res.ContentType = "application/PDF";
    //                res.AppendHeader("content-disposition", "attachment; filename=RPT_HRM_UT0612.pdf");
    //                res.BinaryWrite(pdfBytes);
    //                res.Flush();
    //                res.End();
    //            }
    //        }
    //        catch (Exception ex)
    //        {

    //        }


    #endregion

    #region "Export Report RPT_HRM_UT0613"
    public static void ExportReportRPT_HRM_UT0613(string cmd_budg, string ORG, string DateFrom, string DateTo, string UserName, string UserOrg, DataTable dt, HttpResponse res, string ReportFormat)
    {
        using (ExcelPackage ep = new ExcelPackage())
        {
            ExcelWorksheet ws = ep.Workbook.Worksheets.Add("RPT_HRM_UT0613");

            string per_type_name = "";
            string leave_type_name = "";
            string org_name = "";
            if (dt.Rows.Count > 0)
            {
                per_type_name = dt.Rows[0]["per_type_name"].ToString();
                leave_type_name = dt.Rows[0]["leave_type_name"].ToString();
                if (ORG != null)
                {
                    org_name = " หน่วยงาน " + dt.Rows[0]["org_name"].ToString();
                }
            }

            int cntRow = 2;
            SetReportHeaderStyle(ws, "สำนักงานปลัดสำนักนายกรัฐมนตรี", "A" + cntRow, "A" + cntRow + ":E" + cntRow);
            cntRow += 1;

            SetReportHeaderStyle(ws, "รายงานสถิติการลาแยกตามเดือน/ปี", "A" + cntRow, "A" + cntRow + ":E" + cntRow);
            cntRow += 1;

            SetReportHeaderStyle(ws, "ประเภทบุคลากร " + per_type_name + " ประเภท " + leave_type_name + org_name, "A" + cntRow, "A" + cntRow + ":E" + cntRow);
            cntRow += 1;

            if (cmd_budg.Trim() == "")
            {
                SetReportHeaderStyle(ws, "ตั้งแต่วันที่ " + DateFrom + " ถึงวันที่ " + DateTo, "A" + cntRow, "A" + cntRow + ":E" + cntRow);
                cntRow += 1;
            }
            else
            {
                SetReportHeaderStyle(ws, "ปีงบประมาณ " + cmd_budg, "A" + cntRow, "A" + cntRow + ":E" + cntRow);
                cntRow += 1;
            }
            SetReportHeaderStyle(ws, "พบรายการทั้งหมด" + dt.Rows.Count.ToString() + "รายการ", "A" + cntRow, "A" + cntRow + ":E" + cntRow);
            cntRow += 1;

            DataTable lDt = new DataTable();
            lDt = dt.DefaultView.ToTable(true, "leave_name").Copy();

            DataTable oDt = new DataTable();
            oDt = dt.DefaultView.ToTable(true, "str_start_date").Copy();

            if (ReportFormat == "TABLE")
            {
                #region "Report TABLE"
                //Set Column Width
                ws.Column(1).Width = 40;
                ws.Column(2).Width = 30;
                ws.Column(3).Width = 20;
                ws.Column(4).Width = 20;
                ws.Column(5).Width = 20;

                //Header Row
                SetRowHeaderStyle(ws, "เดือน", cntRow, 1, "A" + cntRow);
                SetRowHeaderStyle(ws, "ประเภทการลา", cntRow, 2, "B" + cntRow);
                SetRowHeaderStyle(ws, "จำนวนคนที่ลา", cntRow, 3, "C" + cntRow);
                SetRowHeaderStyle(ws, "จำนวนวัน", cntRow, 4, "D" + cntRow);
                SetRowHeaderStyle(ws, "จำนวนครั้ง", cntRow, 5, "E" + cntRow);
                cntRow += 1;

                if (oDt.Rows.Count > 0)
                {
                    foreach (DataRow oDr in oDt.Rows)
                    {
                        dt.DefaultView.RowFilter = "str_start_date='" + oDr["str_start_date"].ToString() + "'";

                        string CellRange = "A" + cntRow.ToString() + ":" + "A" + (cntRow + lDt.Rows.Count - 1).ToString();
                        SetDetailStyle(ws, oDr["str_start_date"].ToString(), cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                        SetCellBorder(ws, CellRange, true);
                        ws.Cells[CellRange].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Top;

                        if (lDt.Rows.Count > 0)
                        {
                            //Data Row
                            foreach (DataRow lDr in lDt.Rows)
                            {
                                dt.DefaultView.RowFilter = "str_start_date='" + oDr["str_start_date"].ToString() + "' and leave_name='" + lDr["leave_name"].ToString() + "'";
                                DataView dv = dt.DefaultView;

                                SetDetailStyle(ws, lDr["leave_name"].ToString(), cntRow, 2, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);

                                string person_qty = "0";
                                string day_qty = "0";
                                string leave_qty = "0";
                                if (dv.Count > 0)
                                {
                                    person_qty = dv[0]["person_qty"].ToString();
                                    day_qty = dv[0]["day_qty"].ToString();
                                    leave_qty = dv[0]["leave_qty"].ToString();
                                }

                                SetDetailStyle(ws, person_qty, cntRow, 3, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                                SetDetailStyle(ws, day_qty, cntRow, 4, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                                SetDetailStyle(ws, leave_qty, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);

                                cntRow += 1;
                            }
                        }
                        lDt.Dispose();

                        dt.DefaultView.RowFilter = "";
                    }
                }
                oDt.Dispose();
                #endregion
            }
            else if (ReportFormat == "BAR_GRAPH")
            {
                cntRow = ExportGraph_RPT_HRM_UT0613(ep, oDt, lDt, dt, cntRow, ws, eChartType.ColumnClustered);
            }
            else if (ReportFormat == "LINE_GRAPH")
            {
                cntRow = ExportGraph_RPT_HRM_UT0613(ep, oDt, lDt, dt, cntRow, ws, eChartType.Line);
            }

            //Footer
            cntRow += 3;
            //SetNormalStyle(ws, "วันที่พิมพ์ " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " น.", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            //SetNormalStyle(ws, "ผู้พิมพ์ " + UserName, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            SetNormalStyle(ws, "วันที่พิมพ์ " + printDate + " น.", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            SetNormalStyle(ws, "ผู้พิมพ์ " + UserName + " " + UserOrg, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);

            ExportExcelToClient(res, ep, "RPT_HRM_UT0613");
        }
    }

    private static int ExportGraph_RPT_HRM_UT0613(ExcelPackage ep, DataTable oDt, DataTable lDt, DataTable dt, int cntRow, ExcelWorksheet ws, eChartType GraphType)
    {
        //Set Column Width
        ws.Column(1).Width = 40;  //ชื่อหน่วยงาน

        int cntCol = 2;  //ลำดับของคอลัมน์

        //สำหรับใช้แสดง Graph
        int DataRowStart = cntRow;
        int DataRowEnd = 0;
        int DataColStart = cntCol;
        int DataColEnd = 0;

        //Header Row
        SetRowHeaderStyle(ws, "เดือน", cntRow, 1, "A" + cntRow);
        foreach (DataRow lDr in lDt.Rows)
        {
            ws.Column(cntCol).Width = 30;
            SetRowHeaderStyle(ws, lDr["leave_name"].ToString(), cntRow, cntCol);

            DataColEnd = cntCol;
            cntCol += 1;
        }
        cntRow += 1;

        //คอลัมน์แรก เริ่มรันทุก Row
        foreach (DataRow oDr in oDt.Rows)
        {
            SetDetailStyle(ws, oDr["str_start_date"].ToString(), cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            cntRow += 1;
        }
        DataRowEnd = cntRow - 1;

        string xSerieAddr = ws.Cells[DataRowStart + 1, 1].Address + ":" + ws.Cells[DataRowEnd, 1].Address;

        //สร้าง Graph
        if (GraphType == eChartType.ColumnClustered)
        {
            ExcelBarChart ec = (ExcelBarChart)ws.Drawings.AddChart("", eChartType.ColumnClustered);
            ec.SetPosition(DataRowEnd, 0, 0, 0);  //เริ่มแสดง Graph ที่ Row สุดท้าย
            ec.SetSize(900, 600);
            ec.Legend.Position = eLegendPosition.Bottom;
            ec.DataLabel.ShowValue = true;
            ec.YAxis.Title.Text = "จำนวนคน";
            ec.YAxis.Font.Size = 10;

            cntCol = DataColStart;
            foreach (DataRow lDr in lDt.Rows)
            {
                string StartAddr = ws.Cells[DataRowStart + 1, cntCol].Address;
                string EndAddr = ws.Cells[DataRowEnd, cntCol].Address;

                //Add Series ของ Graph
                ExcelChartSerie sie = ec.Series.Add(StartAddr + ":" + EndAddr, xSerieAddr);
                sie.Header = lDr["leave_name"].ToString();

                cntCol += 1;
            }
        }
        else if (GraphType == eChartType.Line)
        {
            ExcelLineChart ec = (ExcelLineChart)ws.Drawings.AddChart("", eChartType.Line);
            ec.SetPosition(DataRowEnd, 0, 0, 0);  //เริ่มแสดง Graph ที่ Row สุดท้าย
            //ec.SetSize(1125, 700);
            ec.SetSize(900, 600);
            ec.Legend.Position = eLegendPosition.Bottom;
            ec.DataLabel.ShowValue = true;
            ec.YAxis.Title.Text = "จำนวนคน";
            ec.YAxis.Font.Size = 10;

            cntCol = DataColStart;
            foreach (DataRow lDr in lDt.Rows)
            {
                string StartAddr = ws.Cells[DataRowStart + 1, cntCol].Address;
                string EndAddr = ws.Cells[DataRowEnd, cntCol].Address;

                //Add Series ของ Graph
                ExcelChartSerie sie = ec.Series.Add(StartAddr + ":" + EndAddr, xSerieAddr);
                sie.Header = lDr["leave_name"].ToString();

                cntCol += 1;
            }
        }


        //Data Column
        cntCol = DataColStart;
        foreach (DataRow lDr in lDt.Rows)
        {
            cntRow = DataRowStart + 1; //Row แรก เป็น Header
            foreach (DataRow oDr in oDt.Rows)
            {
                dt.DefaultView.RowFilter = "str_start_date='" + oDr["str_start_date"].ToString() + "' and leave_name='" + lDr["leave_name"].ToString() + "'";
                DataView dv = dt.DefaultView;

                int person_qty = 0;
                if (dv.Count > 0)
                {
                    person_qty = Convert.ToInt16(dv[0]["person_qty"]);
                }

                SetNumberValueStyle(ws, person_qty, cntRow, cntCol, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);

                cntRow += 1;
                dt.DefaultView.RowFilter = "";
            }
            cntCol += 1;
        }

        //ซ่อนแถว
        for (int i = 0; i < (oDt.Rows.Count + 1); i++)
        {
            ExcelRow er = ws.Row(i + DataRowStart);
            er.Height = 0.1;
        }

        return cntRow + 35;  //เผื่อพื้นที่สำหรับแสดงกราฟด้วย
    }
   
    #endregion

    #region "Export Report RPT_HRM_UT0614"
    public static void ExportReportRPT_HRM_UT0614(string per_type_name, string ORG, string differentiate_name,string thDate, string UserName, string UserOrg, DataTable dt, HttpResponse res, string ReportFormat)
    {
        using (ExcelPackage ep = new ExcelPackage())
        {
            ExcelWorksheet ws = ep.Workbook.Worksheets.Add("RPT_HRM_UT0614");

            string org_name = "";
            if (dt.Rows.Count > 0)
            {
                if (ORG != null)
                {
                    org_name = " หน่วยงาน " + dt.Rows[0]["org_name"].ToString();
                }
            }

            int cntRow = 2;
            SetReportHeaderStyle(ws, "สำนักงานปลัดสำนักนายกรัฐมนตรี", "A" + cntRow, "A" + cntRow + ":C" + cntRow);
            cntRow += 1;

            SetReportHeaderStyle(ws, "รายงานแสดงจำนวนบุคคลากรใน สปน. จำแนกตาม" + differentiate_name, "A" + cntRow, "A" + cntRow + ":C" + cntRow);
            cntRow += 1;

            SetReportHeaderStyle(ws, "ประเภท " + per_type_name + " " + org_name, "A" + cntRow, "A" + cntRow + ":C" + cntRow);
            cntRow += 1;

            SetReportHeaderStyle(ws, "ณ วันที่ " + thDate , "A" + cntRow, "A" + cntRow + ":C" + cntRow);
            cntRow += 1;

            SetReportHeaderStyle(ws, "พบรายการทั้งหมด " + dt.Rows.Count.ToString() + " รายการ", "A" + cntRow, "A" + cntRow + ":C" + cntRow);
            cntRow += 1;

            DataTable lDt = new DataTable();
            lDt = dt.DefaultView.ToTable(true, "differentiate").Copy();

            DataTable oDt = new DataTable();
            oDt = dt.DefaultView.ToTable(true, "org_name").Copy();

            if (ReportFormat == "TABLE")
            {
                #region "Report TABLE"
                //Set Column Width
                ws.Column(1).Width = 60;
                ws.Column(2).Width = 30;
                ws.Column(3).Width = 20;

                //Header Row
                SetRowHeaderStyle(ws, "หน่วยงาน", cntRow, 1, "A" + cntRow);
                SetRowHeaderStyle(ws, "จำแนก", cntRow, 2, "B" + cntRow);
                SetRowHeaderStyle(ws, "จำนวน(คน)", cntRow, 3, "C" + cntRow);
                cntRow += 1;

                if (oDt.Rows.Count > 0)
                {
                    foreach (DataRow oDr in oDt.Rows)
                    {
                        dt.DefaultView.RowFilter = "org_name='" + oDr["org_name"].ToString() + "'";

                        string CellRange = "A" + cntRow.ToString() + ":" + "A" + (cntRow + lDt.Rows.Count - 1).ToString();
                        SetDetailStyle(ws, oDr["org_name"].ToString(), cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                        SetCellBorder(ws, CellRange, true);
                        ws.Cells[CellRange].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Top;

                        if (lDt.Rows.Count > 0)
                        {
                            //Data Row
                            foreach (DataRow lDr in lDt.Rows)
                            {
                                dt.DefaultView.RowFilter = "org_name='" + oDr["org_name"].ToString() + "' and differentiate='" + lDr["differentiate"].ToString() + "'";
                                DataView dv = dt.DefaultView;

                                SetDetailStyle(ws, lDr["differentiate"].ToString(), cntRow, 2, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);

                                string person_qty = "0";
                                if (dv.Count > 0)
                                {
                                    person_qty = dv[0]["person_qty"].ToString();
                                }

                                SetDetailStyle(ws, person_qty, cntRow, 3, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
         
                                cntRow += 1;
                            }
                        }
                        lDt.Dispose();

                        dt.DefaultView.RowFilter = "";
                    }
                }
                oDt.Dispose();
                #endregion
            }
            else if (ReportFormat == "BAR_GRAPH")
            {
                cntRow = ExportGraph_RPT_HRM_UT0614(ep, oDt, lDt, dt, cntRow, ws, eChartType.ColumnClustered);
            }
            else if (ReportFormat == "LINE_GRAPH")
            {
                cntRow = ExportGraph_RPT_HRM_UT0614(ep, oDt, lDt, dt, cntRow, ws, eChartType.Line);
            }

            //Footer
            cntRow += 3;
            //SetNormalStyle(ws, "วันที่พิมพ์ " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " น.", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            //SetNormalStyle(ws, "ผู้พิมพ์ " + UserName, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            SetNormalStyle(ws, "วันที่พิมพ์ " + printDate + " น.", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            SetNormalStyle(ws, "ผู้พิมพ์ " + UserName + " " + UserOrg, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);

            ExportExcelToClient(res, ep, "RPT_HRM_UT0614");
        }
    }

    private static int ExportGraph_RPT_HRM_UT0614(ExcelPackage ep, DataTable oDt, DataTable lDt, DataTable dt, int cntRow, ExcelWorksheet ws, eChartType GraphType)
    {
        //Set Column Width
        ws.Column(1).Width = 40;  //ชื่อหน่วยงาน

        int cntCol = 2;  //ลำดับของคอลัมน์

        //สำหรับใช้แสดง Graph
        int DataRowStart = cntRow;
        int DataRowEnd = 0;
        int DataColStart = cntCol;
        int DataColEnd = 0;

        //Header Row
        SetRowHeaderStyle(ws, "หน่วยงาน", cntRow, 1, "A" + cntRow);
        foreach (DataRow lDr in lDt.Rows)
        {
            ws.Column(cntCol).Width = 30;
            SetRowHeaderStyle(ws, lDr["differentiate"].ToString(), cntRow, cntCol);

            DataColEnd = cntCol;
            cntCol += 1;
        }
        cntRow += 1;

        //คอลัมน์แรก เริ่มรันทุก Row
        foreach (DataRow oDr in oDt.Rows)
        {
            SetDetailStyle(ws, oDr["org_name"].ToString(), cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            cntRow += 1;
        }
        DataRowEnd = cntRow - 1;

        string xSerieAddr = ws.Cells[DataRowStart + 1, 1].Address + ":" + ws.Cells[DataRowEnd, 1].Address;

        //สร้าง Graph
        if (GraphType == eChartType.ColumnClustered)
        {
            ExcelBarChart ec = (ExcelBarChart)ws.Drawings.AddChart("", eChartType.ColumnClustered);
            ec.SetPosition(DataRowEnd, 0, 0, 0);  //เริ่มแสดง Graph ที่ Row สุดท้าย
            ec.SetSize(900, 600);
            ec.Legend.Position = eLegendPosition.Bottom;
            ec.DataLabel.ShowValue = true;
            ec.YAxis.Title.Text = "จำนวนคน";
            ec.YAxis.Font.Size = 10;

            cntCol = DataColStart;
            foreach (DataRow lDr in lDt.Rows)
            {
                string StartAddr = ws.Cells[DataRowStart + 1, cntCol].Address;
                string EndAddr = ws.Cells[DataRowEnd, cntCol].Address;

                //Add Series ของ Graph
                ExcelChartSerie sie = ec.Series.Add(StartAddr + ":" + EndAddr, xSerieAddr);
                sie.Header = lDr["differentiate"].ToString();

                cntCol += 1;
            }
        }
        else if (GraphType == eChartType.Line)
        {
            ExcelLineChart ec = (ExcelLineChart)ws.Drawings.AddChart("", eChartType.Line);
            ec.SetPosition(DataRowEnd, 0, 0, 0);  //เริ่มแสดง Graph ที่ Row สุดท้าย
            //ec.SetSize(1125, 700);
            ec.SetSize(900, 600);
            ec.Legend.Position = eLegendPosition.Bottom;
            ec.DataLabel.ShowValue = true;
            ec.YAxis.Title.Text = "จำนวนคน";
            ec.YAxis.Font.Size = 10;

            cntCol = DataColStart;
            foreach (DataRow lDr in lDt.Rows)
            {
                string StartAddr = ws.Cells[DataRowStart + 1, cntCol].Address;
                string EndAddr = ws.Cells[DataRowEnd, cntCol].Address;

                //Add Series ของ Graph
                ExcelChartSerie sie = ec.Series.Add(StartAddr + ":" + EndAddr, xSerieAddr);
                sie.Header = lDr["differentiate"].ToString();

                cntCol += 1;
            }
        }


        //Data Column
        cntCol = DataColStart;
        foreach (DataRow lDr in lDt.Rows)
        {
            cntRow = DataRowStart + 1; //Row แรก เป็น Header
            foreach (DataRow oDr in oDt.Rows)
            {
                dt.DefaultView.RowFilter = "org_name='" + oDr["org_name"].ToString() + "' and differentiate='" + lDr["differentiate"].ToString() + "'";
                DataView dv = dt.DefaultView;

                int person_qty = 0;
                if (dv.Count > 0)
                {
                    person_qty = Convert.ToInt16(dv[0]["person_qty"]);
                }

                SetNumberValueStyle(ws, person_qty, cntRow, cntCol, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);

                cntRow += 1;
                dt.DefaultView.RowFilter = "";
            }
            cntCol += 1;
        }

        //ซ่อนแถว
        for (int i = 0; i < (oDt.Rows.Count + 1); i++)
        {
            ExcelRow er = ws.Row(i + DataRowStart);
            er.Height = 0.1;
        }

        return cntRow + 35;  //เผื่อพื้นที่สำหรับแสดงกราฟด้วย
    }

    #endregion

    #region "Export Report RPT_HRM_UT0615"
    public static void ExportReportRPT_HRM_UT0615(string BudgetYear, string DateFrom, string DateTo, string UserName, string UserOrg, DataTable dt, HttpResponse res)
    {

        using (ExcelPackage ep = new ExcelPackage())
        {
            string per_type_name = "";
            if (dt.Rows.Count > 0)
            {
                per_type_name = dt.Rows[0]["per_type_name"].ToString();
            }
            ExcelWorksheet ws = ep.Workbook.Worksheets.Add("RPT_HRM_UT0615");
            int cntRow = 2;
            SetReportHeaderStyle(ws, "สำนักงานปลัดสำนักนายกรัฐมนตรี", "A" + cntRow, "A" + cntRow + ":F" + cntRow);
            cntRow += 1;

            SetReportHeaderStyle(ws, "รายงานจำนวนบุคลากรที่บรรจุ-ลาออก ของ" + per_type_name, "A" + cntRow, "A" + cntRow + ":F" + cntRow);
            cntRow += 1;

            SetReportHeaderStyle(ws, "ตั้งแต่วันที่" + DateFrom + " ถึง " + DateTo, "A" + cntRow, "A" + cntRow + ":F" + cntRow);
            cntRow += 1;

            SetReportHeaderStyle(ws, "พบรายการทั้งหมด" + dt.Rows.Count.ToString() + "รายการ", "A" + cntRow, "A" + cntRow + ":F" + cntRow);
            cntRow += 1;


            SetRowHeaderStyle(ws, "ส่วนราชการ", cntRow, 1, "A" + cntRow + ":A" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "จำนวน(คน)", cntRow, 2, "B" + cntRow + ":E" + cntRow);
            SetRowHeaderStyle(ws, "ยอดยกมา", (cntRow + 1), 2, "B" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "บรรจุ", (cntRow + 1), 3, "C" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "ลาออก", (cntRow + 1), 4, "D" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "รวม", (cntRow + 1), 5, "E" + (cntRow + 1).ToString());
            cntRow += 2;

            //Set Column Width
            ws.Column(1).Width = 50;
            ws.Column(2).Width = 20;
            ws.Column(3).Width = 20;
            ws.Column(4).Width = 20;
            ws.Column(5).Width = 20;

            int seq = 1;
            foreach (DataRow dr in dt.Rows)
            {
                string org_name = dr["org_name"].ToString();
                int balance_emp = Convert.ToInt16(dr["balance_emp"]);
                int new_emp = Convert.ToInt16(dr["new_emp"]);
                int resign_emp = Convert.ToInt16(dr["resign_emp"]);
                int tot = Convert.ToInt16(dr["balance_emp"]) + Convert.ToInt16(dr["new_emp"]) + Convert.ToInt16(dr["resign_emp"]);

                SetDetailStyle(ws, org_name, cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                SetDetailStyle(ws, balance_emp.ToString(), cntRow, 2, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetDetailStyle(ws, new_emp.ToString(), cntRow, 3, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetDetailStyle(ws, resign_emp.ToString(), cntRow, 4, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetDetailStyle(ws, tot.ToString(), cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);

                seq += 1;
                cntRow += 1;
            }
            cntRow += 3;


            //SetNormalStyle(ws, "วันที่พิมพ์ " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " น.", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            //SetNormalStyle(ws, "ผู้พิมพ์ " + UserName, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            SetNormalStyle(ws, "วันที่พิมพ์ " + printDate + " น.", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            SetNormalStyle(ws, "ผู้พิมพ์ " + UserName + " " + UserOrg, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);

            ExportExcelToClient(res, ep, "RPT_HRM_UT0615");



        }

    }
    #endregion

    #region "Export Report RPT_HRM_UT0616"
    public static void ExportReportRPT_HRM_UT0616_POSITION(string UserName, string UserOrg, DataTable dt, HttpResponse res)
    {
        using (ExcelPackage ep = new ExcelPackage())
        {
            ExcelWorksheet ws = ep.Workbook.Worksheets.Add("RPT_HRM_UT0616_POSITION");

            string org_name = "";
            if (dt.Rows.Count > 0) { org_name = dt.Rows[0]["org_name"].ToString(); }

            int cntRow = 2;
            SetReportHeaderStyle(ws, " รายงานตำแหน่งจำแนกตามหน่วยงาน " , "A" + cntRow, "A" + cntRow + ":C" + cntRow);
            cntRow += 1;

            SetReportHeaderStyle(ws, "หน่วยงาน" + org_name, "A" + cntRow, "A" + cntRow + ":C" + cntRow);
            cntRow += 1;

            SetReportHeaderStyle(ws, "พบรายการทั้งหมด" + dt.Rows.Count.ToString() + "รายการ", "A" + cntRow, "A" + cntRow + ":C" + cntRow);
            cntRow += 1;

            SetRowHeaderStyle(ws, "ตำแหน่ง", cntRow, 1, "A" + cntRow );
            SetRowHeaderStyle(ws, "ประเภทตำแหน่ง", cntRow, 2, "B" + cntRow );
            SetRowHeaderStyle(ws, "ผู้ดำรงตำแหน่ง", cntRow, 3, "C" + cntRow );
            cntRow += 1;

            //Set Column Width
            ws.Column(1).Width = 40;
            ws.Column(2).Width = 40;
            ws.Column(3).Width = 40;

            DataTable oDt = new DataTable();
            oDt = dt.DefaultView.ToTable(true, "org_name").Copy();
            if (oDt.Rows.Count > 0)
            {
                for (int i = 0; i <= oDt.Rows.Count - 1; i++)
                {
                    string _org_name = oDt.Rows[i]["org_name"].ToString();
                    SetRowGroupStyle(ws, _org_name, cntRow, 1, "A" + cntRow.ToString() + ":C" + cntRow.ToString(), OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    ws.Cells["A" + cntRow.ToString()].Style.Font.UnderLine = true;
                    cntRow += 1;

                    DataTable dDt = new DataTable();
                    dt.DefaultView.RowFilter = "org_name ='" + _org_name + "'";
                    dDt = dt.DefaultView.ToTable().Copy();

                    int seq = 1;
                    foreach (DataRow dr in dDt.Rows)
                    {
                        string pos_name = dr["pos_name"].ToString();
                        string per_type_name = dr["per_type_name"].ToString();
                        string per_fullname = dr["per_fullname"].ToString();

                        SetDetailStyle(ws, pos_name, cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                        SetDetailStyle(ws, per_type_name, cntRow, 2, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                        SetDetailStyle(ws, per_fullname, cntRow, 3, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                       
                        seq += 1;
                        cntRow += 1;
                    }

                }//for l4Dt
                dt.DefaultView.RowFilter = "";
            }

            cntRow += 3;
            //SetNormalStyle(ws, "วันที่พิมพ์ " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " น.", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            //SetNormalStyle(ws, "ผู้พิมพ์ " + UserName, cntRow, 3, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            SetNormalStyle(ws, "วันที่พิมพ์ " + printDate + " น.", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            SetNormalStyle(ws, "ผู้พิมพ์ " + UserName + " " + UserOrg, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);

            ExportExcelToClient(res, ep, "RPT_HRM_UT0616_POSITION");

        }
    }

    public static void ExportReportRPT_HRM_UT0616_MANPOWER11(string ReportHeader,string PerTypeName,string UserName, string UserOrg, DataTable dt, HttpResponse res)
    {
        using (ExcelPackage ep = new ExcelPackage())
        {
            ExcelWorksheet ws = ep.Workbook.Worksheets.Add("RPT_HRM_UT0616_MANPOWER11");

            string org_name = "";
            if (dt.Rows.Count > 0) { org_name = dt.Rows[0]["org_name"].ToString(); }

            int cntRow = 2;
            SetReportHeaderStyle(ws, ReportHeader + " ประเภท " + PerTypeName , "A" + cntRow, "A" + cntRow + ":L" + cntRow);
            cntRow += 1;

            SetReportHeaderStyle(ws, "หน่วยงาน" + org_name, "A" + cntRow, "A" + cntRow + ":L" + cntRow);
            cntRow += 1;

            SetRowHeaderStyle(ws, "หน่วยงาน", cntRow, 1, "A" + cntRow + ":A" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "ทั่วไป", cntRow, 2, "B" + cntRow + ":E" + cntRow);
            SetRowHeaderStyle(ws, "ปฏิบัติงาน", cntRow + 1, 2, "B" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "ชำนาญงาน", cntRow + 1, 3, "C" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "อาวุโส", cntRow + 1, 4, "D" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "ทักษะพิเศษ", cntRow + 1, 5, "E" + (cntRow + 1).ToString());

            SetRowHeaderStyle(ws, "วิชาการ", cntRow, 6, "F" + cntRow+ ":J" + cntRow);
            SetRowHeaderStyle(ws, "ปฏิบัติการ", cntRow + 1, 6, "F" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "ชำนาญการ", cntRow + 1, 7, "G" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "ชำนาญการพิเศษ", cntRow + 1, 8, "H" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "เชี่ยวชาญ", cntRow + 1, 9, "I" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "ทรงคุณวุฒิ", cntRow + 1, 10, "J" + (cntRow + 1).ToString());

            SetRowHeaderStyle(ws, "อำนวยการ", cntRow, 11, "K" + cntRow+ ":L" + cntRow);
            SetRowHeaderStyle(ws, "ต้น", cntRow + 1, 11, "K" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "สูง", cntRow + 1, 12, "L" + (cntRow + 1).ToString());

            SetRowHeaderStyle(ws, "บริหาร", cntRow, 13, "M" + cntRow+ ":N" + cntRow);
            SetRowHeaderStyle(ws, "ต้น", cntRow + 1, 13, "M" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "สูง", cntRow + 1, 14, "N" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "รวม", cntRow, 15, "O" + cntRow + ":O" + (cntRow + 1).ToString());
            cntRow += 2;

            //Set Column Width
            ws.Column(1).Width = 30;
            ws.Column(2).Width = 20;
            ws.Column(3).Width = 20;
            ws.Column(4).Width = 20;
            ws.Column(5).Width = 20;
            ws.Column(6).Width = 20;
            ws.Column(7).Width = 20;
            ws.Column(8).Width = 20;
            ws.Column(9).Width = 20;
            ws.Column(10).Width = 20;
            ws.Column(11).Width = 20;
            ws.Column(12).Width = 20;
            ws.Column(13).Width = 20;
            ws.Column(14).Width = 20;
            ws.Column(15).Width = 20;

            //int seq = 1;
            foreach (DataRow dr in dt.Rows)
            {
                string gen_work = dr["gen_work"].ToString();
                string gen_expert = dr["gen_expert"].ToString();
                string gen_senior = dr["gen_senior"].ToString();
                string academic_work = dr["academic_work"].ToString();
                string academic_expert = dr["academic_expert"].ToString();
                string academic_specialist = dr["academic_specialist"].ToString();
                string academic_professional = dr["academic_professional"].ToString();
                string director_begin = dr["director_begin"].ToString();
                string director_high = dr["director_high"].ToString();
                string manage_begin = dr["manage_begin"].ToString();
                string manage_high = dr["manage_high"].ToString();
                string gen_spacial = dr["gen_spacial"].ToString();
                string academic_theexpert = dr["academic_theexpert"].ToString();
               
                int tot = Convert.ToInt16(dr["gen_work"]) + Convert.ToInt16(dr["gen_expert"]) + 
                    Convert.ToInt16(dr["gen_senior"]) + Convert.ToInt16(dr["academic_work"]) + 
                    Convert.ToInt16(dr["academic_expert"]) + Convert.ToInt16(dr["academic_specialist"]) + 
                    Convert.ToInt16(dr["academic_professional"]) + Convert.ToInt16(dr["director_begin"]) + 
                    Convert.ToInt16(dr["director_high"]) + Convert.ToInt16(dr["manage_begin"]) +
                    Convert.ToInt16(dr["gen_spacial"]) + Convert.ToInt16(dr["academic_theexpert"]) +
                    Convert.ToInt16(dr["manage_high"]);

                SetDetailStyle(ws, dr["org_name"].ToString(), cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                SetDetailStyle(ws, gen_work, cntRow, 2, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetDetailStyle(ws, gen_expert, cntRow, 3, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetDetailStyle(ws, gen_senior, cntRow, 4, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetDetailStyle(ws, gen_spacial, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);

                SetDetailStyle(ws, academic_work, cntRow, 6, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetDetailStyle(ws, academic_expert, cntRow, 7, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetDetailStyle(ws, academic_specialist, cntRow, 8, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetDetailStyle(ws, academic_professional, cntRow, 9, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetDetailStyle(ws, academic_theexpert, cntRow, 10, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);

                SetDetailStyle(ws, director_begin, cntRow, 11, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetDetailStyle(ws, director_high, cntRow, 12, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetDetailStyle(ws, manage_begin, cntRow, 13, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetDetailStyle(ws, manage_high, cntRow, 14, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetDetailStyle(ws, tot.ToString(), cntRow, 15, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);

                //seq += 1;
                cntRow += 1;
            }

            cntRow += 3;
            SetNormalStyle(ws, "วันที่พิมพ์ " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " น.", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            SetNormalStyle(ws, "ผู้พิมพ์ " + UserName, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);

            ExportExcelToClient(res, ep, "RPT_HRM_UT0616_MANPOWER11");

        }
    }

    public static void ExportReportRPT_HRM_UT0616_MANPOWER14(string ReportHeader, string PerTypeName, string UserName, string UserOrg, DataTable dt, HttpResponse res)
    {
        using (ExcelPackage ep = new ExcelPackage())
        {
            ExcelWorksheet ws = ep.Workbook.Worksheets.Add("RPT_HRM_UT0616_MANPOWER14");

            string org_name = "";
            if (dt.Rows.Count > 0) { org_name = dt.Rows[0]["org_name"].ToString(); }

            int cntRow = 2;
            SetReportHeaderStyle(ws, ReportHeader + " ประเภท " + PerTypeName + "ของ สปน.", "A" + cntRow, "A" + cntRow + ":L" + cntRow);
            cntRow += 1;

            SetReportHeaderStyle(ws, "หน่วยงาน" + org_name, "A" + cntRow, "A" + cntRow + ":L" + cntRow);
            cntRow += 1;

            SetRowHeaderStyle(ws, "หน่วยงาน", cntRow, 1, "A" + cntRow + ":A" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "ทั่วไป", cntRow, 2, "B" + cntRow + ":F" + cntRow);
            SetRowHeaderStyle(ws, "บริการ", cntRow + 1, 2, "B" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "เทคนิค", cntRow + 1, 3, "C" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "บริหารทั่วไป", cntRow + 1, 4, "D" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "วิชาชีพเฉพาะ", cntRow + 1, 5, "E" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "เชี่ยวชาญเฉพาะ", cntRow + 1, 6, "F" + (cntRow + 1).ToString());

            SetRowHeaderStyle(ws, "พิเศษ", cntRow, 7, "G" + cntRow);
            SetRowHeaderStyle(ws, "เชี่ยวชาญพิเศษ", cntRow + 1, 7, "G" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "รวม", cntRow, 8, "H" + cntRow + ":H" + (cntRow + 1).ToString());
            cntRow += 2;

            //Set Column Width
            ws.Column(1).Width = 30;
            ws.Column(2).Width = 20;
            ws.Column(3).Width = 20;
            ws.Column(4).Width = 20;
            ws.Column(5).Width = 20;
            ws.Column(6).Width = 20;
            ws.Column(7).Width = 20;
            ws.Column(8).Width = 20;

            int seq = 1;
            foreach (DataRow dr in dt.Rows)
            {
                string group_service = dr["group_service"].ToString();
                string group_technique = dr["group_technique"].ToString();
                string group_manage = dr["group_manage"].ToString();
                string group_professional = dr["group_professional"].ToString();
                string group_skill = dr["group_skill"].ToString();
                string group_spacial= dr["group_spacial"].ToString();

                int tot = Convert.ToInt16(dr["group_service"]) + Convert.ToInt16(dr["group_technique"]) +
                    Convert.ToInt16(dr["group_manage"]) + Convert.ToInt16(dr["group_professional"]) +
                    Convert.ToInt16(dr["group_skill"]) + Convert.ToInt16(dr["group_spacial"]);

                SetDetailStyle(ws, dr["org_name"].ToString(), cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                SetDetailStyle(ws, group_service, cntRow, 2, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetDetailStyle(ws, group_technique, cntRow, 3, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetDetailStyle(ws, group_manage, cntRow, 4, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetDetailStyle(ws, group_professional, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetDetailStyle(ws, group_skill, cntRow, 6, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetDetailStyle(ws, group_spacial, cntRow, 7, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetDetailStyle(ws, tot.ToString(), cntRow, 8, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);

                seq += 1;
                cntRow += 1;
            }

            cntRow += 3;
            SetNormalStyle(ws, "วันที่พิมพ์ " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " น.", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            SetNormalStyle(ws, "ผู้พิมพ์ " + UserName, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);

            ExportExcelToClient(res, ep, "RPT_HRM_UT0616_MANPOWER14");

        }
    }

    #endregion

    #region "Export Report RPT_HRM_UT0617"
    public static void ExportReportRPT_HRM_UT0617(string DateFrom,string DateTo, string UserName, string UserOrg, DataTable dt, HttpResponse res)
    {
        using (ExcelPackage ep = new ExcelPackage())
        {
            ExcelWorksheet ws = ep.Workbook.Worksheets.Add("RPT_HRM_UT0617");

            string staff_name = "";
            string org_name = "";
            if (dt.Rows.Count > 0)
            {
                staff_name = dt.Rows[0]["staff_name"].ToString();
                org_name = dt.Rows[0]["org_name"].ToString();
            }
            int cntRow = 2;
            SetReportHeaderStyle(ws, "สำนักงานปลัดสำนักนายกรัฐมนตรี", "A" + cntRow, "A" + cntRow + ":G" + cntRow);
            cntRow += 1;

            SetReportHeaderStyle(ws, "รายงานตามประเภทการลา  ของ" + staff_name , "A" + cntRow, "A" + cntRow + ":G" + cntRow);
            cntRow += 1;

            SetReportHeaderStyle(ws, "หน่วยงาน " + org_name, "A" + cntRow, "A" + cntRow + ":G" + cntRow);
            cntRow += 1;

            SetReportHeaderStyle(ws, "ตั้งแต่วันที่  " + DateFrom + " ถึง" + DateTo, "A" + cntRow, "A" + cntRow + ":G" + cntRow);
            cntRow += 1;

            SetReportHeaderStyle(ws, "พบรายการทั้งหมด" + dt.Rows.Count.ToString() + "รายการ", "A" + cntRow, "A" + cntRow + ":G" + cntRow);
            cntRow += 1;

            SetRowHeaderStyle(ws, "เลขที่", cntRow, 1, "A" + cntRow);
            SetRowHeaderStyle(ws, "ประเภทการลา", cntRow, 2, "B" + cntRow);
            SetRowHeaderStyle(ws, "ตั้งแต่วันที่", cntRow, 3, "C" + cntRow);
            SetRowHeaderStyle(ws, "ถึงวันที่", cntRow, 4, "D" + cntRow);
            SetRowHeaderStyle(ws, "ครั้งที่", cntRow, 5, "E" + cntRow);
            SetRowHeaderStyle(ws, "ระยะเวลา(วัน)", cntRow, 6, "F" + cntRow);
            SetRowHeaderStyle(ws, "เหตุที่ลา", cntRow, 7, "G" + cntRow);
            cntRow += 1;

            //Set Column Width
            ws.Column(1).Width = 10;
            ws.Column(2).Width = 30;
            ws.Column(3).Width = 20;
            ws.Column(4).Width = 20;
            ws.Column(5).Width = 20;
            ws.Column(6).Width = 20;
            ws.Column(7).Width = 40;
       
            int seq = 1;
            foreach (DataRow dr in dt.Rows)
            {
                string leave_name = dr["leave_name"].ToString();
                string start_date = dr["start_date"].ToString();
                string end_date = dr["end_date"].ToString();
                string leave_seq = dr["leave_seq"].ToString();
                string leave_day = dr["leave_day"].ToString();
                string leave_reason = dr["leave_reason"].ToString();

                SetDetailStyle(ws, seq.ToString(), cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetDetailStyle(ws, leave_name, cntRow, 2, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                SetDetailStyle(ws, start_date, cntRow, 3, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                SetDetailStyle(ws, end_date, cntRow, 4, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                SetDetailStyle(ws, leave_seq, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetDetailStyle(ws, leave_day, cntRow, 6, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetDetailStyle(ws, leave_reason, cntRow, 7, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);

                seq += 1;
                cntRow += 1;
            }

            cntRow += 3;
            //SetNormalStyle(ws, "วันที่พิมพ์ " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " น.", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            //SetNormalStyle(ws, "ผู้พิมพ์ " + UserName, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            SetNormalStyle(ws, "วันที่พิมพ์ " + printDate + " น.", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            SetNormalStyle(ws, "ผู้พิมพ์ " + UserName + " " + UserOrg, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);

            ExportExcelToClient(res, ep, "RPT_HRM_UT0617");

        }
    }
    #endregion

    #endregion

    #region "__SMM"
    public static void ExportReportSMM_RP01(string OrgSerial,string OrgName ,string UserName, string UserOrg, DataTable dt_level3, DataTable dt_level4, DataTable dt_level5, HttpResponse res)
    {
        #region "SMM_RP01"
        using (ExcelPackage ep = new ExcelPackage())
        {
            ExcelWorksheet ws = ep.Workbook.Worksheets.Add("SMM_RP01");
            int cntRow = 2;
            SetReportHeaderStyle(ws, "รายงานรายละเอียดอำนาจหน้าที่ความรับผิดชอบของหน่วยงาน", "A" + cntRow, "A" + cntRow + ":H" + cntRow);
            cntRow += 1;

            SetReportHeaderStyle(ws, "หน่วยงาน " + OrgName, "A" + cntRow, "A" + cntRow + ":H" + cntRow);
            cntRow += 1;

            if (dt_level3.Rows.Count == 0)
            {
                return;
            }

            #region "__Level3"
                cntRow += 1;
                DataTable dt_s3_detail = new DataTable();
                //dt_level3.DefaultView.RowFilter = "org_serial='" + OrgSerial + "'";
                dt_s3_detail = dt_level3;//.DefaultView.ToTable().Copy();

                SetTextStyle(ws, "หน้าที่ความรับผิดชอบ", "A" + cntRow, true, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                cntRow += 1;

                DataTable dt_s3_group = new DataTable();
                dt_s3_group = dt_s3_detail.DefaultView.ToTable(true, "org_name").Copy();
                int seq_sec3 = 1;
                for (int cnt = 0; cnt <= dt_s3_group.Rows.Count - 1; cnt++)
                {
                    string org_name = dt_s3_group.Rows[cnt]["org_name"].ToString();

                    dt_s3_detail.DefaultView.RowFilter = "org_name='" + org_name + "'";
                    int seq_sec3_detail = 1;
                    foreach (DataRow dr in dt_s3_detail.DefaultView.ToTable().Rows)
                    {
                        string job_detail = dr["job_detail"].ToString();
                        SetTextStyle(ws, seq_sec3_detail.ToString() + "." , "A" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                        SetTextStyle(ws, job_detail, "B" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                        cntRow += 1;
                        seq_sec3_detail += 1;
                    }
                    dt_s3_detail.Dispose();
                    dt_level3.DefaultView.RowFilter = "";
                    seq_sec3 += 1;
                }
                dt_s3_group.Dispose();
                #endregion

            #region "__Level4"
                cntRow += 1;
                DataTable dt_s4_detail = new DataTable();
                dt_s4_detail = dt_level4;

                DataTable dt_s4_group = new DataTable();
                dt_s4_group = dt_s4_detail.DefaultView.ToTable(true, "org_name").Copy();
                int seq_sec4 = 1;
                for (int cnt = 0; cnt <= dt_s4_group.Rows.Count - 1; cnt++)
                {
                    cntRow += 1;
                    string org_name = dt_s4_group.Rows[cnt]["org_name"].ToString();
                    SetTextStyle(ws, "    " +  seq_sec4.ToString() + "." + org_name, "A" + cntRow, true, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    cntRow += 1;
                    SetTextStyle(ws, "    " +  "หน้าที่ความรับผิดชอบ", "A" + cntRow, true, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    cntRow += 1;

                    dt_s4_detail.DefaultView.RowFilter = "org_name='" + org_name + "'";
                    int seq_sec4_detail = 1;
                    foreach (DataRow dr in dt_s4_detail.DefaultView.ToTable().Rows)
                    {
                        string job_detail = dr["job_detail"].ToString();
                        SetTextStyle(ws, "    " + seq_sec4_detail.ToString() + ".", "A" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                        SetTextStyle(ws,job_detail, "B" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                        cntRow += 1;
                        seq_sec4_detail += 1;
                    }
                    dt_s4_detail.Dispose();
                    dt_level4.DefaultView.RowFilter = "";
                    seq_sec4 += 1;
                }
                dt_s3_group.Dispose();
                #endregion

            #region "__Level5"
                cntRow += 1;
                DataTable dt_s5_detail = new DataTable();
                dt_s5_detail = dt_level5;

                DataTable dt_s5_group = new DataTable();
                dt_s5_group = dt_s5_detail.DefaultView.ToTable(true, "org_name").Copy();
                int seq_sec5 = 1;
                for (int cnt = 0; cnt <= dt_s5_group.Rows.Count - 1; cnt++)
                {
                    cntRow += 1;
                    string org_name = dt_s5_group.Rows[cnt]["org_name"].ToString();
                    SetTextStyle(ws, "        " + seq_sec5.ToString() + "." + org_name, "A" + cntRow, true, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    cntRow += 1;
                    SetTextStyle(ws, "        " + "หน้าที่ความรับผิดชอบ", "A" + cntRow, true, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    cntRow += 1;

                    dt_s5_detail.DefaultView.RowFilter = "org_name='" + org_name + "'";
                    int seq_sec5_detail = 1;
                    foreach (DataRow dr in dt_s5_detail.DefaultView.ToTable().Rows)
                    {
                        string job_detail = dr["job_detail"].ToString();
                        SetTextStyle(ws, "        " + seq_sec5_detail.ToString() + ".", "A" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                        SetTextStyle(ws, job_detail, "B" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                        cntRow += 1;
                        seq_sec5_detail += 1;
                    }
                    dt_s5_detail.Dispose();
                    dt_level5.DefaultView.RowFilter = "";
                    seq_sec5 += 1;
                }
               
                #endregion
                dt_s3_group.Dispose();

                cntRow += 3;
                SetNormalStyle(ws, "วันที่พิมพ์ " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " น.", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                SetNormalStyle(ws, "ผู้พิมพ์ " + UserName, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                
                ExportExcelToClient(res, ep, "SMM_RP01");
        }
        #endregion
    }

    #region "Export Report SMM_RP04"
    public static void ExportReportSMM_RP04(DataTable dt_section1,DataTable dt_section2,DataTable dt_section3,DataTable dt_section4,DataTable dt_section5, HttpResponse res)
    {
        #region "SMM_RP04"
        using (ExcelPackage ep = new ExcelPackage())
        {
            ExcelWorksheet ws = ep.Workbook.Worksheets.Add("SMM_RP04");
            int cntRow = 2;

            DataTable dt_pos = new DataTable();
            dt_pos = dt_section1.DefaultView.ToTable(true, "pos_id","per_type").Copy();
            for (int i = 0; i <= dt_pos.Rows.Count - 1; i++)
            {
                string pos_id = dt_pos.Rows[i]["pos_id"].ToString();
                string per_type = dt_pos.Rows[i]["per_type"].ToString();

                SetReportHeaderStyle(ws, "ชื่อส่วนราชการ สำนักงานปลัดสำนักนายกรัฐมนตรี", "A" + cntRow, "A" + cntRow + ":H" + cntRow);
                cntRow += 1;

                SetReportHeaderStyle(ws, "แบบบรรยายลักษณะงาน(Job Description)", "A" + cntRow, "A" + cntRow + ":H" + cntRow);
                cntRow += 1;

                SetNormalStyle(ws, "ตำแหน่งเลขที่ " + pos_id , cntRow, 7, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                cntRow += 1;

                string ADMIN_CODE = "";
                string LINE_CODE = "";
                string CUR_LEV = "";

                #region "__Section1"
                cntRow += 1;
                DataTable dt_pos_detail = new DataTable();
                dt_section1.DefaultView.RowFilter = "pos_id='" + pos_id + "' and per_type='" + per_type + "'";
                dt_pos_detail = dt_section1.DefaultView.ToTable().Copy();

                SetTextStyle(ws, "ส่วนที่ 1 ข้อมูลทั่วไป(Job Title)", "A" + cntRow, true, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                cntRow += 1;
                if (dt_pos_detail.Rows.Count > 0)
                {
                    ADMIN_CODE = dt_pos_detail.Rows[0]["ADMIN_CODE"].ToString();
                    LINE_CODE = dt_pos_detail.Rows[0]["LINE_CODE"].ToString();
                    CUR_LEV = dt_pos_detail.Rows[0]["CUR_LEV"].ToString();

                   string ADMIN_NAME = dt_pos_detail.Rows[0]["ADMIN_NAME"].ToString();
                   string LINE_NAME =dt_pos_detail.Rows[0]["LINE_NAME"].ToString();
                   string LENGTH_NAME =dt_pos_detail.Rows[0]["LENGTH_NAME"].ToString();
                   string ORG_NAME = dt_pos_detail.Rows[0]["ORG_NAME"].ToString();
                   string GROUP_NAME = dt_pos_detail.Rows[0]["GROUP_NAME"].ToString();
                   string PARENT_POSITION_NAME = dt_pos_detail.Rows[0]["PARENT_POSITION_NAME"].ToString();
                   string PARENT_LENGTH_NAME = dt_pos_detail.Rows[0]["PARENT_LENGTH_NAME"].ToString();
                   string under_qty = dt_pos_detail.Rows[0]["under_qty"].ToString();
                   string official_qty = dt_pos_detail.Rows[0]["official_qty"].ToString();                 

                   SetTextStyle(ws, "ชื่อตำแหน่งในการบริหารงาน :", "A" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                   SetTextStyle(ws, ADMIN_NAME, "B" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                   cntRow += 1;

                   SetTextStyle(ws, "ชื่อสายงาน :", "A" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                   SetTextStyle(ws, LINE_NAME, "B" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                   cntRow += 1;

                   SetTextStyle(ws, "ประเภท / ระดับ :", "A" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                   SetTextStyle(ws, LENGTH_NAME, "B" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                   cntRow += 1;

                   SetTextStyle(ws, "ชื่อหน่วยงาน(สำนัก/กอง) :", "A" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                   SetTextStyle(ws, ORG_NAME, "B" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                   cntRow += 1;

                   SetTextStyle(ws, "ชื่อส่วนงาน/กลุ่มงาน/ฝ่าย/งาน :", "A" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                   SetTextStyle(ws, GROUP_NAME, "B" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                   cntRow += 1;

                   SetTextStyle(ws, "ชื่อตำแหน่งผู้บังคับบัญชาโดยตรง :", "A" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                   SetTextStyle(ws, PARENT_POSITION_NAME, "B" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    
                   SetTextStyle(ws, "ประเภท/ระดับ :", "C" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                   SetTextStyle(ws, PARENT_LENGTH_NAME, "D" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                   cntRow += 1;

                   SetTextStyle(ws, "จำนวนผู้บังคับบัญชา  จำนวน", "A" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                   SetTextStyle(ws, under_qty + " คน", "B" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                   SetTextStyle(ws, "ข้าราชการ  ", "C" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                   SetTextStyle(ws, official_qty + " คน", "D" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                   cntRow += 1;
                }
                dt_pos_detail.Dispose();
                dt_section1.DefaultView.RowFilter = "";
                #endregion

                #region "__Section2"
                cntRow += 1;
                DataTable dt_s2_detail = new DataTable();
                dt_section2.DefaultView.RowFilter = "pos_id='" + pos_id + "' and per_type='" + per_type + "'";
                dt_s2_detail = dt_section2.DefaultView.ToTable().Copy();

                SetTextStyle(ws, "ส่วนที่ 2 หน้าที่ความรับผิดชอบโดยสรุป(Job Summary)", "A" + cntRow, true, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                cntRow += 1;

                int seq_sec2 = 1;
                foreach (DataRow dr in dt_s2_detail.Rows)
                { 
                    string job_detail = dr["job_detail"].ToString();
                    SetTextStyle(ws, seq_sec2.ToString(), "A" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    SetTextStyle(ws, job_detail, "B" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    cntRow += 1; 
                    seq_sec2 += 1;
                }
                dt_s2_detail.Dispose();
                dt_section2.DefaultView.RowFilter = "";
                #endregion

                #region "__Section3"
                cntRow += 1;
                DataTable dt_s3_detail = new DataTable();
                dt_section3.DefaultView.RowFilter = "pos_id='" + pos_id + "' and per_type='" + per_type + "'";
                dt_s3_detail = dt_section3.DefaultView.ToTable().Copy();

                SetTextStyle(ws, "ส่วนที่ 3 หน้าที่ความรับผิดชอบที่ทำอยู่ในปัจจุบัน(รวมถึงที่จะต้องทำในอนาคตด้วย)", "A" + cntRow, true, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                cntRow += 1;

                DataTable dt_s3_group = new DataTable();
                dt_s3_group = dt_s3_detail.DefaultView.ToTable(true, "SUB_GROUP_NAME").Copy();
                int seq_sec3 = 1;
                for (int cnt = 0; cnt <= dt_s3_group.Rows.Count - 1; cnt++)
                {
                    string SUB_GROUP_NAME = dt_s3_group.Rows[cnt]["SUB_GROUP_NAME"].ToString();
                    SetTextStyle(ws, seq_sec3.ToString() + ".", "A" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    SetTextStyle(ws, SUB_GROUP_NAME, "B" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    cntRow += 1;

                    dt_s3_detail.DefaultView.RowFilter = "SUB_GROUP_NAME='" + SUB_GROUP_NAME + "'";
                    int seq_sec3_detail = 1;
                    foreach (DataRow dr in dt_s3_detail.DefaultView.ToTable().Rows)
                    {
                        string JD_DETAIL = dr["JD_DETAIL"].ToString();
                        SetTextStyle(ws, seq_sec3.ToString() + "." + seq_sec3_detail.ToString(), "B" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                        SetTextStyle(ws, JD_DETAIL, "C" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                        cntRow += 1;
                        seq_sec3_detail += 1;
                    }
                    dt_s3_detail.Dispose();
                    dt_section3.DefaultView.RowFilter = "";
                    seq_sec3 += 1;
                }
                dt_s3_group.Dispose();
                #endregion

                #region "__Section4"
                cntRow += 1;
                DataTable dt_s4_detail = new DataTable();
                dt_section4.DefaultView.RowFilter = "pos_id='" + pos_id + "' and per_type='" + per_type + "'";
                dt_s4_detail = dt_section4.DefaultView.ToTable().Copy();

                SetTextStyle(ws, "ส่วนที่ 4 คุณสมบัติที่จำเป็นในงาน(Job Description)", "A" + cntRow, true, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                cntRow += 1;

                DataTable dt_s4_group = new DataTable();
                dt_s4_group = dt_s4_detail.DefaultView.ToTable(true, "SUB_GROUP_NAME").Copy();
                int seq_sec4 = 1;
                for (int cnt = 0; cnt <= dt_s4_group.Rows.Count - 1; cnt++)
                {
                    string SUB_GROUP_NAME = dt_s4_group.Rows[cnt]["SUB_GROUP_NAME"].ToString();
                    SetTextStyle(ws, seq_sec4.ToString() + ".", "A" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    SetTextStyle(ws, SUB_GROUP_NAME, "B" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    cntRow += 1;

                    dt_s4_detail.DefaultView.RowFilter = "SUB_GROUP_NAME='" + SUB_GROUP_NAME + "'";
                    int seq_sec4_detail = 1;
                    foreach (DataRow dr in dt_s4_detail.DefaultView.ToTable().Rows)
                    {
                        string JD_DETAIL = dr["JD_DETAIL"].ToString();
                        SetTextStyle(ws, seq_sec4.ToString() + "." + seq_sec4_detail.ToString(), "B" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                        SetTextStyle(ws, JD_DETAIL, "C" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                        cntRow += 1;
                        seq_sec4_detail += 1;
                    }
                    dt_s4_detail.Dispose();
                    dt_section4.DefaultView.RowFilter = "";
                    seq_sec4 += 1;
                }
                dt_s4_group.Dispose();
                #endregion

                #region "__Section5"
                cntRow += 1;
                DataTable dt_s5_detail = new DataTable();
                dt_section5.DefaultView.RowFilter = "ADMIN_CODE='" + ADMIN_CODE + "' and PER_TYPE='" + per_type + "' and LINE_CODE='"+ LINE_CODE +"' and CUR_LEV='"+ CUR_LEV +"'";
                dt_s5_detail = dt_section5.DefaultView.ToTable().Copy();

                SetTextStyle(ws, "ส่วนที่ 5 ความรู้ ทักษะ และสมรรถนะที่จำเป็นในงาน", "A" + cntRow, true, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                cntRow += 1;

                DataTable dt_s5_group = new DataTable();
                dt_s5_group = dt_s5_detail.DefaultView.ToTable(true, "TYPE_NAME").Copy();
                int seq_sec5 = 1;
                for (int cnt = 0; cnt <= dt_s5_group.Rows.Count - 1; cnt++)
                {
                    string TYPE_NAME = dt_s5_group.Rows[cnt]["TYPE_NAME"].ToString();
                    //SetTextStyle(ws, seq_sec5.ToString() + ".", "B" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    SetTextStyle(ws, TYPE_NAME, "A" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    cntRow += 1;

                    dt_s5_detail.DefaultView.RowFilter = "TYPE_NAME='" + TYPE_NAME + "'";
                    int seq_sec5_detail = 1;
                    foreach (DataRow dr in dt_s5_detail.DefaultView.ToTable().Rows)
                    {
                        string ITEM_NAME = dr["ITEM_NAME"].ToString();
                        SetTextStyle(ws, seq_sec5_detail.ToString() + ".", "B" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                        SetTextStyle(ws, ITEM_NAME, "C" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                        cntRow += 1;
                        seq_sec5_detail += 1;
                    }
                    dt_s5_detail.Dispose();
                    dt_section5.DefaultView.RowFilter = "";
                    seq_sec5 += 1;
                }
                dt_s5_group.Dispose();
                #endregion

                #region "__Section6"
                cntRow += 1;
                SetTextStyle(ws, "ส่วนที่ 6 การลงนาม", "A" + cntRow, true, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                cntRow += 1;

                SetTextStyle(ws ,"ชื่อผู้ตรวจสอบ", "A" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                cntRow += 1;

                SetTextStyle(ws, "วันที่ได้จัดทำ", "A" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                cntRow += 1;

                #endregion
                cntRow += 3;

            }//for dt_pos


            dt_pos.Dispose();
            ExportExcelToClient(res, ep, "SMM_RP04");

        }
        #endregion

    }
    #endregion

    #region "Export Report SMM_RP07"
    public static void ExportReportSMM_RP07(string UserName, string UserOrg, DataTable dt, HttpResponse res)
    {
        #region "SMM_RP07"
        using (ExcelPackage ep = new ExcelPackage())
        {
            ExcelWorksheet ws = ep.Workbook.Worksheets.Add("SMM_RP07");
            int cntRow = 2;

            DataTable dt_head = new DataTable();
            dt_head = dt.DefaultView.ToTable(true, "PER_TYPE_NAME", "level_name_full").Copy();
            for (int i = 0; i <= dt_head.Rows.Count - 1; i++)
            {
                string PER_TYPE_NAME = dt_head.Rows[i]["PER_TYPE_NAME"].ToString();
                string level_name_full = dt_head.Rows[i]["level_name_full"].ToString();

                SetReportHeaderStyle(ws, "รายชื่อผู้ที่ขอประเมินบุคคลเข้าสู่ตำแหน่งผู้มีประสบการณ์", "A" + cntRow, "A" + cntRow + ":H" + cntRow);
                cntRow += 1;

                SetReportHeaderStyle(ws, PER_TYPE_NAME + "  ระดับ  " + level_name_full, "A" + cntRow, "A" + cntRow + ":H" + cntRow);
                cntRow += 2;

                DataTable dt_detail = new DataTable();
                dt.DefaultView.RowFilter = "PER_TYPE_NAME='" + PER_TYPE_NAME + "' and level_name_full='" + level_name_full + "'";
                dt_detail = dt.DefaultView.ToTable().Copy();

                SetRowHeaderStyle(ws, "เลขที่คำขอ", cntRow, 1, "A" + cntRow);
                SetRowHeaderStyle(ws, "ผู้ขอประเมิน", cntRow, 2, "B" + cntRow);
                SetRowHeaderStyle(ws, "ตำแหน่งเดิม", cntRow, 3, "C" + cntRow);
                SetRowHeaderStyle(ws, "ตำแหน่งที่ขอ", cntRow, 4, "D" + cntRow);
                SetRowHeaderStyle(ws, "หน่วยงาน", cntRow, 5, "E" + cntRow);
                SetRowHeaderStyle(ws, "วันที่ส่งผลงาน", cntRow, 6, "F" + cntRow);
                SetRowHeaderStyle(ws, "ผลการประเมิน", cntRow, 7, "G" + cntRow);
                SetRowHeaderStyle(ws, "วันที่ผ่านการประเมิน", cntRow, 8, "H" + cntRow);
                cntRow += 1;

                //Set Column Width
                ws.Column(1).Width = 20;
                ws.Column(2).Width = 30;
                ws.Column(3).Width = 30;
                ws.Column(4).Width = 30;
                ws.Column(5).Width = 30;
                ws.Column(6).Width = 20;
                ws.Column(7).Width = 20;
                ws.Column(8).Width = 20;

                int seq = 1;
                foreach (DataRow dr in dt_detail.Rows)
                {
                    string req_by_name = dr["req_by_name"].ToString();
                    string old_pos_name = dr["old_pos_name"].ToString();
                    string req_pos_name = dr["req_pos_name"].ToString();
                    string org_name = dr["org_name"].ToString();
                    string req_date = dr["req_date"].ToString();
                    string approve_status_name = dr["approve_status_name"].ToString();
                    string approve_date = dr["approve_date"].ToString();

                    SetDetailStyle(ws, seq.ToString(), cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                    SetDetailStyle(ws, req_by_name, cntRow, 2, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    SetDetailStyle(ws, old_pos_name, cntRow, 3, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    SetDetailStyle(ws, req_pos_name, cntRow, 4, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    SetDetailStyle(ws, org_name, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    SetDetailStyle(ws, req_date, cntRow, 6, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                    SetDetailStyle(ws, approve_status_name, cntRow, 7, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                    SetDetailStyle(ws, approve_date, cntRow, 8, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                    cntRow += 1;
                    seq += 1;

                }

                dt_detail.Dispose();
                dt.DefaultView.RowFilter = "";

            }//for dt_head


            dt.Dispose();


            cntRow += 3;
            //SetNormalStyle(ws, "วันที่พิมพ์ " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " น.", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            //SetNormalStyle(ws, "ผู้พิมพ์ " + UserName, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            SetNormalStyle(ws, "วันที่พิมพ์ " + printDate + " น.", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            SetNormalStyle(ws, "ผู้พิมพ์ " + UserName + " " + UserOrg, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);

            ExportExcelToClient(res, ep, "SMM_RP07");

        }
        #endregion

    }
    #endregion

    #region "Export Report SMM_UT0403"
    public static void ExportReportSMM_UT0403(DataTable dt_section1, DataTable dt_section2, DataTable dt_section3, DataTable dt_section4, HttpResponse res)
    {
        #region "SMM_UT0403"
        using (ExcelPackage ep = new ExcelPackage())
        {
            ExcelWorksheet ws = ep.Workbook.Worksheets.Add("SMM_UT0403");
            int cntRow = 2;

            DataTable dt_req = dt_section1.DefaultView.ToTable(true, "req_id").Copy();
            for (int i = 0; i <= dt_req.Rows.Count - 1; i++)
            {
                string req_id = dt_req.Rows[i]["req_id"].ToString();

                SetReportHeaderStyle(ws, "รายงานแบบประเมินบุคคลเข้าสู่ตำแหน่งผู้มีประสบการณ์รายบุคคล ", "A" + cntRow, "A" + cntRow + ":H" + cntRow);
                cntRow += 1;


                string REQ_ID = "";
                string REQ_CODE = "";
                string FullName = "";
                string REQ_DATE = "";
                string APPROVE_DATE = "";
                string ENTER_DATE = "";
                string enter_date_period = "";
                string POS_DATE = "";
                string pos_date_period = "";
                string line_age = "";
                string FROM_POS_ID = "";
                string FROM_POS_NAME = "";
                string FROM_LEV = "";
                string FROM_LEVEL_NAME = "";
                string FROM_PER_TYPE_NAME = "";
                string FROM_ADMIN_NAME = "";
                string FROM_LINE_NAME = "";
                string FROM_ORG_NAME = "";
                string TO_POS_NAME = "";
                string TO_LEV = "";
                string TO_LEVEL_NAME = "";
                string TO_PER_TYPE_NAME = "";
                string TO_ADMIN_NAME = "";
                string TO_LINE_NAME = "";
                string TO_ORG_NAME = "";
                string CONCEPT = "";
                string PROCESS_DATE_FROM = "";
                string PROCESS_DATE_TO = "";

                #region "__Section1"
                cntRow += 1;
                DataTable dt_req_detail = new DataTable();
                dt_section1.DefaultView.RowFilter = "req_id='" + req_id + "'";
                dt_req_detail = dt_section1.DefaultView.ToTable().Copy();
             
                if (dt_req_detail.Rows.Count > 0)
                {

                    REQ_ID = dt_req_detail.Rows[0]["REQ_ID"].ToString();
                    REQ_CODE = dt_req_detail.Rows[0]["REQ_CODE"].ToString();
                    FullName = dt_req_detail.Rows[0]["FullName"].ToString();
                    REQ_DATE = dt_req_detail.Rows[0]["REQ_DATE"].ToString();
                    APPROVE_DATE = dt_req_detail.Rows[0]["APPROVE_DATE"].ToString();
                    ENTER_DATE = dt_req_detail.Rows[0]["ENTER_DATE"].ToString();
                    enter_date_period = dt_req_detail.Rows[0]["enter_date_period"].ToString();
                    POS_DATE = dt_req_detail.Rows[0]["POS_DATE"].ToString();
                    pos_date_period = dt_req_detail.Rows[0]["pos_date_period"].ToString();
                    line_age = dt_req_detail.Rows[0]["line_age"].ToString();
                    FROM_POS_ID = dt_req_detail.Rows[0]["FROM_POS_ID"].ToString();
                    FROM_POS_NAME = dt_req_detail.Rows[0]["FROM_POS_NAME"].ToString();
                    FROM_LEV = dt_req_detail.Rows[0]["FROM_LEV"].ToString();
                    FROM_LEVEL_NAME = dt_req_detail.Rows[0]["FROM_LEVEL_NAME"].ToString();
                    FROM_PER_TYPE_NAME = dt_req_detail.Rows[0]["FROM_PER_TYPE_NAME"].ToString();
                    FROM_ADMIN_NAME = dt_req_detail.Rows[0]["FROM_ADMIN_NAME"].ToString();
                    FROM_LINE_NAME = dt_req_detail.Rows[0]["FROM_LINE_NAME"].ToString();
                    FROM_ORG_NAME = dt_req_detail.Rows[0]["FROM_ORG_NAME"].ToString();
                    TO_POS_NAME = dt_req_detail.Rows[0]["TO_POS_NAME"].ToString();
                    TO_LEV = dt_req_detail.Rows[0]["TO_LEV"].ToString();
                    TO_LEVEL_NAME = dt_req_detail.Rows[0]["TO_LEVEL_NAME"].ToString();
                    TO_PER_TYPE_NAME = dt_req_detail.Rows[0]["TO_PER_TYPE_NAME"].ToString();
                    TO_ADMIN_NAME = dt_req_detail.Rows[0]["TO_ADMIN_NAME"].ToString();
                    TO_LINE_NAME = dt_req_detail.Rows[0]["TO_LINE_NAME"].ToString();
                    TO_ORG_NAME = dt_req_detail.Rows[0]["TO_ORG_NAME"].ToString();
                    CONCEPT = dt_req_detail.Rows[0]["CONCEPT"].ToString();
                    PROCESS_DATE_FROM = dt_req_detail.Rows[0]["PROCESS_DATE_FROM"].ToString();
                    PROCESS_DATE_TO = dt_req_detail.Rows[0]["PROCESS_DATE_TO"].ToString();

                    SetTextStyle(ws, "เลขคำขอ :", "A" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    SetTextStyle(ws, REQ_CODE, "B" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    cntRow += 1;

                    SetTextStyle(ws, "ชื่อผู้ขอประเมิน :", "A" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    SetTextStyle(ws, FullName, "B" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    cntRow += 1;

                    SetTextStyle(ws, "วันที่ส่งผลงาน :", "A" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    SetTextStyle(ws, REQ_DATE, "B" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    

                    SetTextStyle(ws, "วันที่ผ่านการประเมิน :", "C" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    SetTextStyle(ws, APPROVE_DATE, "D" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    cntRow += 1;

                    SetTextStyle(ws, "วันที่บรรจุ :", "A" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    SetTextStyle(ws, ENTER_DATE, "B" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    

                    SetTextStyle(ws, "อายุราชการ ณ วันที่ขอ :", "C" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    SetTextStyle(ws, enter_date_period, "D" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    cntRow += 1;

                    SetTextStyle(ws, "ระยะเวลาดำรงตำแหน่งปัจจุบัน :", "A" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    SetTextStyle(ws, pos_date_period, "B" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    cntRow += 1;

                    SetTextStyle(ws, "ระยะเวลาดำรงตำแหน่งในสายงานที่เกี่ยวข้อง :", "A" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    SetTextStyle(ws, line_age, "B" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    cntRow += 1;


                    //ตำแหน่ง
                   
                    SetTextStyle(ws, "ตำแหน่งเดิม :", "A" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    SetTextStyle(ws, FROM_POS_NAME, "B" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    cntRow += 1;
                    SetTextStyle(ws, "ระดับ :", "A" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    SetTextStyle(ws, FROM_LEVEL_NAME, "B" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    cntRow += 1;
                    SetTextStyle(ws, "ประเภทบุคคลากร :", "A" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    SetTextStyle(ws, FROM_PER_TYPE_NAME, "B" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    cntRow += 1;
                    SetTextStyle(ws, "ตำแหน่งทางการบริหาร :", "A" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    SetTextStyle(ws, FROM_ADMIN_NAME, "B" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    cntRow += 1;
                    SetTextStyle(ws, "ตำแหน่งทางวิชาการ :", "A" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    SetTextStyle(ws, FROM_LINE_NAME, "B" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    cntRow += 1;
                     SetTextStyle(ws, "หน่วยงาน :", "A" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    SetTextStyle(ws, FROM_ORG_NAME, "B" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    cntRow += 1;

                    SetTextStyle(ws, "ตำแหน่งที่ขอ :", "A" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    SetTextStyle(ws, TO_POS_NAME, "B" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    cntRow += 1;
                    SetTextStyle(ws, "ระดับ :", "A" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    SetTextStyle(ws, TO_LEVEL_NAME, "B" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    cntRow += 1;
                    SetTextStyle(ws, "ประเภทบุคคลากร :", "A" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    SetTextStyle(ws, TO_PER_TYPE_NAME, "B" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    cntRow += 1;
                    SetTextStyle(ws, "ตำแหน่งทางการบริหาร :", "A" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    SetTextStyle(ws, TO_ADMIN_NAME, "B" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    cntRow += 1;
                    SetTextStyle(ws, "ตำแหน่งทางวิชาการ :", "A" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    SetTextStyle(ws, TO_LINE_NAME, "B" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    cntRow += 1;
                    SetTextStyle(ws, "หน่วยงาน :", "A" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    SetTextStyle(ws, TO_ORG_NAME, "B" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    cntRow += 1;

                   
                }
                dt_req_detail.Dispose();
                dt_section1.DefaultView.RowFilter = "";
                #endregion

                #region "__Section2"
                cntRow += 1;
                DataTable dt_s2_detail = new DataTable();
                dt_section2.DefaultView.RowFilter = "req_id='" + req_id + "'";
                dt_s2_detail = dt_section2.DefaultView.ToTable().Copy();

                SetTextStyle(ws, "ผลงาน", "A" + cntRow, true, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                cntRow += 1;

                if (dt_s2_detail.Rows.Count > 0)
                {
                    string PROJ_NAME = dt_s2_detail.Rows[0]["PROJ_NAME"].ToString();
                    string PROJ_DESCRIPTION = dt_s2_detail.Rows[0]["PROJ_DESCRIPTION"].ToString();
                    string RECOMMENT = dt_s2_detail.Rows[0]["RECOMMENT"].ToString();

                    SetTextStyle(ws, "ชื่อผลงาน :", "A" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    SetTextStyle(ws, PROJ_NAME, "B" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    cntRow += 1;

                    SetTextStyle(ws, "รายละเอียด :", "A" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    SetTextStyle(ws, PROJ_DESCRIPTION, "B" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    cntRow += 1;

                    SetTextStyle(ws, "ข้อเสนอแนะ :", "A" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    SetTextStyle(ws, RECOMMENT, "B" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    cntRow += 1;


                }

                SetTextStyle(ws, "ผู้ร่วมผลงาน", "A" + cntRow, true, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                cntRow += 1;
                int seq_sec2 = 1;
                foreach (DataRow dr in dt_s2_detail.Rows)
                {
                    string team_name = dr["TEAM_NAME"].ToString();
                    SetTextStyle(ws, seq_sec2.ToString(), "A" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    SetTextStyle(ws, team_name, "B" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    cntRow += 1;
                    seq_sec2 += 1;
                }
                dt_s2_detail.Dispose();
                dt_section2.DefaultView.RowFilter = "";
                #endregion

                #region "__Section3"
                cntRow += 1;
                DataTable dt_s3_detail = new DataTable();
                dt_section3.DefaultView.RowFilter = "req_id='" + req_id + "'";
                dt_s3_detail = dt_section3.DefaultView.ToTable().Copy();

                SetTextStyle(ws, "แนวคิด", "A" + cntRow, true, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                cntRow += 1;

                if (dt_s3_detail.Rows.Count > 0)
                {
                    //SELECT a.REQ_ID,str(a.ACTION_ID) as Seq,a.ACTION_DETAIL,r.CONCEPT,r.PROCESS_DATE_FROM,r.PROCESS_DATE_TO 

                    string CONCEPT_3 = dt_s3_detail.Rows[0]["CONCEPT"].ToString();
                    string PROCESS_DATE_FROM_3 = dt_s3_detail.Rows[0]["PROCESS_DATE_FROM"].ToString();
                    string PROCESS_DATE_TO_3 = dt_s3_detail.Rows[0]["PROCESS_DATE_TO"].ToString();

                    SetTextStyle(ws, "แนวคิด :", "A" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    SetTextStyle(ws, CONCEPT_3, "B" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    cntRow += 1;

                    SetTextStyle(ws, "ระยะเวลาดำเนินงาน :", "A" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    SetTextStyle(ws, PROCESS_DATE_FROM_3, "B" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    
                    SetTextStyle(ws, "ถึง ", "C" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    SetTextStyle(ws, PROCESS_DATE_TO_3, "D" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    cntRow += 1;
                }

                SetTextStyle(ws, "การดำเนินการ", "A" + cntRow, true, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                cntRow += 1;

                int seq_sec3 = 1;
                foreach (DataRow dr in dt_s3_detail.Rows)
                {
                    string ACTION_DETAIL = dr["ACTION_DETAIL"].ToString();
                    SetTextStyle(ws, seq_sec3.ToString(), "A" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    SetTextStyle(ws, ACTION_DETAIL, "B" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    cntRow += 1;
                    seq_sec3 += 1;
                }
                dt_s3_detail.Dispose();
                dt_section3.DefaultView.RowFilter = "";
                #endregion

                #region "__Section4"
                cntRow += 1;
                DataTable dt_s4_detail = new DataTable();
                dt_section4.DefaultView.RowFilter = "req_id='" + req_id + "'";
                dt_s4_detail = dt_section4.DefaultView.ToTable().Copy();

                SetTextStyle(ws, "เกณฑ์การประเมิน", "A" + cntRow, true, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                cntRow += 1;

                int SUM_GET_SCORE = 0;
                int SUM_MAX_SCORE = 0;

                int seq_sec4 = 1;
                foreach (DataRow dr in dt_s4_detail.Rows)
                {
                    string COND_NAME = dr["COND_NAME"].ToString();
                    string GET_SCORE = dr["GET_SCORE"].ToString();
                    string MAX_SCORE = dr["MAX_SCORE"].ToString();

                    if (MAX_SCORE != "")
                    { 
                        SetTextStyle(ws, seq_sec4.ToString(), "A" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                        SetTextStyle(ws, COND_NAME, "B" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                        SetTextStyle(ws, GET_SCORE, "C" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                        SetTextStyle(ws, MAX_SCORE, "D" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    }
                    
                    cntRow += 1;
                    seq_sec4 += 1;

                    if (GET_SCORE != "") { SUM_GET_SCORE += Convert.ToInt16(GET_SCORE); }
                    if (MAX_SCORE != "") { SUM_MAX_SCORE += Convert.ToInt16(MAX_SCORE); }
                    
                }
                SetTextStyle(ws, "รวมทั้งหมด", "B" + cntRow, true, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);              

                SetTextStyle(ws, SUM_GET_SCORE.ToString(), "C" + cntRow, true, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                SetTextStyle(ws, SUM_MAX_SCORE.ToString(), "D" + cntRow, true, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                cntRow += 1;

                if (dt_s4_detail.Rows.Count > 0)
                {
                    string PASSED = dt_s4_detail.Rows[0]["PASSED"].ToString();
                    string RECOMMENT = dt_s4_detail.Rows[0]["RECOMMENT"].ToString();
                    string REMARK = dt_s4_detail.Rows[0]["REMARK"].ToString();

                    SetTextStyle(ws, "สรุปผลการผ่านประเมิน :", "A" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    SetTextStyle(ws, PASSED, "B" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    cntRow += 1;

                    SetTextStyle(ws, "ข้อเสนอแนะเพิ่มเติม :", "A" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    SetTextStyle(ws, RECOMMENT, "B" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    cntRow += 1;

                    SetTextStyle(ws, "หมายเหตุ :", "A" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    SetTextStyle(ws, REMARK, "B" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    cntRow += 1;


                }
                dt_s4_detail.Dispose();
                dt_section4.DefaultView.RowFilter = "";
                #endregion

              
                cntRow += 3;

            }//for dt_req


            dt_req.Dispose();
            ExportExcelToClient(res, ep, "SMM_UT0403");

        }
        #endregion

    }
    #endregion

    #region "Export Report SMM_RP08"
    public static void ExportReportSMM_RP08(string DateFrom,string DateTo,string UserName, string UserOrg, DataTable dt, HttpResponse res)
    {
        #region "SMM_RP08"
        using (ExcelPackage ep = new ExcelPackage())
        {
            ExcelWorksheet ws = ep.Workbook.Worksheets.Add("SMM_RP08");
            int cntRow = 2;

            DataTable dt_head = new DataTable();
            dt_head = dt.DefaultView.ToTable(true, "old_pos_name").Copy();
            for (int i = 0; i <= dt_head.Rows.Count - 1; i++)
            {
                string h_old_pos_name = dt_head.Rows[i]["old_pos_name"].ToString();

                SetReportHeaderStyle(ws, "บัญชีรายชื่อผู้ผ่านการประเมินเข้าสู่ตำแหน่งผู้มีประสบการณ์", "A" + cntRow, "A" + cntRow + ":H" + cntRow);
                cntRow += 1;

                SetReportHeaderStyle(ws, "ตำแหน่ง" + h_old_pos_name, "A" + cntRow, "A" + cntRow + ":H" + cntRow);
                cntRow += 1;

                SetReportHeaderStyle(ws, "ตั้งแต่วันที่ " + DateFrom + " ถึง" + DateTo, "A" + cntRow, "A" + cntRow + ":H" + cntRow);
                cntRow += 2;

                DataTable dt_detail = new DataTable();
                dt.DefaultView.RowFilter = "old_pos_name='" + h_old_pos_name + "'";
                dt_detail = dt.DefaultView.ToTable().Copy();

               
                SetRowHeaderStyle(ws, "ลำดับที่", cntRow, 1, "A" + cntRow + ":A" + (cntRow + 1).ToString());
                SetRowHeaderStyle(ws, "ผู้ขอประเมิน", cntRow, 2, "B" + cntRow + ":B" + (cntRow + 1).ToString());
                SetRowHeaderStyle(ws, "ตำแหน่งและส่วนราชการเดิม", cntRow, 3, "C" + cntRow + ":D" + cntRow);
                SetRowHeaderStyle(ws, "ตำแหน่ง/สังกัด", cntRow + 1, 3, "C" + (cntRow + 1).ToString());
                SetRowHeaderStyle(ws, "เลขที่ตำแหน่ง", cntRow + 1, 4, "D" + (cntRow + 1).ToString());
                SetRowHeaderStyle(ws, "ตำแหน่งและส่วนราชการที่ขอประเมิน", cntRow, 5, "E" + cntRow + ":F" + cntRow);
                SetRowHeaderStyle(ws, "ตำแหน่ง/สังกัด", cntRow + 1, 5, "E" + (cntRow + 1).ToString());
                SetRowHeaderStyle(ws, "เลขที่ตำแหน่ง", cntRow + 1, 6, "F" + (cntRow + 1).ToString());
                SetRowHeaderStyle(ws, "วันที่ส่งผลงาน", cntRow + 1, 7, "G" + cntRow + ":G" + (cntRow + 1).ToString());
                SetRowHeaderStyle(ws, "วันที่ผ่านการประเมิน", cntRow + 1, 8, "H" + cntRow + ":H" + (cntRow + 1).ToString());
                cntRow += 2;

                //Set Column Width
                ws.Column(1).Width = 10;
                ws.Column(2).Width = 30;
                ws.Column(3).Width = 30;
                ws.Column(4).Width = 20;
                ws.Column(5).Width = 30;
                ws.Column(6).Width = 20;
                ws.Column(7).Width = 20;
                ws.Column(8).Width = 20;

                int seq = 1;
                foreach (DataRow dr in dt_detail.Rows)
                {
                    string req_by_name = dr["req_by_name"].ToString();
                    string old_pos_name = dr["old_pos_name"].ToString()+ Environment.NewLine + dr["old_org_name"].ToString()+ Environment.NewLine + dr["old_org_name3"].ToString();
                    string req_pos_name = dr["new_pos_name"].ToString()+ Environment.NewLine + dr["new_org_name"].ToString()+ Environment.NewLine + dr["new_org_name3"].ToString();
                    string old_pos_id = dr["old_pos_id"].ToString();
                    string new_pos_id = dr["new_pos_id"].ToString();
                    string req_date = dr["req_date"].ToString();
                    string approve_date = dr["approve_date"].ToString();

                    SetDetailStyle(ws, seq.ToString(), cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                    SetDetailStyle(ws, req_by_name, cntRow, 2, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    SetDetailStyle(ws, old_pos_name, cntRow, 3, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    SetDetailStyle(ws, old_pos_id, cntRow, 4, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    SetDetailStyle(ws, req_pos_name, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    SetDetailStyle(ws, new_pos_id, cntRow, 6, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                    SetDetailStyle(ws, req_date, cntRow, 7, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                    SetDetailStyle(ws, approve_date, cntRow, 8, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                    cntRow += 1;
                    seq += 1;

                }

                dt_detail.Dispose();
                dt.DefaultView.RowFilter = "";

            }//for dt_head


            dt.Dispose();


            cntRow += 3;
            //SetNormalStyle(ws, "วันที่พิมพ์ " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " น.", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            //SetNormalStyle(ws, "ผู้พิมพ์ " + UserName, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            SetNormalStyle(ws, "วันที่พิมพ์ " + printDate + " น.", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            SetNormalStyle(ws, "ผู้พิมพ์ " + UserName + " " + UserOrg, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);

            ExportExcelToClient(res, ep, "SMM_RP08");

        }
        #endregion

    }
    #endregion

    #region "Export Report SMM_RP09"
    public static void ExportReportSMM_RP09(string DateFrom, string DateTo, string UserName, string UserOrg, DataTable dt, HttpResponse res)
    {
        #region "SMM_RP09"
        using (ExcelPackage ep = new ExcelPackage())
        {
            ExcelWorksheet ws = ep.Workbook.Worksheets.Add("SMM_RP09");
            int cntRow = 2;
            SetReportHeaderStyle(ws, "สำนักงานปลัดสำนักนายกรัฐมนตรี", "A" + cntRow, "A" + cntRow + ":G" + cntRow);
            cntRow += 1;

            SetReportHeaderStyle(ws, "รายชื่อผลงานที่ผ่านการประเมินเข้าสู่ตำแหน่งผู้มีประสบการณ์", "A" + cntRow, "A" + cntRow + ":G" + cntRow);
            cntRow += 1;

            SetReportHeaderStyle(ws, "ตั้งแต่วันที่" + DateFrom + "ถึง " + DateTo, "A" + cntRow, "A" + cntRow + ":G" + cntRow);
            cntRow += 1;

            SetRowHeaderStyle(ws, "ลำดับที่", cntRow, 1, "A" + cntRow);
            SetRowHeaderStyle(ws, "ชื่อผลงาน", cntRow, 2, "B" + cntRow);
            SetRowHeaderStyle(ws, "ผู้ขอประเมิน", cntRow, 3, "C" + cntRow);
            SetRowHeaderStyle(ws, "ตำแหน่งและส่วนราชการเดิม", cntRow, 4, "D" + cntRow);
            SetRowHeaderStyle(ws, "ตำแหน่งและส่วนราชการที่ขอประเมิน", cntRow, 5, "E" + cntRow);
            SetRowHeaderStyle(ws, "วันที่ส่งผลงาน", cntRow, 6, "F" + cntRow);
            SetRowHeaderStyle(ws, "วันที่ผ่านการประเมิน", cntRow, 7, "G" + cntRow);
            cntRow += 1;

            ws.Column(1).Width = 10;
            ws.Column(2).Width = 30;
            ws.Column(3).Width = 30;
            ws.Column(4).Width = 30;
            ws.Column(5).Width = 30;
            ws.Column(6).Width = 20;
            ws.Column(7).Width = 20;

            int seq = 1;
            foreach (DataRow dr in dt.Rows)
            {
                string proj_name = dr["proj_name"].ToString().Replace("&nbsp;","").Replace("</br>","");
                string req_by_name = dr["req_by_name"].ToString();
                string OldPosInfo = dr["old_pos_name"].ToString()+ Environment.NewLine + dr["old_org_name"].ToString()+ Environment.NewLine + dr["old_org_name3"].ToString();
                string NewPosInfo = dr["new_pos_name"].ToString()+ Environment.NewLine + dr["new_org_name"].ToString()+ Environment.NewLine + dr["new_org_name3"].ToString();
                string req_date = dr["req_date"].ToString();
                string approve_date = dr["approve_date"].ToString();

                SetDetailStyle(ws, seq.ToString(), cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetDetailStyle(ws, proj_name, cntRow, 2, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                SetDetailStyle(ws, req_by_name, cntRow, 3, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                SetDetailStyle(ws, OldPosInfo, cntRow, 4, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                SetDetailStyle(ws, NewPosInfo, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                SetDetailStyle(ws, req_date, cntRow, 6, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetDetailStyle(ws, approve_date, cntRow, 7, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);

                cntRow += 1;
                seq += 1;

            }//dt_detail
            dt.Dispose();

            cntRow += 3;
            //SetNormalStyle(ws, "วันที่พิมพ์ " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " น.", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            //SetNormalStyle(ws, "ผู้พิมพ์ " + UserName, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            SetNormalStyle(ws, "วันที่พิมพ์ " + printDate + " น.", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            SetNormalStyle(ws, "ผู้พิมพ์ " + UserName + " " + UserOrg, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);

            ExportExcelToClient(res, ep, "SMM_RP09");

        }
        #endregion

    }
    #endregion

    #region "Export Report SMM_RP11"
    public static void ExportReportSMM_RP11(string UserName, string UserOrg, DataTable dt, HttpResponse res)
    {
        #region "SMM_RP11"
        using (ExcelPackage ep = new ExcelPackage())
        {
            string h_org_name = "";
            if (dt.Rows.Count > 0)
            {
                h_org_name = dt.Rows[0]["org_name"].ToString();
            }

            ExcelWorksheet ws = ep.Workbook.Worksheets.Add("ODS_RP01");
            int cntRow = 2;
            SetReportHeaderStyle(ws, "แผนภูมิโครงสร้างการแบ่งส่วนราชการของแต่ละหน่วยงาน", "A" + cntRow, "A" + cntRow + ":C" + cntRow);
            cntRow += 1;

            SetReportHeaderStyle(ws, "หน่วยงาน " + h_org_name + " พบ " + dt.Rows.Count.ToString() + " รายการ", "A" + cntRow, "A" + cntRow + ":C" + cntRow);
            cntRow += 2;

            SetRowHeaderStyle(ws, "โครงสร้าง", cntRow, 1, "A" + cntRow);
            SetRowHeaderStyle(ws, "ประเภทตำแหน่ง", cntRow, 2, "B" + cntRow);
            SetRowHeaderStyle(ws, "ผู้ดำรงตำแหน่ง", cntRow, 3, "C" + cntRow);
            cntRow += 1;


            //Set Column Width
            ws.Column(1).Width = 40;
            ws.Column(2).Width = 40;
            ws.Column(3).Width = 30;

            DataTable dt_upper_org = new DataTable();
            dt_upper_org = dt.DefaultView.ToTable(true, "upper_org_serial", "upper_org_name").Copy();
            for (int i = 0; i <= dt_upper_org.Rows.Count - 1; i++)
            {
                string upper_org_name = dt_upper_org.Rows[i]["upper_org_name"].ToString();
                SetRowGroupStyle(ws, upper_org_name, cntRow, 1, "A" + cntRow + ":C" + cntRow, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);

                string upper_org_serial = dt_upper_org.Rows[i]["upper_org_serial"].ToString();
                DataTable dt_upper_org_detail = new DataTable();
                dt.DefaultView.RowFilter = "upper_org_serial ='" + upper_org_serial + "'";
                dt_upper_org_detail = dt.DefaultView.ToTable(true,"org_name").Copy();
                cntRow += 1;

                for (int j = 0; j <= dt_upper_org_detail.Rows.Count - 1; j++)
                {
                    string org_name = dt_upper_org_detail.Rows[j]["org_name"].ToString();
                    DataTable dt_detail = new DataTable();
                    dt.DefaultView.RowFilter = "upper_org_serial ='" + upper_org_serial + "' and org_name ='" + org_name + "'";
                    dt_detail = dt.DefaultView.ToTable().Copy();

                    SetRowGroupStyle(ws, "       " + org_name, cntRow, 1, "A" + cntRow + ":C" + cntRow, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    cntRow += 1;

                    int seq = 1;
                    foreach (DataRow dr in dt_detail.Rows)
                    {
                        string pos_name = dr["pos_name"].ToString();
                        string per_type_name = dr["per_type_name"].ToString();
                        string person_name = dr["person_name"].ToString();

                        SetDetailStyle(ws, pos_name, cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                        SetDetailStyle(ws, per_type_name, cntRow, 2, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                        SetDetailStyle(ws, person_name, cntRow, 3, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);

                        cntRow += 1;
                        seq += 1;

                    }//dt_detail
                    dt_detail.Dispose();
                    dt.DefaultView.RowFilter = "";

                }//dt_upper_org_detail
                dt_upper_org_detail.Dispose();
                dt.DefaultView.RowFilter = "";

            }//dt_upper_org_detail
            dt_upper_org.Dispose();
            dt.DefaultView.RowFilter = "";

            cntRow += 3;
            //SetNormalStyle(ws, "วันที่พิมพ์ " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " น.", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            //SetNormalStyle(ws, "ผู้พิมพ์ " + UserName, cntRow, 3, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            SetNormalStyle(ws, "วันที่พิมพ์ " + printDate + " น.", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            SetNormalStyle(ws, "ผู้พิมพ์ " + UserName + " " + UserOrg, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);

            ExportExcelToClient(res, ep, "SMM_RP11");

        }
        #endregion

    }
    #endregion

    #region "Export Report SMM_RP12"
    public static void ExportReportSMM_RP12(string DateFrom, string DateTo, string UserName, string UserOrg, DataTable dt, HttpResponse res)
    {
        #region "SMM_RP12"
        using (ExcelPackage ep = new ExcelPackage())
        {
            ExcelWorksheet ws = ep.Workbook.Worksheets.Add("SMM_RP12");
            int cntRow = 2;
            SetReportHeaderStyle(ws, "รายงานการติดตามการดำเนินการในส่วนของข้อเสนอแนวคิด/วิธีการเพื่อพัฒนางานหรือปรับปรุงงานให้มีประสิทธิภาพ", "A" + cntRow, "A" + cntRow + ":F" + cntRow);
            cntRow += 1;

            SetReportHeaderStyle(ws, "วันที่ผ่านการประเมิน" + DateFrom + "ถึง " + DateTo, "A" + cntRow, "A" + cntRow + ":F" + cntRow);
            cntRow += 2;

            SetRowHeaderStyle(ws, "ลำดับ", cntRow, 1, "A" + cntRow);
            SetRowHeaderStyle(ws, "ชื่อ-สกุล", cntRow, 2, "B" + cntRow);
            SetRowHeaderStyle(ws, "ตำแหน่ง", cntRow, 3, "C" + cntRow);
            SetRowHeaderStyle(ws, "หน่วยงาน", cntRow, 4, "D" + cntRow);
            SetRowHeaderStyle(ws, "ข้อเสนอ/แนวคิด/วิธีการพัฒนา", cntRow, 5, "E" + cntRow);
            SetRowHeaderStyle(ws, "วันที่ผ่านการประเมิน", cntRow, 6, "F" + cntRow);
            cntRow += 1;

            //Set Column Width
            ws.Column(1).Width = 10;
            ws.Column(2).Width = 20;
            ws.Column(3).Width = 20;
            ws.Column(4).Width = 20;
            ws.Column(5).Width = 30;
            ws.Column(6).Width = 20;

            int seq = 1;
            foreach (DataRow dr in dt.Rows)
            {
                string fullname = dr["req_by_name"].ToString() + " " + dr["req_by_surname"].ToString();
                string pos_name = dr["pos_name"].ToString();
                string org_name = dr["org_name"].ToString();
                string RECOMMENT = dr["RECOMMENT"].ToString();
                string approve_date = dr["approve_date"].ToString();

                SetDetailStyle(ws, seq.ToString(), cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetDetailStyle(ws, fullname, cntRow, 2, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                SetDetailStyle(ws, pos_name, cntRow, 3, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                SetDetailStyle(ws, org_name, cntRow, 4, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                SetDetailStyle(ws, RECOMMENT, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                SetDetailStyle(ws, approve_date, cntRow, 6, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);

                cntRow += 1;
                seq += 1;

            }
            dt.DefaultView.RowFilter = "";

            cntRow += 3;
            //SetNormalStyle(ws, "วันที่พิมพ์ " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " น.", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            //SetNormalStyle(ws, "ผู้พิมพ์ " + UserName, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            SetNormalStyle(ws, "วันที่พิมพ์ " + printDate + " น.", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            SetNormalStyle(ws, "ผู้พิมพ์ " + UserName + " " + UserOrg, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);

            ExportExcelToClient(res, ep, "SMM_RP12");

        }
        #endregion

    }
    #endregion

    #region "Export Report SMM_RP13"
    public static void ExportReportSMM_RP13(string ProjectStatus,string PosName,string DateFrom, string DateTo, string UserName, string UserOrg, DataTable dt, HttpResponse res)
    {
        #region "SMM_RP13"
        using (ExcelPackage ep = new ExcelPackage())
        {
            ExcelWorksheet ws = ep.Workbook.Worksheets.Add("SMM_RP13");
            int cntRow = 2;
            SetReportHeaderStyle(ws, "รายชื่อข้าราชการการตำแหน่งผู้มีประสบการณ์(ประเภทวิชาการ)จำแนกตามตำแหน่งและระดับ", "A" + cntRow, "A" + cntRow + ":E" + cntRow);
            cntRow += 1;

            SetReportHeaderStyle(ws, "สถานะของผลงาน  " + ProjectStatus + " ตำแหน่ง " + PosName , "A" + cntRow, "A" + cntRow + ":E" + cntRow);
            cntRow += 1;

            SetReportHeaderStyle(ws, "วันที่ผ่านการประเมิน " + DateFrom + "ถึง " + DateTo, "A" + cntRow, "A" + cntRow + ":E" + cntRow);
            cntRow += 2;

            SetRowHeaderStyle(ws, "ลำดับที่", cntRow, 1, "A" + cntRow);
            SetRowHeaderStyle(ws, "ชื่อ-สกุล", cntRow, 2, "B" + cntRow);
            SetRowHeaderStyle(ws, "ตำแหน่ง/ระดับ", cntRow, 3, "C" + cntRow);
            SetRowHeaderStyle(ws, "สังกัด", cntRow, 4, "D" + cntRow);
            SetRowHeaderStyle(ws, "เลขที่", cntRow, 5, "E" + cntRow);
            cntRow += 1;

            //Set Column Width
            ws.Column(1).Width = 20;
            ws.Column(2).Width = 20;
            ws.Column(3).Width = 40;
            ws.Column(4).Width = 30;
            ws.Column(5).Width = 20;

            int seq = 1;
            foreach (DataRow dr in dt.Rows)
            {
                string fullname = dr["req_by_name"].ToString() + " " + dr["req_by_surname"].ToString();
                string pos_name = dr["pos_name"].ToString()+ " " + dr["level_name"].ToString();
                string org_name = dr["org_name"].ToString();
                string pos_id = dr["pos_id"].ToString();

                SetDetailStyle(ws, seq.ToString(), cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetDetailStyle(ws, fullname, cntRow, 2, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                SetDetailStyle(ws, pos_name, cntRow, 3, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                SetDetailStyle(ws, org_name, cntRow, 4, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                SetDetailStyle(ws, pos_id, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);

                cntRow += 1;
                seq += 1;

            }
            dt.DefaultView.RowFilter = "";

            cntRow += 3;
            //SetNormalStyle(ws, "วันที่พิมพ์ " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " น.", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            //SetNormalStyle(ws, "ผู้พิมพ์ " + UserName, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            SetNormalStyle(ws, "วันที่พิมพ์ " + printDate + " น.", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            SetNormalStyle(ws, "ผู้พิมพ์ " + UserName + " " + UserOrg, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);

            ExportExcelToClient(res, ep, "SMM_RP13");

        }
        #endregion

    }
    #endregion
    #endregion

    #region "__ODS"
    #region "Export Report ODS_RP01"
    public static void ExportReportODS_RP01(string UserName, string UserOrg, DataTable dt, HttpResponse res)
    {
        #region "ODS_RP01"
        using (ExcelPackage ep = new ExcelPackage())
        {
            ExcelWorksheet ws = ep.Workbook.Worksheets.Add("ODS_RP01");
            int cntRow = 2;
            string sys_name = "";
            if (dt.Rows.Count > 0)
            {
                sys_name = dt.Rows[0]["sys_name"].ToString();
            }

            DataTable dt_org = new DataTable();
            dt_org = dt.DefaultView.ToTable(true, "org_serial", "org_name_level3").Copy();
            for (int i = 0; i <= dt_org.Rows.Count - 1; i++)
            {
                string org_name_level3 = dt_org.Rows[i]["org_name_level3"].ToString();
                string org_serial = dt_org.Rows[i]["org_serial"].ToString();
                DataTable dt_org_detail = new DataTable();
                dt.DefaultView.RowFilter = "org_serial ='" + org_serial + "'";
                dt_org_detail = dt.DefaultView.ToTable(true, "org_name_level4").Copy();


                cntRow += 2;
                SetReportHeaderStyle(ws, "สำนักงานปลัดสำนักนายกรัฐมนตรี", "A" + cntRow, "A" + cntRow + ":F" + cntRow);
                cntRow += 1;

                SetReportHeaderStyle(ws, "รายงานรายชื่อผู้ที่สิทธิในแต่ละประเภทในแต่ละระบบงาน จำแนกตามหน่วยงาน", "A" + cntRow, "A" + cntRow + ":E" + cntRow);
                cntRow += 1;

                SetReportHeaderStyle(ws, "ระบบงาน" + sys_name + "หน่วยงาน " + org_name_level3, "A" + cntRow, "A" + cntRow + ":E" + cntRow);
                cntRow += 1;

                SetReportHeaderStyle(ws, "พบรายการทั้งหมด" + dt.DefaultView.Count.ToString() + "รายการ", "A" + cntRow, "A" + cntRow + ":E" + cntRow);
                cntRow += 1;

                SetRowHeaderStyle(ws, "ชื่อ-สกุล", cntRow, 1, "A" + cntRow);
                SetRowHeaderStyle(ws, "ตำแหน่ง", cntRow, 2, "B" + cntRow);
                //SetRowHeaderStyle(ws, "อีเมล์", cntRow, 3, "C" + cntRow);
                //SetRowHeaderStyle(ws, "รหัสผู้ใช้", cntRow, 4, "D" + cntRow);
                SetRowHeaderStyle(ws, "บทบาท", cntRow, 3, "C" + cntRow);
                SetRowHeaderStyle(ws, "วันที่มีผล", cntRow, 4, "D" + cntRow);
                SetRowHeaderStyle(ws, "วันที่หมดอายุ", cntRow, 5, "E" + cntRow);
                cntRow += 1;


                //Set Column Width
                ws.Column(1).Width = 30;
                ws.Column(2).Width = 30;
                ws.Column(3).Width = 20;
                ws.Column(4).Width = 20;
                ws.Column(5).Width = 20;
                //ws.Column(6).Width = 20;
                //ws.Column(7).Width = 20;

                //SetRowGroupStyle(ws, org_name_level3, cntRow, 1, "A" + cntRow + ":G" + cntRow, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                //cntRow += 1;

                for (int j = 0; j <= dt_org_detail.Rows.Count - 1; j++)
                {
                    string org_name_level4 = dt_org_detail.Rows[j]["org_name_level4"].ToString();
                    DataTable dt_level_detail = new DataTable();
                    dt.DefaultView.RowFilter = "org_serial ='" + org_serial + "' and org_name_level4 ='" + org_name_level4 + "'";
                    dt_level_detail = dt.DefaultView.ToTable().Copy();

                    SetRowGroupStyle(ws, "       " + org_name_level4, cntRow, 1, "A" + cntRow + ":E" + cntRow, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    cntRow += 1;

                    int seq = 1;
                    foreach (DataRow dr in dt_level_detail.Rows)
                    {
                        string fullname = dr["prefix_name"].ToString() + dr["name"].ToString() + dr["surname"].ToString();
                        string pos_name = dr["pos_name"].ToString();
                        string email = dr["email"].ToString();
                        string user_code = dr["user_code"].ToString();
                        string role_name = dr["role_name"].ToString();
                        string user_efft_date = dr["user_efft_date"].ToString();
                        string user_exp_date = dr["user_exp_date"].ToString();

                        SetDetailStyle(ws, fullname, cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                        SetDetailStyle(ws, pos_name, cntRow, 2, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                        //SetDetailStyle(ws, email, cntRow, 3, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                        //SetDetailStyle(ws, user_code, cntRow, 4, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                        SetDetailStyle(ws, role_name, cntRow, 3, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                        SetDetailStyle(ws, user_efft_date, cntRow, 4, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                        SetDetailStyle(ws, user_exp_date, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);

                        cntRow += 1;
                        seq += 1;

                    }//dt_org_detail
                    dt_level_detail.Dispose();
                    dt_org_detail.Dispose();

                }//dt_org_detail
                dt_org_detail.Dispose();
                dt.DefaultView.RowFilter = "";

            }//dt_org
            dt_org.Dispose();

            cntRow += 3;
            SetNormalStyle(ws, "วันที่พิมพ์ " + printDate + " น.", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            SetNormalStyle(ws, "ผู้พิมพ์ " + UserName + " " + UserOrg, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);


            ExportExcelToClient(res, ep, "ODS_RP01");

        }
        #endregion

    }
    #endregion

    #region "Export Report ODS_RP02"
    public static void ExportReportODS_RP02(string UserName, string UserOrg, DataTable dt, HttpResponse res)
    {
        #region "ODS_RP02"
        using (ExcelPackage ep = new ExcelPackage())
        {
            ExcelWorksheet ws = ep.Workbook.Worksheets.Add("ODS_RP02");
            int cntRow = 2;

            DataTable dt_staff_name = new DataTable();
            dt_staff_name = dt.DefaultView.ToTable(true, "prefix_name", "name", "surname", "pos_name", "level_name", "per_type_name", "user_code", "email", "user_efft_date", "user_exp_date","org_name_level3").Copy();
            for (int i = 0; i <= dt_staff_name.Rows.Count - 1; i++)
            {
                string prefix_name = dt_staff_name.Rows[i]["prefix_name"].ToString();
                string name = dt_staff_name.Rows[i]["name"].ToString();
                string surname=dt_staff_name.Rows[i]["surname"].ToString();

                string staff_name = prefix_name + " " + name + " " + surname;
                string pos_name = dt_staff_name.Rows[i]["pos_name"].ToString();
                string level_name = dt_staff_name.Rows[i]["level_name"].ToString();
                string per_type_name = dt_staff_name.Rows[i]["per_type_name"].ToString();
                string user_code = dt_staff_name.Rows[i]["user_code"].ToString();
                string email = dt_staff_name.Rows[i]["email"].ToString();
                string user_efft_date = dt_staff_name.Rows[i]["user_efft_date"].ToString();
                string user_exp_date = dt_staff_name.Rows[i]["user_exp_date"].ToString();
                string org_name_level3 = dt_staff_name.Rows[i]["org_name_level3"].ToString();

                SetReportHeaderStyle(ws, "สำนักงานปลัดสำนักนายกรัฐมนตรี", "A" + cntRow, "A" + cntRow + ":H" + cntRow);
                cntRow += 1;

                SetReportHeaderStyle(ws, "สิทธิการเข้าใช้ระบบงานของ " + staff_name, "A" + cntRow, "A" + cntRow + ":H" + cntRow);
                cntRow += 2;

                //SetReportHeaderStyle(ws, "-------------------------------------------------------------------------------------", "A" + cntRow, "A" + cntRow + ":H" + cntRow);
                //cntRow += 1;

                SetNormalStyle(ws, org_name_level3 + " สำนักงานปลัดสำนักนายกรัฐมนตรี ", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                cntRow += 2;

                SetTextStyle(ws, "ตำแหน่ง", "A" + cntRow, true, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                SetTextStyle(ws, pos_name, "B" + cntRow , false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                cntRow += 1;

                SetTextStyle(ws, "ระดับ", "A" + cntRow, true, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                SetTextStyle(ws, level_name, "B" + cntRow , false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);

                SetTextStyle(ws, "ประเภท", "D" + cntRow, true, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                SetTextStyle(ws, per_type_name, "E" + cntRow , false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                cntRow += 1;

                SetTextStyle(ws, "รหัสผู้ใช้", "A" + cntRow, true, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                SetTextStyle(ws, user_code, "B" + cntRow , false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);

                SetTextStyle(ws, "อีเมล", "D" + cntRow, true, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                SetTextStyle(ws, email, "E" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);                
                cntRow += 1;

                SetTextStyle(ws, "วันที่เริ่ม", "A" + cntRow, true, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                SetTextStyle(ws, user_efft_date, "B" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);

                SetTextStyle(ws, "วันที่หมดอายุ", "D" + cntRow, true, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                SetTextStyle(ws, user_exp_date, "E" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);                              
                cntRow += 2;

               
                DataTable dt_sys_detail = new DataTable();
                dt.DefaultView.RowFilter = "prefix_name='" + prefix_name + "' and name='" + name + "' and surname='" + surname + "'";
                dt_sys_detail = dt.DefaultView.ToTable().Copy();

                DataTable dt_sys_code = new DataTable();
                dt_sys_code = dt_sys_detail.DefaultView.ToTable(true, "sys_code").Copy();
             
                DataTable dt_org = new DataTable();
                dt_org = dt_sys_detail.DefaultView.ToTable(true, "sys_org_name").Copy();

                SetNormalStyle(ws, "มีสิทธิใช้งานทั้งหมด " + dt_sys_code.Rows.Count.ToString() + " ระบบงานภายใต้ " + dt_org.Rows.Count.ToString() + " หน่วยงานดังนี้", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                cntRow += 2;

            
                SetRowHeaderStyle(ws, "ชื่อย่อระบบ", cntRow, 2, "B" + cntRow);
                SetRowHeaderStyle(ws, "ระบบงาน", cntRow, 3, "C" + cntRow);
                SetRowHeaderStyle(ws, "หน่วยงาน", cntRow, 4, "D" + cntRow);
                SetRowHeaderStyle(ws, "บทบาท", cntRow, 5, "E" + cntRow);
                cntRow += 1;

                //Set Column Width
                ws.Column(2).Width = 10;
                ws.Column(3).Width = 30;
                ws.Column(4).Width = 30;
                ws.Column(5).Width = 20;

                foreach (DataRow dr in dt_sys_detail.Rows)
                {
                    string sys_code = dr["sys_code"].ToString();
                    string sys_name = dr["sys_name"].ToString();
                    string sys_org_name = dr["sys_org_name"].ToString();
                    string role_name = dr["role_name"].ToString();

                    SetDetailStyle(ws, sys_code, cntRow, 2, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                    SetDetailStyle(ws, sys_name, cntRow, 3, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    SetDetailStyle(ws, sys_org_name, cntRow, 4, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    SetDetailStyle(ws, role_name, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    cntRow += 1;
                }

                dt_sys_detail.DefaultView.RowFilter="";
                cntRow += 2;

            }//for staff


            dt.Dispose();
            

            cntRow += 3;
            SetNormalStyle(ws, "วันที่พิมพ์ " + printDate + " น.", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            SetNormalStyle(ws, "ผู้พิมพ์ " + UserName + " " + UserOrg, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);


            ExportExcelToClient(res, ep, "ODS_RP02");

        }
        #endregion

    }
    #endregion

    #region "Export Report ODS_RP03"
    public static void ExportReportODS_RP03(string UserName, string UserOrg, DataTable dt, HttpResponse res)
    {
        #region "ODS_RP03"
        using (ExcelPackage ep = new ExcelPackage())
        {
            ExcelWorksheet ws = ep.Workbook.Worksheets.Add("ODS_RP03");
            int cntRow = 2;

            DataTable dt_staff_name = new DataTable();
            dt_staff_name = dt.DefaultView.ToTable(true, "prefix_name", "name", "surname", "pos_name", "level_name", "per_type_name", "user_code", "user_pwd", "email", "user_efft_date", "user_exp_date", "org_name_level3").Copy();
            for (int i = 0; i <= dt_staff_name.Rows.Count - 1; i++)
            {
                string prefix_name = dt_staff_name.Rows[i]["prefix_name"].ToString();
                string name = dt_staff_name.Rows[i]["name"].ToString();
                string surname = dt_staff_name.Rows[i]["surname"].ToString();

                string staff_name = prefix_name + " " + name + " " + surname;
                string pos_name = dt_staff_name.Rows[i]["pos_name"].ToString();
                string level_name = dt_staff_name.Rows[i]["level_name"].ToString();
                string per_type_name = dt_staff_name.Rows[i]["per_type_name"].ToString();
                string user_code = dt_staff_name.Rows[i]["user_code"].ToString();
                string user_pwd = dt_staff_name.Rows[i]["user_pwd"].ToString();
                string email = dt_staff_name.Rows[i]["email"].ToString();
                string user_efft_date = dt_staff_name.Rows[i]["user_efft_date"].ToString();
                string user_exp_date = dt_staff_name.Rows[i]["user_exp_date"].ToString();
                string org_name_level3 = dt_staff_name.Rows[i]["org_name_level3"].ToString();

                SetReportHeaderStyle(ws, "สำนักงานปลัดสำนักนายกรัฐมนตรี", "A" + cntRow, "A" + cntRow + ":H" + cntRow);
                cntRow += 1;

                SetReportHeaderStyle(ws, "แจ้งชื่อผู้ใช้และรหัสผ่านสำหรับ " + staff_name, "A" + cntRow, "A" + cntRow + ":H" + cntRow);
                cntRow += 2;

                //SetReportHeaderStyle(ws, "-------------------------------------------------------------------------------------", "A" + cntRow, "A" + cntRow + ":H" + cntRow);
                //cntRow += 1;

                SetNormalStyle(ws, org_name_level3 + " สำนักงานปลัดสำนักนายกรัฐมนตรี ", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                cntRow += 2;

                SetTextStyle(ws, "ตำแหน่ง", "A" + cntRow, true, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                SetTextStyle(ws, pos_name + "" + level_name, "B" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                //cntRow += 1;

                //SetTextStyle(ws, "ระดับ", "A" + cntRow, true, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                //SetTextStyle(ws, level_name, "B" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);

                SetTextStyle(ws, "ประเภท", "D" + cntRow, true, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                SetTextStyle(ws, per_type_name, "E" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                cntRow += 1;

                SetTextStyle(ws, "ชื่อผู้ใช้(User Name)", "A" + cntRow, true, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                SetTextStyle(ws, user_code, "B" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);

                SetTextStyle(ws, "รหัสผ่าน", "D" + cntRow, true, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                SetTextStyle(ws, user_pwd, "E" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                cntRow += 1;

                SetTextStyle(ws, "อีเมล", "A" + cntRow, true, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                SetTextStyle(ws, email, "B" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                cntRow += 1;

                SetTextStyle(ws, "วันที่เริ่ม", "A" + cntRow, true, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                SetTextStyle(ws, user_efft_date, "B" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);

                SetTextStyle(ws, "วันที่หมดอายุ", "D" + cntRow, true, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                SetTextStyle(ws, user_exp_date, "E" + cntRow, false, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                cntRow += 2;


                DataTable dt_sys_detail = new DataTable();
                dt.DefaultView.RowFilter = "prefix_name='" + prefix_name + "' and name='" + name + "' and surname='" + surname + "'";
                dt_sys_detail = dt.DefaultView.ToTable().Copy();

                DataTable dt_sys_code = new DataTable();
                dt_sys_code = dt_sys_detail.DefaultView.ToTable(true, "sys_code").Copy();

                DataTable dt_org = new DataTable();
                dt_org = dt_sys_detail.DefaultView.ToTable(true, "sys_org_name").Copy();

                SetNormalStyle(ws, "มีสิทธิใช้งานทั้งหมด " + dt_sys_code.Rows.Count.ToString() + " ระบบงานภายใต้ " + dt_org.Rows.Count.ToString() + " หน่วยงานดังนี้", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                cntRow += 2;


                SetRowHeaderStyle(ws, "ชื่อย่อระบบ", cntRow, 2, "B" + cntRow);
                SetRowHeaderStyle(ws, "ระบบงาน", cntRow, 3, "C" + cntRow);
                SetRowHeaderStyle(ws, "หน่วยงาน", cntRow, 4, "D" + cntRow);
                SetRowHeaderStyle(ws, "บทบาท", cntRow, 5, "E" + cntRow);
                cntRow += 1;

                //Set Column Width
                ws.Column(2).Width = 10;
                ws.Column(3).Width = 30;
                ws.Column(4).Width = 30;
                ws.Column(5).Width = 20;

                foreach (DataRow dr in dt_sys_detail.Rows)
                {
                    string sys_code = dr["sys_code"].ToString();
                    string sys_name = dr["sys_name"].ToString();
                    string sys_org_name = dr["sys_org_name"].ToString();
                    string role_name = dr["role_name"].ToString();

                    SetDetailStyle(ws, sys_code, cntRow, 2, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                    SetDetailStyle(ws, sys_name, cntRow, 3, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    SetDetailStyle(ws, sys_org_name, cntRow, 4, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    SetDetailStyle(ws, role_name, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    cntRow += 1;
                }

                dt_sys_detail.DefaultView.RowFilter = "";
                cntRow += 2;

            }//for staff


            dt.Dispose();


            cntRow += 3;
            SetNormalStyle(ws, "วันที่พิมพ์ " + printDate + " น.", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            SetNormalStyle(ws, "ผู้พิมพ์ " + UserName + " " + UserOrg, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);


            ExportExcelToClient(res, ep, "ODS_RP03");

        }
        #endregion

    }
    #endregion

    #region "Export Report ODS_RP04"
    public static void ExportReportODS_RP04(string DateFrom,string DateTo,string UserName, string UserOrg, DataTable dt, HttpResponse res)
    {
        #region "ODS_RP04"
        using (ExcelPackage ep = new ExcelPackage())
        {
            ExcelWorksheet ws = ep.Workbook.Worksheets.Add("ODS_RP04");
            int cntRow = 2;
            SetReportHeaderStyle(ws, "สำนักงานปลัดสำนักนายกรัฐมนตรี", "A" + cntRow, "A" + cntRow + ":F" + cntRow);
            cntRow += 1;

            SetReportHeaderStyle(ws, "รายงานการเพิ่ม และลบข้อมูลบุคคลตามช่วงเวลาที่กำหนด จำแนกตามหน่วยงาน", "A" + cntRow, "A" + cntRow + ":F" + cntRow);
            cntRow += 1;

            SetReportHeaderStyle(ws, "ช่วงวันที่" + DateFrom + "ถึง " + DateTo, "A" + cntRow, "A" + cntRow + ":F" + cntRow);
            cntRow += 1;

            SetReportHeaderStyle(ws, "พบรายการทั้งหมด" + dt.Rows.Count.ToString() + "รายการ", "A" + cntRow, "A" + cntRow + ":F" + cntRow);
            cntRow += 1;

            SetRowHeaderStyle(ws, "ชื่อ-สกุล", cntRow, 1, "A" + cntRow);
            SetRowHeaderStyle(ws, "ตำแหน่ง", cntRow, 2, "B" + cntRow);
            SetRowHeaderStyle(ws, "ระดับ", cntRow, 3, "C" + cntRow);
            SetRowHeaderStyle(ws, "รหัสผู้ใช้", cntRow, 4, "D" + cntRow);
            SetRowHeaderStyle(ws, "สถานะ (เพิ่ม/ลบ)", cntRow, 5, "E" + cntRow);
            SetRowHeaderStyle(ws, "วันที่เปลี่ยน", cntRow, 6, "F" + cntRow);
            cntRow += 1;

            //Set Column Width
            ws.Column(1).Width = 30;
            ws.Column(2).Width = 30;
            ws.Column(3).Width = 20;
            ws.Column(4).Width = 10;
            ws.Column(5).Width = 20;
            ws.Column(6).Width = 20;

            DataTable dt_org = new DataTable();
            dt_org = dt.DefaultView.ToTable(true, "org_name").Copy();
            for (int i = 0; i <= dt_org.Rows.Count - 1; i++)
            {
                string org_name = dt_org.Rows[i]["org_name"].ToString();
                DataTable dt_detail = new DataTable();
                dt.DefaultView.RowFilter = "org_name ='" + org_name + "'";
                dt_detail = dt.DefaultView.ToTable().Copy();

                SetRowGroupStyle(ws, org_name, cntRow, 1, "A" + cntRow + ":F" + cntRow, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                cntRow += 1;

                int seq = 1;
                foreach (DataRow dr in dt_detail.Rows)
                {
                    string fullname = dr["person_name"].ToString();
                    string pos_name = dr["pos_name"].ToString();
                    string level_name = dr["level_name"].ToString();
                    string user_code = dr["user_code"].ToString();
                    string emp_status = dr["emp_status"].ToString();
                    string last_date = dr["last_date"].ToString();

                    SetDetailStyle(ws, fullname, cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    SetDetailStyle(ws, pos_name, cntRow, 2, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    SetDetailStyle(ws, level_name, cntRow, 3, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    SetDetailStyle(ws, user_code, cntRow, 4, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                    SetDetailStyle(ws, emp_status, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                    SetDetailStyle(ws, last_date, cntRow, 6, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);

                    cntRow += 1;
                    seq += 1;

                }//dt_detail
                dt_detail.Dispose();
                dt.DefaultView.RowFilter = "";

            }//dt_org
            dt_org.Dispose();

            cntRow += 3;
            SetNormalStyle(ws, "วันที่พิมพ์ " + printDate + " น.", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            SetNormalStyle(ws, "ผู้พิมพ์ " + UserName + " " + UserOrg, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);

            ExportExcelToClient(res, ep, "ODS_RP04");

        }
        #endregion

    }
    #endregion

    #region "Export Report ODS_RP05"
    public static void ExportReportODS_RP05(string DateFrom, string DateTo, string UserName, string UserOrg, DataTable dt, HttpResponse res)
    {
        #region "ODS_RP05"
        using (ExcelPackage ep = new ExcelPackage())
        {
            ExcelWorksheet ws = ep.Workbook.Worksheets.Add("ODS_RP05");
            int cntRow = 2;
            SetReportHeaderStyle(ws, "สำนักงานปลัดสำนักนายกรัฐมนตรี", "A" + cntRow, "A" + cntRow + ":F" + cntRow);
            cntRow += 1;

            SetReportHeaderStyle(ws, "รายงานรูปแบบรายงานการแก้ไขชื่อ-นามสกุล ของผู้ใช้ตามช่วงเวลาที่กำหนด", "A" + cntRow, "A" + cntRow + ":F" + cntRow);
            cntRow += 1;

            SetReportHeaderStyle(ws, "ช่วงวันที่" + DateFrom + "ถึง " + DateTo, "A" + cntRow, "A" + cntRow + ":F" + cntRow);
            cntRow += 1;

            SetReportHeaderStyle(ws, "พบรายการทั้งหมด" + dt.Rows.Count.ToString() + "รายการ", "A" + cntRow, "A" + cntRow + ":F" + cntRow);
            cntRow += 1;

            SetRowHeaderStyle(ws, "ลำดับที่", cntRow, 1, "A" + cntRow);
            SetRowHeaderStyle(ws, "ชื่อ-สกุล", cntRow, 2, "B" + cntRow);
            SetRowHeaderStyle(ws, "ชื่อ-สกุล(เดิม)", cntRow, 3, "C" + cntRow);
            SetRowHeaderStyle(ws, "หน่วยงาน", cntRow, 4, "D" + cntRow);
            SetRowHeaderStyle(ws, "ตำแหน่ง", cntRow, 5, "E" + cntRow);
            SetRowHeaderStyle(ws, "ระดับ", cntRow, 6, "F" + cntRow);
            SetRowHeaderStyle(ws, "วันที่เปลี่ยน", cntRow, 7, "G" + cntRow);
            cntRow += 1;

            //Set Column Width
            ws.Column(1).Width = 10;
            ws.Column(2).Width = 20;
            ws.Column(3).Width = 20;
            ws.Column(4).Width = 20;
            ws.Column(5).Width = 20;
            ws.Column(6).Width = 20;
            ws.Column(7).Width = 20;

            int seq = 1;
            foreach (DataRow dr in dt.Rows)
            {


                string new_person_name = dr["new_person_name"].ToString();
                string old_person_name = dr["old_person_name"].ToString();
                string org_name = dr["org_name"].ToString();
                string pos_name = dr["pos_name"].ToString();
                string level_name = dr["level_name"].ToString();
                string created_date = dr["created_date"].ToString();

                SetDetailStyle(ws, seq.ToString(), cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetDetailStyle(ws, new_person_name, cntRow, 2, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                SetDetailStyle(ws, old_person_name, cntRow, 3, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                SetDetailStyle(ws, org_name, cntRow, 4, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                SetDetailStyle(ws, pos_name, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                SetDetailStyle(ws, level_name, cntRow, 6, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                SetDetailStyle(ws, created_date, cntRow, 7, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);

                cntRow += 1;
                seq += 1;

            }
            dt.DefaultView.RowFilter = "";

            cntRow += 3;
            SetNormalStyle(ws, "วันที่พิมพ์ " + printDate + " น.", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            SetNormalStyle(ws, "ผู้พิมพ์ " + UserName + " " + UserOrg, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);

            ExportExcelToClient(res, ep, "ODS_RP05");

        }
        #endregion

    }
    #endregion

    #region "Export Report ODS_RP06"
    public static void ExportReportODS_RP06_1(string DateFrom, string DateTo, string UserName, string UserOrg, DataTable dt, HttpResponse res)
    {
        #region "ODS_RP06_1"
        using (ExcelPackage ep = new ExcelPackage())
        {
            ExcelWorksheet ws = ep.Workbook.Worksheets.Add("ODS_RP06_1");
            int cntRow = 2;
            SetReportHeaderStyle(ws, "สำนักงานปลัดสำนักนายกรัฐมนตรี", "A" + cntRow, "A" + cntRow + ":F" + cntRow);
            cntRow += 1;

            SetReportHeaderStyle(ws, "รายงานรูปแบบรายงานการแก้ไข ระดับ ของผู้ใช้ตามช่วงเวลาที่กำหนด", "A" + cntRow, "A" + cntRow + ":F" + cntRow);
            cntRow += 1;

            SetReportHeaderStyle(ws, "ช่วงวันที่" + DateFrom + "ถึง " + DateTo, "A" + cntRow, "A" + cntRow + ":F" + cntRow);
            cntRow += 1;

            SetReportHeaderStyle(ws, "พบรายการทั้งหมด" + dt.Rows.Count.ToString() + "รายการ", "A" + cntRow, "A" + cntRow + ":F" + cntRow);
            cntRow += 1;

            SetRowHeaderStyle(ws, "ลำดับที่", cntRow, 1, "A" + cntRow);
            SetRowHeaderStyle(ws, "ชื่อ-นามสกุล", cntRow, 2, "B" + cntRow);
            SetRowHeaderStyle(ws, "หน่วยงาน", cntRow, 3, "C" + cntRow);
            SetRowHeaderStyle(ws, "ระดับ(เดิม)", cntRow, 4, "D" + cntRow);
            SetRowHeaderStyle(ws, "ระดับ(ใหม่)", cntRow, 5, "E" + cntRow);
            SetRowHeaderStyle(ws, "วันที่เปลี่ยน", cntRow, 6, "F" + cntRow);
            cntRow += 1;

            //Set Column Width
            ws.Column(1).Width = 10;
            ws.Column(2).Width = 30;
            ws.Column(3).Width = 30;
            ws.Column(4).Width = 20;
            ws.Column(5).Width = 20;
            ws.Column(6).Width = 20;

            int seq = 1;
            foreach (DataRow dr in dt.Rows)
            {

                string person_name = dr["person_name"].ToString();
                string new_org_name = dr["new_org_name"].ToString();
                string old_level_name = dr["old_level_name"].ToString();
                string new_level_name = dr["new_level_name"].ToString();
                string start_date = dr["start_date"].ToString();

                SetDetailStyle(ws, seq.ToString(), cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetDetailStyle(ws, person_name, cntRow, 2, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                SetDetailStyle(ws, new_org_name, cntRow, 3, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                SetDetailStyle(ws, old_level_name, cntRow, 4, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                SetDetailStyle(ws, new_level_name, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                SetDetailStyle(ws, start_date, cntRow, 6, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);

                cntRow += 1;
                seq += 1;
            }
            dt.DefaultView.RowFilter = "";

            cntRow += 3;
            SetNormalStyle(ws, "วันที่พิมพ์ " + printDate + " น.", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            SetNormalStyle(ws, "ผู้พิมพ์ " + UserName + " " + UserOrg, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);

            ExportExcelToClient(res, ep, "ODS_RP06_1");

        }
        #endregion

    }

    public static void ExportReportODS_RP06_2(string DateFrom, string DateTo, string UserName, string UserOrg, DataTable dt, HttpResponse res)
    {
        #region "ODS_RP06_2"
        using (ExcelPackage ep = new ExcelPackage())
        {
            ExcelWorksheet ws = ep.Workbook.Worksheets.Add("ODS_RP06_2");
            int cntRow = 2;
            SetReportHeaderStyle(ws, "สำนักงานปลัดสำนักนายกรัฐมนตรี", "A" + cntRow, "A" + cntRow + ":F" + cntRow);
            cntRow += 1;

            SetReportHeaderStyle(ws, "รายงานรูปแบบรายงานการแก้ไข ตำแหน่ง ของผู้ใช้ตามช่วงเวลาที่กำหนด", "A" + cntRow, "A" + cntRow + ":F" + cntRow);
            cntRow += 1;

            SetReportHeaderStyle(ws, "ช่วงวันที่" + DateFrom + "ถึง " + DateTo, "A" + cntRow, "A" + cntRow + ":F" + cntRow);
            cntRow += 1;

            SetReportHeaderStyle(ws, "พบรายการทั้งหมด" + dt.Rows.Count.ToString() + "รายการ", "A" + cntRow, "A" + cntRow + ":F" + cntRow);
            cntRow += 1;

            SetRowHeaderStyle(ws, "ลำดับที่", cntRow, 1, "A" + cntRow);
            SetRowHeaderStyle(ws, "ชื่อ-นามสกุล", cntRow, 2, "B" + cntRow);
            SetRowHeaderStyle(ws, "หน่วยงาน", cntRow, 3, "C" + cntRow);
            SetRowHeaderStyle(ws, "ตำแหน่ง(เดิม)", cntRow, 4, "D" + cntRow);
            SetRowHeaderStyle(ws, "ตำแหน่ง(ใหม่)", cntRow, 5, "E" + cntRow);
            SetRowHeaderStyle(ws, "วันที่เปลี่ยน", cntRow, 6, "F" + cntRow);
            cntRow += 1;

            //Set Column Width
            ws.Column(1).Width = 10;
            ws.Column(2).Width = 30;
            ws.Column(3).Width = 30;
            ws.Column(4).Width = 20;
            ws.Column(5).Width = 20;
            ws.Column(6).Width = 20;

            int seq = 1;
            foreach (DataRow dr in dt.Rows)
            {

                string person_name = dr["person_name"].ToString();
                string new_org_name = dr["new_org_name"].ToString();
                string old_pos_name = dr["old_pos_name"].ToString();
                string new_pos_name = dr["new_pos_name"].ToString();
                string start_date = dr["start_date"].ToString();

                SetDetailStyle(ws, seq.ToString(), cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetDetailStyle(ws, person_name, cntRow, 2, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                SetDetailStyle(ws, new_org_name, cntRow, 3, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                SetDetailStyle(ws, old_pos_name, cntRow, 4, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                SetDetailStyle(ws, new_pos_name, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                SetDetailStyle(ws, start_date, cntRow, 6, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);

                cntRow += 1;
                seq += 1;
            }
            dt.DefaultView.RowFilter = "";

            cntRow += 3;
            SetNormalStyle(ws, "วันที่พิมพ์ " + printDate + " น.", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            SetNormalStyle(ws, "ผู้พิมพ์ " + UserName + " " + UserOrg, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);

            ExportExcelToClient(res, ep, "ODS_RP06_2");

        }
        #endregion

    }

    public static void ExportReportODS_RP06_3(string DateFrom, string DateTo, string UserName, string UserOrg, DataTable dt, HttpResponse res)
    {
        #region "ODS_RP06_3"
        using (ExcelPackage ep = new ExcelPackage())
        {
            ExcelWorksheet ws = ep.Workbook.Worksheets.Add("ODS_RP06_3");
            int cntRow = 2;
            SetReportHeaderStyle(ws, "สำนักงานปลัดสำนักนายกรัฐมนตรี", "A" + cntRow, "A" + cntRow + ":F" + cntRow);
            cntRow += 1;

            SetReportHeaderStyle(ws, "รายงานรูปแบบรายงานการแก้ไข หน่วยงาน ของผู้ใช้ตามช่วงเวลาที่กำหนด", "A" + cntRow, "A" + cntRow + ":F" + cntRow);
            cntRow += 1;

            SetReportHeaderStyle(ws, "ช่วงวันที่" + DateFrom + "ถึง " + DateTo, "A" + cntRow, "A" + cntRow + ":F" + cntRow);
            cntRow += 1;

            SetReportHeaderStyle(ws, "พบรายการทั้งหมด" + dt.Rows.Count.ToString() + "รายการ", "A" + cntRow, "A" + cntRow + ":F" + cntRow);
            cntRow += 1;

            SetRowHeaderStyle(ws, "ลำดับที่", cntRow, 1, "A" + cntRow);
            SetRowHeaderStyle(ws, "ชื่อ-นามสกุล", cntRow, 2, "B" + cntRow);
            SetRowHeaderStyle(ws, "หน่วยงาน", cntRow, 3, "C" + cntRow);
            SetRowHeaderStyle(ws, "หน่วยงาน(เดิม)", cntRow, 4, "D" + cntRow);
            SetRowHeaderStyle(ws, "หน่วยงาน(ใหม่)", cntRow, 5, "E" + cntRow);
            SetRowHeaderStyle(ws, "วันที่เปลี่ยน", cntRow, 6, "F" + cntRow);
            cntRow += 1;

            //Set Column Width
            ws.Column(1).Width = 10;
            ws.Column(2).Width = 30;
            ws.Column(3).Width = 30;
            ws.Column(4).Width = 20;
            ws.Column(5).Width = 20;
            ws.Column(6).Width = 20;

            int seq = 1;
            foreach (DataRow dr in dt.Rows)
            {

                string person_name = dr["person_name"].ToString();
                string new_org_name = dr["new_org_name"].ToString();
                string old_org_name = dr["old_org_name"].ToString();
                string start_date = dr["start_date"].ToString();

                SetDetailStyle(ws, seq.ToString(), cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetDetailStyle(ws, person_name, cntRow, 2, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                SetDetailStyle(ws, new_org_name, cntRow, 3, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                SetDetailStyle(ws, old_org_name, cntRow, 4, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                SetDetailStyle(ws, new_org_name, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                SetDetailStyle(ws, start_date, cntRow, 6, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);

                cntRow += 1;
                seq += 1;
            }
            dt.DefaultView.RowFilter = "";

            cntRow += 3;
            SetNormalStyle(ws, "วันที่พิมพ์ " + printDate + " น.", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            SetNormalStyle(ws, "ผู้พิมพ์ " + UserName + " " + UserOrg, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);

            ExportExcelToClient(res, ep, "ODS_RP06_3");

        }
        #endregion

    }
    
    #endregion

    #region "Export Report ODS_RP07"
    public static void ExportReportODS_RP07(string DateFrom, string DateTo, string UserName, string UserOrg, DataTable dt, HttpResponse res)
    {
        #region "ODS_RP07"
        using (ExcelPackage ep = new ExcelPackage())
        {
            ExcelWorksheet ws = ep.Workbook.Worksheets.Add("ODS_RP07");
            int cntRow = 2;

            DataTable dt_pos = new DataTable();
            dt_pos = dt.DefaultView.ToTable(true, "POS_NAME").Copy();
            for (int i = 0; i <= dt_pos.Rows.Count - 1; i++)
            {

                string pos_name = dt_pos.Rows[i]["POS_NAME"].ToString();
                DataTable dt_pos_detail = new DataTable();
                dt.DefaultView.RowFilter = "POS_NAME='" + pos_name + "'";
                dt_pos_detail = dt.DefaultView.ToTable().Copy();

                SetReportHeaderStyle(ws, "รายงานการเข้าใช้งานระบบงานจำแนกตามรหัสผู้ใช้ /IP", "A" + cntRow, "A" + cntRow + ":F" + cntRow);
                cntRow += 1;

                SetReportHeaderStyle(ws, "ตำแหน่ง" + pos_name  + "   ช่วงวันที่" + DateFrom + "ถึง " + DateTo, "A" + cntRow, "A" + cntRow + ":F" + cntRow);
                cntRow += 1;

                SetReportHeaderStyle(ws, "พบรายการทั้งหมด" + dt_pos_detail.Rows.Count.ToString() + "รายการ", "A" + cntRow, "A" + cntRow + ":F" + cntRow);
                cntRow += 1;

                SetRowHeaderStyle(ws, "รหัสผู้ใช้", cntRow, 1, "A" + cntRow);
                SetRowHeaderStyle(ws, "ชื่อ-นามสกุล", cntRow, 2, "B" + cntRow);
                SetRowHeaderStyle(ws, "ตำแหน่ง", cntRow, 3, "C" + cntRow);
                //SetRowHeaderStyle(ws, "ระดับ", cntRow, 4, "D" + cntRow);
                SetRowHeaderStyle(ws, "IP Address", cntRow, 4, "D" + cntRow);
                SetRowHeaderStyle(ws, "วันที่", cntRow, 5, "E" + cntRow);
                cntRow += 1;

                //Set Column Width
                ws.Column(1).Width = 20;
                ws.Column(2).Width = 30;
                ws.Column(3).Width = 40;
                ws.Column(4).Width = 20;
                ws.Column(5).Width = 20;
                //ws.Column(6).Width = 20;

                DataTable dt_org = new DataTable();
                dt_org = dt_pos_detail.DefaultView.ToTable(true, "org_name", "org_abbr").Copy();
                for (int j = 0; j <= dt_org.Rows.Count - 1; j++)
                {
                    string org_name = dt_org.Rows[j]["org_name"].ToString();
                    string org_abbr = dt_org.Rows[j]["org_abbr"].ToString();
                    DataTable dt_detail = new DataTable();
                    dt.DefaultView.RowFilter = "org_name ='" + org_name + "'";
                    dt_detail = dt.DefaultView.ToTable().Copy();

                    SetRowGroupStyle(ws, org_name + "(" + org_abbr + ")", cntRow, 1, "A" + cntRow + ":E" + cntRow, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                    cntRow += 1;

                    int seq = 1;
                    string tmpname = "";
                    foreach (DataRow dr in dt_detail.Rows)
                    {
                        string user_code = dr["user_code"].ToString();
                        string person_name = dr["person_name"].ToString(); 
                        string POS_NAME = dr["POS_NAME"].ToString();
                        string LEVEL_NAME_FULL = dr["LEVEL_NAME_FULL"].ToString();
                        string client_ip = dr["client_ip"].ToString();
                        string created_date = dr["created_date"].ToString();

                        if (tmpname != (user_code + "" + person_name))
                        {
                            tmpname = user_code + "" + person_name;
                        }
                        else
                        {
                            user_code = "";
                            person_name = "";
                        }
                        

                        SetDetailStyle(ws, user_code, cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                        SetDetailStyle(ws, person_name, cntRow, 2, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                        SetDetailStyle(ws, POS_NAME + LEVEL_NAME_FULL, cntRow, 3, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                        //SetDetailStyle(ws, LEVEL_NAME_FULL, cntRow, 4, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                        SetDetailStyle(ws, client_ip, cntRow, 4, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                        SetDetailStyle(ws, created_date, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);

                        cntRow += 1;
                        seq += 1;

                    }//dt_detail
                    dt_detail.Dispose();
                    dt.DefaultView.RowFilter = "";

                }//dt_org
                dt_org.Dispose();

            }

            dt.Dispose();
            

            cntRow += 3;
            SetNormalStyle(ws, "วันที่พิมพ์ " + printDate + " น.", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            SetNormalStyle(ws, "ผู้พิมพ์ " + UserName + " " + UserOrg, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);

            ExportExcelToClient(res, ep, "ODS_RP07");

        }
        #endregion

    }
    #endregion

    #region "Export Report ODS_RP08"
    public static void ExportReportODS_RP08(string DateFrom, string DateTo, string UserName, string UserOrg, DataTable dt, HttpResponse res)
    {
        #region "ODS_RP08"
        using (ExcelPackage ep = new ExcelPackage())
        {
            ExcelWorksheet ws = ep.Workbook.Worksheets.Add("ODS_RP08");
            int cntRow = 2;

            SetReportHeaderStyle(ws, "รายงานสถิติการใช้งานระบบสารสนเทศ สปน.", "A" + cntRow, "A" + cntRow + ":E" + cntRow);
            cntRow += 1;

            SetReportHeaderStyle(ws, "ช่วงวันที่" + DateFrom + "ถึง " + DateTo, "A" + cntRow, "A" + cntRow + ":E" + cntRow);
            cntRow += 1;

            SetReportHeaderStyle(ws, "พบรายการทั้งหมด" + dt.Rows.Count.ToString() + "รายการ", "A" + cntRow, "A" + cntRow + ":E" + cntRow);
            cntRow += 1;

            SetRowHeaderStyle(ws, "ลำดับที่", cntRow, 1, "A" + cntRow);
            SetRowHeaderStyle(ws, "ชื่อย่อระบบ", cntRow, 2, "B" + cntRow);
            SetRowHeaderStyle(ws, "ชื่อระบบ(อังกฤษ)", cntRow, 3, "C" + cntRow);
            SetRowHeaderStyle(ws, "ชื่อระบบ(ไทย)", cntRow, 4, "D" + cntRow);
            SetRowHeaderStyle(ws, "สถิติ(จำนวนครั้ง)", cntRow, 5, "E" + cntRow);
            cntRow += 1;

            //Set Column Width
            ws.Column(1).Width = 10;
            ws.Column(2).Width = 20;
            ws.Column(3).Width = 30;
            ws.Column(4).Width = 30;
            ws.Column(5).Width = 20;

            int seq = 1;
            foreach (DataRow dr in dt.Rows)
            {
                string sys_code = dr["sys_code"].ToString();
                string sys_name_eng = dr["sys_name_eng"].ToString();
                string sys_name = dr["sys_name"].ToString();
                string login_qty = dr["login_qty"].ToString();

                SetDetailStyle(ws, seq.ToString(), cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetDetailStyle(ws, sys_code, cntRow, 2, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetDetailStyle(ws, sys_name_eng, cntRow, 3, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                SetDetailStyle(ws, sys_name, cntRow, 4, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                SetDetailStyle(ws, login_qty, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);

                cntRow += 1;
                seq += 1;

            }
            dt.Dispose();

            cntRow += 3;
            SetNormalStyle(ws, "วันที่พิมพ์ " + printDate + " น.", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            SetNormalStyle(ws, "ผู้พิมพ์ " + UserName + " " + UserOrg, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);

            ExportExcelToClient(res, ep, "ODS_RP08");

        }
        #endregion

    }
    #endregion

    #region "Export Report ODS_RP09"
    public static void ExportReportODS_RP09(string DateFrom, string DateTo, string UserName, string UserOrg, DataTable dt, HttpResponse res)
    {
        #region "ODS_RP09"
        using (ExcelPackage ep = new ExcelPackage())
        {
            ExcelWorksheet ws = ep.Workbook.Worksheets.Add("ODS_RP09");
            int cntRow = 2;

            SetReportHeaderStyle(ws, "รายงานรายชื่อและสถิติการเข้าใช้งานระบบสารสนเทศ สปน.", "A" + cntRow, "A" + cntRow + ":Q" + cntRow);
            cntRow += 1;

            SetReportHeaderStyle(ws, "ช่วงวันที่" + DateFrom + "ถึง " + DateTo, "A" + cntRow, "A" + cntRow + ":Q" + cntRow);
            cntRow += 1;

            SetReportHeaderStyle(ws, "พบรายการทั้งหมด" + dt.Rows.Count.ToString() + "รายการ", "A" + cntRow, "A" + cntRow + ":Q" + cntRow);
            cntRow += 1;

            SetRowHeaderStyle(ws, "ชื่อ-สกุล", cntRow, 1, "A" + cntRow + ":A" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "ตำแหน่ง", cntRow, 2, "B" + cntRow + ":B" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "ระบบ(จำนวนครั้ง)", cntRow,3, "C" + cntRow + ":Q" + cntRow);

            SetRowHeaderStyle(ws, "HRM", (cntRow + 1), 3, "C" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "PDM", (cntRow + 1), 4, "D" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "SMM", (cntRow + 1), 5, "E" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "ODS", (cntRow + 1), 6, "F" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "CRS", (cntRow + 1), 7, "G" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "CCB", (cntRow + 1), 8, "H" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "MRM", (cntRow + 1), 9, "I" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "RCS", (cntRow + 1), 10, "J" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "PRS", (cntRow + 1), 11, "K" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "BIS", (cntRow + 1), 12, "L" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "EIS", (cntRow + 1), 13, "M" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "RMS", (cntRow + 1), 14, "N" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "PFS", (cntRow + 1), 15, "O" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "PLN", (cntRow + 1), 16, "P" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "FTS", (cntRow + 1), 17, "Q" + (cntRow + 1).ToString());

            cntRow += 2;

            //Set Column Width
            ws.Column(1).Width = 30;
            ws.Column(2).Width = 30;
            ws.Column(3).Width = 10;
            ws.Column(4).Width = 10;
            ws.Column(5).Width = 10;
            ws.Column(6).Width = 10;
            ws.Column(7).Width = 10;
            ws.Column(8).Width = 10;
            ws.Column(9).Width = 10;
            ws.Column(10).Width = 10;
            ws.Column(11).Width = 10;
            ws.Column(12).Width = 10;
            ws.Column(13).Width = 10;
            ws.Column(14).Width = 10;
            ws.Column(15).Width = 10;
            ws.Column(16).Width = 10;
            ws.Column(17).Width = 10;

            DataTable dt_org = new DataTable();
            dt_org = dt.DefaultView.ToTable(true, "org_name").Copy();
            for (int i = 0; i <= dt_org.Rows.Count - 1; i++)
            {
                string org_name = dt_org.Rows[i]["org_name"].ToString();
                DataTable dt_detail = new DataTable();
                dt.DefaultView.RowFilter = "org_name ='" + org_name + "'";
                dt_detail = dt.DefaultView.ToTable().Copy();

                SetRowGroupStyle(ws, org_name, cntRow, 1, "A" + cntRow + ":Q" + cntRow, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                cntRow += 1;


                int SUM_HRM = 0;
                int SUM_PDM = 0;
                int SUM_SMM = 0;
                int SUM_ODS = 0;
                int SUM_CRS = 0;
                int SUM_CCB = 0;
                int SUM_MRM = 0;
                int SUM_RCS = 0;
                int SUM_PRS = 0;
                int SUM_BIS = 0;
                int SUM_EIS = 0;
                int SUM_RMS = 0;
                int SUM_PFS = 0;
                int SUM_PLN = 0;
                int SUM_FTS = 0;

                int seq = 1;
                foreach (DataRow dr in dt_detail.Rows)
                {
                    string fullname = dr["person_name"].ToString();
                    string pos_name = dr["pos_name"].ToString();

                    int HRM = Convert.ToInt16(dr["HRM"]);
                    int PDM = Convert.ToInt16(dr["PDM"]);
                    int SMM = Convert.ToInt16(dr["SMM"]);
                    int ODS = Convert.ToInt16(dr["ODS"]);
                    int CRS = Convert.ToInt16(dr["CRS"]);
                    int CCB = Convert.ToInt16(dr["CCB"]);
                    int MRM = Convert.ToInt16(dr["MRM"]);
                    int RCS = Convert.ToInt16(dr["RCS"]);
                    int PRS = Convert.ToInt16(dr["PRS"]);
                    int BIS = Convert.ToInt16(dr["BIS"]);
                    int EIS = Convert.ToInt16(dr["EIS"]);
                    int RMS = Convert.ToInt16(dr["RMS"]);
                    int PFS = Convert.ToInt16(dr["PFS"]);
                    int PLN = Convert.ToInt16(dr["PLN"]);
                    int FTS = Convert.ToInt16(dr["FTS"]);

                    SetDetailStyle(ws, fullname, cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    SetDetailStyle(ws, pos_name, cntRow, 2, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    SetDetailStyle(ws, HRM.ToString(), cntRow, 3, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                    SetDetailStyle(ws, PDM.ToString(), cntRow, 4, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                    SetDetailStyle(ws, SMM.ToString(), cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                    SetDetailStyle(ws, ODS.ToString(), cntRow, 6, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                    SetDetailStyle(ws, CRS.ToString(), cntRow, 7, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                    SetDetailStyle(ws, CCB.ToString(), cntRow, 8, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                    SetDetailStyle(ws, MRM.ToString(), cntRow, 9, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                    SetDetailStyle(ws, RCS.ToString(), cntRow, 10, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                    SetDetailStyle(ws, PRS.ToString(), cntRow, 11, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                    SetDetailStyle(ws, BIS.ToString(), cntRow, 12, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                    SetDetailStyle(ws, EIS.ToString(), cntRow, 13, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                    SetDetailStyle(ws, RMS.ToString(), cntRow, 14, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                    SetDetailStyle(ws, PFS.ToString(), cntRow, 15, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                    SetDetailStyle(ws, PLN.ToString(), cntRow, 16, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                    SetDetailStyle(ws, FTS.ToString(), cntRow, 17, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);

                    cntRow += 1;
                    seq += 1;

                    SUM_HRM += HRM;
                    SUM_PDM += PDM;
                    SUM_SMM += SMM;
                    SUM_ODS += ODS;
                    SUM_CRS += CRS;
                    SUM_CCB += CCB;
                    SUM_MRM += MRM;
                    SUM_RCS += RCS;
                    SUM_PRS += PRS;
                    SUM_BIS += BIS;
                    SUM_EIS += EIS;
                    SUM_RMS += RMS;
                    SUM_PFS += PFS;
                    SUM_PLN += PLN;
                    SUM_FTS += FTS;

                }//dt_detail
                dt_detail.Dispose();
                dt.DefaultView.RowFilter = "";


                SetRowGroupStyle(ws, "รวม", cntRow, 1, "A" + cntRow + ":B" + cntRow, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetRowGroupStyle(ws,SUM_HRM.ToString(), cntRow, 3, "C" + cntRow , OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetRowGroupStyle(ws,SUM_PDM.ToString(), cntRow, 4, "D" + cntRow , OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetRowGroupStyle(ws, SUM_SMM.ToString(), cntRow, 5, "E" + cntRow, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetRowGroupStyle(ws, SUM_ODS.ToString(), cntRow, 6, "F" + cntRow, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetRowGroupStyle(ws, SUM_CRS.ToString(), cntRow, 7, "G" + cntRow, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetRowGroupStyle(ws, SUM_CCB.ToString(), cntRow, 8, "H" + cntRow, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetRowGroupStyle(ws, SUM_MRM.ToString(), cntRow, 9, "I" + cntRow, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetRowGroupStyle(ws, SUM_RCS.ToString(), cntRow, 10, "J" + cntRow, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetRowGroupStyle(ws, SUM_PRS.ToString(), cntRow, 11, "K" + cntRow, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetRowGroupStyle(ws, SUM_BIS.ToString(), cntRow, 12, "L" + cntRow, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetRowGroupStyle(ws, SUM_EIS.ToString(), cntRow, 13, "M" + cntRow, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetRowGroupStyle(ws, SUM_RMS.ToString(), cntRow, 14, "N" + cntRow, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetRowGroupStyle(ws, SUM_PFS.ToString(), cntRow, 15, "O" + cntRow, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetRowGroupStyle(ws, SUM_PLN.ToString(), cntRow, 16, "P" + cntRow, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetRowGroupStyle(ws, SUM_FTS.ToString(), cntRow, 17, "Q" + cntRow, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                cntRow += 1;



            }//dt_org
            dt_org.Dispose();

            cntRow += 2;
            SetNormalStyle(ws, "หมายเหตุ ", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            cntRow += 1;

            SetNormalStyle(ws, "ชื่อระบบทั้งหมด ดังนี้", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            cntRow += 1;

            SetNormalStyle(ws, "1. ระบบงานบริหารงานบุคคล HRM (Human Resource Management System)", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            cntRow += 1;

            SetNormalStyle(ws, "2. ระบบบริหารจัดการ Personal Directory และ Time Directory PDM (Personal Directory Management System)", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            cntRow += 1;

            SetNormalStyle(ws, "3. ระบบงานบริหารโครงสร้างหน่วยและอัตรากำลัง SMM (Structure & Manpower Management System)", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            cntRow += 1;

            SetNormalStyle(ws, "4. ระบบงานกำหนดสิทธิ์ผู้ใช้ ODS (Open Directory System)", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            cntRow += 1;

            SetNormalStyle(ws, "5. ระบบงานการฌาปนกิจสงเคราะห์ CRS (Cremation System)", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            cntRow += 1;

            SetNormalStyle(ws, "6. ระบบงานการขอใช้รถยนต์ส่วนกลาง CCB (Central Car Book System)", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            cntRow += 1;

            SetNormalStyle(ws, "7. ระบบงานสื่อประชาสัมพันธ์เสริมสร้างเอกลักษณ์ของชาติ MRM (Media Releases Management System)", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            cntRow += 1;

            SetNormalStyle(ws, "8. ระบบงานเผยแพร่คำสั่ง RCS (Releases Command System)", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            cntRow += 1;

            SetNormalStyle(ws, "9. ระบบงานประชาสัมพันธ์ PRS (Public Relation System)", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            cntRow += 1;

            SetNormalStyle(ws, "10. ระบบข้อมูลคณะกรรมการ BIS (Board Information System)", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            cntRow += 1;

            SetNormalStyle(ws, "11. ระบบข้อมูลกิจการพิเศษ EIS (Errand Information System)", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            cntRow += 1;

            SetNormalStyle(ws, "12. ระบงานจองห้องประชุม RMS (Reservation Meeting System)", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            cntRow += 1;

            SetNormalStyle(ws, "13. ระบบกองทุนสำนักนายกรัฐมนตรี PFS (Prime Fund System)", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            cntRow += 1;

            SetNormalStyle(ws, "14. ระบบงานแผนและการติดตามผลการปฏิบัติ PLN (Plan & Operation  Tracking System)", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            cntRow += 1;

            SetNormalStyle(ws, "15. ระบบงานข้อมูลการติดตามเงินขาดบัญชีและการทุจริต FTS (Fraud Tracking System)", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            cntRow += 1;

            cntRow += 3;
            SetNormalStyle(ws, "วันที่พิมพ์ " + printDate + " น.", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            SetNormalStyle(ws, "ผู้พิมพ์ " + UserName + " " + UserOrg, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);

            ExportExcelToClient(res, ep, "ODS_RP09");

        }
        #endregion

    }
    #endregion

    #endregion

    #region "__DPIS"
    #region "Export Report ReportDPIS_RP01"
    public static void ExportReportDPIS_RP01(string UserName, string UserOrg, DataTable dt, HttpResponse res)
    {
        using (ExcelPackage ep = new ExcelPackage())
        {
            ExcelWorksheet ws = ep.Workbook.Worksheets.Add("DPIS_RP01");

           
            int cntRow = 2;
            SetReportHeaderStyle(ws, "รายงานการจัดการการเชื่อมโยงข้อมูล  ระบบงาน สปน.", "A" + cntRow, "A" + cntRow + ":H" + cntRow);
            cntRow += 1;


            SetRowHeaderStyle(ws, "ลำดับที่", cntRow, 1, "A" + cntRow);
            SetRowHeaderStyle(ws, "ชื่อระบบ", cntRow, 2, "B" + cntRow);
            SetRowHeaderStyle(ws, "ประเภทฐานข้อมูล", cntRow, 3, "C" + cntRow);
            SetRowHeaderStyle(ws, "ชื่อเครื่อง", cntRow, 4, "D" + cntRow);
            SetRowHeaderStyle(ws, "พอร์ต", cntRow, 5, "E" + cntRow);
            SetRowHeaderStyle(ws, "ชื่อฐานข้อมูล", cntRow, 6, "F" + cntRow);
            SetRowHeaderStyle(ws, "รหัสผู้ใช้", cntRow, 7, "G" + cntRow);
            SetRowHeaderStyle(ws, "รหัสผ่าน", cntRow, 8, "H" + cntRow);
            SetRowHeaderStyle(ws, "สถานะ", cntRow, 9, "I" + cntRow);
            cntRow += 1;

            //Set Column Width
            ws.Column(1).Width = 10;
            ws.Column(2).Width = 30;
            ws.Column(3).Width = 30;
            ws.Column(4).Width = 30;
            ws.Column(5).Width = 30;
            ws.Column(6).Width = 30;
            ws.Column(7).Width = 30;
            ws.Column(8).Width = 30;
            ws.Column(9).Width = 30;

          

            int seq = 1;
            foreach (DataRow dr in dt.Rows)
            {
               string system_code = dr["system_code"].ToString();
               string db_type = dr["db_type"].ToString();
               string serverHost = dr["serverHost"].ToString();
               string ServerPort = dr["ServerPort"].ToString();
               string databasename = dr["databasename"].ToString();
               string databaseuserid = dr["databaseuserid"].ToString();
               string databasepassword = dr["databasepassword"].ToString();
               string active_status = dr["active_status"].ToString();
             
                SetDetailStyle(ws, seq.ToString(), cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetDetailStyle(ws, system_code, cntRow, 2, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                SetDetailStyle(ws, db_type, cntRow, 3, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                SetDetailStyle(ws, serverHost, cntRow, 4, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetDetailStyle(ws, ServerPort, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetDetailStyle(ws, databasename, cntRow, 6, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                SetDetailStyle(ws, databaseuserid, cntRow, 7, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                SetDetailStyle(ws, databasepassword, cntRow, 8, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                SetDetailStyle(ws, active_status, cntRow, 9, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);

              
                seq += 1;
                cntRow += 1;
            }

            cntRow += 4;

            SetNormalStyle(ws, "วันที่พิมพ์ " + printDate + " น.", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            SetNormalStyle(ws, "ผู้พิมพ์ " + UserName + " " + UserOrg, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);


            ExportExcelToClient(res, ep, "DPIS_RP01");

        }
    }
    #endregion

    #region "Export Report ReportDPIS_RP02"
    public static void ExportReportDPIS_RP02(string DateFrom,string DateTo,string UserName, string UserOrg, DataTable dt, HttpResponse res)
    {
        using (ExcelPackage ep = new ExcelPackage())
        {
            ExcelWorksheet ws = ep.Workbook.Worksheets.Add("DPIS_RP02");

           
            int cntRow = 2;
            SetReportHeaderStyle(ws, "รายงาน Log การเชื่อมโยงข้อมูล ระบบงาน สปน.", "A" + cntRow, "A" + cntRow + ":H" + cntRow);
            cntRow += 1;

            SetReportHeaderStyle(ws, "ช่วงดำเนินการ" + DateFrom + "  ถึง" + DateTo, "A" + cntRow, "A" + cntRow + ":H" + cntRow);
            cntRow += 1;

            SetRowHeaderStyle(ws, "วันที่", cntRow, 1, "A" + cntRow + ":A" + (cntRow+1).ToString());
            SetRowHeaderStyle(ws, "ชื่อระบบ", cntRow, 2, "B" + cntRow + ":C" + cntRow);
            SetRowHeaderStyle(ws, "ต้นทาง", cntRow + 1, 2, "B" + (cntRow+1).ToString() );
            SetRowHeaderStyle(ws, "ปลายทาง", cntRow + 1, 3, "C" + (cntRow+1).ToString());
            SetRowHeaderStyle(ws, "ประเภทข้อมูล", cntRow , 4, "D" + cntRow + ":D" + (cntRow + 1).ToString());
            SetRowHeaderStyle(ws, "ข้อมูล", cntRow, 5, "E" + cntRow + ":E" + (cntRow + 1).ToString());
            cntRow += 2;

            //Set Column Width
            ws.Column(1).Width = 30;
            ws.Column(2).Width = 20;
            ws.Column(3).Width = 20;
            ws.Column(4).Width = 20;
            ws.Column(5).Width = 50;
          

            int seq = 1;
            foreach (DataRow dr in dt.Rows)
            {
                
               string created_date = dr["created_date"].ToString();
               string sys_code = dr["sys_code"].ToString();
               string to_sys_code = dr["to_sys_code"].ToString();
               string action = dr["action"].ToString();
               string action_desc = dr["action_desc"].ToString();
             
                SetDetailStyle(ws, created_date, cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetDetailStyle(ws, sys_code, cntRow, 2, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                SetDetailStyle(ws, to_sys_code, cntRow, 3, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                SetDetailStyle(ws, action, cntRow, 4, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                SetDetailStyle(ws, action_desc, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                             
                seq += 1;
                cntRow += 1;
            }

            cntRow += 4;

            
            SetNormalStyle(ws, "วันที่พิมพ์ " + printDate + " น.", cntRow, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
            SetNormalStyle(ws, "ผู้พิมพ์ " + UserName + " " + UserOrg, cntRow, 5, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);


            ExportExcelToClient(res, ep, "DPIS_RP02");

        }
    }
    #endregion

    #endregion

    #region "Export Excel to Client"
    private static void ExportExcelToClient(HttpResponse res, ExcelPackage ep, string FileName) {
        try
        {
            //Write it back to the client
            res.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            res.AddHeader("content-disposition", "attachment;  filename=" + FileName + ".xlsx");
            res.BinaryWrite(ep.GetAsByteArray());
            res.End();
        }
        catch (Exception ex) { }
    }
    #endregion

    #region "Report Style"

    private static void SetTextStyle(ExcelWorksheet ws, string txt, string cell, bool FontBold, float FontSize, OfficeOpenXml.Style.ExcelHorizontalAlignment TextAlign) {
        ws.Cells[cell].Value = txt;
        using (ExcelRange h = ws.Cells[cell])
        {
            h.Style.Font.Bold = FontBold;
            h.Style.Font.Name = "TH SarabunPSK";
            h.Style.Font.Size = FontSize;
            h.Style.HorizontalAlignment = TextAlign;
            h.AutoFitColumns();
        }
    }

    private static void SetCellBorder(ExcelWorksheet ws, string CellRange, bool MergeCell) {
        ws.Cells[CellRange].Merge = MergeCell;
        using (ExcelRange h = ws.Cells[CellRange]) {
            h.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
        }
    }

    private static void SetReportHeaderStyle(ExcelWorksheet ws, string txt, string cell, string MergeRang)
    {
        ws.Cells[cell].Value = txt;
        ws.Cells[MergeRang].Merge = true;
        using (ExcelRange h = ws.Cells[cell])
        {
            h.Style.Font.Bold = true;
            h.Style.Font.Name = "TH SarabunPSK";
            h.Style.Font.Size = 20;
            h.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
        }
    }

    private static void SetNormalStyle(ExcelWorksheet ws, string txt, int iRow, int iCol, OfficeOpenXml.Style.ExcelHorizontalAlignment AlignMent) {
        ws.Cells[iRow, iCol].Value = txt;
        using (ExcelRange h = ws.Cells[iRow, iCol])
        {
            h.Style.Font.Bold = true;
            h.Style.Font.Name = "TH SarabunPSK";
            h.Style.Font.Size = 16;
            h.Style.HorizontalAlignment = AlignMent;
        }
    }

    private static void SetRowHeaderStyle(ExcelWorksheet ws, string txt, int iRow, int iCol, string MergeRang)
    {
        ws.Cells[iRow,iCol].Value = txt;
        ws.Cells[MergeRang].Merge = true;
        ws.Cells[MergeRang].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
        using (ExcelRange h = ws.Cells[iRow, iCol])
        {
            h.Style.Font.Bold = true;
            h.Style.Font.Name = "TH SarabunPSK";
            h.Style.Font.Size = 16;
            h.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
        }
    }

    private static void SetRowHeaderStyle(ExcelWorksheet ws, string txt, int iRow, int iCol)
    {
        ws.Cells[iRow, iCol].Value = txt;
        ws.Cells[iRow, iCol].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
        using (ExcelRange h = ws.Cells[iRow, iCol])
        {
            h.Style.Font.Bold = true;
            h.Style.Font.Name = "TH SarabunPSK";
            h.Style.Font.Size = 16;
            h.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
        }
    }

    private static void SetDetailStyle(ExcelWorksheet ws, string txt, int iRow, int iCol,  OfficeOpenXml.Style.ExcelHorizontalAlignment AlignMent)
    {
        ws.Cells[iRow,iCol].Value = txt;
        using (ExcelRange h = ws.Cells[iRow, iCol])
        {
            h.Style.Font.Name = "TH SarabunPSK";
            h.Style.Font.Size = 16;
            h.Style.HorizontalAlignment = AlignMent;
            h.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
        }

    }

    private static void SetNumberValueStyle(ExcelWorksheet ws, int intValue, int iRow, int iCol, OfficeOpenXml.Style.ExcelHorizontalAlignment AlignMent)
    {
        ws.Cells[iRow, iCol].Value = intValue;
        using (ExcelRange h = ws.Cells[iRow, iCol])
        {
            h.Style.Font.Name = "TH SarabunPSK";
            h.Style.Font.Size = 16;
            h.Style.HorizontalAlignment = AlignMent;
            h.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
        }

    }

    private static void SetRowGroupStyle(ExcelWorksheet ws, string txt, int iRow, int iCol, string MergeRang, OfficeOpenXml.Style.ExcelHorizontalAlignment AlignMent)
    {
        ws.Cells[iRow, iCol].Value = txt;
        ws.Cells[MergeRang].Merge = true;
        ws.Cells[MergeRang].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
        using (ExcelRange h = ws.Cells[iRow, iCol])
        {
            h.Style.Font.Bold = true;
            h.Style.Font.Name = "TH SarabunPSK";
            h.Style.Font.Size = 16;
            h.Style.HorizontalAlignment = AlignMent;
        }
    }
    #endregion
}
