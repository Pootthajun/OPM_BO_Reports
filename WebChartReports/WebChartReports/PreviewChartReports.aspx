﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PreviewChartReports.aspx.cs" Inherits="PreviewChartReports" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Chart ID="Chart1" runat="server"  Width="500px" Height="400px" BorderlineColor="Black"  >
            <ChartAreas>
                <asp:ChartArea Name="ChartArea1">
                    <AxisY Title="จำนวนคน">
                    
                    </AxisY>
                </asp:ChartArea>
            </ChartAreas>
            <Legends>
                <asp:Legend Alignment="Center" Docking="Bottom" IsTextAutoFit="False" Name="Default" LegendStyle="Row" />
            </Legends>
            
        </asp:Chart>
    </div>
    
    </form>
</body>
</html>
