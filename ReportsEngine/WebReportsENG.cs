﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Configuration;


namespace ReportsEngine
{
    public class WebReportsENG
    {
        private static string DefaultConnectionString = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();

        #region "Report Query"
        #region "HRM"
        public static DataTable GetQueryReportHRM_UT0612(HttpRequest res)
        {
            string TypeName = res.QueryString["TypeName"];
            string ORG = res.QueryString["ORG"];
            string DateFrom = res.QueryString["DateFrom"];
            string DateTo = res.QueryString["DateTo"];
            string MsLeaveType = res.QueryString["leave_type_id"];
            string UserName = res.QueryString["UserName"];
            string UserOrg = res.QueryString["UserOrg"];
            string BudgetYear = res.QueryString["BudgetYear"];
            string FormatType = res.QueryString["FormatType"];

            //TypeName = "11";
            //DateFrom = "20121001";//"20140101";
            //DateTo = "20140930"; //"20140501";
            //MsLeaveType = "2";
            //UserName = "99999ชื่อสกุล99999";
            //UserOrg = "xxxxxหน่วยงานxxxxx";
            //ORG = "3";//"36";
            //BudgetYear = "2556";  //กรณีเลือกเดือนเดียว ไม่ต้องระบุปีงบประมาณ
            //FormatType = "BAR_GRAPH"; //TABLE, BAR_GRAPH, LINE_GRAPH

            DataTable dt = new DataTable();
            try
            {
                string sql = " select p.per_type, p.per_type_name, lt.leave_type_name, o.org_serial, isnull(o.name_level4,'') + ' ' + isnull(o.name_level3,'') org_name,l.leave_name, " + Environment.NewLine;
                sql += " count(distinct p.id) person_qty, sum(h.leave_day) day_qty, count(h.id) leave_qty " + Environment.NewLine;
                sql += " from   CTLT_ORGANIZE o  " + Environment.NewLine;
                sql += " inner join vw_cmn_person p on p.org_serial=o.org_serial " + Environment.NewLine;
                sql += " inner join HRM_LEAVE_HIS h on p.id=h.person_id  " + Environment.NewLine;
                sql += " inner join PDM_MS_LEAVE l on l.leave_id=h.leave_id " + Environment.NewLine;
                sql += " inner join PDM_MS_LEAVE_TYPE lt on l.leave_type_id=lt.leave_type_id " + Environment.NewLine;
                sql += " where 1=1  " + Environment.NewLine;
                
                SqlParameter[] param = new SqlParameter[5];
                param[0] = setParameter("@_DATE_FROM", SqlDbType.VarChar, DateFrom);
                param[1] = setParameter("@_DATE_TO", SqlDbType.VarChar, DateTo);
                if (TypeName != null)
                {
                    sql += " and p.PER_TYPE = @_PER_TYPE " + Environment.NewLine;
                    param[2] = setParameter("@_PER_TYPE", SqlDbType.VarChar, TypeName);
                }
                if (ORG != null)
                {
                    sql += " and o.org_serial= @_ORG_SERIAL " + Environment.NewLine;
                    param[3] = setParameter("@_ORG_SERIAL", SqlDbType.BigInt, Convert.ToInt64(ORG));
                }
                sql += " and convert(varchar(8),h.[start_date],112) between @_DATE_FROM and @_DATE_TO " + Environment.NewLine;
                sql += " and l.leave_type_id=@_LEAVE_TYPE_ID";
                param[4] = setParameter("@_LEAVE_TYPE_ID", SqlDbType.Int, Convert.ToInt16(MsLeaveType));

                sql += " group by p.per_type, p.per_type_name, lt.leave_type_name, o.org_serial, isnull(o.name_level4,'') + ' ' + isnull(o.name_level3,''),l.leave_name " + Environment.NewLine;
                sql += " order by isnull(o.name_level4,'') + ' ' + isnull(o.name_level3,'')";

                dt = GetDatatable(sql, param);

            }
            catch (Exception ex) {
                dt = new DataTable();
            }

            return dt;
        }

        public static DataTable GetQueryReportHRM_UT0613(HttpRequest req) {
            
            string TypeName = req.QueryString["TypeName"];
            string ORG = req.QueryString["ORG"];
            string DateFrom = req.QueryString["DateFrom"];
            string DateTo = req.QueryString["DateTo"];
            string MsLeaveType = req.QueryString["leave_type_id"];
            string UserName = req.QueryString["UserName"];
            string UserOrg = req.QueryString["UserOrg"];
            string BudgetYear = req.QueryString["BudgetYear"];
            string FormatType = req.QueryString["FormatType"];

            //TypeName = "11";
            //DateFrom = "20121001";//"20140101";
            //DateTo = "20140930"; //"20140501";
            //MsLeaveType = "2";
            //UserName = "99999ชื่อสกุล99999";
            //UserOrg = "xxxxxหน่วยงานxxxxx";
            //ORG = "3";//"36";
            //BudgetYear = "2556";  //กรณีเลือกเดือนเดียว ไม่ต้องระบุปีงบประมาณ
            //FormatType = "BAR_GRAPH"; //TABLE, BAR_GRAPH, LINE_GRAPH

            DataTable dt = new DataTable();

            try {
                string sql = "select distinct p.per_type, p.per_type_name, lt.leave_type_name, o.org_serial, o.org_name, " + Environment.NewLine;
                sql += " year(h.start_date) rep_year, month(h.start_date) rep_month, l.leave_name," + Environment.NewLine;
                sql += " count(distinct p.id) person_qty, sum(h.leave_day) day_qty, count(h.id) leave_qty, " + Environment.NewLine;
                sql += " (select [dbo].[ThaiMonthYear] (month(h.start_date),year(h.start_date)) )  str_start_date" + Environment.NewLine;
                sql += " from   CTLT_ORGANIZE o  " + Environment.NewLine;
                sql += " inner join vw_cmn_person p on p.org_serial=o.org_serial " + Environment.NewLine;
                sql += " inner join HRM_LEAVE_HIS h on p.id=h.person_id  " + Environment.NewLine;
                sql += " inner join PDM_MS_LEAVE l on l.leave_id=h.leave_id " + Environment.NewLine;
                sql += " inner join PDM_MS_LEAVE_TYPE lt on l.leave_type_id=lt.leave_type_id " + Environment.NewLine;
                sql += " where 1=1  " + Environment.NewLine;

                SqlParameter[] param = new SqlParameter[5];
                param[0] = setParameter("@_DATE_FROM", SqlDbType.VarChar, DateFrom);
                param[1] = setParameter("@_DATE_TO", SqlDbType.VarChar, DateTo);
                if (!string.IsNullOrEmpty(TypeName))
                {
                    sql += " and p.PER_TYPE = @_PER_TYPE " + Environment.NewLine;
                    param[2] = setParameter("@_PER_TYPE", SqlDbType.VarChar, TypeName);
                }
                if (!string.IsNullOrEmpty(ORG))
                {
                    sql += " and o.org_serial= @_ORG_SERIAL " + Environment.NewLine;
                    param[3] = setParameter("@_ORG_SERIAL", SqlDbType.BigInt, Convert.ToInt64(ORG));
                }
                sql += " and convert(varchar(8),h.[start_date],112) between @_DATE_FROM and @_DATE_TO " + Environment.NewLine;
                sql += " and lt.leave_type_id=@_LEAVE_TYPE_ID";
                param[4] = setParameter("@_LEAVE_TYPE_ID", SqlDbType.Int, Convert.ToInt16(MsLeaveType));

                sql += " group by p.per_type, p.per_type_name, lt.leave_type_name, o.org_serial, o.org_name,l.leave_name, " + Environment.NewLine;
                sql += " year(h.start_date) , month(h.start_date) " + Environment.NewLine;
                sql += " order by  rep_year,rep_month,leave_type_name";

                dt = GetDatatable(sql, param);
            }
            catch (Exception ex) {
                dt = new DataTable();
            }
            return dt;
        }

        public static DataTable GetQueryReportHRM_UT0614(HttpRequest req) {
            string TypeName = req.QueryString["TypeName"];
            string ORG = req.QueryString["org_serial"];
            string differentiate = req.QueryString["differentiate"];
            string differentiate_name = req.QueryString["differentiate_name"];
            string PerTypeName = req.QueryString["PerTypeName"];
            string UserName = req.QueryString["UserName"];
            string UserOrg = req.QueryString["UserOrg"];
            string FormatType = req.QueryString["FormatType"];

            DataTable dt = new DataTable();
            try {
                string sql = "";
                switch (differentiate)
                {
                    case "SEX":  //เพศ
                        sql = "select p.org_name, case p.sex when '1' then 'ชาย' when '2' then 'หญิง' end differentiate , count(p.id) person_qty " + Environment.NewLine;
                        sql += " from vw_cmn_person p " + Environment.NewLine;
                        sql += " where per_status='1' " + Environment.NewLine;
                        sql += " and p.org_name is not null " + Environment.NewLine;
                        sql += " and p.per_type = @_PER_TYPE " + Environment.NewLine;
                        sql += " and p.org_serial=@_ORG_SERIAL " + Environment.NewLine;
                        sql += " group by p.org_name, case p.sex when '1' then 'ชาย' when '2' then 'หญิง' end " + Environment.NewLine;
                        sql += " order by p.org_name";
                        break;

                    case "DEGREE": //ระดับการศึกษา
                        sql = " select po.org_serial,o.org_name, edl.educ_lev_name differentiate , count(p.id) person_qty " + Environment.NewLine;
                        sql += " from psst_person p " + Environment.NewLine;
                        sql += " inner join psst_position po on p.id=po.id " + Environment.NewLine;
                        sql += " inner join ctlt_organize o on po.org_serial=o.org_serial " + Environment.NewLine;
                        sql += " inner join psst_educ_code edu on edu.educ_code=p.educ_code " + Environment.NewLine;
                        sql += " inner join psst_educ_lev  edl on edl.educ_lev=edu.educ_lev " + Environment.NewLine;
                        sql += " where per_status='1' " + Environment.NewLine;
                        sql += " and p.per_type = @_PER_TYPE " + Environment.NewLine;
                        sql += " and o.org_serial=@_ORG_SERIAL " + Environment.NewLine;
                        sql += " group by  po.org_serial,o.org_name, edl.educ_lev_name " + Environment.NewLine;
                        sql += " order by o.org_name";
                        break;

                    case "LINE":  //ตำแหน่งในสายงาน
                        sql = "select po.org_serial,o.org_name, lc.line_name differentiate , count(p.id) person_qty " + Environment.NewLine;
                        sql += " from psst_person p " + Environment.NewLine;
                        sql += " inner join psst_position po on p.id=po.id " + Environment.NewLine;
                        sql += " inner join ctlt_organize o on po.org_serial=o.org_serial " + Environment.NewLine;
                        sql += " inner join psst_line_code lc on lc.line_code=po.line_code and po.per_type=lc.per_type " + Environment.NewLine;
                        sql += " where per_status='1' " + Environment.NewLine;
                        sql += " and p.per_type = @_PER_TYPE " + Environment.NewLine;
                        sql += " and o.org_serial=@_ORG_SERIAL " + Environment.NewLine;
                        sql += " group by  po.org_serial,o.org_name, lc.line_name " + Environment.NewLine;
                        sql += " order by o.org_name";
                        break;

                    case "ORG":   //หน่วยงาน
                        sql = "select po.org_serial,o.org_name, o.org_name differentiate , count(p.id) person_qty " + Environment.NewLine;
                        sql += " from psst_person p " + Environment.NewLine;
                        sql += " inner join psst_position po on p.id=po.id " + Environment.NewLine;
                        sql += " inner join ctlt_organize o on po.org_serial=o.org_serial " + Environment.NewLine;
                        sql += " where per_status='1' " + Environment.NewLine;
                        sql += " and p.per_type = @_PER_TYPE " + Environment.NewLine;
                        sql += " and o.org_serial=@_ORG_SERIAL " + Environment.NewLine;
                        sql += " group by  po.org_serial,o.org_name " + Environment.NewLine;
                        sql += " order by o.org_name";

                        break;
                    case "LEVEL":   //ระดับตำแหน่ง
                        sql = "select po.org_serial,o.org_name, l.level_name differentiate , count(p.id) person_qty " + Environment.NewLine;
                        sql += " from psst_person p " + Environment.NewLine;
                        sql += " inner join psst_position po on p.id=po.id " + Environment.NewLine;
                        sql += " inner join ctlt_organize o on po.org_serial=o.org_serial " + Environment.NewLine;
                        sql += " inner join psst_per_level l on l.cur_lev=po.cur_lev and l.per_type=po.per_type " + Environment.NewLine;
                        sql += " where per_status='1' " + Environment.NewLine;
                        sql += " and p.per_type = @_PER_TYPE " + Environment.NewLine;
                        sql += " and o.org_serial=@_ORG_SERIAL " + Environment.NewLine;
                        sql += " group by  po.org_serial,o.org_name,l.level_name " + Environment.NewLine;
                        sql += " order by o.org_name";

                        break;
                }

                //กำหนดค่าให้ SqlParameter
                SqlParameter[] param = new SqlParameter[2];
                param[0] = setParameter("@_PER_TYPE", SqlDbType.VarChar, TypeName);
                param[1] = setParameter("@_ORG_SERIAL", SqlDbType.BigInt, Convert.ToInt64(ORG));

                dt = GetDatatable(sql, param);
            }
            catch (Exception ex) {
                dt = new DataTable();
            }
            return dt;
        }
        #endregion

        #region "SMM"
        public static DataTable GetQueryReportSMM_UT0511(HttpRequest req) {
            string ORG = req.QueryString["ORG"];

            //ORG = "25"; ฝ่ายพัสดุอาคารสถานที่และยานพาหนะ

            DataTable dt = new DataTable();
            try {
                string sql = "WITH ORG_REL AS " + Environment.NewLine;
                sql += " (  " + Environment.NewLine;
                sql += "  SELECT ORG.ORG_SERIAL,ORG_NAME, 1 as qry_group,UPPER_ORG_SERIAL, ORG_LEVEL " + Environment.NewLine;
                sql += "  FROM vw_SMM_ORG ORG  " + Environment.NewLine;
                sql += "  WHERE ORG_SERIAL=76  " + Environment.NewLine;
                sql += "  UNION ALL   " + Environment.NewLine;
                sql += "  SELECT ORG.ORG_SERIAL,ORG.ORG_NAME,2 as qry_group,ORG.UPPER_ORG_SERIAL, ORG.ORG_LEVEL " + Environment.NewLine;
                sql += "  FROM vw_SMM_ORG ORG  " + Environment.NewLine;
                sql += "  INNER JOIN ORG_REL ON ORG.UPPER_ORG_SERIAL=ORG_REL.ORG_SERIAL  " + Environment.NewLine;
                sql += "  WHERE ORG.ORG_CODE_DPIS IS NOT NULL  " + Environment.NewLine;
                sql += "  )  " + Environment.NewLine;

                sql += "  select r.org_serial, r.org_name,  " + Environment.NewLine;
                sql += "  po.pos_name, po.per_type_name, isnull(po.per_fullname,'')  person_name, " + Environment.NewLine;
                sql += "  r.qry_group,r.upper_org_serial, r.org_level, po.per_type + '_' + po.pos_id id,og.org_name upper_org_name, " + Environment.NewLine;
                sql += "  (select org_level from ctlt_organize where org_serial=@_ORG_SERIAL) parent_level" + Environment.NewLine;
                sql += "  from ORG_REL r " + Environment.NewLine;
                sql += "  inner join vw_CMN_POSITION po on po.org_serial=r.org_serial " + Environment.NewLine;
                sql += "  left join  ctlt_organize og on  r.upper_org_serial=og.org_serial" + Environment.NewLine;
                sql += "  where 1=1";
                //if (ORG != "76")
                //{
                sql += "and r.org_serial=@_ORG_SERIAL or r.org_serial in (select org_serial from ctlt_organize where upper_org_serial=@_ORG_SERIAL)" + Environment.NewLine;
                //}

                sql += "  order by  r.org_level, r.org_serial,po.pos_name";

                SqlParameter[] p = new SqlParameter[1];
                p[0] = setParameter("@_ORG_SERIAL", SqlDbType.BigInt, Convert.ToInt64(ORG));

                dt = GetDatatable(sql, p);
            }
            catch (Exception ex) {
                dt = new DataTable();
            }
            return dt;
        }


        public static DataTable GetQueryReportSMM_UT0511_2(HttpRequest req)
        {
            string ORG = req.QueryString["ORG"];

            //ORG = "25"; ฝ่ายพัสดุอาคารสถานที่และยานพาหนะ

            DataTable dt = new DataTable();
            try
            {
                string sql = "WITH ORG_REL AS " + Environment.NewLine;
                sql += " (  " + Environment.NewLine;
                sql += "  SELECT ORG.ORG_SERIAL,UPPER_ORG_SERIAL,ORG_NAME,ORG_ABBR,ORG_CODE_DPIS,ORG.ORG_LEVEL " + Environment.NewLine;
                sql += "  FROM vw_SMM_ORG ORG  " + Environment.NewLine;
                sql += "  WHERE ORG_SERIAL=@_ORG_SERIAL  " + Environment.NewLine;
                sql += "  UNION ALL   " + Environment.NewLine;
                sql += "  SELECT ORG.ORG_SERIAL,ORG.UPPER_ORG_SERIAL,ORG.ORG_NAME,ORG.ORG_ABBR,ORG.ORG_CODE_DPIS,ORG.ORG_LEVEL " + Environment.NewLine;
                sql += "  FROM vw_SMM_ORG ORG  " + Environment.NewLine;
                sql += "  INNER JOIN ORG_REL ON ORG.UPPER_ORG_SERIAL=ORG_REL.ORG_SERIAL  " + Environment.NewLine;
                sql += "  WHERE ORG.ORG_CODE_DPIS IS NOT NULL  " + Environment.NewLine;
                sql += "  )  " + Environment.NewLine;

                sql += "  SELECT ORG_REL.ORG_LEVEL,ORG_REL.ORG_SERIAL,ORG_NAME,ORG_ABBR " + Environment.NewLine;
                sql += "  ,CASE WHEN ORG_CODE_DPIS IS NULL THEN 'โครงสร้างตามมอบหมาย' ELSE 'โครงสร้างตามกฎหมาย' END ORG_TYPE" + Environment.NewLine;
                sql += "  ,CASE WHEN ORG_CODE_DPIS IS NULL THEN 'orange' ELSE 'green' END ORG_TYPE_Color " + Environment.NewLine;
                sql += "  ,UPPER_ORG_SERIAL" + Environment.NewLine;
                sql += "  ,CASE WHEN COUNT(JOB.JOB_SEQ)>0 THEN 'images/check.png' ELSE 'images/none.png' END CREATED_JOB  " + Environment.NewLine;
                sql += "  ,CAST(CASE WHEN ORG_CODE_DPIS IS NULL THEN 0 ELSE 1 END AS BIT) Can_Set_Job " + Environment.NewLine;
                sql += "  ,CASE WHEN SMM_ORGANIZE_JOB_HEADER.IS_VERIFY=1 THEN 'images/check.png' ELSE 'images/none.png' END Icon_IS_VERIFY" + Environment.NewLine;
                sql += "  ,ISNULL(SMM_ORGANIZE_JOB_HEADER.IS_VERIFY,0) IS_VERIFY ";
                sql += "   FROM ORG_REL  ";
                sql += "  LEFT JOIN SMM_ORGANIZE_JOB_HEADER ON SMM_ORGANIZE_JOB_HEADER.ORG_SERIAL = ORG_REL.ORG_SERIAL ";
                sql += "  LEFT JOIN SMM_ORGANIZE_JOB JOB ON ORG_REL.ORG_SERIAL=JOB.ORG_SERIAL ";
                sql += "  GROUP BY ORG_REL.ORG_SERIAL ";
                sql += "  ,ORG_NAME,ORG_ABBR,ORG_CODE_DPIS,UPPER_ORG_SERIAL,SMM_ORGANIZE_JOB_HEADER.IS_VERIFY,ORG_REL.ORG_LEVEL ";
                sql += "  order by ORG_LEVEL ";


                SqlParameter[] p = new SqlParameter[1];
                p[0] = setParameter("@_ORG_SERIAL", SqlDbType.BigInt, Convert.ToInt64(ORG));

                dt = GetDatatable(sql, p);
            }
            catch (Exception ex)
            {
                dt = new DataTable();
            }
            return dt;
        }
        #endregion
        #endregion


        #region "Public Function"
        public static string SetDisplayDateFormat(string vDate) { 
            //แปลงวันที่จาก yyyyMMdd ให้เป็น dd MMM yyyy

            string ret = "";
            if (vDate.Length == 8) { 
                int dd = Convert.ToInt16(vDate.Substring(6,2));
                int mm = Convert.ToInt16(vDate.Substring(4,2));
                int yy = Convert.ToInt16(vDate.Substring(0,4));

                DateTime dDate = new DateTime(yy, mm, dd);
                ret = dDate.ToString("dd MMMM yyyy", new System.Globalization.CultureInfo("th-TH"));
            }
            return ret;
        
        }
        #endregion


        #region "Database Function"
        private static DataTable GetDatatable(string sql, SqlParameter[] param)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlConnection conn = new SqlConnection(DefaultConnectionString);
                conn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = 240;

                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;

                if (param != null)
                {
                    foreach (SqlParameter p in param)
                    {
                        if (p != null)
                        {
                            cmd.Parameters.Add(p);
                        }
                    }
                }

                da.Fill(dt);
                da.Dispose();
            }
            catch (Exception ex)
            {
                dt = new DataTable();
            }

            return dt;
        }


        public static SqlParameter setParameter(string pName, SqlDbType pType, object pValue)
        {
            SqlParameter p = new SqlParameter(pName, pType);
            if (pValue != null)
                p.Value = pValue;
            else
                p.Value = DBNull.Value;

            return p;
        }
        #endregion
    }
}
